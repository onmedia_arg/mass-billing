<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/

Route::post("/login", 'Api\AccountController@login');
Route::post("/login_monitor", 'Api\AccountController@login_monitor');

Route::get("/comprobantes/show/{id_comprobante}/{key_access}", 'Api\Comprobantes\ComprobantesController@show');
Route::post("/comprobantes/cargar", 'Api\Comprobantes\ComprobantesController@cargar');
Route::post("/comprobantes/procesarImportacion","Api\Comprobantes\ComprobantesController@procesarImportacion");
Route::get("/comprobantes/prueba", 'Api\Comprobantes\ComprobantesController@prueba');

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
    return Redirect("/backend");
});

Route::get('/ejecutar_procesamiento', function()
{
    Artisan::call('procesar:importaciones_comprobantes');
});

Route::get('/enviar_respuesta', function()
{
    Artisan::call('envio:respuestas_importaciones');
});

Route::get("/reset",function(){
    Artisan::call("db:seed");
});

// WEB BACKEND
Route::get('/backend', 'Backend\Login\LoginController@index');
Route::get('/backend/recuperar_datos', 'Backend\Login\LoginController@recuperar_datos');
Route::post("/backend/forgot_password","Backend\Login\LoginController@forgot_password");
Route::get('/backend/cerrar_sesion', 'Backend\Login\LoginController@cerrar_sesion');
Route::post('/backend/ingresar','Backend\Login\LoginController@ingresar');

Route::group(["namespace"=>"Backend","prefix"=>"backend","middleware"=>"CheckBackend"],function(){

    Route::get("/upload_file_excel_importaciones","UploadFileController@upload_file_excel_importaciones");

    Route::group(["namespace"=>"Desktop","prefix"=>"desktop"],function(){
        Route::get("/","DesktopController@index");
    });

    Route::group(["namespace"=>"Perfil","prefix"=>"perfil"],function(){
        Route::get("/","PerfilController@index");
        Route::post("/update","PerfilController@update");
        Route::post('/upload_foto_perfil', 'UploadPerfilController@upload_foto_perfil');
    });

    Route::group(["namespace"=>"Usuarios","prefix"=>"usuarios"],function(){
        Route::get("/","UsuariosController@index");
        Route::get("/nuevo","UsuariosController@nuevo");
        Route::get("/editar/{id}","UsuariosController@editar");

        Route::post("/get_listado_dt","UsuariosController@get_listado_dt");
        Route::post("/store","UsuariosController@store");
        Route::post("/update","UsuariosController@update");
        Route::post("/delete","UsuariosController@delete");

        Route::post('/upload_foto_perfil', 'UploadUsuariosController@upload_foto_perfil');
    });

    Route::group(["namespace"=>"Afip","prefix"=>"afip"],function(){
        Route::post("/get_datos_persona","AfipController@get_datos_persona");
        Route::get("/errorCertificado","AfipController@errorCertificado");
    });

    Route::group(["namespace"=>"VentanillaElectronica","prefix"=>"ventanilla_electronica"],function(){
        Route::get("/","VentanillaElectronicaController@index");
        Route::post("/get_listado_dt","VentanillaElectronicaController@get_listado_dt");
    });

    Route::group(["namespace"=>"Comprobantes","prefix"=>"comprobantes"],function(){
        Route::get("/","ComprobantesController@index");

        Route::post("/get_listado_dt","ComprobantesController@get_listado_dt");
        Route::post("/store","ComprobantesController@store");
        Route::post("/update","ComprobantesController@update");
        Route::post("/delete","ComprobantesController@delete");


        Route::any("/nuevo","ComprobantesController@nuevo");
        Route::any("/nuevo2","ComprobantesController@nuevo2");
        Route::any("/nuevo3","ComprobantesController@nuevo3");
        
        Route::any("/confirmar_nuevo","ComprobantesController@confirmar_nuevo");

        Route::post("/getDataProductService","ComprobantesController@getDataProductService");

        Route::post("/get_tipos_de_documentos_unicos","ComprobantesController@get_tipos_de_documentos_unicos");
        Route::post("/get_cotizacion_moneda","ComprobantesController@get_cotizacion_moneda");

        Route::get("/generacion_pdf/{id_comprobante}","ComprobantesController@generacion_pdf");

        Route::group(["prefix"=>"importaciones"],function(){
            Route::get("/","ImportacionesController@index");
            Route::post("/get_listado_dt","ImportacionesController@get_listado_dt");
            Route::get("/descargarTemplateImportacion","ImportacionesController@descargarTemplateImportacion");

            

            Route::group(["prefix"=>"detalle_importacion"],function(){
                Route::get("/{id_importacion}","DetalleImportacionController@index");
                Route::post("/get_listado_dt","DetalleImportacionController@get_listado_dt");
                Route::get("/ver_info_comprobante/{id_comprobante}","DetalleImportacionController@ver_info_comprobante");
            });
        });

        Route::group(["prefix"=>"mis_puntos_de_venta"],function(){
            Route::get("/","MisPuntosDeVentaController@index");

            Route::post("/get","MisPuntosDeVentaController@get");
            Route::post("/update","MisPuntosDeVentaController@update");
        });
    });

    Route::group(["namespace"=>"ProductosServicios","prefix"=>"productos_servicios"],function(){
        Route::get("/","ProductosServiciosController@index");
        Route::post("/get_listado_dt","ProductosServiciosController@get_listado_dt");
        Route::post("/store","ProductosServiciosController@store");
        Route::post("/get","ProductosServiciosController@get");
        Route::post("/update","ProductosServiciosController@update");
        Route::post("/delete","ProductosServiciosController@delete");

        Route::post("/busquedaProductoNuevoComprobante","ProductosServiciosController@busquedaProductoNuevoComprobante");
    });


    Route::group(["namespace"=>"MisClientes","prefix"=>"mis_clientes"],function(){
        Route::get("/","MisClientesController@index");
        Route::post("/get_listado_dt","MisClientesController@get_listado_dt");
        Route::post("/store","MisClientesController@store");
        Route::post("/get","MisClientesController@get");
        Route::post("/update","MisClientesController@update");
        Route::post("/delete","MisClientesController@delete");

        Route::post("/busquedaClienteNuevoComprobante","MisClientesController@busquedaClienteNuevoComprobante");
    });

    Route::group(["namespace"=>"Configuraciones","prefix"=>"configuraciones"],function(){

        Route::group(["namespace"=>"Afip","prefix"=>"afip"],function(){
            Route::get("/","AfipController@index");
            Route::post("/update","AfipController@update");
            Route::post("/upload_imagen_logo_comprobante","UploadAfipController@upload_imagen_logo_comprobante");

            Route::get("/descargarCsrParaAfip","AfipController@descargarCsrParaAfip");
        });

        Route::group(["namespace"=>"SMTP","prefix"=>"smtp"],function(){
            Route::get("/","SmtpController@index");
            Route::post("/update","SmtpController@update");
        });
    });

    Route::group(["namespace"=>"Empresas","prefix"=>"empresas"],function(){
        Route::get("/","EmpresasController@index");
        Route::post("/get_listado_dt","EmpresasController@get_listado_dt");
        Route::get("/nuevo","EmpresasController@nuevo");
        Route::post("/store","EmpresasController@store");
        Route::get("/editar/{id}","EmpresasController@editar");
        Route::post("/update","EmpresasController@update");
        Route::get("/descargarCsrParaAfip","EmpresasController@descargarCsrParaAfip");
        Route::post("/delete","EmpresasController@delete");
    });
});

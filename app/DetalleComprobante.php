<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleComprobante extends Model
{
    public $table = "detalle_comprobantes";

    public function get_unidad_de_medida()
    {
        return $this->hasOne("App\UnidadDeMedida","id","id_unidad_medida");
    }
}

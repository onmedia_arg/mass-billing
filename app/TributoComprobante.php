<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TributoComprobante extends Model
{
    public $table = "tributos_comprobantes";

    public function get_tipo_tributo()
    {
        return $this->hasOne("App\TipoTributo","id","id_tipo_tributo");
    }
}

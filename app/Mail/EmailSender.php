<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailSender extends Mailable
{
    use Queueable, SerializesModels;

    public $email_sender;

    private $view_html =null;
    private $view_text =null;
    private $asunto = null;

    private $archivos;
    private $archivos_64;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email_sender,$asunto,$view_html,$view_text = null)
    {
        $this->email_sender = $email_sender;
        $this->view_html = $view_html;
        $this->view_text = $view_text;
        $this->asunto = $asunto;
        $this->archivos = array();
        $this->archivos_64 = array();
    }

    public function add_files($attachment,$the_name)
    {
        $this->archivos[] = ["archivo"=>$attachment,"name"=>$the_name];
    }

    public function add_files_64($attachment,$the_name)
    {
        $this->archivos_64[] = ["archivo"=>$attachment,"name"=>$the_name];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $respuesta = $this->from(env('MAIL_FROM_ADDRESS'))
        ->subject($this->asunto." - ".Config("app.name"))
        ->view($this->view_html);

        if($this->view_text != null)
        {
            $respuesta->text($this->view_text);
        }

        foreach($this->archivos as $filePath){
            $respuesta->attach($filePath["archivo"],$filePath["name"]);
        }

        foreach($this->archivos_64 as $filePath){
            $respuesta->attachData($filePath["archivo"],$filePath["name"]);
        }

        return $respuesta;
    }
}
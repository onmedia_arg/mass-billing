<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadoUsuario extends Model
{
    public $table ="estados_usuarios";

    const ACTIVO = 1;
    const NO_ACTIVO = 1;
}

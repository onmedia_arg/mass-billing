<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoDeComprobante extends Model
{
    public $table = "tipos_de_comprobantes";
}

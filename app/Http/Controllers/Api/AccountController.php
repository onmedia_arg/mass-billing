<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseApiController;

use App\Library\ResponseEstructure;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Hash;

//use GuzzleHttp;

use App\Usuario;
use App\Library\ManagerJWT;

class AccountController extends BaseApiController
{
    public function login(Request $request)
    {
        $response_estructure = new ResponseEstructure();
        $response_estructure->set_response(false);

        $usuario = trim($request->input("usuario")); 
        $password = trim($request->input("password")); 

        $input= [
            "usuario"=>$usuario,
            "password"=>$password,
        ];

        $rules = [
            "usuario"=>"required", 
            "password"=>"required", 
        ];

        $validator = Validator::make($input, $rules);
           
        if ($validator->fails()) {
            $response_estructure->set_response(false);

            $errors = $validator->errors();

            foreach ($errors->all() as $error) {
                $response_estructure->add_message_error($error);
            }
        }
        else
        {
            $usuario_obj = null;

            if(filter_var($usuario, FILTER_VALIDATE_EMAIL))
            {
                $usuario_obj = Usuario::where("correo",$usuario)->where("eliminado",false)->first();
            }
            else
            {
                $usuario_obj = Usuario::where("usuario",$usuario)->where("eliminado",false)->first();
            }
            
            if($usuario_obj && $usuario_obj->eliminado){$usuario_obj = null;}

            if($usuario_obj && $usuario_obj->id_estado_usuario == 1)
            {
                if (Hash::check($password, $usuario_obj->password)) {
                    $ManagerJWT = new ManagerJWT();
                    $jwt = $ManagerJWT->loginUser($usuario_obj->id);

                    $data = array(
                        "jwt" => $jwt,
                    );

                    $response_estructure->set_response(true);
                    $response_estructure->set_data($data);
                }
                else
                {
                    $response_estructure->set_response(false);
                    $response_estructure->add_message_error("Contraseña incorrecta");
                }
            }
            else
            {
                $response_estructure->set_response(false);

                if($usuario_obj)
                {
                    if($usuario_obj->id_estado_usuario != 1)
                    {
                        $response_estructure->add_message_error("El usuario no está activado");
                    }
                }
                else
                {
                    if(filter_var($usuario, FILTER_VALIDATE_EMAIL))
                    {
                        $response_estructure->add_message_error("No se ha encontrado el correo ingresado");
                    }
                    else
                    {
                        $response_estructure->add_message_error("No se ha encontrado el usuario ingresado");
                    }
                }
            }
        }

        return response()->json($response_estructure->get_response_array());
    }

    public function login_monitor(Request $request)
    {
        $response_estructure = new ResponseEstructure();
        $response_estructure->set_response(false);

        $usuario = trim($request->input("usuario")); 
        $password = trim($request->input("password")); 

        $input= [
            "usuario"=>$usuario,
            "password"=>$password,
        ];

        $rules = [
            "usuario"=>"required", 
            "password"=>"required", 
        ];

        $validator = Validator::make($input, $rules);
           
        if ($validator->fails()) {
            $response_estructure->set_response(false);

            $errors = $validator->errors();

            foreach ($errors->all() as $error) {
                $response_estructure->add_message_error($error);
            }
        }
        else
        {
            $usuario_obj = null;

            if(filter_var($usuario, FILTER_VALIDATE_EMAIL))
            {
                $usuario_obj = Usuario::where("correo",$usuario)->where("eliminado",false)->first();
            }
            else
            {
                $usuario_obj = Usuario::where("usuario",$usuario)->where("eliminado",false)->first();
            }
            
            if($usuario_obj && $usuario_obj->eliminado){$usuario_obj = null;}

            if($usuario_obj && $usuario_obj->id_estado_usuario == 1)
            {
                if (Hash::check($password, $usuario_obj->password)) {
                    $ManagerJWT = new ManagerJWT();
                    $jwt = $ManagerJWT->loginUser($usuario_obj->id);

                    $data = array(
                        "jwt" => $jwt,
                    );

                    $response_estructure->set_response(true);
                    $response_estructure->set_data($data);
                }
                else
                {
                    $response_estructure->set_response(false);
                    $response_estructure->add_message_error("Contraseña incorrecta");
                }
            }
            else
            {
                $response_estructure->set_response(false);

                if($usuario_obj)
                {
                    if($usuario_obj->id_estado_usuario != 1)
                    {
                        $response_estructure->add_message_error("El usuario no está activado");
                    }
                }
                else
                {
                    if(filter_var($usuario, FILTER_VALIDATE_EMAIL))
                    {
                        $response_estructure->add_message_error("No se ha encontrado el correo ingresado");
                    }
                    else
                    {
                        $response_estructure->add_message_error("No se ha encontrado el usuario ingresado");
                    }
                }
            }
        }

        return response()->json($response_estructure->get_response_array());
    }

    public function logout(Request $request)
    {
        $ManagerJWT = new ManagerJWT();
        $respuesta = $ManagerJWT->logout($request->input("jwt_token"));

        return $respuesta;
    }
}

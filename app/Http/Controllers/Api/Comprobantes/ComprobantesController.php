<?php

namespace App\Http\Controllers\Api\Comprobantes;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseApiController;
use App\Http\Controllers\Controller;

use App\Library\ResponseEstructure;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

use App\ImportacionComprobante;
use App\EstadoImportacionComprobante;
use App\Comprobante;
use App\EstadoComprobante;
use App\ConfiguracionAfip;

class ComprobantesController  extends BaseApiController
{
    public function cargar(Request $request)
    {
        $response_estructure = $this->validateToken($request->bearerToken());

        if($response_estructure->get_login() == true)
        {
            $response_estructure->set_response(false);

            $id_importacion = (int) $request->input("id_importacion");
            $id_configuracion_afip = (int) $request->input("id_configuracion_afip");
            $comprobantes = json_decode($request->input("comprobantes"), true); 

            $configuracion_afip_obj = ConfiguracionAfip::find($id_configuracion_afip);

            if(!$configuracion_afip_obj)
            {
                $importacion_comprobante_obj = ImportacionComprobante::find($id_importacion);

                if($importacion_comprobante_obj)
                {
                    $configuracion_afip_obj = ConfiguracionAfip::find($importacion_comprobante_obj->id_configuracion_afip);
                }
            }

            if($configuracion_afip_obj)
            {
                // ARMANDO / OBTENIENDO IMPORTACION
                $importacion_comprobante_obj = null;

                if($id_importacion == 0)
                {
                    $importacion_comprobante_obj = new ImportacionComprobante();
                    $importacion_comprobante_obj->id_configuracion_afip = $id_configuracion_afip;
                    $importacion_comprobante_obj->id_estado = EstadoImportacionComprobante::CARGANDOSE;
                    $importacion_comprobante_obj->save();

                    $id_importacion = $importacion_comprobante_obj->id;
                }
                else
                {
                    $importacion_comprobante_obj = ImportacionComprobante::find($id_importacion);
                }

                if($importacion_comprobante_obj)
                {
                    if(is_array($comprobantes) && count($comprobantes) > 0)
                    {
                        $registros_inserts = array();

                        foreach($comprobantes as $comprobante_array)
                        {
                            $registros_inserts[] = $this->getRegistroInsertFromCargaComprobante($comprobante_array,$importacion_comprobante_obj->id,$id_configuracion_afip);
                        }

                        $consultas = $this->getConsultasInsertFromCargaComprobante($registros_inserts);

                        for($i=0; $i < count($consultas);$i++)
                        {
                            if($consultas[$i]["tipo"] == "insert")
                            {
                                DB::insert($consultas[$i]["query"]);
                            }
                            else if($consultas[$i]["tipo"] == "update")
                            {
                                DB::update($consultas[$i]["query"]);
                            }
                        }

                        DB::select(
                            "UPDATE importacion_comprobantes 
                                SET importacion_comprobantes.cantidad_total = (
                                    SELECT count(comprobantes.id) AS cantidad FROM `comprobantes` 
                                    WHERE comprobantes.id_importacion_comprobante = ?
                                )
                            WHERE importacion_comprobantes.id = ?",
                            [$importacion_comprobante_obj->id,$importacion_comprobante_obj->id]
                        );

                        $data = array(
                            "id_importacion" => $id_importacion
                        );

                        $response_estructure->set_data($data);
                        $response_estructure->set_response(true);
                    }
                    else
                    {
                        $response_estructure->set_response(false);
                        $response_estructure->add_message_error("No ha enviado comprobantes");
                    }
                }
                else
                {
                    $response_estructure->set_response(false);
                    $response_estructure->add_message_error("Importación no encontrada");
                }
            }
            else
            {
                $response_estructure->set_response(false);
                $response_estructure->add_message_error("La configuracion de AFIP no existe");
            }
        }

        return response()->json($response_estructure->get_response_array());
    }

    public function procesarImportacion(Request $request)
    {
        $response_estructure = $this->validateToken($request->bearerToken());

        if($response_estructure->get_login() == true)
        {
            $response_estructure->set_response(false);

            $id_importacion = (int) $request->input("id_importacion");
            
            $importacion_comprobante_obj = ImportacionComprobante::find($id_importacion);

            if($importacion_comprobante_obj)
            {
                if($importacion_comprobante_obj->id_estado == EstadoImportacionComprobante::CARGANDOSE)
                {
                    $importacion_comprobante_obj->id_estado = EstadoImportacionComprobante::PENDIENTE;
                    $importacion_comprobante_obj->save();

                    $response_estructure->set_response(true);
                }
                else
                {
                    $response_estructure->set_response(false);
                    $response_estructure->add_message_error("La importación no se encuentra en estado de carga para pasar a espera de procesamiento");
                }
            }
            else
            {
                $response_estructure->set_response(false);
                $response_estructure->add_message_error("Importación no encontrada");
            }
        }
        
        return response()->json($response_estructure->get_response_array());
    }

    public function show(Request $request,$id_comprobante,$key_access)
    {
        $comprobante_obj = Comprobante::where("id",$id_comprobante)->first();

        if($comprobante_obj)
        {
            if($comprobante_obj->key_access == $key_access)
            {
                $base_64_code = $comprobante_obj->get_base_64_comprobante();
                return response($base_64_code)->header("Content-type","application/pdf");
            }
        } 
    }

    private function getRegistroInsertFromCargaComprobante($comprobante_array,$id_importacion_comprobante,$id_configuracion_afip)
    {
        $id_tipo_de_comprobante = (isset($comprobante_array["id_tipo_de_comprobante"]) && is_numeric($comprobante_array["id_tipo_de_comprobante"]) ? $comprobante_array["id_tipo_de_comprobante"] : null);

        $id_tipo_de_comprobante = (isset($comprobante_array["id_tipo_de_comprobante"]) ? $comprobante_array["id_tipo_de_comprobante"] : null);

        if(trim($id_tipo_de_comprobante) == "")
        {
            $id_tipo_de_comprobante = "NULL";
        }

        $punto_de_venta = (isset($comprobante_array["punto_de_venta"]) ? $comprobante_array["punto_de_venta"] : null);

        if(trim($punto_de_venta) == "")
        {
            $punto_de_venta = "NULL";
        }

        $fecha = (isset($comprobante_array["fecha"]) ? trim($comprobante_array["fecha"]) : Date("Y-m-d"));

        $fecha_valid_format = \DateTime::createFromFormat("Y-m-d",$fecha);

        if(!$fecha_valid_format)
        {
            $fecha = "NULL";
        }
        else
        {
            $fecha = "'".$fecha."'";
        }

        $id_concepto = (isset($comprobante_array["id_concepto"]) && is_numeric($comprobante_array["id_concepto"]) ? $comprobante_array["id_concepto"] : null);

        if(trim($id_concepto) == "")
        {
            $id_concepto = "NULL";
        }
        else
        {
            $id_concepto = "'".$id_concepto."'";
        }

        $id_moneda = (isset($comprobante_array["id_moneda"]) ? strtoupper(trim($comprobante_array["id_moneda"])) : null);

        if(trim($id_moneda) != "")
        {
            $id_moneda = "'".addslashes($comprobante_array['id_moneda'])."'";
        }
        else
        {
            $id_moneda = "NULL";
        }

        $cotizacion_del_dolar = (isset($comprobante_array["cotizacion_del_dolar"]) && is_numeric($comprobante_array["cotizacion_del_dolar"]) && $comprobante_array["cotizacion_del_dolar"] > 0 ? $comprobante_array["cotizacion_del_dolar"] : null);
        
        if(trim($cotizacion_del_dolar) == "")
        {
            $cotizacion_del_dolar = "NULL";
        }
        else
        {
            $cotizacion_del_dolar = "'".$cotizacion_del_dolar."'";
        }

        $periodo_facturado_desde = (isset($comprobante_array["periodo_facturado_desde"]) ? $comprobante_array["periodo_facturado_desde"] : null);
        $periodo_facturado_hasta = (isset($comprobante_array["periodo_facturado_hasta"]) ? $comprobante_array["periodo_facturado_hasta"] : null);
        $vencimiento_del_pago =    (isset($comprobante_array["vencimiento_del_pago"]) ? $comprobante_array["vencimiento_del_pago"] : null);

        if($periodo_facturado_desde != null)
        {
            $fecha_valid_format = \DateTime::createFromFormat("Y-m-d",$periodo_facturado_desde);

            if(!$fecha_valid_format)
            {
                $periodo_facturado_desde = "NULL";
            }
            else
            {
                $periodo_facturado_desde = "'".$periodo_facturado_desde."'";
            }
        }
        else
        {
            $periodo_facturado_desde = "NULL";
        }

        if($periodo_facturado_hasta != null)
        {
            $fecha_valid_format = \DateTime::createFromFormat("Y-m-d",$periodo_facturado_hasta);

            if(!$fecha_valid_format)
            {
                $periodo_facturado_hasta = "NULL";
            }
            else
            {
                $periodo_facturado_hasta = "'".$periodo_facturado_hasta."'";
            }
        }
        else
        {
            $periodo_facturado_hasta = "NULL";
        }

        if($vencimiento_del_pago != null)
        {
            $fecha_valid_format = \DateTime::createFromFormat("Y-m-d",$vencimiento_del_pago);

            if(!$fecha_valid_format)
            {
                $vencimiento_del_pago = "NULL";
            }
            else
            {
                $vencimiento_del_pago = "'".$vencimiento_del_pago."'";
            }
        }
        else
        {
            $vencimiento_del_pago = "NULL";
        }
        
        $condicion_iva_receptor = (isset($comprobante_array["condicion_iva_receptor"]) && is_numeric($comprobante_array["condicion_iva_receptor"]) ? $comprobante_array["condicion_iva_receptor"] : null);

        if(trim($condicion_iva_receptor) == "")
        {
            $condicion_iva_receptor = "NULL";
        }
        else
        {
            $condicion_iva_receptor = "'".$condicion_iva_receptor."'";
        }
       
        // HARDCODING
        $tipo_documento_receptor = "'80'"; // SIEMPRE CUIT
        /*$tipo_documento_receptor = (isset($comprobante_array["tipo_documento_receptor"]) && is_numeric($comprobante_array["tipo_documento_receptor"]) ? $comprobante_array["tipo_documento_receptor"] : null);

        if(trim($tipo_documento_receptor) == "")
        {
            $tipo_documento_receptor = "NULL";
        }
        else
        {
            $tipo_documento_receptor = "'".$tipo_documento_receptor."'";
        }*/

        $num_documento_receptor = (isset($comprobante_array["num_documento_receptor"]) && is_numeric($comprobante_array["num_documento_receptor"]) ? $comprobante_array["num_documento_receptor"] : null);
        
        if(trim($num_documento_receptor) == "")
        {
            $num_documento_receptor = "NULL";
        }
        else
        {
            $num_documento_receptor = "'".$num_documento_receptor."'";
        }

        $razon_social_receptor = (isset($comprobante_array["razon_social_receptor"]) ? $comprobante_array["razon_social_receptor"] : "");

        if(trim($razon_social_receptor) != "")
        {
            $razon_social_receptor = "'".addslashes($comprobante_array['razon_social_receptor'])."'";
        }
        else
        {
            $razon_social_receptor = "NULL";
        }

        $correo_receptor = (isset($comprobante_array["correo_receptor"]) ? $comprobante_array["correo_receptor"] : "");

        if(trim($correo_receptor) != "")
        {
            $correo_receptor = "'".addslashes($comprobante_array['correo_receptor'])."'";
        }
        else
        {
            $correo_receptor = "NULL";
        }

        $domicilio_receptor = (isset($comprobante_array["domicilio_receptor"]) ? $comprobante_array["domicilio_receptor"] : "");
        
        if(trim($domicilio_receptor) != "")
        {
            $domicilio_receptor = "'".addslashes($comprobante_array['domicilio_receptor'])."'";
        }
        else
        {
            $domicilio_receptor = "NULL";
        }

        $id_condicion_venta = (isset($comprobante_array["id_condicion_venta"]) && is_numeric($comprobante_array["id_condicion_venta"]) ? $comprobante_array["id_condicion_venta"] : null);

        if(trim($id_condicion_venta) == "")
        {
            $id_condicion_venta = "NULL";
        }
        else
        {
            $id_condicion_venta = "'".$id_condicion_venta."'";
        }
        
        $ImpTotal = (isset($comprobante_array["ImpTotal"]) ? $comprobante_array["ImpTotal"] : null);
        
        if(trim($ImpTotal) == "")
        {
            $ImpTotal = "NULL";
        }
        else
        {
            $ImpTotal = "'".$ImpTotal."'";
        }

        $ImpTotConc = (isset($comprobante_array["ImpTotConc"]) && is_numeric($comprobante_array["ImpTotConc"]) ? $comprobante_array["ImpTotConc"] : null);
        
        if(trim($ImpTotConc) == "")
        {
            $ImpTotConc = "NULL";
        }
        else
        {
            $ImpTotConc = "'".$ImpTotConc."'";
        }

        $ImpNeto = (isset($comprobante_array["ImpNeto"]) && is_numeric($comprobante_array["ImpNeto"]) ? $comprobante_array["ImpNeto"] : null);
        
        if(trim($ImpNeto) == "")
        {
            $ImpNeto = "NULL";
        }
        else
        {
            $ImpNeto = "'".$ImpNeto."'";
        }

        $ImpOpEx = (isset($comprobante_array["ImpOpEx"]) && is_numeric($comprobante_array["ImpOpEx"]) ? $comprobante_array["ImpOpEx"] : null);
        
        if(trim($ImpOpEx) == "")
        {
            $ImpOpEx = "NULL";
        }
        else
        {
            $ImpOpEx = "'".$ImpOpEx."'";
        }

        $ImpIVA = (isset($comprobante_array["ImpIVA"]) && is_numeric($comprobante_array["ImpIVA"]) ? $comprobante_array["ImpIVA"] : null);
        
        if(trim($ImpIVA) == "")
        {
            $ImpIVA = "NULL";
        }
        else
        {
            $ImpIVA = "'".$ImpIVA."'";
        }

        $ImpTrib = (isset($comprobante_array["ImpTrib"]) && is_numeric($comprobante_array["ImpTrib"]) ? $comprobante_array["ImpTrib"] : null);
        
        if(trim($ImpTrib) == "")
        {
            $ImpTrib = "NULL";
        }
        else
        {
            $ImpTrib = "'".$ImpTrib."'";
        }
        
        $descripcion_servicio = (isset($comprobante_array["descripcion_servicio"]) ? $comprobante_array["descripcion_servicio"] : null);
        
        if(trim($descripcion_servicio) != "")
        {
            $descripcion_servicio = "'".addslashes($comprobante_array['descripcion_servicio'])."'";
        }
        else
        {
            $descripcion_servicio = "NULL";
        }
        
        $aclaracion_usuario = (isset($comprobante_array["aclaracion_usuario"]) ? $comprobante_array["aclaracion_usuario"] : null);
        
        if(trim($aclaracion_usuario) != "")
        {
            $aclaracion_usuario = "'".addslashes($comprobante_array['aclaracion_usuario'])."'";
        }
        else
        {
            $aclaracion_usuario = "NULL";
        }
        
        $id_externo = (isset($comprobante_array["id_externo"]) && is_numeric($comprobante_array["id_externo"]) ? $comprobante_array["id_externo"] : null);

        if(trim($id_externo) == "")
        {
            $id_externo = "NULL";
        }
        else
        {
            $id_externo = "'".$id_externo."'";
        }

        $cotizacion_de_la_moneda = "NULL";

        if($id_moneda == "PES")
        {
            $cotizacion_de_la_moneda = 1;
        }
        else
        {                                             
            $cotizacion_de_la_moneda = $cotizacion_del_dolar;
        }

        $detalle_comprobante = (isset($comprobante_array["detalle_comprobante"]) ? $comprobante_array["detalle_comprobante"] : null);

        if(is_array($detalle_comprobante))
        {
            $detalle_comprobante = "'".addslashes(json_encode($detalle_comprobante))."'";
        }
        else
        {
            $detalle_comprobante = "'".addslashes(json_encode(array()))."'";
        }

        return array(
            "id_tipo_de_comprobante" => $id_tipo_de_comprobante,
            "punto_de_venta" => $punto_de_venta,
            "fecha" => $fecha,
            "id_concepto" => $id_concepto,
            "id_moneda" => $id_moneda,
            "cotizacion_de_la_moneda" => $cotizacion_de_la_moneda,
            "cotizacion_del_dolar" => $cotizacion_del_dolar,
            "periodo_facturado_desde" => $periodo_facturado_desde,
            "periodo_facturado_hasta" => $periodo_facturado_hasta,
            "vencimiento_del_pago" => $vencimiento_del_pago,
            "condicion_iva_receptor" => $condicion_iva_receptor,
            "tipo_documento_receptor" => $tipo_documento_receptor,
            "num_documento_receptor" => $num_documento_receptor,
            "razon_social_receptor" => $razon_social_receptor,
            "correo_receptor" => $correo_receptor,
            "domicilio_receptor" => $domicilio_receptor,
            "id_condicion_venta" => $id_condicion_venta,
            "ImpTotal" => $ImpTotal,
            "ImpTotConc" => $ImpTotConc,
            "ImpNeto" => $ImpNeto,
            "ImpOpEx" => $ImpOpEx,
            "ImpIVA" => $ImpIVA,
            "ImpTrib" => $ImpTrib,
            "id_configuracion_afip" => $id_configuracion_afip,
            "id_estado" => EstadoComprobante::PENDIENTE,
            "descripcion_servicio" => $descripcion_servicio,
            "aclaracion_usuario" => $aclaracion_usuario,
            "id_externo" => $id_externo,
            "id_importacion_comprobante" => $id_importacion_comprobante,
            "detalle_comprobante" => $detalle_comprobante
        );
    }

    private function getConsultasInsertFromCargaComprobante($registros_inserts)
    {
        $created_at = Date("Y-m-d H:i:s");
        $updated_at = $created_at;

        $consulta_sql_armando_default = "INSERT INTO `comprobantes` (`id`, `id_tipo_de_comprobante`, `numero_afip`, `CAE`, `CAEFchVto`, `punto_de_venta`, `fecha`, 
        `id_concepto`, `id_moneda`, `cotizacion_de_la_moneda`, `cotizacion_del_dolar`, 
        `periodo_facturado_desde`, `periodo_facturado_hasta`, `vencimiento_del_pago`, 
        `condicion_iva_receptor`, `tipo_documento_receptor`, `num_documento_receptor`, 
        `razon_social_receptor`, `correo_receptor`, `domicilio_receptor`, 
        `id_condicion_venta`, `ImpTotal`, `ImpTotConc`, `ImpNeto`, `ImpOpEx`, 
        `ImpIVA`, `ImpTrib`, `id_estado`, 
        `descripcion_servicio`, `aclaracion_usuario`, 
        `codigo_de_barras`, `id_externo`, `id_configuracion_afip`,`id_importacion_comprobante`, `detalle_comprobante`, `created_at`, `updated_at`) VALUES ";

        $consulta_sql_armando = $consulta_sql_armando_default;

        $contados = 0;
        $values_totales = 0;

        $consultas = array();

        for($i=0; $i < count($registros_inserts);$i++)
        {
            $row = $registros_inserts[$i];

            $values = " (NULL, ".$row['id_tipo_de_comprobante'].", NULL, NULL, NULL, ".$row['punto_de_venta'].", ".$row['fecha'].", 
            ".$row['id_concepto'].", ".$row['id_moneda'].", ".$row['cotizacion_de_la_moneda'].", ".$row['cotizacion_del_dolar'].", 
            ".$row['periodo_facturado_desde'].", ".$row['periodo_facturado_hasta'].", ".$row['vencimiento_del_pago'].", 
            ".$row['condicion_iva_receptor'].", ".$row['tipo_documento_receptor'].", ".$row['num_documento_receptor'].", 
            ".$row['razon_social_receptor'].", ".$row['correo_receptor'].", ".$row['domicilio_receptor'].", 
            ".$row['id_condicion_venta'].", ".$row['ImpTotal'].", ".$row['ImpTotConc'].", ".$row['ImpNeto'].", ".$row['ImpOpEx'].", 
            ".$row['ImpIVA'].", ".$row['ImpTrib'].", ".$row['id_estado'].", 
            ".$row['descripcion_servicio'].", ".$row['aclaracion_usuario'].", 
            NULL, ".$row['id_externo'].", ".$row['id_configuracion_afip'].", ".$row['id_importacion_comprobante'].", ".$row["detalle_comprobante"].", '".$created_at."', '".$updated_at."') ";

            $consulta_sql_armando.= $values.",";

            $contados++;

            if($contados == 200)
            {
                $consultas[] = array("tipo"=>"insert", "query" => substr($consulta_sql_armando,0,strlen($consulta_sql_armando)-1).";");
                $consulta_sql_armando = $consulta_sql_armando_default;
                $contados = 0;
            }
        }

        if($contados != 0)
        {
            $consultas[] = array("tipo"=>"insert", "query" => substr($consulta_sql_armando,0,strlen($consulta_sql_armando)-1).";");
        }

        return $consultas;
    }

    public function prueba(Request $request)
    {
        $comprobantes = array();

        for($i=1; $i <= 2;$i++)
        {
            $comprobantes[] = array(
                "id_tipo_de_comprobante" => "11",
                "punto_de_venta" => "1",
                "fecha" => Date("Y-m-d"),
                "id_concepto" => "2",
                "id_moneda" => "DOL",
                "cotizacion_del_dolar" => "60",
                "periodo_facturado_desde" => "2020-09-01",
                "periodo_facturado_hasta" => "2020-09-29",
                "vencimiento_del_pago" => Date("Y-m-d"),
                "condicion_iva_receptor" => "1",
                //"tipo_documento_receptor" => "1",
                "num_documento_receptor" => "20316552235",
                "razon_social_receptor" => "",
                "correo_receptor" => "",
                "domicilio_receptor" => "",
                "id_condicion_venta" => "1",
                "ImpTotal" => "90",
                //"ImpTotConc" => "",
                //"ImpNeto" => "90",
                //"ImpOpEx" => "",
                //"ImpIVA" => "",
                //"ImpTrib" => "",
                //"id_configuracion_afip" => "",
                //"descripcion_servicio" => "",
                //"aclaracion_usuario" => "",
                "id_externo" => "1",
                "detalle_comprobante" => array(
                    array(
                        "codigo" => "01",	
                        "producto_servicio" => "Prueba ' ",		
                        "cantidad" => "1",		
                        "id_unidad_medida" => "1",		
                        "precio_unitario" => "100",		
                        "porcentaje_bonificacion" => "10",		
                        "importe_bonificacion" => "10",	
                        "subtotal" => "90"
                    )
                )
            );
        }

        return response()->json($comprobantes);
    }
}

<?php

namespace App\Http\Controllers\Backend\GeneracionPdf;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mpdf\Mpdf;

use DNS1D;

class GeneracionPdfController extends Controller
{
    public function comprobante()
    {
        $html = view('backend/generacion_pdf/generacion_comprobante',array())->render();
        
        $namefile = 'pdf_marito_oliveira'.time().'.pdf';
 
        $defaultConfig = (new \Mpdf\Config\ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];
 
        $defaultFontConfig = (new \Mpdf\Config\FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];

        $mpdf = new Mpdf([
            'fontDir' => array_merge($fontDirs, [
                public_path() . '/fonts',
            ]),
            /*'fontdata' => $fontData + [
                'arial' => [
                    //'R' => 'arial.ttf',
                ],
            ],*/
            'default_font' => 'arial',
            "format" => "A4",
            //"format" => [264.8,188.9],
        ]);

        $mpdf->SetDisplayMode('fullpage');
        $mpdf->WriteHTML($html);
        $mpdf->SetFooter(
        '<div style="heitght: 300px;margin-top: 15px;">
            <p style="text-align: center;font-size: 10px;font-weight: bold;">Pág. {PAGENO}</p>

            <table width="100%">
                <tr>
                    <td width="50%">
                        <img src="data:image/png;base64,'.DNS1D::getBarcodePng("4445", "EAN13").'" alt="barcode"/>
                        <p>4445</p>
                    </td>
                    <td width="50%" style="text-align: center;">
                        <p><b>CAE:</b> 1221312312312</p> 
                        <p><b>Fecha de Vto. de CAE:</b> 12/12/2019</p> 
                    </td>
                </tr>
            </table>
        </div>');

        $mpdf->Output($namefile,"I");
    }
}

<?php
namespace App\Http\Controllers\Backend\Configuraciones\Afip;

use Illuminate\Http\Request;
use App\Http\Controllers\Backend\ABM_Core;

use App\Library\ResponseEstructure;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

use Response;

use App\ConfiguracionAfip;

class AfipController extends ABM_Core
{
    public function __construct()
    {
        $this->link_controlador = url('/backend/configuraciones/afip').'/';
        $this->entity ="Configuracion Afip";
        $this->title_page = "Configuracion Afip";
        $this->columns = [];
        $this->is_ajax = false;
    }
    
    public function index(Request $request)
    {
        $this->share_parameters();

        $id_configuracion_afip = $request->session()->get("id_configuracion_afip");
        $row_obj = ConfiguracionAfip::find($id_configuracion_afip);

        return View("backend.configuraciones.afip.browse")
        ->with("row_obj",$row_obj);
    }

    public function update(Request $request)
    {
        $response_estructure = new ResponseEstructure();
        $response_estructure->set_response(false);

        $id_configuracion_afip = $request->session()->get("id_configuracion_afip");
        $row_obj = ConfiguracionAfip::find($id_configuracion_afip);
        
        if($row_obj)
        {
            $ambiente = trim($request->input("ambiente"));

            $razon_social = trim($request->input("razon_social"));
            $cuit = trim($request->input("cuit"));
            $domicilio_comercial = trim($request->input("domicilio_comercial"));
            $ingresos_brutos = trim($request->input("ingresos_brutos"));
            $inicio_actividades = trim($request->input("inicio_actividades"));
            $id_condicion_iva = trim($request->input("id_condicion_iva"));
            $logo_comprobante = $request->input("logo_comprobante");

            $key_afip_produccion = trim($request->input("key_afip_produccion"));
            $key_afip_testing = trim($request->input("key_afip_testing"));
            
            $input= [
                "ambiente" => $ambiente,

                "razon_social" => $razon_social,
                "cuit" => $cuit,
                "domicilio_comercial" => $domicilio_comercial,
                "ingresos_brutos" => $ingresos_brutos,
                "inicio_actividades" => $inicio_actividades,
                "id_condicion_iva" => $id_condicion_iva,

                "key_afip_produccion"=>$key_afip_produccion,
                "key_afip_testing"=>$key_afip_testing,
            ];

            $rules = [
                "ambiente"=>"required",

                "razon_social" =>"required",
                "cuit" =>"required",
                "domicilio_comercial" =>"required",
                "ingresos_brutos" =>"required",
                "inicio_actividades" =>"required",
                "id_condicion_iva" =>"required",
            ];

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $response_estructure->set_response(false);

                $errors = $validator->errors();

                foreach ($errors->all() as $error) {
                    $response_estructure->add_message_error($error);
                }
            }
            else
            {
                // GUARDANDO DATOS DE PRODUCCION
                if($row_obj->razon_social != $razon_social)
                {
                    if(env('GENERACION_AUTOMATICA_CSR_AFIP') == 1)
                    {
                        $row_obj->csr_afip_produccion = $this->generarCsrParaAfip("produccion",$razon_social,$cuit);
                        $row_obj->csr_afip_testing = $this->generarCsrParaAfip("testing",$razon_social,$cuit);
                    }
                }
                else
                {
                    if(trim($row_obj->csr_afip_produccion) == "")
                    {
                        $row_obj->csr_afip_produccion = $this->generarCsrParaAfip("produccion",$razon_social,$cuit);
                    }

                    if(trim($row_obj->csr_afip_testing) == "")
                    {
                        $row_obj->csr_afip_testing = $this->generarCsrParaAfip("testing",$razon_social,$cuit);
                    }
                }

                $row_obj->ambiente = $ambiente;
                $row_obj->razon_social = $razon_social;
                $row_obj->cuit = $cuit;
                $row_obj->domicilio_comercial = $domicilio_comercial;
                $row_obj->ingresos_brutos = $ingresos_brutos;
                $row_obj->inicio_actividades = $inicio_actividades;
                $row_obj->id_condicion_iva = $id_condicion_iva;
                $row_obj->logo_comprobante = $logo_comprobante;

                $row_obj->key_afip_produccion = $key_afip_produccion;
                $this->guardarKeyDeAfip("produccion",$key_afip_produccion);

                $row_obj->key_afip_testing = $key_afip_testing;
                $this->guardarKeyDeAfip("testing",$key_afip_testing);
                
                $row_obj->save();
                $response_estructure->set_response(true);
            }
        }
        else
        {
            $response_estructure->set_response(false);
            $response_estructure->add_message_error("Configuración no encontrada");
        }
        
        return response()->json($response_estructure->get_response_array());
    }

    private function generarCsrParaAfip($ambiente,$razon_social,$cuit)
    {
        $id_configuracion_afip = session("id_configuracion_afip");

        $ds = DIRECTORY_SEPARATOR;

        $DIRECTORY_TO_SAVE = storage_path('app').$ds."certificados_ws/".$ambiente."/";

        exec("openssl genrsa -out ".$DIRECTORY_TO_SAVE."private_server_".$id_configuracion_afip.".key 2048");
        exec("openssl req -new -key ".$DIRECTORY_TO_SAVE."private_server_".$id_configuracion_afip.".key -subj '/C=AR/O=".$razon_social."/CN=".env('CN_AFIP')."/serialNumber=CUIT ".$cuit."' -out ".$DIRECTORY_TO_SAVE."request_csr_afip_".$id_configuracion_afip.".csr");

        if(!file_exists($DIRECTORY_TO_SAVE.'request_csr_afip_'.$id_configuracion_afip.'.csr'))
        {
            file_put_contents($DIRECTORY_TO_SAVE.'request_csr_afip_'.$id_configuracion_afip.'.csr','');
        }

        return file_get_contents($DIRECTORY_TO_SAVE.'request_csr_afip_'.$id_configuracion_afip.'.csr');
    }

    private function guardarKeyDeAfip($ambiente,$key)
    {
        $id_configuracion_afip = session("id_configuracion_afip");

        $ds = DIRECTORY_SEPARATOR;
        $DIRECTORY_TO_SAVE = storage_path('app').$ds."certificados_ws/".$ambiente."/";
        file_put_contents($DIRECTORY_TO_SAVE.'key_from_afip_'.$id_configuracion_afip.'.key',$key);
    }

    public function descargarCsrParaAfip(Request $request)
    {
        $id_configuracion_afip = session("id_configuracion_afip");
        $fileName = "request_csr_afip_".$id_configuracion_afip.".csr";
        $ambiente = $request->input("ambiente");
        
        $id_configuracion_afip = $request->session()->get("id_configuracion_afip");
        $row_obj = ConfiguracionAfip::find($id_configuracion_afip);

        if($row_obj && ($ambiente == "produccion" || $ambiente == "testing"))
        {
            $ds = DIRECTORY_SEPARATOR;
            $DIRECTORY_TO_SAVE = storage_path('app').$ds."certificados_ws/".$ambiente."/";

            if(!file_exists($DIRECTORY_TO_SAVE.'request_csr_afip_'.$id_configuracion_afip.'.csr'))
            {
                file_put_contents($DIRECTORY_TO_SAVE.'request_csr_afip_'.$id_configuracion_afip.'.csr','');
            }

            $content = file_get_contents($DIRECTORY_TO_SAVE.$fileName);

            $headers = [
                'Content-type' => 'text/plain', 
                'Content-Disposition' => sprintf('attachment; filename="%s"', $fileName),
                'Content-Length' => strlen($content)
            ];

            return Response::make($content, 200, $headers);
        }
    }
}

<?php
namespace App\Http\Controllers\Backend\Configuraciones\Afip;

use App\Http\Controllers\Backend\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Pion\Laravel\ChunkUpload\Exceptions\UploadMissingFileException;
use Pion\Laravel\ChunkUpload\Handler\AbstractHandler;
use Pion\Laravel\ChunkUpload\Handler\HandlerFactory;
use Pion\Laravel\ChunkUpload\Receiver\FileReceiver;

use App\Http\Controllers\Backend\Uploader\UploadController;

use Intervention\Image\Facades\Image;

use App\Usuario;

class UploadAfipController extends UploadController
{
    /**
     * Handles the file upload
     *
     * @param FileReceiver $receiver
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @throws UploadMissingFileException
     *
     */
    public function upload_imagen_logo_comprobante(Request $request,FileReceiver $receiver)
    {
        $this->folder_upload = "storage/imagenes/logo_comprobante";

        // compruebe si la carga se realizó correctamente,
        //lance la excepción o devuelva la respuesta que necesita
        if ($receiver->isUploaded() === false) {
            throw new UploadMissingFileException();
        }

        // recibe el archivo
        $save = $receiver->receive();

        // verificar si la carga ha finalizado
        //(en el modo de trozos enviará archivos más pequeños)
        if ($save->isFinished()) {
            // guarda el archivo y devuelve cualquier respuesta que necesites

            $respuesta = $this->saveFile($save->getFile());

            $respuesta_array = $respuesta->getData(true);
            $realpath_image = $respuesta_array["path"]."/".$respuesta_array["name"];

            $image = Image::make($realpath_image);

            $image->resize(290,90,function($c){
              $c->aspectRatio();
            });

            $image->resizeCanvas(290, 90, 'center', false, 'ffffff');

            $image->save($realpath_image);

            $save = $request->input("save");
            $id_usuario = (int) $request->input("id_usuario");

            if($save == true && $id_usuario > 0)
            {
                $row_obj = Usuario::find($id_usuario);

                if($row_obj)
                {
                    $row_obj->foto_perfil = $respuesta_array["name"];
                    $row_obj->save();
                }
            }

            return $respuesta;
        }

        // estamos en modo chunk, enviemos el progreso actual
        /** @var AbstractHandler $handler */

        $handler = $save->handler();

        return response()->json([
            "done" => $handler->getPercentageDone()
        ]);
    }
}

<?php

namespace App\Http\Controllers\Backend\Configuraciones\SMTP;

use Illuminate\Http\Request;
use App\Http\Controllers\Backend\ABM_Core;

use App\Library\ResponseEstructure;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

use Response;

use App\ConfiguracionSmtp;

class SmtpController extends ABM_Core
{
    public function __construct()
    {
        $this->link_controlador = url('/backend/configuraciones/smtp').'/';
        $this->entity ="Configuracion SMTP";
        $this->title_page = "Configuracion SMTP";
        $this->columns = [];
        $this->is_ajax = false;
    }
    
    public function index(Request $request)
    {
        $this->share_parameters();

        $id_configuracion_afip = $request->session()->get("id_configuracion_afip");
        $row_obj = ConfiguracionSmtp::find($id_configuracion_afip);

        return View("backend.configuraciones.smtp.browse")
        ->with("row_obj",$row_obj);
    }

    public function update(Request $request)
    {
        $response_estructure = new ResponseEstructure();
        $response_estructure->set_response(false);

        $id_configuracion_afip = $request->session()->get("id_configuracion_afip");
        $row_obj = ConfiguracionSmtp::find($id_configuracion_afip);
        
        if($row_obj)
        {
            $MAIL_FROM_ADDRESS = trim($request->input("MAIL_FROM_ADDRESS"));
            $MAIL_DRIVER = trim($request->input("MAIL_DRIVER"));
            $MAIL_HOST = trim($request->input("MAIL_HOST"));
            $MAIL_PORT = trim($request->input("MAIL_PORT"));
            $MAIL_USERNAME = trim($request->input("MAIL_USERNAME"));
            $MAIL_PASSWORD = trim($request->input("MAIL_PASSWORD"));
            $MAIL_ENCRYPTION = trim($request->input("MAIL_ENCRYPTION"));
            
            $input= [
                "MAIL_FROM_ADDRESS" => $MAIL_FROM_ADDRESS,
                "MAIL_DRIVER" => $MAIL_DRIVER,
                "MAIL_HOST" => $MAIL_HOST,
                "MAIL_PORT" => $MAIL_PORT,
                "MAIL_USERNAME" => $MAIL_USERNAME,
                "MAIL_PASSWORD" => $MAIL_PASSWORD,
                "MAIL_ENCRYPTION" => $MAIL_ENCRYPTION,
            ];

            $rules = [
                "MAIL_FROM_ADDRESS" => "required",
                "MAIL_DRIVER" => "required",
                "MAIL_HOST" => "required",
                "MAIL_PORT" => "required",
                "MAIL_USERNAME" => "required",
                "MAIL_PASSWORD" => "required",
                "MAIL_ENCRYPTION" => "required",
            ];

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $response_estructure->set_response(false);

                $errors = $validator->errors();

                foreach ($errors->all() as $error) {
                    $response_estructure->add_message_error($error);
                }
            }
            else
            {
                $row_obj->MAIL_FROM_ADDRESS = $MAIL_FROM_ADDRESS;
                $row_obj->MAIL_DRIVER = $MAIL_DRIVER;
                $row_obj->MAIL_HOST = $MAIL_HOST;
                $row_obj->MAIL_PORT = $MAIL_PORT;
                $row_obj->MAIL_USERNAME = $MAIL_USERNAME;
                $row_obj->MAIL_PASSWORD = $MAIL_PASSWORD;
                $row_obj->MAIL_ENCRYPTION = $MAIL_ENCRYPTION;
                $row_obj->save();

                $response_estructure->set_response(true);
            }
        }
        else
        {
            $response_estructure->set_response(false);
            $response_estructure->add_message_error("Configuración no encontrada");
        }
        
        return response()->json($response_estructure->get_response_array());
    }
}

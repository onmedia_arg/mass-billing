<?php

namespace App\Http\Controllers\Backend\MercadoPago;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Backend\ABM_Core;

use App\Library\ResponseEstructure;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

use App\ConfiguracionMercadoPago;

use MP; // MERCADOPAGO LIBRARY
use App\CobroPagoServicio;
use App\EstadoCobroServicio;

class MercadoPagoController extends ABM_Core
{
	public function __construct()
    {
        $this->link_controlador = url('/backend/mp').'/';
        $this->carpeta_views = "backend.mercado_pago.";

        $this->entity ="";
        $this->title_page = "";

        $this->columns = [];

        $this->is_ajax = false;
        $this->add_active = false;
        $this->delete_active = false;
    }

    public function index(Request $request)
    {
        $this->validar_administrador();

        $this->title_page = "Configuración MercadoPago";

        $this->share_parameters();

        $row_obj = ConfiguracionMercadoPago::first();

        return View($this->carpeta_views."browse")
        ->with("row_obj",$row_obj);
    }

    public function update(Request $request)
    {
        $this->validar_administrador();

        $response_estructure = new ResponseEstructure();
        $response_estructure->set_response(false);

        $EN_PRODUCCION = trim($request->input("EN_PRODUCCION"));
        $CLIENT_ID = trim($request->input("CLIENT_ID"));
        $CLIENT_SECRET = trim($request->input("CLIENT_SECRET"));
        $CLIENT_ID_SANDBOX = trim($request->input("CLIENT_ID_SANDBOX"));
        $CLIENT_SECRET_SANDBOX = trim($request->input("CLIENT_SECRET_SANDBOX"));
        $VENDEDOR_ID = trim($request->input("VENDEDOR_ID"));
        $VENDEDOR_NICKNAME = trim($request->input("VENDEDOR_NICKNAME"));
        $VENDEDOR_PASSWORD = trim($request->input("VENDEDOR_PASSWORD"));
        $VENDEDOR_EMAIL = trim($request->input("VENDEDOR_EMAIL"));
        $COMPRADOR_ID = trim($request->input("COMPRADOR_ID"));
        $COMPRADOR_NICKNAME = trim($request->input("COMPRADOR_NICKNAME"));
        $COMPRADOR_PASSWORD = trim($request->input("COMPRADOR_PASSWORD"));
        $COMPRADOR_EMAIL = trim($request->input("COMPRADOR_EMAIL"));

        $row_obj = ConfiguracionMercadoPago::first();
        
        if($row_obj)
        {
            $input= [
                "EN_PRODUCCION"=>$EN_PRODUCCION,
            ];
    
            $rules = [
                "EN_PRODUCCION"=>"required",
            ];

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $response_estructure->set_response(false);

                $errors = $validator->errors();

                foreach ($errors->all() as $error) {
                    $response_estructure->add_message_error($error);
                }
            }
            else
            {
                $row_obj->EN_PRODUCCION = $EN_PRODUCCION;
                $row_obj->CLIENT_ID = $CLIENT_ID;
                $row_obj->CLIENT_SECRET = $CLIENT_SECRET;
                $row_obj->CLIENT_ID_SANDBOX = $CLIENT_ID_SANDBOX;
                $row_obj->CLIENT_SECRET_SANDBOX = $CLIENT_SECRET_SANDBOX;
                $row_obj->VENDEDOR_ID = $VENDEDOR_ID;
                $row_obj->VENDEDOR_NICKNAME = $VENDEDOR_NICKNAME;
                $row_obj->VENDEDOR_PASSWORD = $VENDEDOR_PASSWORD;
                $row_obj->VENDEDOR_EMAIL = $VENDEDOR_EMAIL;
                $row_obj->COMPRADOR_ID = $COMPRADOR_ID;
                $row_obj->COMPRADOR_NICKNAME = $COMPRADOR_NICKNAME;
                $row_obj->COMPRADOR_PASSWORD = $COMPRADOR_PASSWORD;
                $row_obj->COMPRADOR_EMAIL = $COMPRADOR_EMAIL;

                $row_obj->save();

                $response_estructure->set_response(true);
            }
        }
        else
        {
            $response_estructure->set_response(false);
            $response_estructure->add_message_error($this->text_no_search);
        }
        

        return response()->json($response_estructure->get_response_array());
    }

    public function crear_usuarios_de_prueba()
    {
        $this->validar_administrador();
        
        $config_mp = ConfiguracionMercadoPago::find(1);

        if($config_mp)
        {
            $config_mp = new MP("2497133636663218", "RPmudXX4mv5A8Jl7QyoRbwyJO36xXKGX");

            $body = array(
                "site_id" => "MLA"
            );
  
            $result = $config_mp->post('/users/test_user', $body);
  
            var_dump($result);
  
            /*VENDEDOR
            ["id"]=> int(477007788) 
            ["nickname"]=> string(8) "TT984252" 
            ["password"]=> string(10) "qatest5908" 
            ["site_status"]=> string(6) "active" 
            ["email"]=> string(31) "test_user_50957479@testuser.com"

            COMPRADOR
            ["id"]=> int(477005050) 
            ["nickname"]=> string(12) "TEST0UOI6NIT" 
            ["password"]=> string(10) "qatest1226" 
            ["site_status"]=> string(6) "active" 
            ["email"]=> string(31) "test_user_47309305@testuser.com"

            Argentina	4509 9535 6623 3704
            APRO
            */
        }
    }

    public function pagar(Request $request,$id_referencia_pago)
    {
        /*
        $carrito_software_obj = CarritoSoftware::where("id",$id_carrito)
        ->where("id_cli_soft",$request->session()->get("id_cli_soft"))
        ->where("id_estado",EstadoCarritoSoftware::$ESTADO_PAGO_PENDIENTE)
        ->first();
        */

        /*

        if($carrito_software_obj)
        {
        */
            $config_mp = ConfiguracionMercadoPago::find(1);
        
            if($config_mp)
            {
                $CLIENTE_ID_AND_SECRET = $this->get_client_id_and_secret($config_mp);

                $CLIENT_ID = $CLIENTE_ID_AND_SECRET["CLIENT_ID"];
                $CLIENT_SECRET = $CLIENTE_ID_AND_SECRET["CLIENT_SECRET"];

                $MercadoPagoLibrary = new MP($CLIENT_ID, $CLIENT_SECRET);

                $access_token = $MercadoPagoLibrary->get_access_token();
                $data = array('site_id' => 'MLA');

                $preference_data = array(
                    "external_reference" => encrypt("plan_numero_pago_".$id_referencia_pago),
                    "items" => array(
                        array(
                            "id" => "plan_numero_pago_".$id_referencia_pago,
                            "title" => "Pago #".$id_referencia_pago,
                            "category_id" => "Ventas",
                            "quantity" => 1,
                            "unit_price" => floatval(number_format((float) 120.42, 2, '.', '')),
                        )
                    ),
                    "auto_return" => "approved",
                    "back_urls" => array(
                        "failure" => url("/mercado_pago/respuesta_pago"),
                        "pending" => url("/mercado_pago/respuesta_pago"),
                        "success" => url("/mercado_pago/respuesta_pago"),
                    )
                );

                $preference = $MercadoPagoLibrary->create_preference($preference_data);

                return Redirect($preference['response']['init_point']);
            }
        /*
        }
        */
    }

    public function respuesta_pago(Request $request)
    {
        $config_mp = ConfiguracionMercadoPago::find(1);

        if($config_mp)
        {
            $CLIENTE_ID_AND_SECRET = $this->get_client_id_and_secret($config_mp);

            $CLIENT_ID = $CLIENTE_ID_AND_SECRET["CLIENT_ID"];
            $CLIENT_SECRET = $CLIENTE_ID_AND_SECRET["CLIENT_SECRET"];

            $config_mp = new MP ($CLIENT_ID, $CLIENT_SECRET);

            $external_reference = $request->input("external_reference");

            $decrypt_external_reference = decrypt($external_reference);

            if(strpos($decrypt_external_reference,"pago_numero_") !== FALSE)
            {
                $id_referencia_pago = str_replace("pago_numero_","",$decrypt_external_reference);

                $cobro_pago_servicio_obj = CobroPagoServicio::where("id",$id_referencia_pago)
                ->where("id_estado",EstadoCobroServicio::PENDIENTE)
                ->first();

                if($cobro_pago_servicio_obj)
                {
                    $payment_info = $config_mp->get_payment_info($request->input("collection_id"));

                    $status_code = $payment_info["status"];
                    $transaction_amount = $payment_info["response"]["transaction_amount"];

                    $precio = floatval(number_format((float) $cobro_pago_servicio_obj->precio, 2, '.', ''));

                    if($payment_info["status"] == 200 && $precio == $payment_info["response"]["transaction_amount"])
                    {
                        $view_to_load = "";

                        if($payment_info["response"]["status"] == 'approved')
                        {
                            
                            if($cobro_pago_servicio_obj->id_estado  != EstadoCobroServicio::ACTIVO)
                            {
                                $cobro_pago_servicio_obj->id_estado = EstadoCobroServicio::ACTIVO;
                                //$cobro_pago_servicio_obj->fecha_hora_de_pago = Date("Y-m-d H:i:s");
                                
                                $fecha_actual = date('Y-m-d');
                                $fecha_vencimiento = strtotime('+1 month',strtotime($fecha_actual));
                                $fecha_vencimiento = date('Y-m-d',$fecha_vencimiento);

                                $cobro_pago_servicio_obj->fecha_vencimiento = $fecha_vencimiento;
                                $cobro_pago_servicio_obj->id_mercado_pago = $request->input("collection_id");
                                $cobro_pago_servicio_obj->save();
                            }

                            $view_to_load = "pago_correcto";
                        }
                        else if($payment_info["response"]["status"] == 'rejected' || $payment_info["response"]["status"] == "cancelled")
                        {
                            
                            if($cobro_pago_servicio_obj->id_estado  != EstadoCobroServicio::ERROR_EN_PAGO)
                            {
                                $cobro_pago_servicio_obj->id_estado = EstadoCobroServicio::ERROR_EN_PAGO;
                                $cobro_pago_servicio_obj->id_mercado_pago = $request->input("collection_id");
                                $cobro_pago_servicio_obj->save();
                            }

                            $view_to_load = "pago_rechazado";
                        }
                        else if($payment_info["response"]["status"] == 'in_process')
                        {
                            
                            if($cobro_pago_servicio_obj->id_estado  != EstadoCobroServicio::PAGO_PENDIENTE)
                            {
                                $cobro_pago_servicio_obj->id_estado = EstadoCobroServicio::PAGO_PENDIENTE;
                                $cobro_pago_servicio_obj->id_mercado_pago = $request->input("collection_id");
                                $cobro_pago_servicio_obj->save();
                            }

                            $view_to_load = "pago_pendiente";
                        }

                        
                        $request->session()->flash("id_cobro_pago_servicio",$cobro_pago_servicio_obj->id);

                        switch($view_to_load)
                        {
                            case "pago_correcto":
                                $request->session()->flash("permiso_vista_respuesta_pago",true);
                                return Redirect("/backend/mercado_pago/pago_correcto");
                            break;

                            case "pago_rechazado":
                                $request->session()->flash("permiso_vista_respuesta_pago",true);
                                return Redirect("/backend/mercado_pago/pago_rechazado");
                            break;

                            case "pago_pendiente":
                                $request->session()->flash("permiso_vista_respuesta_pago",true);
                                return Redirect("/backend/mercado_pago/pago_pendiente");
                            break;
                        }
                    }
                }
            }
        }
    }

    public function pago_pendiente(Request $request)
    {
        if($request->session()->get("permiso_vista_respuesta_pago") === true)
        {
            $id_cobro_pago_servicio = $request->session()->get("id_cobro_pago_servicio");

            $cobro_pago_servicio_obj = CobroPagoServicio::where("id",$id_cobro_pago_servicio)->first();

            if($cobro_pago_servicio_obj)
            {
                return View($this->carpeta_views."pago_pendiente")
                ->with("cobro_pago_servicio_obj",$cobro_pago_servicio_obj);
            }
        }
        else
        {
            return Redirect("/backend/desktop");
        }
    }

    public function error_de_pago(Request $request)
    {
        if($request->session()->get("permiso_vista_respuesta_pago") === true)
        {
            $id_cobro_pago_servicio = $request->session()->get("id_cobro_pago_servicio");

            $cobro_pago_servicio_obj = CobroPagoServicio::where("id",$id_cobro_pago_servicio)->first();

            if($cobro_pago_servicio_obj)
            {
                return View($this->carpeta_views."error_de_pago")
                ->with("cobro_pago_servicio_obj",$cobro_pago_servicio_obj);
            }
        }
        else
        {
            return Redirect("/backend/desktop");
        }
    }

    public function pago_correcto(Request $request)
    {
        if($request->session()->get("permiso_vista_respuesta_pago") === true)
        {
            $id_cobro_pago_servicio = $request->session()->get("id_cobro_pago_servicio");

            $cobro_pago_servicio_obj = CobroPagoServicio::where("id",$id_cobro_pago_servicio)->first();

            if($cobro_pago_servicio_obj)
            {
                return View($this->carpeta_views."pago_correcto")
                ->with("cobro_pago_servicio_obj",$cobro_pago_servicio_obj);
            }
        }
        else
        {
            return Redirect("/backend/desktop");
        }
    }

    public function mp_notificacion_ipn(Request $request)
    {
        $config_mp = ConfiguracionMercadoPago::find(1);

        if($config_mp)
        {
            $CLIENTE_ID_AND_SECRET = $this->get_client_id_and_secret($config_mp);

            $CLIENT_ID = $CLIENTE_ID_AND_SECRET["CLIENT_ID"];
            $CLIENT_SECRET = $CLIENTE_ID_AND_SECRET["CLIENT_SECRET"];

            $config_mp = new MP ($CLIENT_ID, $CLIENT_SECRET);

            $id_pago = $request->input("id");

            $payment_info = $config_mp->get_payment_info($id_pago);
            $external_reference = $payment_info["response"]["external_reference"];
            $decrypt_external_reference = decrypt($external_reference);

            if(strpos($decrypt_external_reference,"pago_numero_") !== FALSE)
            {
                $id_referencia_pago = str_replace("pago_numero_","",$decrypt_external_reference);

                $cobro_pago_servicio_obj = CobroPagoServicio::where("id",$id_referencia_pago)
                ->first();

                if($cobro_pago_servicio_obj)
                {
                    $status_code = $payment_info["status"];
                    $transaction_amount = $payment_info["response"]["transaction_amount"];

                    $precio = floatval(number_format((float) $cobro_pago_servicio_obj->precio, 2, '.', ''));

                    if($payment_info["status"] == 200 && $precio == $payment_info["response"]["transaction_amount"])
                    {
                        if($payment_info["response"]["status"] == 'approved')
                        {
                            if($cobro_pago_servicio_obj->id_estado != EstadoCobroServicio::ACTIVO)
                            {
                                $cobro_pago_servicio_obj->id_estado = EstadoCobroServicio::ACTIVO;
                                //$cobro_pago_servicio_obj->fecha_hora_de_pago = Date("Y-m-d H:i:s");
                                
                                
                                $fecha_actual = date('Y-m-d');
                                $fecha_vencimiento = strtotime('+1 month',strtotime($fecha_actual));
                                $fecha_vencimiento = date('Y-m-d',$fecha_vencimiento);
                                
                                $cobro_pago_servicio->fecha_vencimiento = $fecha_vencimiento;
                                $cobro_pago_servicio_obj->id_mercado_pago = $request->input("id");
                                $cobro_pago_servicio_obj->save();
                            }
                        }
                        else if($payment_info["response"]["status"] == 'rejected' || $payment_info["response"]["status"] == "cancelled")
                        {
                            
                            if($cobro_pago_servicio_obj->id_estado  != EstadoCobroServicio::ERROR_EN_PAGO)
                            {
                                $cobro_pago_servicio_obj->id_estado = EstadoCobroServicio::ERROR_EN_PAGO;
                                $cobro_pago_servicio_obj->id_mercado_pago = $request->input("id");
                                $cobro_pago_servicio_obj->save();
                            }
                        }
                        else if($payment_info["response"]["status"] == 'in_process')
                        {
                            
                            if($cobro_pago_servicio_obj->id_estado  != EstadoCobroServicio::PAGO_PENDIENTE)
                            {
                                $cobro_pago_servicio_obj->id_estado = EstadoCobroServicio::PAGO_PENDIENTE;
                                $cobro_pago_servicio_obj->id_mercado_pago = $request->input("id");
                                $cobro_pago_servicio_obj->save();
                            }
                        }
                    }
                }
            }
        }
    }

    private function get_client_id_and_secret(ConfiguracionMercadoPago $config_mp)
    {
        $CLIENT_ID = $config_mp->CLIENT_ID;
        $CLIENT_SECRET = $config_mp->CLIENT_SECRET;

        if($config_mp->EN_PRODUCCION == false)
        {
            $CLIENT_ID = $config_mp->CLIENT_ID_SANDBOX;
            $CLIENT_SECRET = $config_mp->CLIENT_SECRET_SANDBOX;
        }

        return ["CLIENT_ID"=>$CLIENT_ID,"CLIENT_SECRET"=>$CLIENT_SECRET];
    }
}
<?php

namespace App\Http\Controllers\Backend\Contacto;

use Illuminate\Http\Request;
use App\Http\Controllers\Backend\ABM_Core;

use Illuminate\Support\Facades\Mail;
use App\Mail\EmailSender;

use App\Library\ResponseEstructure;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

use App\Configuracion;

class ContactoController extends ABM_Core
{
    public function __construct()
    {
        $this->link_controlador = url('/backend/contacto').'/';
        $this->carpeta_views = "backend.contacto.";

        $this->entity ="Contacto";
        $this->title_page = "Contacto";

        $this->columns = [
        ];

        $this->edit_active = false;
        $this->delete_active = false;
        $this->is_ajax = false;
    }

    public function index(Request $request)
    {
        $this->title_page = "Contacto";
        $this->share_parameters();
        return View($this->carpeta_views."browse");
    }

    public function enviarMensaje(Request $request)
    {
        $response_estructure = new ResponseEstructure();
        $response_estructure->set_response(false);

        $nombre = $request->session()->get("nombre");
        $correo = $request->session()->get("correo");
        $mensaje = $request->input("mensaje");

        $input = [
            "nombre" => $nombre,
            "correo" => $correo,
            "mensaje" => $mensaje,
        ];

        $rules = [
            "nombre" => "required|min:3",
            "correo" => "required|email",
            "mensaje" => "required|min:10"
        ];

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            $response_estructure->set_response(false);

            $errors = $validator->errors();

            foreach ($errors->all() as $error) {
                $response_estructure->add_message_error($error);
            }
        }
        else
        {
            
            $email_sender = new \stdClass();
            $email_sender->title = "Nuevo Contacto de cliente desde el sistema";
            $email_sender->nombre = $nombre;
            $email_sender->correo =$correo;
            $email_sender->mensaje = $mensaje;

            $email_notificacion_contacto = Configuracion::where("clave","MAIL_NOTIFICACION_CONTACTO")->first();

            if($email_notificacion_contacto)
            {
                Mail::to($email_notificacion_contacto->valor)->send(
                    new EmailSender(
                        $email_sender,$email_sender->title,
                        "emails.nuevo_contacto_cliente"
                    )
                );
            }

            $response_estructure->set_response(true);
        }

        return response()->json($response_estructure->get_response_array());
    }
}

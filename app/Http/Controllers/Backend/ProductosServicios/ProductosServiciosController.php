<?php
namespace App\Http\Controllers\Backend\ProductosServicios;

use Illuminate\Http\Request;
use App\Http\Controllers\Backend\ABM_Core;

use App\Library\ResponseEstructure;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

use App\ProductoServicio;
use App\UnidadDeMedida;

class ProductosServiciosController extends ABM_Core
{
    public function __construct()
    {
        $this->link_controlador = url('/backend/productos_servicios').'/';
        $this->entity ="Producto/Servicio";
        $this->title_page = "Productos/Servicios";

        $this->columns = array(
            ["name"=>"Código","reference"=>"productos_servicios.codigo"],
            ["name"=>"Descripcion","reference"=>"productos_servicios.descripcion"],
            ["name"=>"Precio","reference"=>"productos_servicios.precio"],
            ["name"=>"Precio (USD)","reference"=>"productos_servicios.precio_dolar"],
            ["name"=>"Tipo","reference"=>"productos_servicios.tipo"],
            ["name"=>"Tipo Unidad","reference"=>"unidades_de_medida.descripcion"],
            ["name"=>"Acciones","reference"=>null]
        );

        $this->is_ajax = true;

    }
    
    public function index(Request $request)
    {
        $this->share_parameters();

        $unidades_de_medidas = UnidadDeMedida::orderBy("descripcion")->get();

        return View("backend.productos_servicios.browse")
        ->with("unidades_de_medidas",$unidades_de_medidas);
    }

    public function get_listado_dt(Request $request)
    {
        $id_configuracion_afip = $request->session()->get("id_configuracion_afip");

        $consulta_orm_principal = DB::table("productos_servicios")
        ->leftJoin("unidades_de_medida", "unidades_de_medida.id","=","productos_servicios.id_unidad_medida")
        ->select(
            "productos_servicios.*",
            "unidades_de_medida.descripcion as unidades_de_medida_descripcion"
        )
        ->where("eliminado",0)
        ->where("id_configuracion_afip",$id_configuracion_afip);

        $totalData = $consulta_orm_principal->count();

        $totalFiltered = $totalData;

        $search = $request->input("search");
        $start = $request->input('start');
        $length = $request->input('length');
        $order = $request->input('order');

        $resultado = array();

        if(!empty($search['value']))
        {
            $consulta_orm_principal = $consulta_orm_principal
            ->where(function($query) use($search){
                $query->where("productos_servicios.codigo","like","%".$search['value']."%")
                ->orWhere("productos_servicios.descripcion","like","%".$search['value']."%")
                ->orWhere("productos_servicios.precio","like","%".$search['value']."%")
                ->orWhere("productos_servicios.precio_dolar","like","%".$search['value']."%")
                ->orWhere("productos_servicios.tipo","like","%".$search['value']."%")
                ->orWhere("unidades_de_medida.descripcion","like","%".$search['value']."%");
            });
        }

        $consulta_orm_principal = $consulta_orm_principal->take($length)->skip($start);
        $totalFiltered=$consulta_orm_principal->count();

        $columna_a_ordenar = (int)$order[0]["column"];

        if(isset($this->columns[$columna_a_ordenar]) && $this->columns[$columna_a_ordenar]["reference"] != null){
          $resultado = $consulta_orm_principal->orderBy($this->columns[$columna_a_ordenar]["reference"],$order[0]["dir"])->get();
        }
        else{
          $resultado = $consulta_orm_principal->orderBy("id","desc")->get();
        }
    	
        $data= array();
        
        foreach($resultado as $result_row)
        {
            $row_of_data = array();

            $row_of_data[]=strip_tags($result_row->codigo);
            $row_of_data[]=strip_tags($result_row->descripcion);
            $row_of_data[]=strip_tags($result_row->precio);
            $row_of_data[]=strip_tags($result_row->precio_dolar);
            $row_of_data[]=strip_tags($result_row->tipo);
            $row_of_data[]=strip_tags($result_row->unidades_de_medida_descripcion);
            
            $buttons_actions = "<div class='form-button-action'>";

            if($this->edit_active)
            {
                if($this->is_ajax)
                {
                    $buttons_actions .= 
                    "<button onclick='abrir_modal_editar(".$result_row->id.")' type='button' data-toggle='tooltip' title='' class='btn-sm ".$this->config_buttons["edit"]["class"]."' data-original-title='".$this->config_buttons["edit"]["title"]."'>
                        <i class='".$this->config_buttons["edit"]["icon"]."'></i>
                    </button>";
                }
                else{
                    $buttons_actions .= 
                    "<a href='".$this->link_controlador."editar/".$result_row->id."' data-toggle='tooltip' title='' class='btn-sm ".$this->config_buttons["edit"]["class"]."' data-original-title='".$this->config_buttons["edit"]["title"]."'>
                        <i class='".$this->config_buttons["edit"]["icon"]."'></i>
                    </a>";
                }
            }

            if($this->delete_active)
            {
                $buttons_actions.=
                "<button onclick='eliminar(".$result_row->id.")' type='button' data-toggle='tooltip' title='' class='btn-sm ".$this->config_buttons["delete"]["class"]."' data-original-title='".$this->config_buttons["delete"]["title"]."'>
                    <i class='".$this->config_buttons["delete"]["icon"]."'></i>
                </button>";
            }

            $buttons_actions.="</div>";

            
            $row_of_data[]=$buttons_actions;

            $data[]=$row_of_data;
        }

		$json_data = array(
            "draw"            => intval($request->input('draw')), 
            "recordsTotal"    => intval($totalData), 
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data
        );

    	return response()->json($json_data);
    }

    public function get(Request $request)
    {
        $id_configuracion_afip = $request->session()->get("id_configuracion_afip");

        $response_estructure = new ResponseEstructure();
        $response_estructure->set_response(false);

        $id = $request->input("id");

        $row_obj = ProductoServicio::where("id",$id)->where("id_configuracion_afip",$id_configuracion_afip)->first();
        
        if($row_obj)
        {
            $response_estructure->set_response(true);
            $response_estructure->set_data($row_obj);
        }
        else
        {
            $response_estructure->set_response(false);
            $response_estructure->add_message_error($this->text_no_search);
        }
        

        return response()->json($response_estructure->get_response_array());
    }

    public function store(Request $request)
    {
        $id_configuracion_afip = $request->session()->get("id_configuracion_afip");

        $response_estructure = new ResponseEstructure();
        $response_estructure->set_response(false);

        $codigo = trim(strtolower($request->input("codigo")));
        $descripcion = trim($request->input("descripcion"));
        $precio = (double) trim(strtolower($request->input("precio")));
        $precio_dolar = (double) trim(strtolower($request->input("precio_dolar")));
        $id_unidad_medida = trim(strtolower($request->input("id_unidad_medida")));
        $tipo = trim(strtolower($request->input("tipo")));

        $input= [
            "código"=>$codigo,
            "descripción"=>$descripcion,
            "precio"=>$precio,
            "precio_dolar"=>$precio_dolar,
            "unidad_de_medida"=>$id_unidad_medida,
            "tipo"=>$tipo,
        ];

        $rules = [
            "código"=>"required",
            "descripción"=>"required",
            "unidad_de_medida"=>"required",
            "tipo"=>"required"
        ];

        if($precio > 0)
        {
            $precio_dolar = null;
        }
        else if($precio_dolar > 0)
        {
            $precio = null;
        }

        if(trim($precio_dolar) == "" && trim($precio) == "")
        {
            $rules["precio"] = "required";
        }

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            $response_estructure->set_response(false);

            $errors = $validator->errors();

            foreach ($errors->all() as $error) {
                $response_estructure->add_message_error($error);
            }
        }
        else
        {
            $producto_servicio_obj = ProductoServicio::where("codigo",$codigo)
            ->where("id_configuracion_afip",$id_configuracion_afip)
            ->where("eliminado",0)->first();

            if($producto_servicio_obj)
            {
                $response_estructure->set_response(false);
                $response_estructure->add_message_error("El código de producto/servicio ya está en uso");
            }
            else
            {
                if($precio <= 0){
                    $precio = null;
                }

                if($precio_dolar <= 0){
                    $precio_dolar = null;
                }

                $row_obj = new ProductoServicio();
                $row_obj->codigo = $codigo;
                $row_obj->descripcion = $descripcion;
                $row_obj->precio = $precio;
                $row_obj->precio_dolar = $precio_dolar;
                $row_obj->id_unidad_medida = $id_unidad_medida;
                $row_obj->tipo = $tipo;
                $row_obj->id_configuracion_afip = $id_configuracion_afip;

                $row_obj->save();

                $response_estructure->set_response(true);
            }
        }

        return response()->json($response_estructure->get_response_array());
    }

    public function update(Request $request)
    {
        $id_configuracion_afip = $request->session()->get("id_configuracion_afip");

        $response_estructure = new ResponseEstructure();
        $response_estructure->set_response(false);

        $id = $request->input("id");

        $row_obj = ProductoServicio::where("id",$id)
        ->where("id_configuracion_afip",$id_configuracion_afip)
        ->where("eliminado",0)
        ->first();
        
        if($row_obj)
        {
            $codigo = trim(strtolower($request->input("codigo")));
            $descripcion = trim($request->input("descripcion"));
            $precio = (double) trim(strtolower($request->input("precio")));
            $precio_dolar = (double) trim(strtolower($request->input("precio_dolar")));
            $id_unidad_medida = trim(strtolower($request->input("id_unidad_medida")));
            $tipo = trim(strtolower($request->input("tipo")));

            $input= [
                "código"=>$codigo,
                "descripción"=>$descripcion,
                "precio"=>$precio,
                "precio_dolar"=>$precio_dolar,
                "unidad_de_medida"=>$id_unidad_medida,
                "tipo"=>$tipo,
            ];

            $rules = [

                "código"=>"required",
                "descripción"=>"required",
                "unidad_de_medida"=>"required",
                "tipo"=>"required"
            ];

            if($precio > 0)
            {
                $precio_dolar = null;
            }
            else if($precio_dolar > 0)
            {
                $precio = null;
            }

            if(trim($precio_dolar) == "" && trim($precio) == "")
            {
                $rules["precio"] = "required";
            }

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $response_estructure->set_response(false);

                $errors = $validator->errors();

                foreach ($errors->all() as $error) {
                    $response_estructure->add_message_error($error);
                }
            }
            else
            {
                $producto_servicio_obj = ProductoServicio::where("codigo",$codigo)
                ->where("id_configuracion_afip",$id_configuracion_afip)
                ->where("id","<>",$id)
                ->where("eliminado",0)
                ->first();

                if($producto_servicio_obj)
                {
                    $response_estructure->set_response(false);
                    $response_estructure->add_message_error("El código de producto/servicio ya está en uso");
                }
                else
                {
                    if($precio <= 0){
                        $precio = null;
                    }
                    if($precio_dolar <= 0){
                        $precio_dolar = null;
                    }

                    $row_obj->codigo = $codigo;
                    $row_obj->descripcion = $descripcion;
                    $row_obj->precio = $precio;
                    $row_obj->precio_dolar = $precio_dolar;
                    $row_obj->id_unidad_medida = $id_unidad_medida;
                    $row_obj->tipo = $tipo;

                    $row_obj->save();

                    $response_estructure->set_response(true);
                }
            }
        }
        else
        {
            $response_estructure->set_response(false);
            $response_estructure->add_message_error($this->text_no_search);
        }
        
        return response()->json($response_estructure->get_response_array());
    }

    public function delete(Request $request)
    {
        $id_configuracion_afip = $request->session()->get("id_configuracion_afip");

        $response_estructure = new ResponseEstructure();
        $response_estructure->set_response(false);

        $id = $request->input("id");

        $row_obj = ProductoServicio::where("id",$id)->where("id_configuracion_afip",$id_configuracion_afip)->first();
        
        if($row_obj)
        {
            $row_obj->eliminado = 1;
            $row_obj->save();
            $response_estructure->set_response(true);
        }
        else
        {
            $response_estructure->set_response(false);
            $response_estructure->add_message_error($this->text_no_search);
        }


        return response()->json($response_estructure->get_response_array());
    }

    public function busquedaProductoNuevoComprobante(Request $request)
    {
        $id_configuracion_afip = $request->session()->get("id_configuracion_afip");

        $busqueda = trim($request->input("q"));

    	$resultados_busqueda = ProductoServicio::select("productos_servicios.*")
    	->where("productos_servicios.id_configuracion_afip",$id_configuracion_afip)
    	->where("productos_servicios.eliminado",0)
		->where(function($query) use ($busqueda){
            $query->where("productos_servicios.codigo","like","%".$busqueda."%")
            ->orWhere("productos_servicios.descripcion","like","%".$busqueda."%");
        });

    	$resultados_busqueda =$resultados_busqueda->take(10)
		->get();

    	$respuesta = array();

    	if(count($resultados_busqueda) > 0)
    	{
	    	foreach($resultados_busqueda as $resultado_busqueda)
	    	{
	    		$respuesta[]= array("id"=>$resultado_busqueda->id,"text"=>$resultado_busqueda->codigo." - ".$resultado_busqueda->descripcion );
	    	}
    	}
    	else
    	{
			$respuesta[]= array("id"=>"agregar_".$busqueda,"text"=>$busqueda);
    	}

    	return response()->json($respuesta);
    }
}

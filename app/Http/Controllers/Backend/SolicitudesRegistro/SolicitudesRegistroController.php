<?php
namespace App\Http\Controllers\Backend\SolicitudesRegistro;

use Illuminate\Http\Request;
use App\Http\Controllers\Backend\ABM_Core;

use App\Library\ResponseEstructure;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

use App\SolicitudRegistro;

class SolicitudesRegistroController extends ABM_Core
{
    public function __construct()
    {
        $this->link_controlador = url('/backend/solicitudes_de_registro').'/';
        $this->entity ="Solicitud de Registro";
        $this->title_page = "Solicitudes de Registro";

        $this->columns = array(
            ["name"=>"CUIT","reference"=>"solicitudes_registros.cuit"],
            ["name"=>"Nombre","reference"=>"solicitudes_registros.nombre"],
            ["name"=>"Apellido","reference"=>"solicitudes_registros.apellido"],
            ["name"=>"Correo","reference"=>"solicitudes_registros.correo"],
            ["name"=>"Teléfono","reference"=>"solicitudes_registros.telefono"],
            ["name"=>"Contactado","reference"=>"solicitudes_registros.contactado"],
            ["name"=>"Plan","reference"=>"planes.nombre"],
            ["name"=>"Acciones","reference"=>null]
        );

        $this->is_ajax = true;
        $this->add_active = false;
    }
    
    public function index(Request $request)
    {
        $this->validar_administrador();

        $this->share_parameters();

        return View("backend.solicitudes_registros.browse");
    }

    public function get_listado_dt(Request $request)
    {
        $this->validar_administrador();

        $consulta_orm_principal = DB::table("solicitudes_registros")
        ->join("planes","planes.id","=","solicitudes_registros.id_plan")
        ->select(
            "solicitudes_registros.*",
            "planes.nombre as planes_nombre"
        );

        $totalData = $consulta_orm_principal->count();

        $totalFiltered = $totalData;

        $search = $request->input("search");
        $start = $request->input('start');
        $length = $request->input('length');
        $order = $request->input('order');

        $resultado = array();

        if(!empty($search['value']))
        {
            $consulta_orm_principal = $consulta_orm_principal
            ->where(function($query) use($search){
                $query->where("solicitudes_registros.cuit","like","%".$search['value']."%")
                ->orWhere("solicitudes_registros.nombre","like","%".$search['value']."%")
                ->orWhere("solicitudes_registros.apellido","like","%".$search['value']."%")
                ->orWhere("solicitudes_registros.correo","like","%".$search['value']."%")
                ->orWhere("solicitudes_registros.telefono","like","%".$search['value']."%")
                ->orWhere("solicitudes_registros.contactado","like","%".$search['value']."%")
                ->orWhere("planes.nombre","like","%".$search['value']."%"); 
            });
        }

        $consulta_orm_principal = $consulta_orm_principal->take($length)->skip($start);
        $totalFiltered=$consulta_orm_principal->count();

        $columna_a_ordenar = (int)$order[0]["column"];

        if(isset($this->columns[$columna_a_ordenar]) && $this->columns[$columna_a_ordenar]["reference"] != null){
          $resultado = $consulta_orm_principal->orderBy($this->columns[$columna_a_ordenar]["reference"],$order[0]["dir"])->get();
        }
        else{
          $resultado = $consulta_orm_principal->orderBy("id","desc")->get();
        }
    	
        $data= array();
        
        foreach($resultado as $result_row)
        {
            $row_of_data = array();

            $row_of_data[]=strip_tags($result_row->cuit);
            $row_of_data[]=strip_tags($result_row->nombre);
            $row_of_data[]=strip_tags($result_row->apellido);
            $row_of_data[]=strip_tags($result_row->correo);
            $row_of_data[]=strip_tags($result_row->telefono);

            if($result_row->contactado)
            {
                $row_of_data[]='<span class="badge text-white" style="background-color: #28a745;">SI</span>';
            }
            else
            {
                $row_of_data[]='<span class="badge text-white" style="background-color: #dc3545;">NO</span>';
            }
            
            $row_of_data[]=strip_tags($result_row->planes_nombre);
            
            $buttons_actions = "<div class='form-button-action'>";

            if($this->edit_active)
            {
                if($this->is_ajax)
                {
                    $buttons_actions .= 
                    "<button onclick='abrir_modal_editar(".$result_row->id.")' type='button' data-toggle='tooltip' title='' class='btn-sm ".$this->config_buttons["edit"]["class"]."' data-original-title='".$this->config_buttons["edit"]["title"]."'>
                        <i class='".$this->config_buttons["edit"]["icon"]."'></i>
                    </button>";
                }
                else{
                    $buttons_actions .= 
                    "<a href='".$this->link_controlador."editar/".$result_row->id."' data-toggle='tooltip' title='' class='btn-sm ".$this->config_buttons["edit"]["class"]."' data-original-title='".$this->config_buttons["edit"]["title"]."'>
                        <i class='".$this->config_buttons["edit"]["icon"]."'></i>
                    </a>";
                }
            }

            if($this->delete_active)
            {
                $buttons_actions.=
                "<button onclick='eliminar(".$result_row->id.")' type='button' data-toggle='tooltip' title='' class='btn-sm ".$this->config_buttons["delete"]["class"]."' data-original-title='".$this->config_buttons["delete"]["title"]."'>
                    <i class='".$this->config_buttons["delete"]["icon"]."'></i>
                </button>";
            }

            $buttons_actions.="</div>";

            
            $row_of_data[]=$buttons_actions;

            $data[]=$row_of_data;
        }

		$json_data = array(
            "draw"            => intval($request->input('draw')), 
            "recordsTotal"    => intval($totalData), 
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data
        );

    	return response()->json($json_data);
    }

    public function get(Request $request)
    {
        $this->validar_administrador();

        $response_estructure = new ResponseEstructure();
        $response_estructure->set_response(false);

        $id = $request->input("id");

        $row_obj = SolicitudRegistro::where("id",$id)->first();
        
        if($row_obj)
        {
            $response_estructure->set_response(true);
            $response_estructure->set_data($row_obj);
        }
        else
        {
            $response_estructure->set_response(false);
            $response_estructure->add_message_error($this->text_no_search);
        }
        

        return response()->json($response_estructure->get_response_array());
    }
    
    public function update(Request $request)
    {
        $this->validar_administrador();

        $response_estructure = new ResponseEstructure();
        $response_estructure->set_response(false);

        $id = $request->input("id");

        $row_obj = SolicitudRegistro::where("id",$id)
        ->first();
        
        if($row_obj)
        {
            $contactado = $request->input("contactado");
            

            $input= [
                "contactado"=>$contactado,
            ];
    
            $rules = [
                "contactado"=>"required",
            ];

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $response_estructure->set_response(false);

                $errors = $validator->errors();

                foreach ($errors->all() as $error) {
                    $response_estructure->add_message_error($error);
                }
            }
            else
            {
                $row_obj->contactado = $contactado;
                $row_obj->save();
                 
                $response_estructure->set_response(true);
            }
        }
        else
        {
            $response_estructure->set_response(false);
            $response_estructure->add_message_error($this->text_no_search);
        }
        
        return response()->json($response_estructure->get_response_array());
    }

    public function delete(Request $request)
    {
        $this->validar_administrador();
        
        $response_estructure = new ResponseEstructure();
        $response_estructure->set_response(false);

        $id = $request->input("id");

        $row_obj = SolicitudRegistro::where("id",$id)->first();
        
        if($row_obj)
        {
            $row_obj->delete();
            $response_estructure->set_response(true);
        }
        else
        {
            $response_estructure->set_response(false);
            $response_estructure->add_message_error($this->text_no_search);
        }


        return response()->json($response_estructure->get_response_array());
    }

}

<?php

namespace App\Http\Controllers\Backend\VentanillaElectronica;

use Illuminate\Http\Request;
use App\Http\Controllers\Backend\ABM_Core;

use Illuminate\Support\Facades\Mail;
use App\Mail\EmailSender;

use App\Library\ResponseEstructure;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

use App\Usuario;

use App\Library\WebserviceAfip\WebserviceAfip;

use Mpdf\Mpdf;

class VentanillaElectronicaController extends ABM_Core
{
    protected $ws_afip = null;

    public function __construct()
    {
        $this->link_controlador = url('/backend/ventanilla_electronica').'/';
        $this->carpeta_views = "backend.ventanilla_electronica.";

        $this->entity ="Ventanilla Electronica";
        $this->title_page = "Ventanilla Electronica";

        $this->columns = [
            ["name"=>"Asunto","reference"=>null],
            ["name"=>"Fecha","reference"=>null],
            //["name"=>"Adjuntos","reference"=>null],
            ["name"=>"Estado","reference"=>null],
            ["name"=>"","reference"=>null]
        ];

        $this->add_active = false;
        $this->edit_active = false;
        $this->delete_active = false;
        $this->is_ajax = false;
    }

    public function index(Request $request)
    {
        $this->share_parameters();

        try
        {
            $id_configuracion_afip = $request->session()->get("id_configuracion_afip");
            $this->ws_afip = new WebserviceAfip($id_configuracion_afip);
            $this->ws_afip->getTiposDeConceptos();

            return View($this->carpeta_views."browse");
        }
        catch(\Exception $e)
        {
            $request->session()->flash("errorCertificado",1);
            return Redirect('/backend/afip/errorCertificado');
        }
    }

    public function get_listado_dt(Request $request)
    {
        $id_usuario = $request->session()->get("id");

        $id_usuario_filtro = (int)$request->input("id_usuario_filtro");

        if($id_usuario_filtro > 0)
        {
            $id_rol = $request->session()->get("id_rol");

            if($id_rol == 1)
            {
                $id_usuario = $id_usuario_filtro;
            }
        }

        try
        {
            $this->ws_afip = new WebserviceAfip($id_usuario);

            // revision
            $search = $request->input("search");
            $start = $request->input('start');
            $length = $request->input('length');

            if($start == 0){$start = 1;}

            $fechaDesde = strtotime('-360 day',strtotime(date('Y-m-d'))) ;
            $fechaDesde = date('Y-m-d', $fechaDesde);

            $parametros_filter = array(
                "fechaDesde"=>$fechaDesde,
                "pagina"=>$start,
                "resultadosPorPagina"=>$length,
            );

            $resultado = $this->ws_afip->consultarComunicaciones($parametros_filter);

            $totalData = 0;
            
            $totalFiltered = 0;

            if(isset($resultado["ns1consultarComunicacionesResponse"]["ns2RespuestaPaginada"]["totalItems"]))
            {
                $totalData = $resultado["ns1consultarComunicacionesResponse"]["ns2RespuestaPaginada"]["totalItems"];
            }
            
            $data= array();

            $data_resultado = array();

            if(isset($resultado["ns1consultarComunicacionesResponse"]["ns2RespuestaPaginada"]["ns4items"]))
            {
                $data_resultado = $resultado["ns1consultarComunicacionesResponse"]["ns2RespuestaPaginada"]["ns4items"];
            }

            
            
            foreach($data_resultado as $result_row)
            {
                for($i=0; $i < count($result_row);$i++)
                {
                    $resultado_row_index = $result_row[$i];
                    $row_of_data = array();

                    $row_of_data[]=$resultado_row_index["asunto"];
        
                    $fecha_publicacion = $resultado_row_index["fechaPublicacion"];
        
                    if(trim($fecha_publicacion) != "")
                    {
                        $fecha_publicacion = \DateTime::createFromFormat("Y-m-d H:i:s",$fecha_publicacion);
                        $fecha_publicacion = $fecha_publicacion->format("d/m/Y H:i");
                    }
        
                    $row_of_data[]=strip_tags($fecha_publicacion);
                    
                    // NO LEIDA
                    if($resultado_row_index["estado"] == 1)
                    {
                        $row_of_data[]='<span class="badge text-white" style="background-color: #dc3545">No leído</span>';
                    }
                    // LEIDO
                    else
                    {
                        $row_of_data[]='<span class="badge text-white" style="background-color: #28a745">Leído</span>';
                    }

                    $buttons_actions = "<div class='form-button-action'>";
        
                    $buttons_actions.= 
                    "<button data-toggle='tooltip' title='' class='btn-sm ".$this->config_buttons["watch"]["class"]."' data-original-title='".$this->config_buttons["watch"]["title"]."' onClick='consumirComunicacion(".$resultado_row_index["idComunicacion"].")'>
                        <i class='".$this->config_buttons["watch"]["icon"]."'></i> Leer
                    </button>";
                    $buttons_actions.="</div>";
        
        
                    $row_of_data[]=$buttons_actions;
        
                    $data[]=$row_of_data;
                }
            }

            $totalFiltered = count($data);

            $json_data = array(
                "draw"            => intval($request->input('draw')), 
                "recordsTotal"    => intval($totalData), 
                "recordsFiltered" => intval($totalFiltered), 
                "data"            => $data
            );

            return response()->json($json_data);
        }
        catch(\Exception $e)
        {
        }  
    }

    public function consumirComunicacion(Request $request)
    {
        $response_estructure = new ResponseEstructure();
        $response_estructure->set_response(false);

        $id_usuario = $request->session()->get("id");

        $this->ws_afip = new WebserviceAfip($id_usuario);

        $idComunicacion = $request->input("idComunicacion");
        $incluirAdjuntos = $request->input("incluirAdjuntos");

        $parametros = array(
            "idComunicacion"=>$idComunicacion,
            "incluirAdjuntos"=>true
        );

        $resultado = $this->ws_afip->consumirComunicacion($parametros);

        $data = array();

        if(isset($resultado["ns1consumirComunicacionResponse"]["ns2Comunicacion"]))
        {
            $datos_comunicacion = $resultado["ns1consumirComunicacionResponse"]["ns2Comunicacion"];

            $data = array(
                "asunto"=>$datos_comunicacion["asunto"],
                "mensaje"=>$datos_comunicacion["mensaje"],
                "tiene_adjunto"=>false,
                "listado_adjuntos"=>array()
            );

            if($resultado["ns1consumirComunicacionResponse"]["ns2Comunicacion"]["tieneAdjunto"] == true)
            {
                $data["tiene_adjunto"] = TRUE;

                $ns4adjuntos = $resultado["ns1consumirComunicacionResponse"]["ns2Comunicacion"]["ns4adjuntos"];
            
                $listado_adjuntos = array();

                foreach($ns4adjuntos as $adjunto_row)
                {
                    $listado_adjuntos[] = $adjunto_row["filename"];
                }

                $data["listado_adjuntos"] = $listado_adjuntos;
            }

            $response_estructure->set_response(true);
        }

        $response_estructure->set_data($data);
        return response()->json($response_estructure->get_response_array());
    }
}

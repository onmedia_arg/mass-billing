<?php

namespace App\Http\Controllers\Backend\Desktop;

use Illuminate\Http\Request;
use App\Http\Controllers\Backend\Controller;
use Illuminate\Support\Facades\DB;

use App\Library\WebserviceAfip\WebserviceAfip;
use Illuminate\Support\Facades\Storage;

class DesktopController extends Controller
{
    protected $ws_afip = null;

    public function index(Request $request)
    {
        $id_configuracion_afip = $request->session()->get("id_configuracion_afip");
        
        $estado_certificado = false;
        $estado_de_servidor = null;
        $produccion = false;

        try
        {
            $this->ws_afip = new WebserviceAfip($id_configuracion_afip);
            $produccion = $this->ws_afip->isProduccion();

            $estado_de_servidor = $this->ws_afip->obtenerEstadoDeServidor();

            $this->ws_afip->getTiposDeConceptos();
            $estado_certificado = true;
        }catch(\Exception $e){
        }

        return View("backend.desktop.desktop")
        ->with("estado_de_servidor",$estado_de_servidor)
        ->with("estado_certificado",$estado_certificado)
        ->with("produccion",$produccion);
    }

    public function importacion(Request $request)
    {
        $contenido_archivo = file_get_contents(public_path('cuit.txt'));
        $contenido_archivo = explode("\n",$contenido_archivo);
        
        $this->ws_afip = new WebserviceAfip($request->session()->get("id"));

        for($i=1; $i < count($contenido_archivo);$i++)
        {
            $row = $contenido_archivo[$i];

            $cuit = trim($row);

            if($cuit != "")
            {
                $datos_persona = $this->ws_afip->get_datos_persona($cuit);

                //dd(property_exists($datos_persona, "errorConstancia"));

                $nombre = "";
                $apellido = "";
                $estadoClave = "";
                $idPersona = "";
                $mesCierre = "";
                $tipoClave = "";
                $tipoPersona = "";

                
                $dependencia_codPostal = "";
                $dependencia_descripcionDependencia = "";
                $dependencia_descripcionProvincia = "";
                $dependencia_direccion = "";
                $dependencia_idDependencia = "";
                $dependencia_idProvincia = "";
                $dependencia_localidad = "";

                $domicilioFiscal_codPostal = "";
                $domicilioFiscal_descripcionProvincia = "";
                $domicilioFiscal_direccion = "";
                $domicilioFiscal_idProvincia = "";
                $domicilioFiscal_localidad = "";
                $domicilioFiscal_tipoDomicilio = "";

                if($datos_persona)
                {
                    if(property_exists($datos_persona, "datosGenerales"))
                    {
                        if(property_exists($datos_persona->datosGenerales, "apellido")){$apellido = $datos_persona->datosGenerales->apellido;}

                        if(property_exists($datos_persona->datosGenerales, "estadoClave")){$estadoClave = $datos_persona->datosGenerales->estadoClave;}

                        if(property_exists($datos_persona->datosGenerales, "idPersona")){$idPersona = $datos_persona->datosGenerales->idPersona;}

                        if(property_exists($datos_persona->datosGenerales, "mesCierre")){$mesCierre = $datos_persona->datosGenerales->mesCierre;}

                        if(property_exists($datos_persona->datosGenerales, "nombre")){$nombre = $datos_persona->datosGenerales->nombre;}

                        if(property_exists($datos_persona->datosGenerales, "tipoClave")){$tipoClave = $datos_persona->datosGenerales->tipoClave;}

                        if(property_exists($datos_persona->datosGenerales, "tipoPersona")){$tipoPersona = $datos_persona->datosGenerales->tipoPersona;}

                        if(property_exists($datos_persona->datosGenerales, "dependencia"))
                        {
                            if(property_exists($datos_persona->datosGenerales->dependencia, "codPostal")){$dependencia_codPostal = $datos_persona->datosGenerales->dependencia->codPostal;}

                            if(property_exists($datos_persona->datosGenerales->dependencia, "descripcionDependencia")){$dependencia_descripcionDependencia = $datos_persona->datosGenerales->dependencia->descripcionDependencia;}
                        
                            if(property_exists($datos_persona->datosGenerales->dependencia, "descripcionProvincia")){$dependencia_descripcionProvincia = $datos_persona->datosGenerales->dependencia->descripcionProvincia;}
                
                            if(property_exists($datos_persona->datosGenerales->dependencia, "direccion")){$dependencia_direccion = $datos_persona->datosGenerales->dependencia->direccion;}
                        
                            if(property_exists($datos_persona->datosGenerales->dependencia, "idDependencia")){$dependencia_idDependencia = $datos_persona->datosGenerales->dependencia->idDependencia;}
                        
                            if(property_exists($datos_persona->datosGenerales->dependencia, "idProvincia")){$dependencia_idProvincia = $datos_persona->datosGenerales->dependencia->idProvincia;}
                            
                            if(property_exists($datos_persona->datosGenerales->dependencia, "localidad")){$dependencia_localidad = $datos_persona->datosGenerales->dependencia->localidad;}
                        }

                        if(property_exists($datos_persona->datosGenerales, "domicilioFiscal"))
                        {
                            if(property_exists($datos_persona->datosGenerales->domicilioFiscal, "codPostal")){$domicilioFiscal_codPostal = $datos_persona->datosGenerales->domicilioFiscal->codPostal;}
                            
                            if(property_exists($datos_persona->datosGenerales->domicilioFiscal, "descripcionProvincia")){$domicilioFiscal_descripcionProvincia = $datos_persona->datosGenerales->domicilioFiscal->descripcionProvincia;}
                            
                            if(property_exists($datos_persona->datosGenerales->domicilioFiscal, "direccion")){$domicilioFiscal_direccion = $datos_persona->datosGenerales->domicilioFiscal->direccion;}
                            
                            if(property_exists($datos_persona->datosGenerales->domicilioFiscal, "idProvincia")){$domicilioFiscal_idProvincia = $datos_persona->datosGenerales->domicilioFiscal->idProvincia;}
                            
                            if(property_exists($datos_persona->datosGenerales->domicilioFiscal, "localidad")){$domicilioFiscal_localidad = $datos_persona->datosGenerales->domicilioFiscal->localidad;}
                            
                            if(property_exists($datos_persona->datosGenerales->domicilioFiscal, "tipoDomicilio")){$domicilioFiscal_tipoDomicilio = $datos_persona->datosGenerales->domicilioFiscal->tipoDomicilio;}
                        }
                        
                    }
                }

                

                if($datos_persona)
                {
                    $informacion_cuit = new \App\InformacionCuits();
                    $informacion_cuit->cuit = $cuit;
                    $informacion_cuit->nombre = $nombre;
                    $informacion_cuit->apellido = $apellido;
                    $informacion_cuit->estadoClave = $estadoClave;
                    $informacion_cuit->idPersona = $idPersona;
                    $informacion_cuit->mesCierre = $mesCierre;
                    $informacion_cuit->tipoClave = $tipoClave;
                    $informacion_cuit->tipoPersona = $tipoPersona;
                    $informacion_cuit->dependencia_codPostal = $dependencia_codPostal;
                    $informacion_cuit->dependencia_descripcionDependencia = $dependencia_descripcionDependencia;
                    $informacion_cuit->dependencia_descripcionProvincia = $dependencia_descripcionProvincia;
                    $informacion_cuit->dependencia_direccion = $dependencia_direccion;
                    $informacion_cuit->dependencia_idDependencia = $dependencia_idDependencia;
                    $informacion_cuit->dependencia_idProvincia = $dependencia_idProvincia;
                    $informacion_cuit->dependencia_localidad = $dependencia_localidad;
                    $informacion_cuit->domicilioFiscal_codPostal = $domicilioFiscal_codPostal;
                    $informacion_cuit->domicilioFiscal_descripcionProvincia = $domicilioFiscal_descripcionProvincia;
                    $informacion_cuit->domicilioFiscal_direccion = $domicilioFiscal_direccion;
                    $informacion_cuit->domicilioFiscal_idProvincia = $domicilioFiscal_idProvincia;
                    $informacion_cuit->domicilioFiscal_localidad = $domicilioFiscal_localidad;
                    $informacion_cuit->domicilioFiscal_tipoDomicilio = $domicilioFiscal_tipoDomicilio;
                    $informacion_cuit->save();
                }
                
            }
            

            if($i == 20){die;}
        }
    }
}

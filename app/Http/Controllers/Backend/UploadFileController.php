<?php
namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Pion\Laravel\ChunkUpload\Exceptions\UploadMissingFileException;
use Pion\Laravel\ChunkUpload\Handler\AbstractHandler;
use Pion\Laravel\ChunkUpload\Handler\HandlerFactory;
use Pion\Laravel\ChunkUpload\Receiver\FileReceiver;

use App\Http\Controllers\Backend\Uploader\UploadController;

use Intervention\Image\Facades\Image;

use App\Usuario;

class UploadFileController extends UploadController
{
    /**
     * Handles the file upload
     *
     * @param FileReceiver $receiver
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @throws UploadMissingFileException
     *
     */
    /*
    public function upload_foto_perfil(Request $request,FileReceiver $receiver)
    {
        $this->folder_upload = "storage/imagenes/usuarios";

        if($this->validateImageFile())
        {
            if ($receiver->isUploaded() === false) {
                throw new UploadMissingFileException();
            }

            $save = $receiver->receive();

            if ($save->isFinished()) {

                $respuesta = $this->saveFile($save->getFile());

                $respuesta_array = $respuesta->getData(true);
                $realpath_image = $respuesta_array["path"]."/".$respuesta_array["name"];

                $image = Image::make($realpath_image);

                $image->resize(500,500,function($c){
                $c->aspectRatio();
                });

                $image->resizeCanvas(500, 500, 'center', false, 'ffffff');

                $image->save($realpath_image);

                $save = $request->input("save");

                return $respuesta;
            }

            $handler = $save->handler();

            return response()->json([
                "done" => $handler->getPercentageDone()
            ]);
        }
    }

    public function upload_imagen_producto(Request $request,FileReceiver $receiver)
    {
        $this->folder_upload = "storage/imagenes/productos";

        if($this->validateImageFile())
        {
            if ($receiver->isUploaded() === false) {
                throw new UploadMissingFileException();
            }

            $save = $receiver->receive();

            if ($save->isFinished()) {

                $respuesta = $this->saveFile($save->getFile());

                $respuesta_array = $respuesta->getData(true);
                $realpath_image = $respuesta_array["path"]."/".$respuesta_array["name"];

                $image = Image::make($realpath_image);

                $image->resize(500,500,function($c){
                    $c->aspectRatio();
                });

                $image->resizeCanvas(500, 500, 'center', false, 'ffffff');

                $image->save($realpath_image);

                $save = $request->input("save");

                return $respuesta;
            }

            $handler = $save->handler();

            return response()->json([
                "done" => $handler->getPercentageDone()
            ]);
        }
    }

    public function upload_imagen_categoria(Request $request,FileReceiver $receiver)
    {
        $this->folder_upload = "storage/imagenes/categorias_productos";

        if($this->validateImageFile())
        {
            if ($receiver->isUploaded() === false) {
                throw new UploadMissingFileException();
            }

            $save = $receiver->receive();

            if ($save->isFinished()) {

                $respuesta = $this->saveFile($save->getFile());

                $respuesta_array = $respuesta->getData(true);
                $realpath_image = $respuesta_array["path"]."/".$respuesta_array["name"];

                $image = Image::make($realpath_image);

                $image->resize(300,150,function($c){
                    $c->aspectRatio();
                });

                $image->resizeCanvas(300, 150, 'center', false, 'ffffff');

                $image->save($realpath_image);

                $save = $request->input("save");

                return $respuesta;
            }

            $handler = $save->handler();

            return response()->json([
                "done" => $handler->getPercentageDone()
            ]);
        }
    }

    public function upload_imagen_distribuidor(Request $request,FileReceiver $receiver)
    {
        $this->folder_upload = "storage/imagenes/distribuidores";

        if($this->validateImageFile())
        {
            if ($receiver->isUploaded() === false) {
                throw new UploadMissingFileException();
            }

            $save = $receiver->receive();

            if ($save->isFinished()) {

                $respuesta = $this->saveFile($save->getFile());

                $respuesta_array = $respuesta->getData(true);
                $realpath_image = $respuesta_array["path"]."/".$respuesta_array["name"];

                $image = Image::make($realpath_image);

                $image->resize(300,150,function($c){
                    $c->aspectRatio();
                });

                $image->resizeCanvas(300, 150, 'center', false, 'ffffff');

                $image->save($realpath_image);

                $save = $request->input("save");

                return $respuesta;
            }

            $handler = $save->handler();

            return response()->json([
                "done" => $handler->getPercentageDone()
            ]);
        }
    }
    */

    public function upload_file_excel_importaciones(Request $request,FileReceiver $receiver)
    {
        $this->folder_upload = "storage/upload_file_excel_importaciones";

        if ($receiver->isUploaded() === false) {
            throw new UploadMissingFileException();
        }

        $save = $receiver->receive();

        if ($save->isFinished()) {
            
            $respuesta = $this->saveFile($save->getFile());

            $respuesta_array = $respuesta->getData(true);
            $realpath_file = $respuesta_array["path"]."/".$respuesta_array["name"];

            $id_zona_importacion = $request->input("id_zona_importacion");
            $referencia_id = (int) $request->input('referencia_id');

            if($referencia_id == 0){ $referencia_id = null;}

            $importacion_obj = new \App\Importacion();
            $importacion_obj->archivo = $respuesta_array["name"];
            $importacion_obj->id_zona_importacion = $request->input("id_zona_importacion");
            $importacion_obj->ultimo_row_leido = 0;
            $importacion_obj->cant_total_rows = null;
            $importacion_obj->referencia_id = $referencia_id;
            $importacion_obj->save();

            $save = $request->input("save");
            
            return response()->json($respuesta_array);
        }

        $handler = $save->handler();

        return response()->json([
            "done" => $handler->getPercentageDone()
        ]);
        
    }

    private function validateImageFile()
    {
        $upload_permission = false;
        
        if(isset($_FILES["file"]["name"]))
        {
            $name_file = $_FILES["file"]["name"];

            if(strlen($name_file) > 4)
            {
                $formato = strtolower(substr($name_file,strlen($name_file)-4,4));
            
                if($formato == ".png" || $formato == ".jpg" || $formato == ".gif")
                {
                    $upload_permission = true;
                }
                else
                {
                    $formato = strtolower(substr($name_file,strlen($name_file)-5,5));
                    
                    if($formato == ".jpeg" || $formato == ".webp")
                    {
                        $upload_permission = true;
                    }
                }
            }
        }

        return $upload_permission;
    }

    private function validatePdfFile()
    {
        $upload_permission = false;
        
        if(isset($_FILES["file"]["name"]))
        {
            $name_file = $_FILES["file"]["name"];

            if(strlen($name_file) > 4)
            {
                $formato = strtolower(substr($name_file,strlen($name_file)-4,4));
            
                if($formato == ".pdf")
                {
                    $upload_permission = true;
                }
            }
        }

        return $upload_permission;
    }
}
<?php

namespace App\Http\Controllers\Backend\Afip;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Backend\ABM_Core;

use Illuminate\Support\Facades\Mail;
use App\Mail\EmailSender;

use App\Library\ResponseEstructure;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

use App\Library\WebserviceAfip\WebserviceAfip;

use App\TipoDocumento;

class AfipController extends ABM_Core
{
    protected $ws_afip = null;

    public function __construct()
    {
        $this->link_controlador = url('/backend/afip').'/';
        $this->carpeta_views = "backend.afip.";

        $this->entity ="AFIP";
        $this->title_page = "AFIP";

        $this->columns = [];

        $this->add_active = false;
        $this->edit_active = false;
        $this->delete_active = false;
        $this->is_ajax = false;
    }

    public function get_datos_persona(Request $request)
    {
        $id_configuracion_afip = $request->session()->get("id_configuracion_afip");

        $response_estructure = new ResponseEstructure();
        $response_estructure->set_response(true);

        $data = array(
            "domicilio_comercial"=> "",
            "razon_social" => "",
        );

        $numero_documento = $request->input("numero_documento");
        $id_tipo_documento = $request->input("id_tipo_documento");

        if($id_tipo_documento == TipoDocumento::$CUIT)
        {
            $numero_documento = ((double) str_replace("-","",$numero_documento));

            if(strlen($numero_documento) == 11)
            {
                $datos_persona = null;

                try
                {
                    $this->ws_afip = new WebserviceAfip($id_configuracion_afip);
                    $datos_persona = $this->ws_afip->get_datos_persona($numero_documento);

                    $direccion = "";
                    $localidad = "";
                    $razon_social = "";

                    if(property_exists($datos_persona,"datosGenerales"))
                    {
                        if(property_exists($datos_persona->datosGenerales,"domicilioFiscal"))
                        {
                            if(property_exists($datos_persona->datosGenerales->domicilioFiscal,"direccion"))
                            {
                                $direccion=$datos_persona->datosGenerales->domicilioFiscal->direccion;
                            }

                            if(property_exists($datos_persona->datosGenerales->domicilioFiscal,"localidad"))
                            {
                                $localidad=$datos_persona->datosGenerales->domicilioFiscal->localidad;
                            }
                        }

                        if(property_exists($datos_persona->datosGenerales,"razonSocial"))
                        {
                            $razon_social = $datos_persona->datosGenerales->razonSocial;
                        }
                        else
                        {
                            if(property_exists($datos_persona->datosGenerales,"nombre") 
                                && property_exists($datos_persona->datosGenerales,"apellido"))
                            {
                                $razon_social = $datos_persona->datosGenerales->apellido.' '.$datos_persona->datosGenerales->nombre;
                            }
                        }

                        $response_estructure->set_response(true);
                    }

                    $data["domicilio_comercial"] = $direccion.' - '.$localidad;
                    $data["razon_social"] = $razon_social;
                }
                catch(\Exception $e)
                {
                    $response_estructure->set_response(false);
                    $response_estructure->add_message_error("No se pudo conectar a AFIP para obtener los datos automaticamente, si sigue sucediendo contactese con nosotros");
                }
            }
        }

        $response_estructure->set_data($data);

        return response()->json($response_estructure->get_response_array());
    }

    public function errorCertificado(Request $request)
    {
        $this->share_parameters();

        if($request->session()->get("errorCertificado") == 1)
        {
            return View($this->carpeta_views."error_certificado");
        }
        else
        {
            return Redirect('/backend/desktop');
        }
    }
}

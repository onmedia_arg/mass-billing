<?php
namespace App\Http\Controllers\Backend\Usuarios;

use Illuminate\Http\Request;
use App\Http\Controllers\Backend\ABM_Core;

use App\Library\ResponseEstructure;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

use App\Usuario;
use App\Rol;
use App\EstadoUsuario;
use App\ConfiguracionAfip;

class UsuariosController extends ABM_Core
{
    public function __construct()
    {
        $this->link_controlador = url('/backend/usuarios').'/';
        $this->entity ="Usuario";
        $this->title_page = "Usuarios";

        $this->columns = ["Correo","Nombre","Apellido","Foto","Estado","Acciones"];

        $this->columns = array(
            ["name"=>"ID","reference"=>"usuarios.id"],
            ["name"=>"Correo","reference"=>"usuarios.correo"],
            ["name"=>"Nombre","reference"=>"usuarios.nombre"],
            ["name"=>"Apellido","reference"=>"usuarios.apellido"],
            ["name"=>"Foto","reference"=>"usuarios.foto_perfil"],
            ["name"=>"Estado","reference"=>"estados_usuarios.estado"],
            ["name"=>"Empresa","reference"=>DB::raw("CONCAT(configuraciones_afip.razon_social,' ',configuraciones_afip.cuit)")],
            ["name"=>"Acciones","reference"=>null]
        );

        $this->is_ajax = false;
    }
    
    public function index(Request $request)
    {
        $this->share_parameters();
        return View("backend.usuarios.browse");
    }

    public function get_listado_dt(Request $request)
    {
        $id_configuracion_afip = $request->session()->get("id_configuracion_afip");

        $consulta_orm_principal = DB::table("usuarios")
        ->join("roles","roles.id","=","usuarios.id_rol")
        ->join("estados_usuarios","estados_usuarios.id","=","usuarios.id_estado_usuario")
        ->join("configuraciones_afip","configuraciones_afip.id","=","usuarios.id_configuracion_afip")
        ->select(
            "usuarios.*",
            "estados_usuarios.estado as estados_usuarios_estado",
            "estados_usuarios.color as estados_usuarios_color",
            "configuraciones_afip.cuit as configuraciones_afip_cuit",
            "configuraciones_afip.razon_social as configuraciones_afip_razon_social"
        )
        //->where("id_configuracion_afip",$id_configuracion_afip)
        ->where("usuarios.eliminado",0);

    	$totalData = $consulta_orm_principal->count();
        
        $totalFiltered = $totalData;
        
        $search = $request->input("search");
        $start = $request->input('start');
        $length = $request->input('length');
        $order = $request->input('order');

        $resultado = array();

        if(!empty($search['value'])) 
        {
            $consulta_orm_principal = $consulta_orm_principal
            ->where(function($query) use($search){
                $query->where("usuarios.id","like","%".$search['value']."%")
                ->orWhere("usuarios.correo","like","%".$search['value']."%")
                ->orWhere("usuarios.nombre","like","%".$search['value']."%")
                ->orWhere("usuarios.apellido","like","%".$search['value']."%")
                ->orWhere("estados_usuarios.estado","like","%".$search['value']."%");
            });
        }

        $consulta_orm_principal = $consulta_orm_principal->take($length)->skip($start);
        $totalFiltered=$consulta_orm_principal->count();

        $columna_a_ordenar = (int)$order[0]["column"];

        if(isset($this->columns[$columna_a_ordenar]) && $this->columns[$columna_a_ordenar]["reference"] != null){
          $resultado = $consulta_orm_principal->orderBy($this->columns[$columna_a_ordenar]["reference"],$order[0]["dir"])->get();
        }
        else{
          $resultado = $consulta_orm_principal->orderBy("id","desc")->get();
        }
    	
        $data= array();
        
        foreach($resultado as $result_row)
        {
            $row_of_data = array();

            $row_of_data[]=strip_tags($result_row->id);
            $row_of_data[]=strip_tags($result_row->correo);
            $row_of_data[]=strip_tags(ucwords($result_row->nombre));
            $row_of_data[]=strip_tags(ucwords($result_row->apellido));

            $row_of_data[]="<img src='".asset('storage/imagenes/usuarios/'.strip_tags($result_row->foto_perfil))."' style='width: 50px;' />";

            $row_of_data[]='<span class="badge text-white" style="background-color: '.strip_tags($result_row->estados_usuarios_color).'">'.strip_tags($result_row->estados_usuarios_estado).'</span>';

            $row_of_data[] = strip_tags($result_row->configuraciones_afip_razon_social." ".$result_row->configuraciones_afip_cuit);

            $buttons_actions = "<div class='form-button-action'>";

            if($this->edit_active)
            {
                if($this->is_ajax)
                {
                    $buttons_actions .= 
                    "<button onclick='abrir_modal_editar(".$result_row->id.")' type='button' data-toggle='tooltip' title='' class='btn-sm ".$this->config_buttons["edit"]["class"]."' data-original-title='".$this->config_buttons["edit"]["title"]."'>
                        <i class='".$this->config_buttons["edit"]["icon"]."'></i>
                    </button>";
                }
                else{
                    $buttons_actions .= 
                    "<a href='".$this->link_controlador."editar/".$result_row->id."' data-toggle='tooltip' title='' class='btn-sm ".$this->config_buttons["edit"]["class"]."' data-original-title='".$this->config_buttons["edit"]["title"]."'>
                        <i class='".$this->config_buttons["edit"]["icon"]."'></i>
                    </a>";
                }
            }

            if($this->delete_active)
            {
                $buttons_actions.=
                "<button onclick='eliminar(".$result_row->id.")' type='button' data-toggle='tooltip' title='' class='btn-sm ".$this->config_buttons["delete"]["class"]."' data-original-title='".$this->config_buttons["delete"]["title"]."'>
                    <i class='".$this->config_buttons["delete"]["icon"]."'></i>
                </button>";
            }

            $buttons_actions.="</div>";

            
            $row_of_data[]=$buttons_actions;

            $data[]=$row_of_data;
        }

		$json_data = array(
            "draw"            => intval($request->input('draw')), 
            "recordsTotal"    => intval($totalData), 
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data
        );

    	return response()->json($json_data);
    }

    public function nuevo(Request $request)
    {
        $this->title_page = $this->config_buttons["add"]["title"]." ".$this->entity;
        $this->share_parameters();

        $estados_usuarios = EstadoUsuario::all();
        $configuraciones_afip = ConfiguracionAfip::orderBy("razon_social")
        ->orderBy("cuit")
        ->get();

        return View("backend.usuarios.add")
        ->with("estados_usuarios",$estados_usuarios)
        ->with("configuraciones_afip",$configuraciones_afip);
    }

    public function get(Request $request)
    {
        $response_estructure = new ResponseEstructure();
        $response_estructure->set_response(false);

        $id = $request->input("id");
        $id_configuracion_afip = $request->session()->get("id_configuracion_afip");

        $row_obj = Usuario::where("id",$id)
        //->where("id_configuracion_afip",$id_configuracion_afip)
        ->first();
        
        if($row_obj)
        {
            $response_estructure->set_response(true);
            $response_estructure->set_data($row_obj);
        }
        else
        {
            $response_estructure->set_response(false);
            $response_estructure->add_message_error($this->text_no_search);
        }
        

        return response()->json($response_estructure->get_response_array());
    }

    public function store(Request $request)
    {
        $response_estructure = new ResponseEstructure();
        $response_estructure->set_response(false);

        $correo = trim(strtolower($request->input("correo")));
        $usuario = trim(strtolower($request->input("usuario")));
        $nombre = trim(ucwords($request->input("nombre")));
        $apellido = trim(ucwords($request->input("apellido")));
        $password = trim($request->input("password"));
        $id_estado_usuario = $request->input("id_estado_usuario");
        $foto_perfil = trim($request->input("foto_perfil"));
        $id_configuracion_afip = $request->input("id_configuracion_afip");

        $input= [
            "correo"=>$correo,
            "usuario"=>$usuario,
            "nombre"=>$nombre,
            "apellido"=>$apellido,
            "contraseña"=>$password,
            "estado"=>$id_estado_usuario,
            "foto_de_perfil"=>$foto_perfil,
            "id_configuracion_afip" =>$id_configuracion_afip
        ];

        $rules = [
            "correo"=>"required|email|unique:usuarios,correo",
            "usuario"=>"required|unique:usuarios,usuario",
            "nombre"=>"required|min:3",
            "apellido"=>"required|min:3",
            "contraseña"=>"required|min:8",
            "estado"=>"required|numeric",
            "foto_de_perfil"=>"required",
            "id_configuracion_afip" =>"required|numeric"
        ];

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            $response_estructure->set_response(false);

            $errors = $validator->errors();

            foreach ($errors->all() as $error) {
                $response_estructure->add_message_error($error);
            }
        }
        else
        {
            $usuario_obj = new Usuario();

            $usuario_obj->correo = $correo;
            $usuario_obj->usuario = $usuario;
            $usuario_obj->nombre = $nombre;
            $usuario_obj->apellido = $apellido;
            $usuario_obj->password = bcrypt($password);
            $usuario_obj->id_estado_usuario = $id_estado_usuario;
            $usuario_obj->foto_perfil = $foto_perfil;
            $usuario_obj->id_configuracion_afip = $id_configuracion_afip;

            $usuario_obj->id_rol = Rol::ADMINISTRADOR;
            $usuario_obj->save();

            $response_estructure->set_response(true);
        }

        return response()->json($response_estructure->get_response_array());
    }

    public function editar(Request $request,$id)
    {
        $this->title_page = $this->config_buttons["edit"]["title"]." ".$this->entity;
        $this->share_parameters();

        $id_configuracion_afip = $request->session()->get("id_configuracion_afip");

        $row_obj = Usuario::where("id",$id)
        //->where("id_configuracion_afip",$id_configuracion_afip)
        ->first();

        if($row_obj && $row_obj->eliminado == false)
        {
            $estados_usuarios = EstadoUsuario::all();

            return View("backend.usuarios.edit")
            ->with("estados_usuarios",$estados_usuarios)
            ->with("row_obj",$row_obj);
        }
    }

    public function update(Request $request)
    {
        $response_estructure = new ResponseEstructure();
        $response_estructure->set_response(false);

        $id = $request->input("id");
        $id_configuracion_afip = $request->session()->get("id_configuracion_afip");

        $usuario_obj = Usuario::where("id",$id)
        //->where("id_configuracion_afip",$id_configuracion_afip)
        ->first();
        
        if($usuario_obj && $usuario_obj->eliminado == false)
        {
            $correo = trim(strtolower($request->input("correo")));
            $usuario = trim(strtolower($request->input("usuario")));
            $nombre = trim(ucwords($request->input("nombre")));
            $apellido = trim(ucwords($request->input("apellido")));
            $id_estado_usuario = $request->input("id_estado_usuario");
            $foto_perfil = trim($request->input("foto_perfil"));

            $password = trim($request->input("password_new"));
            $password2 = trim($request->input("password_new2"));
            
            $input= [
                "correo"=>$correo,
                "usuario"=>$usuario,
                "nombre"=>$nombre,
                "apellido"=>$apellido,
                "estado"=>$id_estado_usuario,
                "foto_de_perfil"=>$foto_perfil
            ];

            $rules = [
                "correo"=>"required|email|unique:usuarios,correo,".$id,
                "usuario"=>"required|unique:usuarios,usuario,".$id,
                "nombre"=>"required|min:3",
                "apellido"=>"required|min:3",
                "estado"=>"required|numeric", 
                "foto_de_perfil"=>"required", 
            ];

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $response_estructure->set_response(false);

                $errors = $validator->errors();

                foreach ($errors->all() as $error) {
                    $response_estructure->add_message_error($error);
                }
            }
            else
            {
                $response_estructure->set_response(true);

                
                if($password != "" || $password2 != "")
                {
                    if($password != $password2)
                    {
                        $response_estructure->set_response(false);
                        $response_estructure->add_message_error("Las contraseñas ingresadas no coinciden");
                    }
                    else
                    {
                        $validator = Validator::make(
                            ["contraseña"=>$password], 
                            ["contraseña"=>"required|min:8"]
                        );

                        if ($validator->fails()) {

                            $response_estructure->set_response(false);
                            
                            $errors = $validator->errors();

                            foreach ($errors->all() as $error) {
                                $response_estructure->add_message_error($error);
                            }
                        }

                    }
                }

                if($response_estructure->get_response() === TRUE)
                {
                    $usuario_obj->correo = $correo;
                    $usuario_obj->usuario = $usuario;
                    $usuario_obj->nombre = $nombre;
                    $usuario_obj->apellido = $apellido;

                    if($password != "")
                    {
                        $usuario_obj->password = bcrypt($password);
                    }
                    
                    $usuario_obj->id_estado_usuario = $id_estado_usuario;
                    $usuario_obj->foto_perfil = $foto_perfil;
                    $usuario_obj->save();
                }
            }
        }
        else
        {
            $response_estructure->set_response(false);
            $response_estructure->add_message_error("Usuario no encontrado");
        }
        
        return response()->json($response_estructure->get_response_array());
    }

    public function delete(Request $request)
    {
        $response_estructure = new ResponseEstructure();
        $response_estructure->set_response(false);

        $id = $request->input("id");
        $id_configuracion_afip = $request->session()->get("id_configuracion_afip");

        $row_obj = Usuario::where("id",$id)
        //->where("id_configuracion_afip",$id_configuracion_afip)
        ->first();
        
        if($row_obj)
        {
            $row_obj->eliminado = 1;
            $row_obj->save();

            $response_estructure->set_response(true);
        }
        else
        {
            $response_estructure->set_response(false);
            $response_estructure->add_message_error($this->text_no_search);
        }


        return response()->json($response_estructure->get_response_array());
    }

    public function search_for_cuit(Request $request)
    {
        $busqueda = $request->input("q");
    
        $usuarios = DB::table("usuarios")
        ->where(function($query) use($busqueda){
          $query->where('cuit',"like","%".$busqueda."%")
          ->orWhere('razon_social',"like","%".$busqueda."%")
          ->orWhere(DB::raw('CONCAT(nombre," ",apellido)'),"like","%".$busqueda."%")
          ->orWhere(DB::raw('CONCAT(apellido," ",nombre)'),"like","%".$busqueda."%");
        })
        ->groupBy("cuit")
        ->take(10)
        ->get();
    
        $respuesta = array();
    
        if($usuarios)
        {
            foreach($usuarios as $usuarios_row)
            {
                $razon_social = $usuarios_row->razon_social;

                if(empty(trim($razon_social)))
                {
                    $razon_social = $usuarios_row->apellido.' '.$usuarios_row->nombre;
                }

                $respuesta[]= [
                    "id" => $usuarios_row->cuit,
                    "text" => $usuarios_row->cuit.' '.$razon_social
                ];
            }
        }
    
        return response()->json($respuesta);
    }
}

<?php
namespace App\Http\Controllers\Backend\Usuarios;

use Illuminate\Http\Request;
use App\Http\Controllers\Backend\ABM_Core;

use App\Library\ResponseEstructure;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

use Response;

use App\Usuario;
use App\EstadoUsuario;
use App\CondicionIva;

class UsuariosComunesController extends ABM_Core
{
    public function __construct()
    {
        $this->link_controlador = url('/backend/usuarios_comunes').'/';
        $this->entity ="Usuario común";
        $this->title_page = "Usuarios comunes";

        $this->columns = array(
            ["name"=>"Correo","reference"=>"usuarios.correo"],
            ["name"=>"Nombre","reference"=>"usuarios.nombre"],
            ["name"=>"Apellido","reference"=>"usuarios.apellido"],
            ["name"=>"Foto","reference"=>"usuarios.foto_perfil"],
            ["name"=>"Estado","reference"=>"estados_usuarios.estado"],
            ["name"=>"Acciones","reference"=>null]
        );
        
        $this->is_ajax = false;
    }
    
    public function index(Request $request)
    {
        $this->validar_administrador();
        
        $this->share_parameters();
        return View("backend.usuarios_comunes.browse");
    }

    public function get_listado_dt(Request $request)
    {
        $this->validar_administrador();

        $consulta_orm_principal = DB::table("usuarios")
        ->join("roles","roles.id","=","usuarios.id_rol")
        ->join("estados_usuarios","estados_usuarios.id","=","usuarios.id_estado_usuario")
        ->select(
            "usuarios.*",
            "estados_usuarios.estado as estados_usuarios_estado",
            "estados_usuarios.color as estados_usuarios_color"
        )
        ->where("eliminado",0)
        ->where("id_rol",2);

        $totalData = $consulta_orm_principal->count();

        $totalFiltered = $totalData;

        $search = $request->input("search");
        $start = $request->input('start');
        $length = $request->input('length');
        $order = $request->input('order');

        $resultado = array();

        if(!empty($search['value']))
        {
            $consulta_orm_principal = $consulta_orm_principal
            ->where(function($query) use($search){
                $query->where("usuarios.correo","like","%".$search['value']."%")
                ->orWhere("usuarios.nombre","like","%".$search['value']."%")
                ->orWhere("usuarios.apellido","like","%".$search['value']."%")
                ->orWhere("estados_usuarios.estado","like","%".$search['value']."%");
            });
        }

        $consulta_orm_principal = $consulta_orm_principal->take($length)->skip($start);
        $totalFiltered=$consulta_orm_principal->count();

        $columna_a_ordenar = (int)$order[0]["column"];

        if(isset($this->columns[$columna_a_ordenar]) && $this->columns[$columna_a_ordenar]["reference"] != null){
          $resultado = $consulta_orm_principal->orderBy($this->columns[$columna_a_ordenar]["reference"],$order[0]["dir"])->get();
        }
        else{
          $resultado = $consulta_orm_principal->orderBy("id","desc")->get();
        }
    	
        $data= array();
        
        foreach($resultado as $result_row)
        {
            $row_of_data = array();

            $row_of_data[]=strip_tags($result_row->correo);
            $row_of_data[]=strip_tags(ucwords($result_row->nombre));
            $row_of_data[]=strip_tags(ucwords($result_row->apellido));

            $row_of_data[]="<img src='".asset('storage/imagenes/usuarios/'.strip_tags($result_row->foto_perfil))."' style='width: 50px;' />";

            $row_of_data[]='<span class="badge text-white" style="background-color: '.strip_tags($result_row->estados_usuarios_color).'">'.strip_tags($result_row->estados_usuarios_estado).'</span>';

            $buttons_actions = "<div class='form-button-action'>";

            if($this->edit_active)
            {
                if($this->is_ajax)
                {
                    $buttons_actions .= 
                    "<button onclick='abrir_modal_editar(".$result_row->id.")' type='button' data-toggle='tooltip' title='' class='btn-sm ".$this->config_buttons["edit"]["class"]."' data-original-title='".$this->config_buttons["edit"]["title"]."'>
                        <i class='".$this->config_buttons["edit"]["icon"]."'></i>
                    </button>";
                }
                else{
                    $buttons_actions .= 
                    "<a href='".$this->link_controlador."editar/".$result_row->id."' data-toggle='tooltip' title='' class='btn-sm ".$this->config_buttons["edit"]["class"]."' data-original-title='".$this->config_buttons["edit"]["title"]."'>
                        <i class='".$this->config_buttons["edit"]["icon"]."'></i>
                    </a>";
                }
            }

            if($this->delete_active)
            {
                $buttons_actions.=
                "<button onclick='eliminar(".$result_row->id.")' type='button' data-toggle='tooltip' title='' class='btn-sm ".$this->config_buttons["delete"]["class"]."' data-original-title='".$this->config_buttons["delete"]["title"]."'>
                    <i class='".$this->config_buttons["delete"]["icon"]."'></i>
                </button>";
            }

            $buttons_actions.="</div>";

            $row_of_data[]=$buttons_actions;

            $data[]=$row_of_data;
        }

		$json_data = array(
            "draw"            => intval($request->input('draw')), 
            "recordsTotal"    => intval($totalData), 
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data
        );

        return response()->json($json_data);
    }

    public function nuevo(Request $request)
    {
        $this->validar_administrador();

        $this->title_page = $this->config_buttons["add"]["title"]." ".$this->entity;
        $this->share_parameters();

        $estados_usuarios = EstadoUsuario::all();
        $condiciones_frente_al_iva = CondicionIva::all();

        return View("backend.usuarios_comunes.add")
        ->with("estados_usuarios",$estados_usuarios)
        ->with("condiciones_frente_al_iva",$condiciones_frente_al_iva);
    }

    public function get(Request $request)
    {
        $this->validar_administrador();

        $response_estructure = new ResponseEstructure();
        $response_estructure->set_response(false);

        $id = $request->input("id");

        $row_obj = Rol::find($id);
        
        if($row_obj)
        {
            $response_estructure->set_response(true);
            $response_estructure->set_data($row_obj);
        }
        else
        {
            $response_estructure->set_response(false);
            $response_estructure->add_message_error($this->text_no_search);
        }
        
        return response()->json($response_estructure->get_response_array());
    }

    public function store(Request $request)
    {
        $this->validar_administrador();

        $response_estructure = new ResponseEstructure();
        $response_estructure->set_response(false);

        $dni = trim(strtolower($request->input("dni")));
        $cuit = trim(strtolower($request->input("cuit")));
        $dni_parte_delantera = trim(strtolower($request->input("dni_parte_delantera")));
        $dni_parte_trasera = trim(strtolower($request->input("dni_parte_trasera")));

        $correo = trim(strtolower($request->input("correo")));
        $usuario = trim(strtolower($request->input("usuario")));
        $nombre = trim(ucwords($request->input("nombre")));
        $apellido = trim(ucwords($request->input("apellido")));
        $password = trim($request->input("password"));
        $id_estado_usuario = $request->input("id_estado_usuario");
        $id_condicion_iva = $request->input("id_condicion_iva");
        $razon_social = trim($request->input("razon_social"));
        $ingresos_brutos = $request->input("ingresos_brutos");
        $inicio_actividades = $request->input("inicio_actividades");
        $foto_perfil = trim($request->input("foto_perfil"));

        $pais = trim($request->input("pais"));
        $provincia = trim($request->input("provincia"));
        $ciudad = trim($request->input("ciudad"));
        $domicilio = trim($request->input("domicilio"));
        $telefono = (int) $request->input("telefono");
        $caracteristica_telefono = (int) $request->input("caracteristica_telefono");

        $domicilio_comercial = trim($request->input("domicilio_comercial"));
        $nro_inscripcion_municipal = trim($request->input("nro_inscripcion_municipal"));
        $actividad = trim($request->input("actividad"));
        $trabaja_en_relacion_dependencia = trim($request->input("trabaja_en_relacion_dependencia"));
        $aporta_caja_previsional = trim($request->input("aporta_caja_previsional"));
        $jubilado = trim($request->input("jubilado"));
        $facturacion_anual_estimada = trim($request->input("facturacion_anual_estimada"));
        $local_comercial = trim($request->input("local_comercial"));
        $metros_cuadrados_local = trim($request->input("metros_cuadrados_local"));
        $monto_alquileres = trim($request->input("monto_alquileres"));
        $kw_consumidos = trim($request->input("kw_consumidos"));
        $obra_social = trim($request->input("obra_social"));

        $tus_impuestos = trim($request->input("tus_impuestos"));
        $margen_facturacion_categoria = trim($request->input("margen_facturacion_categoria"));

        $input= [
            "dni"=>$dni,
            "cuit"=>$cuit,
            "dni_parte_delantera"=>$dni_parte_delantera,
            "dni_parte_trasera"=>$dni_parte_trasera,
            
            "correo"=>$correo,
            "usuario"=>$usuario,
            "nombre"=>$nombre,
            "apellido"=>$apellido,
            "password"=>$password,
            "id_estado_usuario"=>$id_estado_usuario,
            "id_condicion_iva"=>$id_condicion_iva,
            "razon_social"=>$razon_social,
            "ingresos_brutos"=>$ingresos_brutos,
            "inicio_actividades"=>$inicio_actividades,
            "foto_perfil"=>$foto_perfil,

            "pais"=>$pais, 
            "provincia"=>$provincia, 
            "ciudad"=>$ciudad, 
            "domicilio"=>$domicilio,
            "telefono"=>$telefono, 
            "caracteristica_telefono"=>$caracteristica_telefono, 

            "domicilio_comercial"=>$domicilio_comercial,
            "nro_inscripcion_municipal"=>$nro_inscripcion_municipal,
            "actividad"=>$actividad,
            "trabaja_en_relacion_dependencia"=>$trabaja_en_relacion_dependencia,
            "aporta_caja_previsional"=>$aporta_caja_previsional,
            "jubilado"=>$jubilado,
            "facturacion_anual_estimada"=>$facturacion_anual_estimada,
            "local_comercial"=>$local_comercial,
            "metros_cuadrados_local"=>$metros_cuadrados_local,
            "monto_alquileres"=>$monto_alquileres,
            "kw_consumidos"=>$kw_consumidos,
            "obra_social"=>$obra_social
        ];

        $rules = [
            "dni"=>"required|numeric|min:10000000|max:99999999|unique:usuarios,dni",
            "cuit"=>"required|numeric|min:10000000000|max:99999999999|unique:usuarios,cuit",
            "dni_parte_delantera"=>"required",
            "dni_parte_trasera"=>"required",

            "correo"=>"required|email|unique:usuarios,correo",
            "usuario"=>"required|unique:usuarios,usuario",
            "nombre"=>"required|min:3",
            "apellido"=>"required|min:3",
            "password"=>"required|min:8",
            "id_estado_usuario"=>"required|numeric",
            "id_condicion_iva"=>"required|numeric",
            "razon_social"=>"required|min:3",
            "ingresos_brutos"=>"required|min:3",
            "inicio_actividades"=>"required|required|date_format:d/m/Y",

            "pais"=>"required|min:3",
            "provincia"=>"required|min:3",
            "ciudad"=>"required|min:3",
            "domicilio"=>"required|min:3",
            "telefono"=>"required|numeric|min:100000|max:99999999",
            "caracteristica_telefono"=>"required|numeric|min:1|max:9999",
            "foto_perfil"=>"required",
            "trabaja_en_relacion_dependencia"=>"required",
            "aporta_caja_previsional"=>"required",
            "jubilado"=>"required",
            "local_comercial"=>"required",
        ];

        if($margen_facturacion_categoria != "")
        {
            $input["margen_facturacion_categoria"] = $margen_facturacion_categoria;
            $rules["margen_facturacion_categoria"] = "required|regex:/^\d+(\.\d{1,2})?$/";
        }
        else
        {
            $margen_facturacion_categoria = null;
        }

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            $response_estructure->set_response(false);

            $errors = $validator->errors();

            foreach ($errors->all() as $error) {
                $response_estructure->add_message_error($error);
            }
        }
        else
        {
            $inicio_actividades = \DateTime::createFromFormat("d/m/Y",$inicio_actividades);
            $inicio_actividades = $inicio_actividades->format("Y-m-d");
            
            $usuario_obj = new Usuario();

            $usuario_obj->dni = $dni;
            $usuario_obj->cuit = $cuit;
            $usuario_obj->dni_parte_delantera = $dni_parte_delantera;
            $usuario_obj->dni_parte_trasera = $dni_parte_trasera;
            
            $usuario_obj->correo = $correo;
            $usuario_obj->usuario = $usuario;
            $usuario_obj->nombre = $nombre;
            $usuario_obj->apellido = $apellido;
            $usuario_obj->password = bcrypt($password);
            $usuario_obj->id_estado_usuario = $id_estado_usuario;
            $usuario_obj->id_condicion_iva = $id_condicion_iva;
            $usuario_obj->razon_social = $razon_social;
            $usuario_obj->ingresos_brutos = $ingresos_brutos;
            $usuario_obj->inicio_actividades = $inicio_actividades;
            $usuario_obj->foto_perfil = $foto_perfil;

            $usuario_obj->pais = $pais;
            $usuario_obj->provincia = $provincia;
            $usuario_obj->ciudad = $ciudad;
            $usuario_obj->domicilio = $domicilio;
            $usuario_obj->telefono = $telefono;
            $usuario_obj->caracteristica_telefono = $caracteristica_telefono;

            $usuario_obj->domicilio_comercial = $domicilio_comercial;
            $usuario_obj->nro_inscripcion_municipal = $nro_inscripcion_municipal;
            $usuario_obj->actividad = $actividad;
            $usuario_obj->trabaja_en_relacion_dependencia = $trabaja_en_relacion_dependencia;
            $usuario_obj->aporta_caja_previsional = $aporta_caja_previsional;
            $usuario_obj->jubilado = $jubilado;
            $usuario_obj->facturacion_anual_estimada = $facturacion_anual_estimada;
            $usuario_obj->local_comercial = $local_comercial;
            $usuario_obj->metros_cuadrados_local = $metros_cuadrados_local;
            $usuario_obj->monto_alquileres = $monto_alquileres;
            $usuario_obj->kw_consumidos = $kw_consumidos;
            $usuario_obj->obra_social = $obra_social;

            if(trim($tus_impuestos) == "")
            {
                $usuario_obj->tus_impuestos = null;
            }
            else
            {
                $usuario_obj->tus_impuestos = $tus_impuestos;
            }

            $usuario_obj->margen_facturacion_categoria = $margen_facturacion_categoria;

            $usuario_obj->id_rol = 2; // CLIENTE
            $usuario_obj->save();
            
            $usuario_obj->generarCsrParaAfip();
            $response_estructure->set_response(true);
        }

        return response()->json($response_estructure->get_response_array());
    }

    public function editar(Request $request,$id)
    {
        $this->validar_administrador();

        $this->title_page = $this->config_buttons["edit"]["title"]." ".$this->entity;
        $this->share_parameters();

        $row_obj = Usuario::find($id);

        if($row_obj && $row_obj->eliminado == false)
        {
            $row_obj->inicio_actividades = \DateTime::createFromFormat("Y-m-d",$row_obj->inicio_actividades);
            $row_obj->inicio_actividades = $row_obj->inicio_actividades->format("d/m/Y");

            if(trim($row_obj->vencimiento_certificado) != "")
            {
                $row_obj->vencimiento_certificado = \DateTime::createFromFormat("Y-m-d",$row_obj->vencimiento_certificado);
                $row_obj->vencimiento_certificado = $row_obj->vencimiento_certificado->format("d/m/Y");
            }

            $estados_usuarios = EstadoUsuario::all();
            $condiciones_frente_al_iva = CondicionIva::all();

            return View("backend.usuarios_comunes.edit")
            ->with("estados_usuarios",$estados_usuarios)
            ->with("row_obj",$row_obj)
            ->with("condiciones_frente_al_iva",$condiciones_frente_al_iva);
        }
    }

    public function update(Request $request)
    {
        $this->validar_administrador();

        $response_estructure = new ResponseEstructure();
        $response_estructure->set_response(false);

        $id = $request->input("id");

        $usuario_obj = Usuario::find($id);
        
        if($usuario_obj && $usuario_obj->eliminado == false)
        {
            $dni = trim(strtolower($request->input("dni")));
            $cuit = trim(strtolower($request->input("cuit")));
            $dni_parte_delantera = trim(strtolower($request->input("dni_parte_delantera")));
            $dni_parte_trasera = trim(strtolower($request->input("dni_parte_trasera")));

            $correo = trim(strtolower($request->input("correo")));
            $usuario = trim(strtolower($request->input("usuario")));
            $nombre = trim(ucwords($request->input("nombre")));
            $apellido = trim(ucwords($request->input("apellido")));
            $id_estado_usuario = $request->input("id_estado_usuario");
            $id_condicion_iva = $request->input("id_condicion_iva");
            $razon_social = trim($request->input("razon_social"));
            $ingresos_brutos = $request->input("ingresos_brutos");
            $inicio_actividades = $request->input("inicio_actividades");
            $foto_perfil = trim($request->input("foto_perfil"));

            $password = trim($request->input("password_new"));
            $password2 = trim($request->input("password_new2"));

            $pais = trim($request->input("pais"));
            $provincia = trim($request->input("provincia"));
            $ciudad = trim($request->input("ciudad"));
            $domicilio = trim($request->input("domicilio"));
            $telefono = (int) $request->input("telefono");
            $caracteristica_telefono = (int) $request->input("caracteristica_telefono");

            
            //$csr_ws_afip = trim(strtolower($request->input("csr_ws_afip")));
            $csr_ws_de_afip = $request->input("csr_ws_de_afip");
            $vencimiento_certificado = trim($request->input("vencimiento_certificado")); 

            $domicilio_comercial = trim($request->input("domicilio_comercial"));
            $nro_inscripcion_municipal = trim($request->input("nro_inscripcion_municipal"));
            $actividad = trim($request->input("actividad"));
            $trabaja_en_relacion_dependencia = trim($request->input("trabaja_en_relacion_dependencia"));
            $aporta_caja_previsional = trim($request->input("aporta_caja_previsional"));
            $jubilado = trim($request->input("jubilado"));
            $facturacion_anual_estimada = trim($request->input("facturacion_anual_estimada"));
            $local_comercial = trim($request->input("local_comercial"));
            $metros_cuadrados_local = trim($request->input("metros_cuadrados_local"));
            $monto_alquileres = trim($request->input("monto_alquileres"));
            $kw_consumidos = trim($request->input("kw_consumidos"));
            $obra_social = trim($request->input("obra_social"));

            $tus_impuestos = trim($request->input("tus_impuestos"));
            $margen_facturacion_categoria = trim($request->input("margen_facturacion_categoria"));

            $input= [
                "dni"=>$dni,
                "cuit"=>$cuit,
                "dni_parte_delantera"=>$dni_parte_delantera,
                "dni_parte_trasera"=>$dni_parte_trasera,
                "correo"=>$correo,
                "usuario"=>$usuario,
                "nombre"=>$nombre,
                "apellido"=>$apellido,
                "id_estado_usuario"=>$id_estado_usuario,
                "id_condicion_iva"=>$id_condicion_iva,
                "razon_social"=>$razon_social,
                "ingresos_brutos"=>$ingresos_brutos,
                "inicio_actividades"=>$inicio_actividades,
                "domicilio"=>$domicilio,
                "foto_perfil"=>$foto_perfil, 

                "pais"=>$pais, 
                "provincia"=>$provincia, 
                "ciudad"=>$ciudad, 
                "telefono"=>$telefono, 
                "caracteristica_telefono"=>$caracteristica_telefono, 

                "domicilio_comercial"=>$domicilio_comercial,
                "nro_inscripcion_municipal"=>$nro_inscripcion_municipal,
                "actividad"=>$actividad,
                "trabaja_en_relacion_dependencia"=>$trabaja_en_relacion_dependencia,
                "aporta_caja_previsional"=>$aporta_caja_previsional,
                "jubilado"=>$jubilado,
                "facturacion_anual_estimada"=>$facturacion_anual_estimada,
                "local_comercial"=>$local_comercial,
                "metros_cuadrados_local"=>$metros_cuadrados_local,
                "monto_alquileres"=>$monto_alquileres,
                "kw_consumidos"=>$kw_consumidos,
                "obra_social"=>$obra_social
            ];

            $rules = [
                "dni"=>"required|numeric|min:10000000|max:99999999|unique:usuarios,dni,".$id,
                "cuit"=>"required|numeric|min:10000000000|max:99999999999|unique:usuarios,cuit,".$id,
                "dni_parte_delantera"=>"required",
                "dni_parte_trasera"=>"required",

                "correo"=>"required|email|unique:usuarios,correo,".$id,
                "usuario"=>"required|unique:usuarios,usuario,".$id,
                "nombre"=>"required|min:3",
                "apellido"=>"required|min:3",
                "id_estado_usuario"=>"required|numeric", 
                "id_condicion_iva"=>"required|numeric", 
                "razon_social"=>"required|min:3",
                "ingresos_brutos"=>"required|min:3",
                "inicio_actividades"=>"required|required|date_format:d/m/Y",
                "domicilio"=>"required|min:3",
                
                "pais"=>"required|min:3",
                "provincia"=>"required|min:3",
                "ciudad"=>"required|min:3",
                "telefono"=>"required|numeric|min:100000|max:99999999",
                "caracteristica_telefono"=>"required|numeric|min:1|max:9999",
                "trabaja_en_relacion_dependencia"=>"required",
                "aporta_caja_previsional"=>"required",
                "jubilado"=>"required",
                "local_comercial"=>"required",
            ];

            if($vencimiento_certificado != "")
            {
                $input["vencimiento_certificado"] = $vencimiento_certificado;
                $rules["vencimiento_certificado"] = "required|date_format:d/m/Y";
            }

            if($margen_facturacion_categoria != "")
            {
                $input["margen_facturacion_categoria"] = $margen_facturacion_categoria;
                $rules["margen_facturacion_categoria"] = "required|regex:/^\d+(\.\d{1,2})?$/";
            }
            else
            {
                $margen_facturacion_categoria = null;
            }

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $response_estructure->set_response(false);

                $errors = $validator->errors();

                foreach ($errors->all() as $error) {
                    $response_estructure->add_message_error($error);
                }
            }
            else
            {
                $response_estructure->set_response(true);

                
                if($password != "" || $password2 != "")
                {
                    if($password != $password2)
                    {
                        $response_estructure->set_response(false);
                        $response_estructure->add_message_error("Las contraseñas ingresadas no coinciden");
                    }
                    else
                    {
                        $validator = Validator::make(
                            ["password"=>$password], 
                            ["password"=>"required|min:8"]
                        );

                        if ($validator->fails()) {

                            $response_estructure->set_response(false);
                            
                            $errors = $validator->errors();

                            foreach ($errors->all() as $error) {
                                $response_estructure->add_message_error($error);
                            }
                        }

                    }
                }

                if($response_estructure->get_response() === TRUE)
                {
                    $inicio_actividades = \DateTime::createFromFormat("d/m/Y",$inicio_actividades);
                    $inicio_actividades = $inicio_actividades->format("Y-m-d");

                    $cambio_de_cuit = false;

                    if(trim($usuario_obj->cuit) != trim($cuit))
                    {
                        $cambio_de_cuit = true;
                    }

                    $usuario_obj->dni = $dni;
                    $usuario_obj->cuit = $cuit;
                    $usuario_obj->dni_parte_delantera = $dni_parte_delantera;
                    $usuario_obj->dni_parte_trasera = $dni_parte_trasera;
                    $usuario_obj->foto_perfil = $foto_perfil;

                    $usuario_obj->correo = $correo;
                    $usuario_obj->usuario = $usuario;
                    $usuario_obj->nombre = $nombre;
                    $usuario_obj->apellido = $apellido;

                    $usuario_obj->pais = $pais;
                    $usuario_obj->provincia = $provincia;
                    $usuario_obj->ciudad = $ciudad;
                    $usuario_obj->telefono = $telefono;
                    $usuario_obj->caracteristica_telefono = $caracteristica_telefono;

                    //$usuario_obj->csr_ws_afip = $csr_ws_afip;
                    $usuario_obj->csr_ws_de_afip = $csr_ws_de_afip;

                    if(trim($vencimiento_certificado))
                    {
                        $vencimiento_certificado = \DateTime::createFromFormat("d/m/Y",$vencimiento_certificado);
                        $vencimiento_certificado = $vencimiento_certificado->format("Y-m-d");

                        $usuario_obj->vencimiento_certificado = $vencimiento_certificado;
                    }

                    $usuario_obj->id_rol = 2; //cliente

                    if($password != "")
                    {
                        $usuario_obj->password = bcrypt($password);
                    }
                    
                    $usuario_obj->id_estado_usuario = $id_estado_usuario;
                    $usuario_obj->id_condicion_iva = $id_condicion_iva;
                    $usuario_obj->razon_social = $razon_social;
                    $usuario_obj->ingresos_brutos = $ingresos_brutos;
                    $usuario_obj->inicio_actividades = $inicio_actividades;
                    $usuario_obj->domicilio = $domicilio;

                    $usuario_obj->domicilio_comercial = $domicilio_comercial;
                    $usuario_obj->nro_inscripcion_municipal = $nro_inscripcion_municipal;
                    $usuario_obj->actividad = $actividad;
                    $usuario_obj->trabaja_en_relacion_dependencia = $trabaja_en_relacion_dependencia;
                    $usuario_obj->aporta_caja_previsional = $aporta_caja_previsional;
                    $usuario_obj->jubilado = $jubilado;
                    $usuario_obj->facturacion_anual_estimada = $facturacion_anual_estimada;
                    $usuario_obj->local_comercial = $local_comercial;
                    $usuario_obj->metros_cuadrados_local = $metros_cuadrados_local;
                    $usuario_obj->monto_alquileres = $monto_alquileres;
                    $usuario_obj->kw_consumidos = $kw_consumidos;
                    $usuario_obj->obra_social = $obra_social;

                    if(trim($tus_impuestos) == "")
                    {
                        $usuario_obj->tus_impuestos = null;
                    }
                    else
                    {
                        $usuario_obj->tus_impuestos = $tus_impuestos;
                    }
                    
                    $usuario_obj->margen_facturacion_categoria = $margen_facturacion_categoria;

                    $usuario_obj->save();

                    if($cambio_de_cuit)
                    {
                        $usuario_obj->generarCsrParaAfip();
                    }
                }
            }
        }
        else
        {
            $response_estructure->set_response(false);
            $response_estructure->add_message_error("Usuario no encontrado");
        }
        
        return response()->json($response_estructure->get_response_array());
    }

    public function delete(Request $request)
    {
        $this->validar_administrador();
        
        $response_estructure = new ResponseEstructure();
        $response_estructure->set_response(false);

        $id = $request->input("id");

        $row_obj = Usuario::find($id);
        
        if($row_obj)
        {
            $row_obj->eliminado = 1;
            $row_obj->save();
            $response_estructure->set_response(true);
        }
        else
        {
            $response_estructure->set_response(false);
            $response_estructure->add_message_error($this->text_no_search);
        }


        return response()->json($response_estructure->get_response_array());
    }

    public function descargarCsrParaAfip(Request $request)
    {
        $id_usuario = $request->input("id_usuario");

        $usuario_obj = Usuario::find($id_usuario);

        if($usuario_obj)
        {
            $fileName = "pedido_usuario_".$id_usuario."_csr";
    
            $content = $usuario_obj->csr_ws_afip;
    
            // use headers in order to generate the download
            $headers = [
            'Content-type' => 'text/plain', 
            'Content-Disposition' => sprintf('attachment; filename="%s"', $fileName),
            'Content-Length' => strlen($content)
            ];
    
            // make a response, with the content, a 200 response code and the headers
            return Response::make($content, 200, $headers);
        }
    }
}

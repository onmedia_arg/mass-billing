<?php
namespace App\Http\Controllers\Backend\Comprobantes;

use Illuminate\Http\Request;
use App\Http\Controllers\Backend\ABM_Core;

use App\Library\ResponseEstructure;
use App\Library\WebserviceAfip\WebserviceAfip;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

use App\PuntoDeVenta;

class MisPuntosDeVentaController extends ABM_Core
{
    public function __construct()
    {
        $this->link_controlador = url('/backend/comprobantes/mis_puntos_de_venta').'/';
        $this->entity ="Mi Punto de venta";
        $this->title_page = "Mis Puntos de Ventas";
        $this->columns = ["Número","Tipo de emisión","Bloqueado","Fecha de Baja","Acciones"];
        $this->is_ajax = false;
        
        $this->add_active = false;
    }
    
    public function index(Request $request)
    {
        try
        {
            $id_configuracion_afip = $request->session()->get("id_configuracion_afip");
    
            $this->ws_afip = new WebserviceAfip($id_configuracion_afip);
            $puntos_de_venta_db = $this->ws_afip->ConsultarPuntosDeVentas();
            
            $this->share_parameters();
    
            return View("backend.comprobantes.mis_puntos_de_venta.browse")
            ->with("puntos_de_venta_db",$puntos_de_venta_db);
        }
        catch(\Exception $e)
        {
            $request->session()->flash("errorCertificado",1);

            return Redirect('/backend/afip/errorCertificado');
        }
    }

    public function get(Request $request)
    {
        $response_estructure = new ResponseEstructure();
        $response_estructure->set_response(false);

        $id = $request->input("id");
        $id_configuracion_afip = $request->session()->get("id_configuracion_afip");

        $row_obj = PuntoDeVenta::where("id",$id)->where("id_configuracion_afip",$id_configuracion_afip)->first();
        
        if($row_obj)
        {
            $response_estructure->set_response(true);
            $response_estructure->set_data($row_obj);
        }
        else
        {
            $response_estructure->set_response(false);
            $response_estructure->add_message_error($this->text_no_search);
        }

        return response()->json($response_estructure->get_response_array());
    }

    public function update(Request $request)
    {
        $response_estructure = new ResponseEstructure();
        $response_estructure->set_response(false);

        $id = $request->input("id");
        $id_configuracion_afip = $request->session()->get("id_configuracion_afip");

        $row_obj = PuntoDeVenta::where("id",$id)->where("id_configuracion_afip",$id_configuracion_afip)->first();
        
        if($row_obj)
        {
            $descripcion = trim($request->input("descripcion"));

            $input= [
                "descripcion"=>$descripcion
            ];

            $rules = [
                "descripcion"=>"required"
            ];

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $response_estructure->set_response(false);

                $errors = $validator->errors();

                foreach ($errors->all() as $error) {
                    $response_estructure->add_message_error($error);
                }
            }
            else
            {
                $row_obj->descripcion = $descripcion;
                $row_obj->save();

                $response_estructure->set_response(true);
            }
        }
        else
        {
            $response_estructure->set_response(false);
            $response_estructure->add_message_error("Usuario no encontrado");
        }
        
        return response()->json($response_estructure->get_response_array());
    }
}

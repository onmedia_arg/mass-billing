<?php
namespace App\Http\Controllers\Backend\Comprobantes;

use Illuminate\Http\Request;
use App\Http\Controllers\Backend\ABM_Core;

use Illuminate\Support\Facades\Mail;
use App\Mail\EmailSender;

use App\Library\ResponseEstructure;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

use Mpdf\Mpdf;
use DNS1D;

use App\ConfiguracionAfip;
use App\Comprobante;
use App\EstadoComprobante;
use App\DetalleComprobante;
use App\ComprobanteAsociado;
use App\TributoComprobante;
use App\TipoTributo;
use App\TipoDeComprobante;

use App\Library\WebserviceAfip\WebserviceAfip;

use App\Concepto;
use App\UnidadDeMedida;
use App\CondicionIva;
use App\CondicionDeVenta;

use App\ProductoServicio;

use App\PuntoDeVenta;

class ComprobantesController extends ABM_Core
{
    protected $ws_afip = null;

    public function __construct()
    {
        $this->link_controlador = url('/backend/comprobantes').'/';
        $this->carpeta_views = "backend.comprobantes.";

        $this->entity ="Comprobante";
        $this->title_page = "Comprobantes";

        $this->columns = [
          ["name"=>"#","reference"=>"comprobantes.id"],
          ["name"=>"Tipo","reference"=>"tipos_de_comprobantes.Desc"],
          ["name"=>"Número","reference"=>"comprobantes.numero_afip"],
          ["name"=>"Fecha","reference"=>DB::raw("DATE_FORMAT(comprobantes.created_at,'%d/%m%Y %H:%i')")],
          ["name"=>"Estado","reference"=>"estado_comprobante.estado"],
          ["name"=>"Acciones","reference"=>null]
        ];

        $this->edit_active = false;
        $this->delete_active = false;
        $this->is_ajax = false;
    }

    public function index(Request $request)
    {
        $this->share_parameters();

        $ver = $request->input("ver");

        return View($this->carpeta_views."browse")
        ->with("ver",$ver); 
    }

    public function get_listado_dt(Request $request)
    {
        $consulta_orm_principal = DB::table("comprobantes")
        ->join("tipos_de_comprobantes","tipos_de_comprobantes.Id","=","comprobantes.id_tipo_de_comprobante")
        ->join("estado_comprobante","estado_comprobante.id","=","comprobantes.id_estado")
        ->select(
            "comprobantes.*",
            "tipos_de_comprobantes.Desc as tipos_de_comprobantes_Desc",
            "estado_comprobante.id as estado_comprobante_id",
            "estado_comprobante.color as estado_comprobante_color",
            "estado_comprobante.estado as estado_comprobante_estado"
        )
        ->where("id_configuracion_afip",$request->session()->get("id_configuracion_afip"));

    	$totalData = $consulta_orm_principal->count();

        $totalFiltered = $totalData;

        $search = $request->input("search");
        $start = $request->input('start');
        $length = $request->input('length');
        $order = $request->input('order');

        $resultado = array();

        if(!empty($search['value']))
        {
            $consulta_orm_principal = $consulta_orm_principal
            ->where(function($query) use($search){
                $query->where("comprobantes.id","like","%".$search['value']."%");
            });
        }

        $consulta_orm_principal = $consulta_orm_principal->take($length)->skip($start);
        $totalFiltered=$consulta_orm_principal->count();

        $columna_a_ordenar = (int)$order[0]["column"];

        if(isset($this->columns[$columna_a_ordenar]) && $this->columns[$columna_a_ordenar]["reference"] != null){
          $resultado = $consulta_orm_principal->orderBy($this->columns[$columna_a_ordenar]["reference"],$order[0]["dir"])->get();
        }
        else{
          $resultado = $consulta_orm_principal->orderBy("id","desc")->get();
        }

        $data= array();

        foreach($resultado as $result_row)
        {
            $row_of_data = array();

            $row_of_data[]=strip_tags($result_row->id);
            $row_of_data[]=strip_tags($result_row->tipos_de_comprobantes_Desc);
            $row_of_data[]=strip_tags($result_row->numero_afip);

            $result_row->created_at = \DateTime::createFromFormat("Y-m-d H:i:s",$result_row->created_at);

            $row_of_data[]=strip_tags($result_row->created_at->format("d/m/Y H:s"));

            $row_of_data[]='<span class="badge text-white" style="background-color: '.strip_tags($result_row->estado_comprobante_color).'">'.strip_tags($result_row->estado_comprobante_estado).'</span>';

            $buttons_actions = "<div class='form-button-action'>";

            if($result_row->id_estado == EstadoComprobante::REALIZADO)
            {
                $buttons_actions.= 
                "<a target='_blank' href='".$this->link_controlador."generacion_pdf/".$result_row->id."' data-toggle='tooltip' title='' class='btn-sm ".$this->config_buttons["watch"]["class"]."' data-original-title='".$this->config_buttons["watch"]["title"]."'>
                    <i class='".$this->config_buttons["watch"]["icon"]."'></i> VER EN PDF
                </a>";
            }

            if($this->edit_active)
            {
                if($this->is_ajax)
                {
                    $buttons_actions .=
                    "<button onclick='abrir_modal_editar(".$result_row->id.")' type='button' data-toggle='tooltip' title='' class='btn-sm ".$this->config_buttons["edit"]["class"]."' data-original-title='".$this->config_buttons["edit"]["title"]."'>
                        <i class='".$this->config_buttons["edit"]["icon"]."'></i>
                    </button>";
                }
                else{
                    $buttons_actions .=
                    "<a href='".$this->link_controlador."editar/".$result_row->id."' data-toggle='tooltip' title='' class='btn-sm ".$this->config_buttons["edit"]["class"]."' data-original-title='".$this->config_buttons["edit"]["title"]."'>
                        <i class='".$this->config_buttons["edit"]["icon"]."'></i>
                    </a>";
                }
            }

            if($this->delete_active)
            {
                $buttons_actions.=
                "<button onclick='abrir_modal_eliminar(".$result_row->id.")' type='button' data-toggle='tooltip' title='' class='btn-sm ".$this->config_buttons["delete"]["class"]."' data-original-title='".$this->config_buttons["delete"]["title"]."'>
                    <i class='".$this->config_buttons["delete"]["icon"]."'></i>
                </button>";
            }

            $buttons_actions.="</div>";


            $row_of_data[]=$buttons_actions;

            $data[]=$row_of_data;
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

    	return response()->json($json_data);
    }

    public function nuevo(Request $request)
    {
        $this->clear_session_paso_2($request);
        $this->clear_session_paso_3($request);

        if($request->isMethod("post"))
        {
            $comprobante_a_generar = $request->session()->get("comprobante_a_generar");

            $response_estructure = new ResponseEstructure();
            $response_estructure->set_response(true);

            $punto_de_venta = trim($request->input("punto_de_venta"));
            $tipo_de_comprobante = trim($request->input("tipo_de_comprobante"));

            if($punto_de_venta == ""){
                $response_estructure->set_response(false);
                $response_estructure->add_message_error("Seleccione un punto de venta");
            }

            if($tipo_de_comprobante == ""){
                $response_estructure->set_response(false);
                $response_estructure->add_message_error("Seleccione un tipo de comprobante");
            }

            if($comprobante_a_generar->comprobanteDisponible($tipo_de_comprobante) == FALSE){

                $response_estructure->set_response(false);
                $response_estructure->add_message_error("Solo se encuentran disponibles las Facturas C,Notas de Débito,Notas de Crédito C y Recibo C");
            }

            if($response_estructure->get_response() == true){
                
                $comprobante_a_generar->punto_de_venta = $punto_de_venta;
                $comprobante_a_generar->id_tipo_de_comprobante = $tipo_de_comprobante;

                $request->session()->put("comprobante_a_generar",$comprobante_a_generar);
            }

            return response()->json($response_estructure->get_response_array());
        }
        else
        {
            $comprobante_a_generar = new Comprobante();
            $request->session()->put("comprobante_a_generar",$comprobante_a_generar);

            $id_configuracion_afip = $request->session()->get("id_configuracion_afip");

            try
            {
                $this->ws_afip = new WebserviceAfip($id_configuracion_afip);

                $this->title_page = $this->config_buttons["add"]["title"]." ".$this->entity;
                $this->share_parameters();
    
                $puntos_de_venta = $this->ws_afip->ConsultarPuntosDeVentas();
                $tipos_de_comprobantes = $this->ws_afip->getTiposDeComprobantesDisponibles();
                
                return View($this->carpeta_views."add")
                ->with("tipos_de_comprobantes",$tipos_de_comprobantes)
                ->with("puntos_de_venta",$puntos_de_venta)
                ->with("comprobante_a_generar",$comprobante_a_generar);
            }
            catch(\Exception $e)
            {
                $request->session()->flash("errorCertificado",1);

                return Redirect('/backend/afip/errorCertificado');
            }
        }
    }

    public function nuevo2(Request $request)
    {
        $this->clear_session_paso_2($request);
        $this->clear_session_paso_3($request);

        if($request->isMethod("post"))
        {
            $comprobante_a_generar = $request->session()->get("comprobante_a_generar");

            $response_estructure = new ResponseEstructure();
            $response_estructure->set_response(true);
            
            $fecha_del_comprobante = trim($request->input("fecha_del_comprobante"));
            $concepto = trim($request->input("concepto"));
            $moneda = trim($request->input("moneda"));
            $MonCotiz = trim($request->input("MonCotiz"));
            $periodo_facturado_desde = trim($request->input("periodo_facturado_desde"));
            $periodo_facturado_hasta = trim($request->input("periodo_facturado_hasta"));
            $periodo_facturado_vto = trim($request->input("periodo_facturado_vto"));

            $cotizacion_del_dolar = $MonCotiz;

            $input = [
                "fecha_del_comprobante"=>$fecha_del_comprobante,
                "concepto"=>$concepto,
                "moneda"=>$moneda,
            ];

            $rules = [
                "fecha_del_comprobante"=>"required|date_format:d/m/Y",
                "concepto"=>"required",
                "moneda"=>"required"
            ];
            
            if($concepto == $comprobante_a_generar::$CONCEPTO_SERVICIOS || $concepto == $comprobante_a_generar::$CONCEPTO_PRODUCTOS_Y_SERVICIOS)
            {
                $input["periodo_facturado_desde"] = $periodo_facturado_desde;
                $input["periodo_facturado_hasta"] = $periodo_facturado_hasta;
                $input["periodo_facturado_vto"] = $periodo_facturado_vto;

                $rules["periodo_facturado_desde"] = "required|date_format:d/m/Y";
                $rules["periodo_facturado_hasta"] = "required|date_format:d/m/Y";
                $rules["periodo_facturado_vto"] = "required|date_format:d/m/Y";
            }

            if($moneda != "PES" && $moneda != "")
            {
                $input["cotizacion_de_la_moneda"] = $MonCotiz;

                $rules["cotizacion_de_la_moneda"] = "required|regex:/^\d+(\.\d{1,2})?$/";
            }

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $response_estructure->set_response(false);

                $errors = $validator->errors();

                foreach ($errors->all() as $error) {
                    $response_estructure->add_message_error($error);
                }
            }
            else
            {

                // VALIDACIÓN FECHA
                $fecha_a_validar = \DateTime::createFromFormat("d/m/Y",$fecha_del_comprobante);
                $fecha_a_validar = $fecha_a_validar->format("Y-m-d");

                $fecha_de_hoy = Date("Y-m-d");

                /*
                Para servicios se puede hacer 10 días para atrás y 10 para adelante.

                Productos 5 para atrás y ninguno para adelante.

                Productos y servicios, ídem servicios.

                Tener en cuenta que en ningún caso se puede emitir una factura retroactiva 
                con fecha anterior a la última emitida en el punto de venta. 
                Ejemplo, emitir una el 27/07, no puedo ir mas atrás de esa fecha.

                */

                $date1 = new \DateTime($fecha_de_hoy);
                $date2 = new \DateTime($fecha_a_validar);
                $diff = $date1->diff($date2);

                $diferencia_en_dias = $diff->days;

                // SERVICIOS
                if($concepto == $comprobante_a_generar::$CONCEPTO_SERVICIOS 
                || $concepto == $comprobante_a_generar::$CONCEPTO_PRODUCTOS_Y_SERVICIOS)
                {
                    if($diferencia_en_dias > 10)
                    {
                        $response_estructure->set_response(false);
                        $response_estructure->add_message_error("La fecha para servicios debe ser 10 días para atrás o 10 para adelante máximo");
                    }

                    if($periodo_facturado_vto < Date("d/m/Y"))
                    {
                        $response_estructure->set_response(false);
                        $response_estructure->add_message_error("La fecha del vencimiento del pago no puede ser menor a la fecha actual");
                    }
                }
                else // PRODUCTOS
                {
                    if($fecha_de_hoy < $fecha_a_validar)
                    {
                        $response_estructure->set_response(false);
                        $response_estructure->add_message_error("La fecha para productos debe ser igual a la fecha de hoy o puede tener 5 días para atrás como máximo");
                    }
                    else
                    {
                        if($diferencia_en_dias > 5)
                        {
                            $response_estructure->set_response(false);
                            $response_estructure->add_message_error("La fecha para productos debe ser igual a la fecha de hoy o puede tener 5 días para atrás como máximo");
                        }
                    }
                }
                // FIN VALIDACIÓN FECHA


                if($response_estructure->get_response() === TRUE)
                {
                    if($moneda == "PES")
                    {
                        $MonCotiz = 1;
                    }

                    $fecha_del_comprobante = \DateTime::createFromFormat("d/m/Y",$fecha_del_comprobante);
                    $fecha_del_comprobante = $fecha_del_comprobante->format("Y-m-d");

                    $periodo_facturado_desde = \DateTime::createFromFormat("d/m/Y",$periodo_facturado_desde);
                    $periodo_facturado_desde = $periodo_facturado_desde->format("Y-m-d");

                    $periodo_facturado_hasta = \DateTime::createFromFormat("d/m/Y",$periodo_facturado_hasta);
                    $periodo_facturado_hasta = $periodo_facturado_hasta->format("Y-m-d");

                    $periodo_facturado_vto = \DateTime::createFromFormat("d/m/Y",$periodo_facturado_vto);
                    $periodo_facturado_vto = $periodo_facturado_vto->format("Y-m-d");

                    

                    $comprobante_a_generar->fecha = $fecha_del_comprobante;
                    $comprobante_a_generar->id_concepto = $concepto;
                    $comprobante_a_generar->id_moneda = $moneda;
                    $comprobante_a_generar->cotizacion_de_la_moneda = $MonCotiz;
                    $comprobante_a_generar->periodo_facturado_desde = $periodo_facturado_desde;
                    $comprobante_a_generar->periodo_facturado_hasta = $periodo_facturado_hasta;
                    $comprobante_a_generar->vencimiento_del_pago = $periodo_facturado_vto;

                    $comprobante_a_generar->cotizacion_del_dolar = $cotizacion_del_dolar;

                    $request->session()->put("comprobante_a_generar",$comprobante_a_generar);
                }
            }

            return response()->json($response_estructure->get_response_array());
        }
        else
        {
            $this->validar_paso1($request);

            $comprobante_a_generar = $request->session()->get("comprobante_a_generar");

            $this->title_page = "DATOS DE EMISIÓN (PASO 1 DE 3)";
            $this->share_parameters();

            $id_configuracion_afip = $request->session()->get("id_configuracion_afip");
            $this->ws_afip = new WebserviceAfip($id_configuracion_afip);

            $tipos_de_monedas = $this->ws_afip->geTiposMonedasDisponibles(); 
            $conceptos = $this->ws_afip->getTiposDeConceptos();

            return View($this->carpeta_views."add2")
            ->with("comprobante_a_generar",$comprobante_a_generar)
            ->with("tipos_de_monedas",$tipos_de_monedas)
            ->with("conceptos",$conceptos);
        }
    }

    public function nuevo3(Request $request)
    {
        $comprobante_a_generar = $request->session()->get("comprobante_a_generar");

        if($request->isMethod("post"))
        {
            $response_estructure = new ResponseEstructure();
            $response_estructure->set_response(true);

            $tipo_de_comprobante = $comprobante_a_generar->id_tipo_de_comprobante;

            $condicion_frente_al_iva = (int)$request->input("condicion_frente_al_iva");
            $idIVAReceptor = (int)$request->input("idIVAReceptor");
            //$nrodocreceptor = trim($request->input("nrodocreceptor"));
            $razon_social = trim($request->input("razon_social"));
            $correo = trim($request->input("correo"));
            $domicilio_comercial = trim($request->input("domicilio_comercial"));
            $id_condicion_de_venta = (int) $request->input("id_condicion_de_venta");
            $descripcion_servicio = $request->input("descripcion_servicio");


            $ImpTotal = $request->input("ImpTotal");
            $ImpTotConc = $request->input("ImpTotConc");
            //$ImpNeto = $request->input("ImpNeto");
            $ImpOpEx = $request->input("ImpOpEx");
            $ImpIVA = $request->input("ImpIVA");
            //$ImpTrib = $request->input("ImpTrib");

            $detalle_comprobante = array();
            $detalle_tributos = array();
            $comprobantes_asociados = array();

            if($comprobante_a_generar->comprobanteConDetalle())
            {
                $detalle_comprobante = json_decode($request->input("detalle_comprobante"),true);
            }

            if($comprobante_a_generar->comprobanteConTributos())
            {
                $detalle_tributos = json_decode($request->input("detalle_tributos"),true);
            }

            if($comprobante_a_generar->comprobanteConComprobantesAsociados())
            {
                $comprobantes_asociados = json_decode($request->input("comprobantes_asociados"),true);
            }

            $ImpNeto = $request->input("subtotal");
            $ImpTrib = $request->input("total_tributos");

            if($condicion_frente_al_iva == 0){
                $response_estructure->add_message_error("Seleccione la condición frente al iva");
                $response_estructure->set_response(false);
            }

            if($idIVAReceptor == 0){
                $response_estructure->add_message_error("Seleccione el tipo de documento");
                $response_estructure->set_response(false);
            }

            /*
            if($nrodocreceptor == ""){
                $response_estructure->add_message_error("Ingrese el número de documento");
                $response_estructure->set_response(false);
            }
            */

            if($id_condicion_de_venta == 0){
                $response_estructure->add_message_error("Seleccione una condición de venta");
                $response_estructure->set_response(false);
            }

            // VERIFICO CARGA DE DETALLE FACTURA
            if($comprobante_a_generar->comprobanteConDetalle())
            {
                if(count($detalle_comprobante) == 0)
                {
                    $response_estructure->add_message_error("Debe cargar un detalle de factura");
                    $response_estructure->set_response(false);
                }
            
                $subtotal_calculado = 0;

                if($response_estructure->get_response() === TRUE)
                {
                    for($i=0; $i < count($detalle_comprobante);$i++)
                    {
                        $codigo = $detalle_comprobante[$i]["codigo"];
                        $producto_servicio = $detalle_comprobante[$i]["producto_servicio"];
                        $cantidad = $detalle_comprobante[$i]["cantidad"];
                        $id_unidad_medida = $detalle_comprobante[$i]["id_unidad_medida"];
                        $nombre_unidad = $detalle_comprobante[$i]["nombre_unidad"];
                        $precio_unitario = (double)$detalle_comprobante[$i]["precio_unitario"];
                        $porcentaje_bonificacion = (double)$detalle_comprobante[$i]["porcentaje_bonificacion"];
                        $importe_bonificacion = (double)$detalle_comprobante[$i]["importe_bonificacion"];
                        $subtotal = (double)$detalle_comprobante[$i]["subtotal"];

                        $total_calculando = $cantidad * $precio_unitario;
                        $importe_bonificacion_calculando =  ($total_calculando * $porcentaje_bonificacion) / 100;
                    
                        $subtotal_calculado += ($total_calculando - $importe_bonificacion_calculando);
                    }    
                }

                if($ImpNeto != $subtotal_calculado)
                {
                    $response_estructure->add_message_error("El subtotal pasado no es igual al calculado");
                    $response_estructure->set_response(false);
                }
            }

            // SI ES UNA FACTURA C  o NOTA DE DEBITO C  o NOTA DE CREDITO C o RECIBO C
            // VERIFICO CARGA DE TRIBUTOS FACTURA
            if($comprobante_a_generar->comprobanteConTributos())
            {
                $total_tributos_calculado = 0;

                if($response_estructure->get_response() === TRUE)
                {
                    for($i=0; $i < count($detalle_tributos);$i++)
                    {
                        $descripcion = $detalle_tributos[$i]["descripcion"];
                        $detalle = $detalle_tributos[$i]["detalle"];
                        $base_imponible = $detalle_tributos[$i]["base_imponible"];
                        $alicuota = $detalle_tributos[$i]["alicuota"];
                        $importe = $detalle_tributos[$i]["importe"];

                        $importe_porcentaje_alicuota = ($alicuota * $base_imponible) / 100;
                        $total_tributos_calculado += $importe_porcentaje_alicuota;
                    }    
                }

                if($ImpTrib != $total_tributos_calculado)
                {
                    $response_estructure->add_message_error("El total de tributos pasado no es igual al calculado");
                    $response_estructure->set_response(false);
                }
            }

            $id_cliente = trim($request->input("id_cliente"));

            $cliente_obj = null;

            if($response_estructure->get_response() === TRUE)
            {
                if(strpos($id_cliente,"Agregar:") !== FALSE)
                {
                    $id_cliente = str_replace("Agregar:","",$id_cliente);

                    // SI ES UN CUIT
                    if($idIVAReceptor == 80)
                    {
                        $cuit_limpio = "";

                        for($i=0; $i < strlen($id_cliente);$i++)
                        {
                            if(is_numeric($id_cliente[$i]))
                            {
                                $cuit_limpio .= $id_cliente[$i];
                            }
                        }

                        $id_cliente = $cuit_limpio;
                    }

                    $cliente_obj = \App\MisClientes::where("documento",trim($id_cliente))
                    ->where("id_configuracion_afip",$request->session()->get("id_configuracion_afip"))
                    ->first();

                    if(!$cliente_obj)
                    {
                        $cliente_obj = new \App\MisClientes();
                    }

                    $cliente_obj->nombre_razon_social = $razon_social;
                    $cliente_obj->id_tipo_documento = $idIVAReceptor;
                    $cliente_obj->documento = trim($id_cliente);
                    $cliente_obj->correo = $correo;
                    $cliente_obj->telefono = "";
                    $cliente_obj->domicilio_comercial = $domicilio_comercial;
                    $cliente_obj->id_condicion_iva = $condicion_frente_al_iva;
                    $cliente_obj->eliminado = 0;
                    $cliente_obj->id_configuracion_afip = $request->session()->get("id_configuracion_afip");
                    $cliente_obj->save();

                    $id_cliente = $cliente_obj->id;
                }
                else
                {
                    $cliente_obj = \App\MisClientes::where("id",$id_cliente)
                    ->where("id_configuracion_afip",$request->session()->get("id_configuracion_afip"))
                    ->where("eliminado",0)
                    ->first();

                    if(!$cliente_obj) {
                        $response_estructure->set_response(false);
                        $response_estructure->add_message_error("No se encontró el cliente");
                    }
                    else
                    {
                        $cliente_obj->correo = $correo;
                        $cliente_obj->id_condicion_iva = $condicion_frente_al_iva;
                        $cliente_obj->save();
                    }
                }
            }
            
            if($response_estructure->get_response() === TRUE)
            {

                $nrodocreceptor = $cliente_obj->documento;
                // AGREGANDO A SESSION TODOS LOS PARAMETROS

                // HARDCODING MONOTRIBUTISTA
                $ImpTotal = ($ImpNeto + $ImpTrib);

                $comprobante_a_generar->condicion_iva_receptor = $condicion_frente_al_iva;
                $comprobante_a_generar->tipo_documento_receptor = $idIVAReceptor;
                $comprobante_a_generar->num_documento_receptor = $nrodocreceptor;
                $comprobante_a_generar->razon_social_receptor = $razon_social;
                $comprobante_a_generar->correo_receptor = $correo;
                $comprobante_a_generar->domicilio_receptor = $domicilio_comercial;
                $comprobante_a_generar->id_condicion_venta = $id_condicion_de_venta;
                $comprobante_a_generar->descripcion_servicio = $descripcion_servicio;

                $comprobante_a_generar->ImpTotal = $ImpTotal;
                $comprobante_a_generar->ImpTotConc = $ImpTotConc;
                $comprobante_a_generar->ImpNeto = $ImpNeto;
                $comprobante_a_generar->ImpOpEx = $ImpOpEx;
                $comprobante_a_generar->ImpIVA = $ImpIVA;
                $comprobante_a_generar->ImpTrib = $ImpTrib;

                $request->session()->put("comprobante_a_generar",$comprobante_a_generar);

                $request->session()->put("detalle_comprobante_new_c",$detalle_comprobante);
                $request->session()->put("detalle_tributos_new_c",$detalle_tributos);
                $request->session()->put("comprobantes_asociados_new_c",$comprobantes_asociados);
            }

            return response()->json($response_estructure->get_response_array());
        }
        else
        {
            $this->validar_paso1($request);
            $this->validar_paso2($request);

            $this->title_page = "DATOS DEL RECEPTOR (PASO 2 DE 3)";
            $this->share_parameters();

            $id_configuracion_afip = $request->session()->get("id_configuracion_afip");
            $this->ws_afip = new WebserviceAfip($id_configuracion_afip);

            $condiciones_frente_al_iva = $this->ws_afip->get_condiciones_frente_al_iva();
            $condiciones_de_venta = $this->ws_afip->get_condiciones_de_venta();
            $puntos_de_venta = $this->ws_afip->ConsultarPuntosDeVentas();
            $tipos_de_iva = $this->ws_afip->getTiposAlicuotas();
            $tipos_de_tributos = $this->ws_afip->getTiposDeTributosDisponibles();
            $unidades_de_medida = UnidadDeMedida::all();

            return View($this->carpeta_views."add3")
            ->with("condiciones_frente_al_iva",$condiciones_frente_al_iva)
            ->with("condiciones_de_venta",$condiciones_de_venta)
            ->with("puntos_de_venta",$puntos_de_venta)
            ->with("tipos_de_iva",$tipos_de_iva)
            ->with("unidades_de_medida",$unidades_de_medida)
            ->with("tipos_de_tributos",$tipos_de_tributos)
            ->with("comprobante_a_generar",$comprobante_a_generar);
        }   
    }

    public function confirmar_nuevo(Request $request)
    {
        $comprobante_a_generar = $request->session()->get("comprobante_a_generar");

        if($request->isMethod("post"))
        {
            $id_configuracion_afip = $request->session()->get("id_configuracion_afip");

            $response_estructure = new ResponseEstructure();
            $response_estructure->set_response(false);

            // ASIGNO LAS PROPIEDADES AL OBJETO COMPROBANTE

            $comprobante_a_generar->id_configuracion_afip = $id_configuracion_afip;

            $aclaracion_usuario = $request->input("aclaracion_usuario");

            $comprobante_a_generar->aclaracion_usuario = $aclaracion_usuario;

            $comprobante_a_generar->id_estado=EstadoComprobante::REALIZADO; // REALIZADA

            // GUARDO EL COMPROBANTE PARA TENER SU ID
            $comprobante_a_generar->save();

            $detalle_comprobante = array();

            $ImpNeto = 0;

            // COLECCION DE DetalleComprobante
            $detalles_comprobantes_objs = array();

            if($comprobante_a_generar->comprobanteConDetalle())
            {
                // TRABAJANDO CON EL DETALLE DEL COMPROBANTE
                $detalle_comprobante = $request->session()->get("detalle_comprobante_new_c");

                for($i=0; $i < count($detalle_comprobante);$i++)
                {
                    $cantidad = $detalle_comprobante[$i]["cantidad"];
                    $precio_unitario = (double)$detalle_comprobante[$i]["precio_unitario"];
                    $porcentaje_bonificacion = (double)$detalle_comprobante[$i]["porcentaje_bonificacion"];

                    $total_calculando = $cantidad * $precio_unitario;
                    $importe_bonificacion_calculando =  ($total_calculando * $porcentaje_bonificacion) / 100;
                    $subtotal = ($total_calculando - $importe_bonificacion_calculando);

                    $codigo_comprobante = $detalle_comprobante[$i]["codigo"];

                    if(strpos($codigo_comprobante,"agregar_") !== FALSE)
                    {
                        $codigo_comprobante = str_replace("agregar_","",$codigo_comprobante);
                    }

                    // CREACION DEL OBJETO MODEL
                    $detalle_comprobante_obj = new DetalleComprobante();
                    $detalle_comprobante_obj->codigo = $detalle_comprobante[$i]["codigo"];
                    $detalle_comprobante_obj->producto_servicio= $detalle_comprobante[$i]["producto_servicio"];
                    $detalle_comprobante_obj->cantidad= $detalle_comprobante[$i]["cantidad"];
                    $detalle_comprobante_obj->id_unidad_medida= $detalle_comprobante[$i]["id_unidad_medida"];
                    $detalle_comprobante_obj->precio_unitario= (double)$detalle_comprobante[$i]["precio_unitario"];;
                    $detalle_comprobante_obj->porcentaje_bonificacion= (double)$detalle_comprobante[$i]["porcentaje_bonificacion"];;
                    $detalle_comprobante_obj->importe_bonificacion= (double)$importe_bonificacion_calculando;
                    $detalle_comprobante_obj->subtotal= $subtotal;
                    $detalle_comprobante_obj->id_comprobante = $comprobante_a_generar->id;
                    $detalle_comprobante_obj->save();


                    $detalles_comprobantes_objs[] = $detalle_comprobante_obj;

                    $ImpNeto += $subtotal;
                }  
            }
            // RECIBO C
            else if($comprobante_a_generar->id_tipo_de_comprobante == 15) 
            {
                $ImpNeto = $comprobante_a_generar->ImpNeto_new_c;
            }

            // FIN TRABAJANDO CON EL DETALLE DE LA FACTURA

            // TRABAJANDO CON DETALLES DE TRIBUTOS
            $detalle_tributos = $request->session()->get("detalle_tributos_new_c");
            
            //$total_tributos_calculado = 0;

            // COLECCION DE Tributos
            $detalles_tributos_objs = array();

            for($i=0; $i < count($detalle_tributos);$i++)
            {
                $tributo_obj = new TributoComprobante();
                $tributo_obj->descripcion = $detalle_tributos[$i]["descripcion"];
                $tributo_obj->detalle = $detalle_tributos[$i]["detalle"];
                $tributo_obj->base_imponible = $detalle_tributos[$i]["base_imponible"];
                $tributo_obj->alicuota = $detalle_tributos[$i]["alicuota"];
                $tributo_obj->importe = $detalle_tributos[$i]["importe"];
                $tributo_obj->id_tipo_tributo = $detalle_tributos[$i]["id_tipo"];

                // ME FIJO SI EXISTE EL TIPO DE TRIBUTO, SINO, LO CREO.
                $tipo_tributo_obj = TipoTributo::find($detalle_tributos[$i]["id_tipo"]);

                if(!$tipo_tributo_obj)
                {
                    $tipo_tributo_obj = new TipoTributo();
                    $tipo_tributo_obj->Id = $detalle_tributos[$i]["id_tipo"];
                    $tipo_tributo_obj->Desc = $tributo_obj->Desc;
                    $tipo_tributo_obj->FchDesde = $tributo_obj->FchDesde;
                    $tipo_tributo_obj->FchHasta = $tributo_obj->FchHasta;
                    $tipo_tributo_obj->save();
                }

                $tributo_obj->id_comprobante = $comprobante_a_generar->id;
                $tributo_obj->save();

                $detalles_tributos_objs[] = $tributo_obj;
            }   
            // FIN TRABAJANDO CON DETALLES DE TRIBUTOS

            // TRABAJANDO CON COMPROBANTES ASOCIADOS
            $comprobantes_asociados = $request->session()->get("comprobantes_asociados_new_c");

            for($i=0; $i < count($comprobantes_asociados);$i++)
            {
                // CREACION DEL OBJETO MODEL
                $comprobantes_asociados_obj = new ComprobanteAsociado();
                $comprobantes_asociados_obj->id_tipo_de_comprobante = $comprobantes_asociados[$i]["id_tipo_de_comprobante"];
                $comprobantes_asociados_obj->id_punto_de_venta = $comprobantes_asociados[$i]["id_punto_de_venta"];
                $comprobantes_asociados_obj->comprobante = $comprobantes_asociados[$i]["comprobante"];
                $comprobantes_asociados_obj->save();
            }  
            // FIN TRABAJANDO CON COMPROBANTES ASOCIADOS

            $comprobante_a_generar->id_configuracion_afip = $id_configuracion_afip;

            $response_estructure->set_response(true); 

            try
            {
                $this->ws_afip = new WebserviceAfip($id_configuracion_afip);
                $comprobante_a_generar = $this->ws_afip->crearFacturaWithCAE($comprobante_a_generar,$detalles_comprobantes_objs,$detalles_tributos_objs,$comprobantes_asociados);
                
                $comprobante_a_generar->save();

                $comprobante_a_generar->codigo_de_barras = $comprobante_a_generar->obtenerCodigoDeBarras();
            }
            catch(\Exception $e)
            {
                $response_estructure->set_response(false);
                $response_estructure->add_message_error($e->getMessage());
            }

            
            $comprobante_a_generar->save();

            $comprobante_a_generar->send_comprobante_creado($comprobante_a_generar); // ENVIA AL CORREO RECEPTOR (SI TIENE) 
            
            $this->limpiarSesionComprobante($request);
            
            return response()->json($response_estructure->get_response_array());
        }   
        else
        {
            $tipo_de_comprobante = $comprobante_a_generar->id_tipo_de_comprobante;

            $tipo_de_comprobante_obj = TipoDeComprobante::find($tipo_de_comprobante);

            $titulo_pagina = "CONFIRMAR";

            if($tipo_de_comprobante_obj)
            {
                $titulo_pagina.= " ".strtoupper($tipo_de_comprobante_obj->Desc);
            }

            $this->title_page = $titulo_pagina; // THE HARD
            $this->share_parameters();

            // PASO 2
            $fecha_del_comprobante = $comprobante_a_generar->fecha;

            $periodo_facturado_desde = $comprobante_a_generar->periodo_facturado_desde;
            $periodo_facturado_hasta = $comprobante_a_generar->periodo_facturado_hasta;
            $periodo_facturado_vto = $comprobante_a_generar->vencimiento_del_pago;

            $fecha_del_comprobante = \DateTime::createFromFormat("Y-m-d",$fecha_del_comprobante);
            $fecha_del_comprobante = $fecha_del_comprobante->format("d/m/Y");

            if($comprobante_a_generar->id_concepto == $comprobante_a_generar::$CONCEPTO_SERVICIOS 
            || $comprobante_a_generar->id_concepto == $comprobante_a_generar::$CONCEPTO_PRODUCTOS_Y_SERVICIOS)
            {
                $periodo_facturado_desde = \DateTime::createFromFormat("Y-m-d",$periodo_facturado_desde);
                $periodo_facturado_desde = $periodo_facturado_desde->format("d/m/Y");

                $periodo_facturado_hasta = \DateTime::createFromFormat("Y-m-d",$periodo_facturado_hasta);
                $periodo_facturado_hasta = $periodo_facturado_hasta->format("d/m/Y");

                $periodo_facturado_vto = \DateTime::createFromFormat("Y-m-d",$periodo_facturado_vto);
                $periodo_facturado_vto = $periodo_facturado_vto->format("d/m/Y");
            }

            $detalle_comprobante = $request->session()->get("detalle_comprobante_new_c");
            $detalle_tributos = $request->session()->get("detalle_tributos_new_c");
            $comprobantes_asociados = $request->session()->get("comprobantes_asociados_new_c");

            $id_configuracion_afip = $request->session()->get("id_configuracion_afip");
            $configuracion_afip_obj = ConfiguracionAfip::find($id_configuracion_afip);

            $this->ws_afip = new WebserviceAfip($id_configuracion_afip);

            $concepto_obj = Concepto::find($comprobante_a_generar->id_concepto);
            $tipos_de_documentos = $this->ws_afip->obtenerTiposDeDocumentosDisponibles(); 

            $condicion_iva_obj = CondicionIva::find($comprobante_a_generar->condicion_iva_receptor);
            $condicion_venta_obj = CondicionDeVenta::find($comprobante_a_generar->id_condicion_venta);

            $punto_de_venta_obj = PuntoDeVenta::where("numero",$comprobante_a_generar->punto_de_venta)->where("id_configuracion_afip",$id_configuracion_afip)->first();

            return View($this->carpeta_views."confirmar_nuevo")
            ->with("comprobante_a_generar",$comprobante_a_generar)
            ->with("tipo_de_comprobante_obj",$tipo_de_comprobante_obj)
            ->with("fecha_del_comprobante",$fecha_del_comprobante)
            ->with("periodo_facturado_desde",$periodo_facturado_desde)
            ->with("periodo_facturado_hasta",$periodo_facturado_hasta)
            ->with("periodo_facturado_vto",$periodo_facturado_vto)
            ->with("detalle_comprobante",$detalle_comprobante)
            ->with("detalle_tributos",$detalle_tributos)
            ->with("comprobantes_asociados",$comprobantes_asociados)
            ->with("concepto_obj",$concepto_obj)
            ->with("tipos_de_documentos",$tipos_de_documentos)
            ->with("condicion_iva_obj",$condicion_iva_obj)
            ->with("condicion_venta_obj",$condicion_venta_obj)
            ->with("punto_de_venta_obj",$punto_de_venta_obj)
            ->with("configuracion_afip_obj",$configuracion_afip_obj);  
        }
    }

    private function validar_condicion_iva($request)
    {
        $id_condicion_iva = $request->session()->get("id_condicion_iva");

        if($id_condicion_iva == 6)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private function validar_paso1(Request $request)
    {
        $comprobante_a_generar = $request->session()->get("comprobante_a_generar");

        if(!$comprobante_a_generar)
        {
            return Redirect('/backend/comprobantes/nuevo');
        }

        if( empty(trim($comprobante_a_generar->punto_de_venta)) || 
            empty(trim($comprobante_a_generar->id_tipo_de_comprobante)))
        {
            return Redirect('/backend/comprobantes/nuevo');
        }
    }

    private function validar_paso2(Request $request)
    {
        $validado = true;

        $comprobante_a_generar = $request->session()->get("comprobante_a_generar");

        if($comprobante_a_generar)
        {
            $fecha_del_comprobante_new_c = trim($request->session()->get("fecha_del_comprobante_new_c"));
            $concepto_new_c = trim($request->session()->get("concepto_new_c"));
            $moneda_new_c = trim($request->session()->get("moneda_new_c"));

            if(empty(trim($comprobante_a_generar->fecha)) || empty(trim($comprobante_a_generar->id_concepto)) || empty(trim($comprobante_a_generar->id_moneda)))
            {
                $validado = false;
            }
            else
            {
                if($comprobante_a_generar->id_concepto == $comprobante_a_generar::$CONCEPTO_SERVICIOS 
                || $comprobante_a_generar->id_concepto == $comprobante_a_generar::$CONCEPTO_PRODUCTOS_Y_SERVICIOS)
                {
                    if(empty(trim($comprobante_a_generar->periodo_facturado_desde)) || empty(trim($comprobante_a_generar->periodo_facturado_hasta)) || empty(trim($comprobante_a_generar->vencimiento_de_pago)))
                    {
                        $validado = false;
                    }
                }
            }

            if(!$validado)
            {
                return Redirect('/backend/comprobantes/nuevo');
            }
        }
        else
        {
            $validado = false;
        }

        if(!$validado)
        {
            return Redirect('/backend/comprobantes/nuevo');
        }
    }

    private function clear_session_paso_1(Request $request)
    {
        $comprobante_a_generar = $request->session()->get("comprobante_a_generar");

        if($comprobante_a_generar)
        {
            $comprobante_a_generar->punto_de_venta = null;
            $comprobante_a_generar->id_tipo_de_comprobante = null;

            $request->session()->put("comprobante_a_generar",$comprobante_a_generar);
        }
    }

    private function clear_session_paso_2(Request $request)
    {
        $comprobante_a_generar = $request->session()->get("comprobante_a_generar");

        if($comprobante_a_generar)
        {
            $comprobante_a_generar->fecha = null;
            $comprobante_a_generar->id_concepto = null;
            $comprobante_a_generar->id_moneda = null;
            $comprobante_a_generar->cotizacion_de_la_moneda = null;
            $comprobante_a_generar->periodo_facturado_desde = null;
            $comprobante_a_generar->periodo_facturado_hasta = null;
            $comprobante_a_generar->vencimiento_del_pago = null;

            $request->session()->put("comprobante_a_generar",$comprobante_a_generar);
        }
    }

    private function clear_session_paso_3(Request $request)
    {
        $comprobante_a_generar = $request->session()->get("comprobante_a_generar");

        if($comprobante_a_generar)
        {
            $comprobante_a_generar->condicion_iva_receptor = null;
            $comprobante_a_generar->tipo_documento_receptor = null;
            $comprobante_a_generar->num_documento_receptor = null;
            $comprobante_a_generar->razon_social_receptor = null;
            $comprobante_a_generar->correo_receptor = null;
            $comprobante_a_generar->domicilio_receptor = null;
            $comprobante_a_generar->id_condicion_venta = null;
            $comprobante_a_generar->ImpTotal = null;
            $comprobante_a_generar->ImpTotConc = null;
            $comprobante_a_generar->ImpNeto = null;
            $comprobante_a_generar->ImpOpEx = null;
            $comprobante_a_generar->ImpIVA = null;
            $comprobante_a_generar->ImpTrib = null;

            $request->session()->put("detalle_comprobante_new_c",null);
            $request->session()->put("detalle_tributos_new_c",null);
            $request->session()->put("comprobantes_asociados_new_c",null);

            $request->session()->put("comprobante_a_generar",$comprobante_a_generar);
        }
    }
    
    private function limpiarSesionComprobante(Request $request)
    {
        $request->session()->put("comprobante_a_generar",null);

        $this->clear_session_paso_2($request);
        $this->clear_session_paso_3($request);
    }
    
    public function get_tipos_de_documentos_unicos(Request $request)
    {
        $response_estructure = new ResponseEstructure();
        $response_estructure->set_response(true);

        $id_condicion_frente_al_iva = $request->input("id_condicion_frente_al_iva");

        $id_configuracion_afip = $request->session()->get("id_configuracion_afip");
        $this->ws_afip = new WebserviceAfip($id_configuracion_afip);

        $response_estructure->set_response(true);
        $response_estructure->set_data($this->ws_afip->get_tipos_de_documentos_unicos($id_condicion_frente_al_iva));
        
        return response()->json($response_estructure->get_response_array());
    }

    public function get_cotizacion_moneda(Request $request)
    {
        $response_estructure = new ResponseEstructure();
        $response_estructure->set_response(true);

        $id_condicion_frente_al_iva = $request->input("id_condicion_frente_al_iva");

        $id_configuracion_afip = $request->session()->get("id_configuracion_afip");
        $this->ws_afip = new WebserviceAfip($id_configuracion_afip);

        $moneda_id = $request->input("moneda_id");
        
        $cotizacion_moneda_hoy = $this->ws_afip->get_cotizacion_moneda($moneda_id);

        $response_estructure->set_response(true);
        $response_estructure->set_data(["cotizacion"=>$cotizacion_moneda_hoy->ResultGet->MonCotiz]);
        
        return response()->json($response_estructure->get_response_array());
    }

    public function generacion_pdf(Request $request,$id_comprobante)
    {
        $comprobante_obj = Comprobante::where("id",$id_comprobante)->where("id_configuracion_afip",$request->session()->get("id_configuracion_afip"))->first();

        if($comprobante_obj){

            $base_64_code = $comprobante_obj->get_base_64_comprobante();
            return response($base_64_code)->header("Content-type","application/pdf");
        } 
    }

    public function getDataProductService(Request $request)
    {
        $comprobante_a_generar = $request->session()->get("comprobante_a_generar");

        $response_estructure = new ResponseEstructure();
        $response_estructure->set_response(false);

        if($comprobante_a_generar)
        {
            $id = $request->input("id");
            $id_configuracion_afip = $request->session()->get("id_configuracion_afip");

            $row_obj = ProductoServicio::where("id",$id)
            ->where("id_configuracion_afip",$id_configuracion_afip)
            ->first();
        
            if($row_obj)
            {
                $data = [
                    "id"              => $row_obj->id,
                    "descripcion"     => $row_obj->descripcion,
                    "id_unidad_medida"=> $row_obj->id_unidad_medida,
                    "precio"          => 0,
                ];

                if(WebserviceAfip::isEnableCurrency($comprobante_a_generar->id_moneda))
                {
                    if(WebserviceAfip::isCurrencyIsDollarEEUU($comprobante_a_generar->id_moneda))
                    {
                        // IS PESOS ARG
                        if(!empty(trim($row_obj->precio)))
                        {
                            if($comprobante_a_generar->cotizacion_del_dolar > 0)
                            {
                                $data["precio"] = number_format(($row_obj->precio / $comprobante_a_generar->cotizacion_del_dolar),2,".","");
                            }
                        }
                        // IS DOLLAR EEUU
                        else if(!empty(trim($row_obj->precio_dolar)))
                        {
                            $data["precio"] = $row_obj->precio_dolar;
                        }
                    }
                    else if(WebserviceAfip::isCurrencyIsPesosArg($comprobante_a_generar->id_moneda))
                    {
                        // IS DOLLAR EEUU
                        if(!empty(trim($row_obj->precio_dolar)))
                        {
                            if($comprobante_a_generar->cotizacion_del_dolar > 0)
                            {
                                $data["precio"] = number_format(($row_obj->precio_dolar * $comprobante_a_generar->cotizacion_del_dolar),2,".","");
                            }
                        }
                        // IS PESOS ARG
                        else if(!empty(trim($row_obj->precio)))
                        {
                            $data["precio"] = $row_obj->precio;
                        }
                    }
                }

                $response_estructure->set_data($data);
                $response_estructure->set_response(true);
            }
            else
            {
                $response_estructure->set_response(false);
                $response_estructure->add_message_error($this->text_no_search);
            }
        }

        return response()->json($response_estructure->get_response_array());
    }
}

<?php

namespace App\Http\Controllers\backend\comprobantes;

use Illuminate\Http\Request;
use App\Http\Controllers\Backend\ABM_Core;

use Illuminate\Support\Facades\Mail;
use App\Mail\EmailSender;

use App\Library\ResponseEstructure;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

use Mpdf\Mpdf;
use DNS1D;

use App\Comprobante;
use App\DetalleComprobante;
use App\ComprobanteAsociado;
use App\TributoComprobante;
use App\TipoTributo;
use App\TipoDeComprobante;

use App\Library\WebserviceAfip\WebserviceAfip;

use App\Concepto;
use App\UnidadDeMedida;
use App\CondicionIva;
use App\CondicionDeVenta;

class AsistenteAyudaController extends ABM_Core
{
    public function __construct()
    {
        $this->link_controlador = url('/backend/comprobantes/asistente_ayuda').'/';
        $this->carpeta_views = "backend.comprobantes.solicitud_asistencia.";

        $this->entity ="Ayuda";
        $this->title_page = "Asistente de ayuda";

        $this->columns = [];

        $this->add_active = false;
        $this->edit_active = false;
        $this->delete_active = false;
        $this->is_ajax = false;
    }

    public function index(Request $request,$id_comprobante)
    {
        $this->validar_administrador();

        $this->link_controlador .=$id_comprobante;

        $this->share_parameters();

        $comprobante_ayuda_obj = Comprobante::find($id_comprobante);

        $this->clear_session_paso_1($request);
        $this->clear_session_paso_2($request);
        $this->clear_session_paso_3($request);
        
        // SI EL COMPROBANTE EXISTE Y NECESITA AYUDA
        if($comprobante_ayuda_obj && $comprobante_ayuda_obj->id_estado == 2)
        {
            $id_usuario = $comprobante_ayuda_obj->id_usuario;

            $request->session()->put("comprobante_ayuda_obj",$comprobante_ayuda_obj);

            if($request->isMethod("post"))
            {
                $response_estructure = new ResponseEstructure();
                $response_estructure->set_response(true);

                $punto_de_venta = trim($request->input("punto_de_venta"));
                $tipo_de_comprobante = trim($request->input("tipo_de_comprobante"));

                if($punto_de_venta == ""){
                    $response_estructure->set_response(false);
                    $response_estructure->add_message_error("Seleccione un punto de venta");
                }

                if($tipo_de_comprobante == ""){
                    $response_estructure->set_response(false);
                    $response_estructure->add_message_error("Seleccione un tipo de comprobante");
                }

                if($response_estructure->get_response() == true){

                    $comprobante_ayuda_obj->punto_de_venta = $punto_de_venta;
                    $comprobante_ayuda_obj->id_tipo_de_comprobante = $tipo_de_comprobante;

                    $request->session()->put("comprobante_ayuda_obj",$comprobante_ayuda_obj);
                }

                return response()->json($response_estructure->get_response_array());
            }
            else
            {
                $this->title_page = $this->config_buttons["add"]["title"]." ".$this->entity;
                $this->share_parameters();

                try
                {
                    $this->ws_afip = new WebserviceAfip($id_usuario);
                    $puntos_de_venta = $this->ws_afip->ConsultarPuntosDeVentas();
                    $tipos_de_comprobantes = $this->ws_afip->getTiposDeComprobantesDisponibles();
                }
                catch(\Exception $e)
                {
                    $request->session()->flash("errorCertificado",1);
                    return Redirect('/backend/afip/errorCertificado');
                }

                return View($this->carpeta_views."ayuda_1")
                ->with("tipos_de_comprobantes",$tipos_de_comprobantes)
                ->with("puntos_de_venta",$puntos_de_venta)
                ->with("comprobante_ayuda_obj",$comprobante_ayuda_obj);
            }
        }
    }

    public function nuevo2(Request $request,$id_comprobante)
    {   
        $this->validar_administrador();

        $this->clear_session_paso_2($request);
        $this->clear_session_paso_3($request);

        $comprobante_ayuda_obj = $request->session()->get("comprobante_ayuda_obj");

        $this->link_controlador .=$comprobante_ayuda_obj->id;

        if($request->isMethod("post"))
        {
            $response_estructure = new ResponseEstructure();
            $response_estructure->set_response(true);
            
            $fecha_del_comprobante = trim($request->input("fecha_del_comprobante"));
            $concepto = trim($request->input("concepto"));
            $moneda = trim($request->input("moneda"));
            $MonCotiz = trim($request->input("MonCotiz"));
            $periodo_facturado_desde = trim($request->input("periodo_facturado_desde"));
            $periodo_facturado_hasta = trim($request->input("periodo_facturado_hasta"));
            $periodo_facturado_vto = trim($request->input("periodo_facturado_vto"));

            $input = [
                "fecha_del_comprobante"=>$fecha_del_comprobante,
                "concepto"=>$concepto,
                "moneda"=>$moneda,
            ];

            $rules = [
                "fecha_del_comprobante"=>"required|date_format:d/m/Y",
                "concepto"=>"required",
                "moneda"=>"required"
            ];
            
            if($concepto == $comprobante_ayuda_obj::$CONCEPTO_SERVICIOS 
            || $concepto == $comprobante_ayuda_obj::$CONCEPTO_PRODUCTOS_Y_SERVICIOS)
            {
                $input["periodo_facturado_desde"] = $periodo_facturado_desde;
                $input["periodo_facturado_hasta"] = $periodo_facturado_hasta;
                $input["periodo_facturado_vto"] = $periodo_facturado_vto;

                $rules["periodo_facturado_desde"] = "required|date_format:d/m/Y";
                $rules["periodo_facturado_hasta"] = "required|date_format:d/m/Y";
                $rules["periodo_facturado_vto"] = "required|date_format:d/m/Y";
            }

            if($moneda != "PES" && $moneda != "")
            {
                $input["cotizacion_de_la_moneda"] = $MonCotiz;

                $rules["cotizacion_de_la_moneda"] = "required|regex:/^\d+(\.\d{1,2})?$/";
            }

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $response_estructure->set_response(false);

                $errors = $validator->errors();

                foreach ($errors->all() as $error) {
                    $response_estructure->add_message_error($error);
                }
            }
            else
            {

                // VALIDACIÓN FECHA
                $fecha_a_validar = \DateTime::createFromFormat("d/m/Y",$fecha_del_comprobante);
                $fecha_a_validar = $fecha_a_validar->format("Y-m-d");

                $fecha_de_hoy = Date("Y-m-d");

                /*
                Para servicios se puede hacer 10 días para atrás y 10 para adelante.

                Productos 5 para atrás y ninguno para adelante.

                Productos y servicios, ídem servicios.

                Tener en cuenta que en ningún caso se puede emitir una factura retroactiva 
                con fecha anterior a la última emitida en el punto de venta. 
                Ejemplo, emitir una el 27/07, no puedo ir mas atrás de esa fecha.

                */

                $date1 = new \DateTime($fecha_de_hoy);
                $date2 = new \DateTime($fecha_a_validar);
                $diff = $date1->diff($date2);

                $diferencia_en_dias = $diff->days;

                // SERVICIOS
                if($concepto == $comprobante_ayuda_obj::$CONCEPTO_SERVICIOS 
                || $concepto == $comprobante_ayuda_obj::$CONCEPTO_PRODUCTOS_Y_SERVICIOS)
                {
                    if($diferencia_en_dias > 10)
                    {
                        $response_estructure->set_response(false);
                        $response_estructure->add_message_error("La fecha para servicios debe ser 10 días para atrás o 10 para adelante máximo");
                    }
                }
                else // PRODUCTOS
                {
                    if($fecha_de_hoy < $fecha_a_validar)
                    {
                        $response_estructure->set_response(false);
                        $response_estructure->add_message_error("La fecha para productos debe ser igual a la fecha de hoy o puede tener 5 días para atrás como máximo");
                    }
                    else
                    {
                        if($diferencia_en_dias > 5)
                        {
                            $response_estructure->set_response(false);
                            $response_estructure->add_message_error("La fecha para productos debe ser igual a la fecha de hoy o puede tener 5 días para atrás como máximo");
                        }
                    }
                }
                // FIN VALIDACIÓN FECHA


                if($response_estructure->get_response() === TRUE)
                {
                    if($moneda == "PES")
                    {
                        $MonCotiz = 1;
                    }

                    $fecha_del_comprobante = \DateTime::createFromFormat("d/m/Y",$fecha_del_comprobante);
                    $fecha_del_comprobante = $fecha_del_comprobante->format("Y-m-d");

                    $periodo_facturado_desde = \DateTime::createFromFormat("d/m/Y",$periodo_facturado_desde);
                    $periodo_facturado_desde = $periodo_facturado_desde->format("Y-m-d");

                    $periodo_facturado_hasta = \DateTime::createFromFormat("d/m/Y",$periodo_facturado_hasta);
                    $periodo_facturado_hasta = $periodo_facturado_hasta->format("Y-m-d");

                    $periodo_facturado_vto = \DateTime::createFromFormat("d/m/Y",$periodo_facturado_vto);
                    $periodo_facturado_vto = $periodo_facturado_vto->format("Y-m-d");

                    $comprobante_ayuda_obj->fecha = $fecha_del_comprobante;
                    $comprobante_ayuda_obj->id_concepto = $concepto;
                    $comprobante_ayuda_obj->id_moneda = $moneda;
                    $comprobante_ayuda_obj->cotizacion_de_la_moneda = $MonCotiz;
                    $comprobante_ayuda_obj->periodo_facturado_desde = $periodo_facturado_desde;
                    $comprobante_ayuda_obj->periodo_facturado_hasta = $periodo_facturado_hasta;
                    $comprobante_ayuda_obj->vencimiento_del_pago = $periodo_facturado_vto;

                    $request->session()->put("comprobante_ayuda_obj",$comprobante_ayuda_obj);
                }
            }

            return response()->json($response_estructure->get_response_array());
        }
        else
        {
            $this->validar_paso1($request);

            $this->title_page = "DATOS DE EMISIÓN (PASO 1 DE 3)";
            $this->share_parameters();

            $this->ws_afip = new WebserviceAfip($comprobante_ayuda_obj->id_usuario);

            $tipos_de_monedas = $this->ws_afip->geTiposMonedasDisponibles(); 
            $conceptos = $this->ws_afip->getTiposDeConceptos();

            $fecha_del_comprobante = \DateTime::createFromFormat("Y-m-d",$comprobante_ayuda_obj->fecha);
            $fecha_del_comprobante =$fecha_del_comprobante->format("d/m/Y");

            $periodo_facturado_desde = \DateTime::createFromFormat("Y-m-d",$comprobante_ayuda_obj->periodo_facturado_desde);
            $periodo_facturado_desde =$periodo_facturado_desde->format("d/m/Y");

            $periodo_facturado_hasta = \DateTime::createFromFormat("Y-m-d",$comprobante_ayuda_obj->periodo_facturado_hasta);
            $periodo_facturado_hasta =$periodo_facturado_hasta->format("d/m/Y");

            $vencimiento_del_pago = \DateTime::createFromFormat("Y-m-d",$comprobante_ayuda_obj->vencimiento_del_pago);
            $vencimiento_del_pago =$vencimiento_del_pago->format("d/m/Y");

            $id_rol = $request->session()->get("id_rol");

           return View($this->carpeta_views."ayuda_2")
            ->with("id_rol",$id_rol)
            ->with("fecha_del_comprobante",$fecha_del_comprobante )
            ->with("comprobante_ayuda_obj",$comprobante_ayuda_obj )
            ->with("tipos_de_monedas",$tipos_de_monedas)
            ->with("conceptos",$conceptos)
            ->with("tipo_de_comprobante_new_c",$comprobante_ayuda_obj->id_tipo_de_comprobante)
            ->with("periodo_facturado_desde",$periodo_facturado_desde)
            ->with("periodo_facturado_hasta",$periodo_facturado_hasta)
            ->with("vencimiento_del_pago",$vencimiento_del_pago);
        }
    }
    
    public function nuevo3(Request $request,$id_comprobante)
    {
        $this->validar_administrador();

        $comprobante_ayuda_obj = $request->session()->get("comprobante_ayuda_obj");

        $this->link_controlador .=$comprobante_ayuda_obj->id;

        if($request->isMethod("post"))
        {
            $response_estructure = new ResponseEstructure();
            $response_estructure->set_response(true);

            $tipo_de_comprobante = $comprobante_ayuda_obj->id_tipo_de_comprobante;
            
            $condicion_frente_al_iva = (int)$request->input("condicion_frente_al_iva");
            $idIVAReceptor = (int)$request->input("idIVAReceptor");
            $nrodocreceptor = trim($request->input("nrodocreceptor"));
            $razon_social = trim($request->input("razon_social"));
            $correo = trim($request->input("correo"));
            $domicilio_comercial = trim($request->input("domicilio_comercial"));
            $id_condicion_de_venta = (int) $request->input("id_condicion_de_venta");
            $descripcion_servicio = $request->input("descripcion_servicio");


            $ImpTotal = $request->input("ImpTotal");
            $ImpTotConc = $request->input("ImpTotConc");
            //$ImpNeto = $request->input("ImpNeto");
            $ImpOpEx = $request->input("ImpOpEx");
            $ImpIVA = $request->input("ImpIVA");
            //$ImpTrib = $request->input("ImpTrib");

            $detalle_comprobante = array();
            $detalle_tributos = array();
            $comprobantes_asociados = array();

            if($comprobante_ayuda_obj->comprobanteConDetalle())
            {
                $detalle_comprobante = json_decode($request->input("detalle_comprobante"),true);
            }

            if($comprobante_ayuda_obj->comprobanteConTributos())
            {
                $detalle_tributos = json_decode($request->input("detalle_tributos"),true);
            }

            if($comprobante_ayuda_obj->comprobanteConComprobantesAsociados())
            {
                $comprobantes_asociados = json_decode($request->input("comprobantes_asociados"),true);
            }

            $ImpNeto = $request->input("subtotal");
            $ImpTrib = $request->input("total_tributos");

            if($condicion_frente_al_iva == 0){
                $response_estructure->add_message_error("Seleccione la condición frente al iva");
                $response_estructure->set_response(false);
            }

            if($idIVAReceptor == 0){
                $response_estructure->add_message_error("Seleccione el tipo de documento");
                $response_estructure->set_response(false);
            }

            if($nrodocreceptor == ""){
                $response_estructure->add_message_error("Ingrese el número de documento");
                $response_estructure->set_response(false);
            }

            if($id_condicion_de_venta == 0){
                $response_estructure->add_message_error("Seleccione una condición de venta");
                $response_estructure->set_response(false);
            }

            // VERIFICO CARGA DE DETALLE FACTURA
            if($comprobante_ayuda_obj->comprobanteConDetalle())
            {
                if(count($detalle_comprobante) == 0)
                {
                    $response_estructure->add_message_error("Debe cargar un detalle de factura");
                    $response_estructure->set_response(false);
                }
            
                $subtotal_calculado = 0;

                if($response_estructure->get_response() === TRUE)
                {
                    for($i=0; $i < count($detalle_comprobante);$i++)
                    {
                        $codigo = $detalle_comprobante[$i]["codigo"];
                        $producto_servicio = $detalle_comprobante[$i]["producto_servicio"];
                        $cantidad = $detalle_comprobante[$i]["cantidad"];
                        $id_unidad_medida = $detalle_comprobante[$i]["id_unidad_medida"];
                        $nombre_unidad = $detalle_comprobante[$i]["nombre_unidad"];
                        $precio_unitario = (double)$detalle_comprobante[$i]["precio_unitario"];
                        $porcentaje_bonificacion = (double)$detalle_comprobante[$i]["porcentaje_bonificacion"];
                        $importe_bonificacion = (double)$detalle_comprobante[$i]["importe_bonificacion"];
                        $subtotal = (double)$detalle_comprobante[$i]["subtotal"];

                        $total_calculando = $cantidad * $precio_unitario;
                        $importe_bonificacion_calculando =  ($total_calculando * $porcentaje_bonificacion) / 100;
                    
                        $subtotal_calculado += ($total_calculando - $importe_bonificacion_calculando);
                    }    
                }

                if($ImpNeto != $subtotal_calculado)
                {
                    $response_estructure->add_message_error("El subtotal pasado no es igual al calculado $ImpNeto != $subtotal_calculado");
                    $response_estructure->set_response(false);
                }
            }

            if($comprobante_ayuda_obj->comprobanteConTributos())
            {
                $total_tributos_calculado = 0;

                if($response_estructure->get_response() === TRUE)
                {
                    for($i=0; $i < count($detalle_tributos);$i++)
                    {
                        $descripcion = $detalle_tributos[$i]["descripcion"];
                        $detalle = $detalle_tributos[$i]["detalle"];
                        $base_imponible = $detalle_tributos[$i]["base_imponible"];
                        $alicuota = $detalle_tributos[$i]["alicuota"];
                        $importe = $detalle_tributos[$i]["importe"];

                        $importe_porcentaje_alicuota = ($alicuota * $base_imponible) / 100;
                        $total_tributos_calculado += $importe_porcentaje_alicuota;
                    }    
                }

                if($ImpTrib != $total_tributos_calculado)
                {
                    $response_estructure->add_message_error("El total de tributos pasado no es igual al calculado");
                    $response_estructure->set_response(false);
                }
            }

            if($response_estructure->get_response() === TRUE)
            {
                // AGREGANDO A SESSION TODOS LOS PARAMETROS

                // HARDCODING MONOTRIBUTISTA
                $ImpTotal = ($ImpNeto + $ImpTrib);

                $comprobante_ayuda_obj->condicion_iva_receptor = $condicion_frente_al_iva;
                $comprobante_ayuda_obj->tipo_documento_receptor = $idIVAReceptor;
                $comprobante_ayuda_obj->num_documento_receptor = $nrodocreceptor;
                $comprobante_ayuda_obj->razon_social_receptor = $razon_social;
                $comprobante_ayuda_obj->correo_receptor = $correo;
                $comprobante_ayuda_obj->domicilio_receptor = $domicilio_comercial;
                $comprobante_ayuda_obj->id_condicion_venta = $id_condicion_de_venta;
                $comprobante_ayuda_obj->descripcion_servicio = $descripcion_servicio;

                $comprobante_ayuda_obj->ImpTotal = $ImpTotal;
                $comprobante_ayuda_obj->ImpTotConc = $ImpTotConc;
                $comprobante_ayuda_obj->ImpNeto = $ImpNeto;
                $comprobante_ayuda_obj->ImpOpEx = $ImpOpEx;
                $comprobante_ayuda_obj->ImpIVA = $ImpIVA;
                $comprobante_ayuda_obj->ImpTrib = $ImpTrib;

                $request->session()->put("comprobante_ayuda_obj",$comprobante_ayuda_obj);

                $request->session()->put("detalle_comprobante_ayuda",$detalle_comprobante);
                $request->session()->put("detalle_tributos_ayuda",$detalle_tributos);
                $request->session()->put("comprobantes_asociados_ayuda",$comprobantes_asociados);
            }

            return response()->json($response_estructure->get_response_array());
        }
        else
        {
            $this->validar_paso1($request);
            $this->validar_paso2($request);

            $this->title_page = "DATOS DEL RECEPTOR (PASO 2 DE 3)";
            $this->share_parameters();

            $this->ws_afip = new WebserviceAfip($comprobante_ayuda_obj->id_usuario);

            $condiciones_frente_al_iva = $this->ws_afip->get_condiciones_frente_al_iva();
            $condiciones_de_venta = $this->ws_afip->get_condiciones_de_venta();
            $puntos_de_venta = $this->ws_afip->ConsultarPuntosDeVentas();
            $tipos_de_iva = $this->ws_afip->getTiposAlicuotas();
            $tipos_de_tributos = $this->ws_afip->getTiposDeTributosDisponibles();
            $unidades_de_medida = UnidadDeMedida::all();

            $tipos_de_documentos_unicos = $this->ws_afip->get_tipos_de_documentos_unicos($comprobante_ayuda_obj->condicion_iva_receptor);

            return View($this->carpeta_views."ayuda_3")
            ->with("condiciones_frente_al_iva",$condiciones_frente_al_iva)
            ->with("condiciones_de_venta",$condiciones_de_venta)
            ->with("puntos_de_venta",$puntos_de_venta)
            ->with("tipos_de_iva",$tipos_de_iva)
            ->with("unidades_de_medida",$unidades_de_medida)
            ->with("tipos_de_tributos",$tipos_de_tributos)
            ->with("tipos_de_documentos_unicos",$tipos_de_documentos_unicos)
            ->with("comprobante_ayuda_obj",$comprobante_ayuda_obj);
        }   
    }

    public function confirmar_nuevo(Request $request,$id_comprobante)
    {
        $this->validar_administrador();

        $comprobante_ayuda_obj = $request->session()->get("comprobante_ayuda_obj");
        $this->link_controlador .=$comprobante_ayuda_obj->id;

        if($request->isMethod("post"))
        {
            $response_estructure = new ResponseEstructure();
            $response_estructure->set_response(false);

            // ASIGNO LAS PROPIEDADES AL OBJETO COMPROBANTE

            $aclaracion_usuario = $request->input("aclaracion_usuario");

            $comprobante_ayuda_obj->aclaracion_usuario = $aclaracion_usuario;

            $comprobante_ayuda_obj->id_estado=1; // REALIZADA

            // GUARDO EL COMPROBANTE PARA TENER SU ID
            $comprobante_ayuda_obj->save();

            $detalle_comprobante = array();

            $ImpNeto = 0;

            // COLECCION DE DetalleComprobante
            $detalles_comprobantes_objs = array();

            if($comprobante_ayuda_obj->comprobanteConDetalle())
            {
                // TRABAJANDO CON EL DETALLE DEL COMPROBANTE
                $detalle_comprobante = $request->session()->get("detalle_comprobante_ayuda");

                for($i=0; $i < count($detalle_comprobante);$i++)
                {
                    $cantidad = $detalle_comprobante[$i]["cantidad"];
                    $precio_unitario = (double)$detalle_comprobante[$i]["precio_unitario"];
                    $porcentaje_bonificacion = (double)$detalle_comprobante[$i]["porcentaje_bonificacion"];

                    $total_calculando = $cantidad * $precio_unitario;
                    $importe_bonificacion_calculando =  ($total_calculando * $porcentaje_bonificacion) / 100;
                    $subtotal = ($total_calculando - $importe_bonificacion_calculando);

                    // CREACION DEL OBJETO MODEL
                    $detalle_comprobante_obj = new DetalleComprobante();
                    $detalle_comprobante_obj->codigo = $detalle_comprobante[$i]["codigo"];
                    $detalle_comprobante_obj->producto_servicio= $detalle_comprobante[$i]["producto_servicio"];
                    $detalle_comprobante_obj->cantidad= $detalle_comprobante[$i]["cantidad"];
                    $detalle_comprobante_obj->id_unidad_medida= $detalle_comprobante[$i]["id_unidad_medida"];
                    $detalle_comprobante_obj->precio_unitario= (double)$detalle_comprobante[$i]["precio_unitario"];;
                    $detalle_comprobante_obj->porcentaje_bonificacion= (double)$detalle_comprobante[$i]["porcentaje_bonificacion"];;
                    $detalle_comprobante_obj->importe_bonificacion= (double)$importe_bonificacion_calculando;
                    $detalle_comprobante_obj->subtotal= $subtotal;
                    $detalle_comprobante_obj->id_comprobante = $comprobante_ayuda_obj->id;
                    $detalle_comprobante_obj->save();


                    $detalles_comprobantes_objs[] = $detalle_comprobante_obj;

                    $ImpNeto += $subtotal;
                }  
            }
            // RECIBO C
            else if($comprobante_ayuda_obj->id_tipo_de_comprobante == 15) 
            {
                $ImpNeto = $comprobante_ayuda_obj->ImpNeto_new_c;
            }

            // FIN TRABAJANDO CON EL DETALLE DE LA FACTURA

            // TRABAJANDO CON DETALLES DE TRIBUTOS
            $detalle_tributos = $request->session()->get("detalle_tributos_ayuda");
            
            //$total_tributos_calculado = 0;

            // COLECCION DE Tributos
            $detalles_tributos_objs = array();

            for($i=0; $i < count($detalle_tributos);$i++)
            {
                $tributo_obj = new TributoComprobante();
                $tributo_obj->descripcion = $detalle_tributos[$i]["descripcion"];
                $tributo_obj->detalle = $detalle_tributos[$i]["detalle"];
                $tributo_obj->base_imponible = $detalle_tributos[$i]["base_imponible"];
                $tributo_obj->alicuota = $detalle_tributos[$i]["alicuota"];
                $tributo_obj->importe = $detalle_tributos[$i]["importe"];
                $tributo_obj->id_tipo_tributo = $detalle_tributos[$i]["id_tipo"];

                // ME FIJO SI EXISTE EL TIPO DE TRIBUTO, SINO, LO CREO.
                $tipo_tributo_obj = TipoTributo::find($detalle_tributos[$i]["id_tipo"]);

                if(!$tipo_tributo_obj)
                {
                    $tipo_tributo_obj = new TipoTributo();
                    $tipo_tributo_obj->Id = $detalle_tributos[$i]["id_tipo"];
                    $tipo_tributo_obj->Desc = $tributo_obj->Desc;
                    $tipo_tributo_obj->FchDesde = $tributo_obj->FchDesde;
                    $tipo_tributo_obj->FchHasta = $tributo_obj->FchHasta;
                    $tipo_tributo_obj->save();
                }

                $tributo_obj->id_comprobante = $comprobante_ayuda_obj->id;
                $tributo_obj->save();

                $detalles_tributos_objs[] = $tributo_obj;
            }   
            // FIN TRABAJANDO CON DETALLES DE TRIBUTOS

            // TRABAJANDO CON COMPROBANTES ASOCIADOS
            $comprobantes_asociados = $request->session()->get("comprobantes_asociados_ayuda");

            for($i=0; $i < count($comprobantes_asociados);$i++)
            {
                // CREACION DEL OBJETO MODEL
                $comprobantes_asociados_obj = new ComprobanteAsociado();
                $comprobantes_asociados_obj->id_tipo_de_comprobante = $comprobantes_asociados[$i]["id_tipo_de_comprobante"];
                $comprobantes_asociados_obj->id_punto_de_venta = $comprobantes_asociados[$i]["id_punto_de_venta"];
                $comprobantes_asociados_obj->comprobante = $comprobantes_asociados[$i]["comprobante"];
                $comprobantes_asociados_obj->save();
            }  
            // FIN TRABAJANDO CON COMPROBANTES ASOCIADOS

            $response_estructure->set_response(true); 

            try
            {
                $this->ws_afip = new WebserviceAfip($comprobante_ayuda_obj->id_usuario);
                $comprobante_ayuda_obj = $this->ws_afip->crearFacturaWithCAE($comprobante_ayuda_obj,$detalles_comprobantes_objs,$detalles_tributos_objs,$comprobantes_asociados);
                $comprobante_ayuda_obj->save();
            }
            catch(\Exception $e)
            {
                $response_estructure->set_response(false);
                $response_estructure->add_message_error($e->getMessage());
            }

            if($response_estructure->get_response())
            {
                try
                {
                    $comprobante_ayuda_obj->send_comprobante_creado(); // ENVIA AL CORREO RECEPTOR (SI TIENE) 
                }
                catch(\Exception $e)
                {

                }
            }
            
            $this->limpiarSesionComprobante($request);
            
            return response()->json($response_estructure->get_response_array());
        }   
        else
        {
            $tipo_de_comprobante = $comprobante_ayuda_obj->id_tipo_de_comprobante;

            $tipo_de_comprobante_obj = TipoDeComprobante::find($tipo_de_comprobante);

            $titulo_pagina = "CONFIRMAR AYUDA";

            if($tipo_de_comprobante_obj)
            {
                $titulo_pagina.= " ".strtoupper($tipo_de_comprobante_obj->Desc);
            }

            $this->title_page = $titulo_pagina; // THE HARD
            $this->share_parameters();

            
            // PASO 2
            $fecha_del_comprobante = $comprobante_ayuda_obj->fecha;

            $periodo_facturado_desde = $comprobante_ayuda_obj->periodo_facturado_desde;
            $periodo_facturado_hasta = $comprobante_ayuda_obj->periodo_facturado_hasta;
            $periodo_facturado_vto = $comprobante_ayuda_obj->vencimiento_del_pago;

            $fecha_del_comprobante = \DateTime::createFromFormat("Y-m-d",$fecha_del_comprobante);
            $fecha_del_comprobante = $fecha_del_comprobante->format("d/m/Y");

            if($comprobante_ayuda_obj->id_concepto == $comprobante_ayuda_obj::$CONCEPTO_SERVICIOS 
            || $comprobante_ayuda_obj->id_concepto == $comprobante_ayuda_obj::$CONCEPTO_PRODUCTOS_Y_SERVICIOS)
            {
                $periodo_facturado_desde = \DateTime::createFromFormat("Y-m-d",$periodo_facturado_desde);
                $periodo_facturado_desde = $periodo_facturado_desde->format("d/m/Y");

                $periodo_facturado_hasta = \DateTime::createFromFormat("Y-m-d",$periodo_facturado_hasta);
                $periodo_facturado_hasta = $periodo_facturado_hasta->format("d/m/Y");

                $periodo_facturado_vto = \DateTime::createFromFormat("Y-m-d",$periodo_facturado_vto);
                $periodo_facturado_vto = $periodo_facturado_vto->format("d/m/Y");
            }

            $detalle_comprobante = $request->session()->get("detalle_comprobante_ayuda");
            $detalle_tributos = $request->session()->get("detalle_tributos_ayuda");
            $comprobantes_asociados = $request->session()->get("comprobantes_asociados_ayuda");

            
            $this->ws_afip = new WebserviceAfip($comprobante_ayuda_obj->id_usuario);
            $concepto_obj = Concepto::find($comprobante_ayuda_obj->id_concepto);
            $tipos_de_documentos = $this->ws_afip->obtenerTiposDeDocumentosDisponibles(); 

            $condicion_iva_obj = CondicionIva::find($comprobante_ayuda_obj->condicion_iva_receptor);
            $condicion_venta_obj = CondicionDeVenta::find($comprobante_ayuda_obj->id_condicion_venta);

            return View($this->carpeta_views."confirmar_ayuda")
            ->with("comprobante_ayuda_obj",$comprobante_ayuda_obj)
            ->with("tipo_de_comprobante_obj",$tipo_de_comprobante_obj)
            ->with("fecha_del_comprobante",$fecha_del_comprobante)
            ->with("periodo_facturado_desde",$periodo_facturado_desde)
            ->with("periodo_facturado_hasta",$periodo_facturado_hasta)
            ->with("periodo_facturado_vto",$periodo_facturado_vto)
            ->with("detalle_comprobante",$detalle_comprobante)
            ->with("detalle_tributos",$detalle_tributos)
            ->with("comprobantes_asociados",$comprobantes_asociados)
            ->with("concepto_obj",$concepto_obj)
            ->with("tipos_de_documentos",$tipos_de_documentos)
            ->with("condicion_iva_obj",$condicion_iva_obj)
            ->with("condicion_venta_obj",$condicion_venta_obj);  
        }
    }


    private function validar_paso1(Request $request)
    {
        $comprobante_ayuda_obj = $request->session()->get("comprobante_ayuda_obj");

        if($comprobante_ayuda_obj)
        {
            if( 
                empty(trim($comprobante_ayuda_obj->punto_de_venta)) || 
                empty(trim($comprobante_ayuda_obj->id_tipo_de_comprobante))
            )
            {
                return Redirect('/backend/comprobantes/nuevo');
            }
        }
        else
        {
            return false;
        }
    }

    private function validar_paso2(Request $request)
    {
        $validado = true;

        $comprobante_ayuda_obj = $request->session()->get("comprobante_ayuda_obj");

        if($comprobante_ayuda_obj)
        {
            $fecha_del_comprobante_new_c = trim($request->session()->get("fecha_del_comprobante_new_c"));
            $concepto_new_c = trim($request->session()->get("concepto_new_c"));
            $moneda_new_c = trim($request->session()->get("moneda_new_c"));

            if(empty(trim($comprobante_ayuda_obj->fecha)) || empty(trim($comprobante_ayuda_obj->id_concepto)) || empty(trim($comprobante_ayuda_obj->id_moneda)))
            {
                $validado = false;
            }
            else
            {
                if($comprobante_ayuda_obj->id_concepto == $comprobante_ayuda_obj::$CONCEPTO_SERVICIOS 
                || $comprobante_ayuda_obj->id_concepto == $comprobante_ayuda_obj::$CONCEPTO_PRODUCTOS_Y_SERVICIOS)
                {
                    if(empty(trim($comprobante_ayuda_obj->periodo_facturado_desde)) || empty(trim($comprobante_ayuda_obj->periodo_facturado_hasta)) || empty(trim($comprobante_ayuda_obj->vencimiento_de_pago)))
                    {
                        $validado = false;
                    }
                }
            }

            if(!$validado)
            {
                return Redirect('/backend/comprobantes/nuevo');
            }
        }
        else
        {
            $validado = false;
        }

        if(!$validado)
        {
            return Redirect('/backend/comprobantes/nuevo');
        }
    }

    private function clear_session_paso_1(Request $request)
    {
        $comprobante_ayuda_obj = $request->session()->get("comprobante_ayuda_obj");

        if($comprobante_ayuda_obj)
        {
            $comprobante_ayuda_obj->punto_de_venta = null;
            $comprobante_ayuda_obj->id_tipo_de_comprobante = null;

            $request->session()->put("comprobante_ayuda_obj",$comprobante_ayuda_obj);
        }
    }

    private function clear_session_paso_2(Request $request)
    {
        /*$comprobante_ayuda_obj = $request->session()->get("comprobante_ayuda_obj");

        if($comprobante_ayuda_obj)
        {
            $comprobante_ayuda_obj->fecha = null;
            $comprobante_ayuda_obj->id_concepto = null;
            $comprobante_ayuda_obj->id_moneda = null;
            $comprobante_ayuda_obj->cotizacion_de_la_moneda = null;
            $comprobante_ayuda_obj->periodo_facturado_desde = null;
            $comprobante_ayuda_obj->periodo_facturado_hasta = null;
            $comprobante_ayuda_obj->vencimiento_del_pago = null;

            $request->session()->put("comprobante_ayuda_obj",$comprobante_ayuda_obj);
        }*/
    }

    private function clear_session_paso_3(Request $request)
    {
        /*$comprobante_ayuda_obj = $request->session()->get("comprobante_ayuda_obj");

        if($comprobante_ayuda_obj)
        {
            $comprobante_ayuda_obj->condicion_iva_receptor = null;
            $comprobante_ayuda_obj->tipo_documento_receptor = null;
            $comprobante_ayuda_obj->num_documento_receptor = null;
            $comprobante_ayuda_obj->razon_social_receptor = null;
            $comprobante_ayuda_obj->correo_receptor = null;
            $comprobante_ayuda_obj->domicilio_receptor = null;
            $comprobante_ayuda_obj->id_condicion_venta = null;
            $comprobante_ayuda_obj->ImpTotal = null;
            $comprobante_ayuda_obj->ImpTotConc = null;
            $comprobante_ayuda_obj->ImpNeto = null;
            $comprobante_ayuda_obj->ImpOpEx = null;
            $comprobante_ayuda_obj->ImpIVA = null;
            $comprobante_ayuda_obj->ImpTrib = null;

            $request->session()->put("detalle_comprobante_ayuda",null);
            $request->session()->put("detalle_tributos_ayuda",null);
            $request->session()->put("comprobantes_asociados_ayuda",null);

            $request->session()->put("comprobante_ayuda_obj",$comprobante_ayuda_obj);
        }*/
    }
    
    private function limpiarSesionComprobante(Request $request)
    {
        $this->clear_session_paso_2($request);
        $this->clear_session_paso_3($request);
    }
}

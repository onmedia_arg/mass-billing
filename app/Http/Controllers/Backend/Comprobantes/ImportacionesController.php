<?php
namespace App\Http\Controllers\Backend\Comprobantes;

use Illuminate\Http\Request;
use App\Http\Controllers\Backend\ABM_Core;

use Illuminate\Support\Facades\Mail;
use App\Mail\EmailSender;

use App\Library\ResponseEstructure;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

use Mpdf\Mpdf;
use DNS1D;

use App\Comprobante;
use App\DetalleComprobante;
use App\ComprobanteAsociado;
use App\TributoComprobante;
use App\TipoTributo;
use App\TipoDeComprobante;

use App\Concepto;
use App\UnidadDeMedida;
use App\CondicionIva;
use App\CondicionDeVenta;

use App\ProductoServicio;

use App\PuntoDeVenta;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ImportacionesController extends ABM_Core
{
    protected $ws_afip = null;

    public function __construct()
    {
        $this->link_controlador = url('/backend/comprobantes/importaciones').'/';
        $this->carpeta_views = "backend.comprobantes.importaciones.";

        $this->entity ="Importación";
        $this->title_page = "Importaciones";
        
        $this->columns = [
          ["name"=>"#","reference" => "importacion_comprobantes.id"],
          ["name"=>"Fecha","reference" => DB::raw("DATE_FORMAT(importacion_comprobantes.created_at,'%d/%m%Y %H:%i')")],
          ["name"=>"Total","reference" => "importacion_comprobantes.cantidad_total"],
          ["name"=>"Realizados","reference" => "importacion_comprobantes.cantidad_realizados"],
          ["name"=>"Errores","reference" => "importacion_comprobantes.cantidad_con_errores"],
          ["name"=>"Pendientes","reference" => "importacion_comprobantes.cantidad_pendientes"],
          ["name"=>"Estado","reference" => "importacion_comprobantes.estado_importacion_comprobantes"],
          ["name"=>"Acciones","reference" => null]
        ];

        $this->add_active = false;
        $this->edit_active = false;
        $this->delete_active = false;
        $this->is_ajax = true;
    }

    public function index(Request $request)
    {
        $this->share_parameters();

        $ver = $request->input("ver");

        return View($this->carpeta_views."browse")
        ->with("ver",$ver); 
    }

    public function get_listado_dt(Request $request)
    {
        $consulta_orm_principal = DB::table("importacion_comprobantes")
        ->join("estados_importaciones_comprobantes","estados_importaciones_comprobantes.id","=","importacion_comprobantes.id_estado")
        ->select(
            "importacion_comprobantes.*",
            "estados_importaciones_comprobantes.id as estados_importaciones_comprobantes_id",
            "estados_importaciones_comprobantes.color as estados_importaciones_comprobantes_color",
            "estados_importaciones_comprobantes.estado as estados_importaciones_comprobantes_estado"
        );

    	$totalData = $consulta_orm_principal->count();

        $totalFiltered = $totalData;

        $search = $request->input("search");
        $start = $request->input('start');
        $length = $request->input('length');
        $order = $request->input('order');

        $resultado = array();

        if(!empty($search['value']))
        {
            $consulta_orm_principal = $consulta_orm_principal
            ->where(function($query) use($search){
                $query->where("importacion_comprobantes.id","like","%".$search['value']."%");
            });
        }

        $consulta_orm_principal = $consulta_orm_principal->take($length)->skip($start);
        $totalFiltered=$consulta_orm_principal->count();

        $columna_a_ordenar = (int)$order[0]["column"];

        if(isset($this->columns[$columna_a_ordenar]) && $this->columns[$columna_a_ordenar]["reference"] != null){
          $resultado = $consulta_orm_principal->orderBy($this->columns[$columna_a_ordenar]["reference"],$order[0]["dir"])->get();
        }
        else{
          $resultado = $consulta_orm_principal->orderBy("id","desc")->get();
        }

        $data= array();

        foreach($resultado as $result_row)
        {
            $row_of_data = array();

            $fecha = \DateTime::createFromFormat("Y-m-d H:i:s",$result_row->created_at);

            $row_of_data[]=strip_tags($result_row->id);
            $row_of_data[]=strip_tags($fecha->format("d/m/Y H:i")." HS");
            $row_of_data[]=strip_tags($result_row->cantidad_total);
            $row_of_data[]=strip_tags($result_row->cantidad_realizados);
            $row_of_data[]=strip_tags($result_row->cantidad_con_errores);
            $row_of_data[]=strip_tags($result_row->cantidad_pendientes);

            $row_of_data[]='<span class="badge text-white" style="background-color: '.strip_tags($result_row->estados_importaciones_comprobantes_color).'">'.strip_tags($result_row->estados_importaciones_comprobantes_estado).'</span>';
            
            $buttons_actions = "<div class='form-button-action'>";

            $buttons_actions.= 
            "<a href='".$this->link_controlador."detalle_importacion/".$result_row->id."' data-toggle='tooltip' title='' class='btn-sm ".$this->config_buttons["watch"]["class"]."' data-original-title='".$this->config_buttons["watch"]["title"]."'>
                <i class='".$this->config_buttons["watch"]["icon"]."'></i> DETALLE
            </a>";

            if($this->edit_active)
            {
                if($this->is_ajax)
                {
                    $buttons_actions .=
                    "<button onclick='abrir_modal_editar(".$result_row->id.")' type='button' data-toggle='tooltip' title='' class='btn-sm ".$this->config_buttons["edit"]["class"]."' data-original-title='".$this->config_buttons["edit"]["title"]."'>
                        <i class='".$this->config_buttons["edit"]["icon"]."'></i>
                    </button>";
                }
                else{
                    $buttons_actions .=
                    "<a href='".$this->link_controlador."editar/".$result_row->id."' data-toggle='tooltip' title='' class='btn-sm ".$this->config_buttons["edit"]["class"]."' data-original-title='".$this->config_buttons["edit"]["title"]."'>
                        <i class='".$this->config_buttons["edit"]["icon"]."'></i>
                    </a>";
                }
            }

            if($this->delete_active)
            {
                $buttons_actions.=
                "<button onclick='abrir_modal_eliminar(".$result_row->id.")' type='button' data-toggle='tooltip' title='' class='btn-sm ".$this->config_buttons["delete"]["class"]."' data-original-title='".$this->config_buttons["delete"]["title"]."'>
                    <i class='".$this->config_buttons["delete"]["icon"]."'></i>
                </button>";
            }

            $buttons_actions.="</div>";


            $row_of_data[]=$buttons_actions;

            $data[]=$row_of_data;
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

    	return response()->json($json_data);
    }

    public function generacion_pdf(Request $request,$id_comprobante)
    {
        $comprobante_obj = Comprobante::find($id_comprobante);

        if($comprobante_obj){

            $base_64_code = $comprobante_obj->get_base_64_comprobante();
            return response($base_64_code)->header("Content-type","application/pdf");
        } 
    }

    public function descargarTemplateImportacion(Request $request)
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->getColumnDimension('A')->setWidth(15);
        $sheet->getColumnDimension('B')->setWidth(15);
        $sheet->getColumnDimension('C')->setWidth(100);
        $sheet->getColumnDimension('D')->setWidth(13);
        $sheet->getColumnDimension('E')->setWidth(20);
    
        $sheet->setCellValue('A1', 'ID');
        $sheet->setCellValue('B1', 'NOMBRE');
        $sheet->setCellValue('C1', 'DESCRIPCION');
        $sheet->setCellValue('D1', 'PRECIO');
        $sheet->setCellValue('E1', 'CATEGORIA');

        $cells_with_styles = array('A1','B1','C1','D1','E1');

        foreach($cells_with_styles as $cell_with_style)
        {
            $sheet->getStyle($cell_with_style)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('1269db');
            $sheet->getStyle($cell_with_style)->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
            $sheet->getStyle($cell_with_style)->getFont()->setBold(true);
        }

        $numRow = 2;

        $sheet->setCellValue('A'.$numRow, "");
        $sheet->setCellValue('B'.$numRow, "CARTERA");
        $sheet->setCellValue('C'.$numRow, "DESCRIPCION CARTERA");
        $sheet->setCellValue('D'.$numRow, "200,50");
        $sheet->setCellValue('E'.$numRow, "1");
        
        $writer = new Xlsx($spreadsheet);
 
        $filename = 'template-importacion';
 
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"'); 
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        
        $writer->save('php://output');
    }
}

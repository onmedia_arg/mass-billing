<?php
namespace App\Http\Controllers\Backend\Planes;

use Illuminate\Http\Request;
use App\Http\Controllers\Backend\ABM_Core;

use App\Library\ResponseEstructure;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

use App\Plan;

class PlanesController extends ABM_Core
{
    public function __construct()
    {
        $this->link_controlador = url('/backend/configuraciones/planes').'/';
        $this->entity ="Plan";
        $this->title_page = "Planes";

        $this->columns = [
            ["name"=>"#","reference"=>"planes.id"],
            ["name"=>"Nombre","reference"=>"planes.nombre"],
            ["name"=>"Precio","reference"=>"planes.precio"],
            ["name"=>"Activo","reference"=>"planes.activo"],
            ["name"=>"Acciones","reference"=>null]
        ];

        $this->is_ajax = true;
        $this->edit_active = true;
        $this->delete_active = true;
    }
    
    public function index(Request $request)
    {
        $this->validar_administrador();
        
        $this->share_parameters();
        return View("backend.planes.browse");
    }

    public function get_listado_dt(Request $request)
    {
        $this->validar_administrador();

        $consulta_orm_principal = DB::table("planes")
        ->select(
            "planes.*"
        )
        ->where("eliminado",0);

    	$totalData = $consulta_orm_principal->count();
        
        $totalFiltered = $totalData;
        
        $search = $request->input("search");
        $start = $request->input('start');
        $length = $request->input('length');
        $order = $request->input('order');

        $resultado = array();

        if(!empty($search['value']))
        {
            $consulta_orm_principal = $consulta_orm_principal
            ->where(function($query) use($search){
                $query->where("planes.id","like","%".$search['value']."%")
                ->orWhere("planes.nombre","like","%".$search['value']."%")
                ->orWhere("planes.precio","like","%".$search['value']."%")
                ->orWhere("planes.activo","like","%".$search['value']."%");
            }) ;
        }

        $consulta_orm_principal = $consulta_orm_principal->take($length)->skip($start);
        $totalFiltered=$consulta_orm_principal->count();

        $columna_a_ordenar = (int)$order[0]["column"];

        if(isset($this->columns[$columna_a_ordenar]) && $this->columns[$columna_a_ordenar]["reference"] != null){
          $resultado = $consulta_orm_principal->orderBy($this->columns[$columna_a_ordenar]["reference"],$order[0]["dir"])->get();
        }
        else{
          $resultado = $consulta_orm_principal->orderBy("id","desc")->get();
        }
    	
        $data= array();
        
        foreach($resultado as $result_row)
        {
            $row_of_data = array();

            $row_of_data[]=strip_tags($result_row->id);
            $row_of_data[]=strip_tags($result_row->nombre);
            $row_of_data[]=strip_tags(number_format($result_row->precio,2,",","."));

            if($result_row->activo == true)
            {
                $row_of_data[]='<span class="badge text-white" style="background-color: #28a745;">SI</span>';
            }
            else
            {
                $row_of_data[]='<span class="badge text-white" style="background-color: #dc3545;">NO</span>';
            }

            $buttons_actions = "<div class='form-button-action'>";

            if($this->edit_active)
            {
                if($this->is_ajax)
                {
                    $buttons_actions .= 
                    "<button onclick='abrir_modal_editar(".$result_row->id.")' type='button' data-toggle='tooltip' title='' class='btn-sm ".$this->config_buttons["edit"]["class"]."' data-original-title='".$this->config_buttons["edit"]["title"]."'>
                        <i class='".$this->config_buttons["edit"]["icon"]."'></i>
                    </button>";
                }
                else{
                    $buttons_actions .= 
                    "<a href='".$this->link_controlador."editar/".$result_row->id."' data-toggle='tooltip' title='' class='btn-sm ".$this->config_buttons["edit"]["class"]."' data-original-title='".$this->config_buttons["edit"]["title"]."'>
                        <i class='".$this->config_buttons["edit"]["icon"]."'></i>
                    </a>";
                }
            }

            if($this->delete_active)
            {
                $buttons_actions.=
                "<button onclick='eliminar(".$result_row->id.")' type='button' data-toggle='tooltip' title='' class='btn-sm ".$this->config_buttons["delete"]["class"]."' data-original-title='".$this->config_buttons["delete"]["title"]."'>
                    <i class='".$this->config_buttons["delete"]["icon"]."'></i>
                </button>";
            }

            $buttons_actions.="</div>";
            
            $row_of_data[]=$buttons_actions;

            $data[]=$row_of_data;
        }

		$json_data = array(
            "draw"            => intval($request->input('draw')), 
            "recordsTotal"    => intval($totalData), 
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data
        );

    	return response()->json($json_data);
    }

    public function get(Request $request)
    {
        $this->validar_administrador();

        $response_estructure = new ResponseEstructure();
        $response_estructure->set_response(false);

        $id = $request->input("id");

        $row_obj = Plan::find($id);
        
        if($row_obj)
        {
            $response_estructure->set_response(true);
            $response_estructure->set_data($row_obj);
        }
        else
        {
            $response_estructure->set_response(false);
            $response_estructure->add_message_error($this->text_no_search);
        }
        

        return response()->json($response_estructure->get_response_array());
    }

    public function store(Request $request)
    {
        $this->validar_administrador();

        $response_estructure = new ResponseEstructure();
        $response_estructure->set_response(false);

        $nombre = trim(($request->input("nombre")));
        $descripcion = trim(($request->input("descripcion")));
        $precio = trim(($request->input("precio")));
        $activo = trim(($request->input("activo")));

        $input= [
            "nombre"=>$nombre,
            "descripcion"=>$descripcion,
            "precio"=>$precio,
            "activo"=>$activo,
        ];

        $rules = [
            "nombre"=>"required",
            "descripcion"=>"required",
            "precio"=>"required|regex:/^\d+(\.\d{1,2})?$/",
            "activo"=>"required",
        ];

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            $response_estructure->set_response(false);

            $errors = $validator->errors();

            foreach ($errors->all() as $error) {
                $response_estructure->add_message_error($error);
            }
        }
        else
        {
            $row_obj = new Plan();
            $row_obj->nombre = $nombre;
            $row_obj->descripcion = $descripcion;
            $row_obj->precio = $precio;
            $row_obj->activo = $activo;
            $row_obj->save();

            $response_estructure->set_response(true);
        }

        return response()->json($response_estructure->get_response_array());
    }

    public function editar(Request $request,$id)
    {
        $this->validar_administrador();

        $this->title_page = $this->config_buttons["edit"]["title"]." ".$this->entity;
        $this->share_parameters();

        $row_obj = Usuario::find($id);

        if($row_obj && $row_obj->eliminado == false)
        {
            $row_obj->inicio_actividades = \DateTime::createFromFormat("Y-m-d",$row_obj->inicio_actividades);
            $row_obj->inicio_actividades = $row_obj->inicio_actividades->format("d/m/Y");
            
            $estados_usuarios = EstadoUsuario::all();
            $condiciones_frente_al_iva = CondicionIva::all();

            return View("backend.usuarios.edit")
            ->with("estados_usuarios",$estados_usuarios)
            ->with("row_obj",$row_obj)
            ->with("condiciones_frente_al_iva",$condiciones_frente_al_iva);
        }
    }

    public function update(Request $request)
    {
        $this->validar_administrador();

        $response_estructure = new ResponseEstructure();
        $response_estructure->set_response(false);

        $id = $request->input("id");

        $row_obj = Plan::find($id);
        
        if($row_obj && $row_obj->eliminado == false)
        {
            $nombre = trim(($request->input("nombre")));
            $descripcion = trim(($request->input("descripcion")));
            $precio = trim(($request->input("precio")));
            $activo = trim(($request->input("activo")));

            $input= [
                "nombre"=>$nombre,
                "descripcion"=>$descripcion,
                "precio"=>$precio,
                "activo"=>$activo,
            ];

            $rules = [
                "nombre"=>"required",
                "descripcion"=>"required",
                "precio"=>"required|regex:/^\d+(\.\d{1,2})?$/",
                "activo"=>"required",
            ];

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $response_estructure->set_response(false);

                $errors = $validator->errors();

                foreach ($errors->all() as $error) {
                    $response_estructure->add_message_error($error);
                }
            }
            else
            {
                $row_obj->nombre = $nombre;
                $row_obj->descripcion = $descripcion;
                $row_obj->precio = $precio;
                $row_obj->activo = $activo;
                $row_obj->save();

                $response_estructure->set_response(true);
            }
        }
        else
        {
            $response_estructure->set_response(false);
            $response_estructure->add_message_error("Usuario no encontrado");
        }
        
        return response()->json($response_estructure->get_response_array());
    }

    public function delete(Request $request)
    {
        $this->validar_administrador();
        
        $response_estructure = new ResponseEstructure();
        $response_estructure->set_response(false);

        $id = $request->input("id");

        $row_obj = Plan::find($id);
        
        if($row_obj)
        {
            $row_obj->eliminado = 1;
            $row_obj->save();

            $response_estructure->set_response(true);
        }
        else
        {
            $response_estructure->set_response(false);
            $response_estructure->add_message_error($this->text_no_search);
        }


        return response()->json($response_estructure->get_response_array());
    }
}

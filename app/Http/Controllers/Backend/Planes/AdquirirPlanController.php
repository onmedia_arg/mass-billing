<?php

namespace App\Http\Controllers\Backend\Planes;

use Illuminate\Http\Request;
use App\Http\Controllers\Backend\ABM_Core;

use App\Library\ResponseEstructure;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

use App\Plan;
use App\ConfiguracionMercadoPago;
use MP; // MERCADOPAGO LIBRARY
use App\CobroPagoServicio;
use App\EstadoCobroServicio;

class AdquirirPlanController extends ABM_Core
{
    public function __construct()
    {
        $this->link_controlador = url('/backend/planes/adquirir').'/';
        $this->entity ="Plan";
        $this->title_page = "Planes";

        $this->columns = [
            
        ];

        $this->is_ajax = true;
        $this->edit_active = true;
        $this->delete_active = true;
    }

    public function index(Request $request)
    {
        $cobro_pago_servicio = CobroPagoServicio::where("id_usuario",$request->session()->get("id"))
        ->where("id_estado",EstadoCobroServicio::ACTIVO)
        ->first();

        // SI NO TIENE UN SERVICIO ACTIVO
        if(!$cobro_pago_servicio)
        {
            $this->share_parameters();
            $planes = Plan::where("eliminado",0)->where("activo",1)->orderBy("precio")->get();

            $cobro_pago_servicio = CobroPagoServicio::where("id_usuario",$request->session()->get("id"))
            ->where(function($query){
                $query->where("id_estado",EstadoCobroServicio::PAGO_PENDIENTE);
            })
            ->first();

            return View("backend.planes.adquirir.browse")
            ->with("planes",$planes)
            ->with("cobro_pago_servicio",$cobro_pago_servicio);
        }
        // SI YA TIENE SERVICIO
        else
        {
            return Redirect("/backend/desktop");
        }
    }

    public function adquirir_plan(Request $request,$id_plan)
    {
        $config_mp = ConfiguracionMercadoPago::find(1);
        
        $plan_seleccionado = Plan::where("eliminado",0)
        ->where("activo",1)
        ->where("id",$id_plan)
        ->first();

        if($plan_seleccionado && $config_mp)
        {
            $cobro_pago_servicio = new CobroPagoServicio();
            $cobro_pago_servicio->id_usuario = $request->session()->get("id");
            $cobro_pago_servicio->id_plan = $plan_seleccionado->id;
            $cobro_pago_servicio->duracion_en_meses = 1;
            $cobro_pago_servicio->id_mercado_pago = null;
            $cobro_pago_servicio->precio = $plan_seleccionado->precio;
            $cobro_pago_servicio->id_estado = EstadoCobroServicio::PENDIENTE;
            $cobro_pago_servicio->save();

            $CLIENTE_ID_AND_SECRET = $this->get_client_id_and_secret($config_mp);

            $CLIENT_ID = $CLIENTE_ID_AND_SECRET["CLIENT_ID"];
            $CLIENT_SECRET = $CLIENTE_ID_AND_SECRET["CLIENT_SECRET"];

            $MercadoPagoLibrary = new MP($CLIENT_ID, $CLIENT_SECRET);

            $access_token = $MercadoPagoLibrary->get_access_token();
            $data = array('site_id' => 'MLA');

            $preference_data = array(
                "external_reference" => encrypt("pago_numero_".$cobro_pago_servicio->id),
                "items" => array(
                    array(
                        "id" => "pago_numero_".$cobro_pago_servicio->id,
                        "title" => $plan_seleccionado->nombre. " - Pago #".$cobro_pago_servicio->id,
                        "category_id" => "Ventas",
                        "quantity" => 1,
                        "unit_price" => floatval(number_format((float) $cobro_pago_servicio->precio, 2, '.', '')),
                    )
                ),
                "auto_return" => "approved",
                "back_urls" => array(
                    "failure" => url("/mercado_pago/respuesta_pago"),
                    "pending" => url("/mercado_pago/respuesta_pago"),
                    "success" => url("/mercado_pago/respuesta_pago"),
                )
            );

            $preference = $MercadoPagoLibrary->create_preference($preference_data);

            return Redirect($preference['response']['init_point']);
        }
        else
        {
            echo "NO SE ENCONTRÓ EL PLAN SELECCIONADO";
        }
    }

    private function get_client_id_and_secret(ConfiguracionMercadoPago $config_mp)
    {
        $CLIENT_ID = $config_mp->CLIENT_ID;
        $CLIENT_SECRET = $config_mp->CLIENT_SECRET;

        if($config_mp->EN_PRODUCCION == false)
        {
            $CLIENT_ID = $config_mp->CLIENT_ID_SANDBOX;
            $CLIENT_SECRET = $config_mp->CLIENT_SECRET_SANDBOX;
        }

        return ["CLIENT_ID"=>$CLIENT_ID,"CLIENT_SECRET"=>$CLIENT_SECRET];
    }
}

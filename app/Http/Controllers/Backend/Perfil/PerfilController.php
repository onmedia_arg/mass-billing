<?php

namespace App\Http\Controllers\Backend\Perfil;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Backend\ABM_Core;
use App\Usuario;
use App\Library\ResponseEstructure;

class PerfilController extends ABM_Core
{
    public function __construct()
    {
        $this->link_controlador = url('/backend/perfil').'/';
        $this->entity ="Mi Perfil";
        $this->title_page = "Mi Perfil";
        $this->abm_messages["success_edit"]["description"] = "Se ha editado correctamente el perfil!";
    }
    
    public function index(Request $request)
    {
        $this->share_parameters();

        $row_obj = Usuario::find($request->session()->get("id"));

        return View("backend.perfil.mi_perfil")
        ->with("row_obj",$row_obj);
    }

    public function update(Request $request)
    {
        $response_estructure = new ResponseEstructure();
        $response_estructure->set_response(false);

        $id = $request->session()->get("id");

        $usuario_obj = Usuario::find($id);
        
        if($usuario_obj && $usuario_obj->eliminado == false)
        {
            $dni = trim(strtolower($request->input("dni")));
            $cuit = trim(strtolower($request->input("cuit")));
            $dni_parte_delantera = trim(strtolower($request->input("dni_parte_delantera")));
            $dni_parte_trasera = trim(strtolower($request->input("dni_parte_trasera")));

            $correo = trim(strtolower($request->input("correo")));
            $nombre = trim(ucwords($request->input("nombre")));
            $apellido = trim(ucwords($request->input("apellido")));
            $foto_perfil = trim(ucwords($request->input("foto_perfil")));

            $password = trim($request->input("password_new"));
            $password2 = trim($request->input("password_new2"));

            $pais = trim($request->input("pais"));
            $provincia = trim($request->input("provincia"));
            $ciudad = trim($request->input("ciudad"));
            $telefono = (int) $request->input("telefono");
            $caracteristica_telefono = (int) $request->input("caracteristica_telefono");

            $input= [
                "dni"=>$dni,
                "cuit"=>$cuit,
                "dni_parte_delantera"=>$dni_parte_delantera,
                "dni_parte_trasera"=>$dni_parte_trasera,
                "correo"=>$correo,
                "nombre"=>$nombre,
                "apellido"=>$apellido,
                "foto_perfil"=>$foto_perfil, 

                "pais"=>$pais, 
                "provincia"=>$provincia, 
                "ciudad"=>$ciudad, 
                "telefono"=>$telefono, 
                "caracteristica_telefono"=>$caracteristica_telefono, 
            ];

            $rules = [
                "dni"=>"required|numeric|min:10000000|max:99999999|unique:usuarios,dni,".$id,
                "cuit"=>"required|numeric|min:10000000000|max:99999999999|unique:usuarios,cuit,".$id,
                "dni_parte_delantera"=>"required",
                "dni_parte_trasera"=>"required",

                "correo"=>"required|email|unique:usuarios,correo,".$id,
                "nombre"=>"required|min:3",
                "apellido"=>"required|min:3",

                
                "pais"=>"required|min:3",
                "provincia"=>"required|min:3",
                "ciudad"=>"required|min:3",
                "telefono"=>"required|numeric|min:100000|max:99999999",
                "caracteristica_telefono"=>"required|numeric|min:1|max:9999",
            ];

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $response_estructure->set_response(false);

                $errors = $validator->errors();

                foreach ($errors->all() as $error) {
                    $response_estructure->add_message_error($error);
                }
            }
            else
            {
                $response_estructure->set_response(true);

                
                if($password != "" || $password2 != "")
                {
                    if($password != $password2)
                    {
                        $response_estructure->set_response(false);
                        $response_estructure->add_message_error("Las contraseñas ingresadas no coinciden");
                    }
                    else
                    {
                        $validator = Validator::make(
                            ["password"=>$password], 
                            ["password"=>"required|min:8"]
                        );

                        if ($validator->fails()) {

                            $response_estructure->set_response(false);
                            
                            $errors = $validator->errors();

                            foreach ($errors->all() as $error) {
                                $response_estructure->add_message_error($error);
                            }
                        }
                    }
                }

                if($response_estructure->get_response() === TRUE)
                {
                    $usuario_obj->dni = $dni;
                    $usuario_obj->cuit = $cuit;
                    $usuario_obj->dni_parte_delantera = $dni_parte_delantera;
                    $usuario_obj->dni_parte_trasera = $dni_parte_trasera;
                    $usuario_obj->foto_perfil = $foto_perfil;

                    $usuario_obj->correo = $correo;
                    $usuario_obj->nombre = $nombre;
                    $usuario_obj->apellido = $apellido;

                    $usuario_obj->pais = $pais;
                    $usuario_obj->provincia = $provincia;
                    $usuario_obj->ciudad = $ciudad;
                    $usuario_obj->telefono = $telefono;
                    $usuario_obj->caracteristica_telefono = $caracteristica_telefono;

                    if($password != "")
                    {
                        $usuario_obj->password = bcrypt($password);
                    }
                    
                    $usuario_obj->save();
                }
            }
        }
        else
        {
            $response_estructure->set_response(false);
            $response_estructure->add_message_error("Usuario no encontrado");
        }
        
        return response()->json($response_estructure->get_response_array());
    }
}

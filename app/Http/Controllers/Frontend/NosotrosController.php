<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NosotrosController extends Controller
{
  public function index(Request $request)
  {
    return View("frontend.nosotros");
  }
}

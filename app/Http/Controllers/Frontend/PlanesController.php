<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Library\GoogleRecaptchaV3;
use App\Library\ResponseEstructure;
use Illuminate\Support\Facades\Validator;

use App\Plan;
use App\SolicitudRegistro;

class PlanesController extends Controller
{
    public function index(Request $request)
    {
        return View("frontend.planes.index");
    }

    public function adquirir(Request $request,$id_plan)
    {
        $plan_obj = Plan::where("eliminado",0)
        ->where("activo",1)
        ->where("id",$id_plan)
        ->first();

        if($plan_obj)
        {
            return View("frontend.planes.adquirir")
            ->with("plan_obj",$plan_obj);
        }
    }

    public function saveSolicitudRegistro(Request $request)
    {
        $response_estructure = new ResponseEstructure();
        $response_estructure->set_response(false);

        $token = $request->input("token");
        $id_plan =   trim($request->input("id_plan"));
        $cuit =   trim($request->input("cuit"));
        $nombre =   trim($request->input("nombre"));
        $apellido = trim($request->input("apellido"));
        $correo =   trim($request->input("correo"));
        $telefono = trim($request->input("telefono"));

        $input= [
            "cuit"=>$cuit,
            "nombre"=>$nombre,
            "apellido"=>$apellido,
            "correo"=>$correo,
            "telefono"=>$telefono,
            "id_plan" => $id_plan
        ];

        $rules = [
            "cuit"=>"required|min:11|max:13",
            "nombre"=>"required|min:3",
            "apellido"=>"required|min:3",
            "correo"=>"required|email",
            "telefono"=>"required|min:6",
            "id_plan" => "required"
        ];

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            $response_estructure->set_response(false);

            $errors = $validator->errors();

            foreach ($errors->all() as $error) {
                $response_estructure->add_message_error($error);
            }
        }
        else
        {
            $GoogleRecaptchaV3 = new GoogleRecaptchaV3();

            if($GoogleRecaptchaV3->validate($token,0.6))
            {
                $solicitud_registro = new SolicitudRegistro();
                $solicitud_registro->cuit = $cuit;
                $solicitud_registro->nombre = $nombre;
                $solicitud_registro->apellido = $apellido;
                $solicitud_registro->correo = $correo;
                $solicitud_registro->telefono = $telefono;
                $solicitud_registro->id_plan = $id_plan;
                $solicitud_registro->save();

                $response_estructure->set_response(true);
            }
            else
            {
                $response_estructure->set_response(false);
                $response_estructure->add_message_error("Ha ocurrido un error");
            }
        }

        return response()->json($response_estructure->get_response_array());
    }
}
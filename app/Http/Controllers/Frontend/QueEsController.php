<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QueEsController extends Controller
{
    public function index(Request $request)
    {
        return View("frontend.que_es");
    }
}

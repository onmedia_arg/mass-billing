<?php

namespace App\Http\Controllers\Frontend\Login;

use Illuminate\Http\Request;
use App\Http\Controllers\Backend\Controller;
use App\Library\SessionHelper;
use Illuminate\Support\Facades\Hash;

use App\Library\ResponseEstructure;
use Illuminate\Support\Facades\Validator;

use App\Usuario;

use App\Library\LoginCore;

class LoginController extends Controller
{
    public function index(Request $request)
    {
        return View("frontend.login.login");
    }

    public function recuperar_datos()
    {
        return View("frontend.login.forgot");
    }

    public function ingresar(Request $request)
    {
        $login_core = new LoginCore();
        $respuesta = $login_core->ingresar($request);

        if($respuesta["response"] == true)
        {
            $respuesta["redirect"] = url('/');
        }

        return response()->json($respuesta);
    }

    public function registrarse(Request $request)
    {
        return View("frontend.login.register");
    }

    public function registro(Request $request)
    {
        $response_estructure = new ResponseEstructure();
        $response_estructure->set_response(false);

        $nombre = trim(strtolower($request->input("nombre")));
        $correo = trim(strtolower($request->input("correo")));
        $telefono = trim(strtolower($request->input("telefono")));
        $terminos_y_condiciones = json_decode($request->input("terminos_y_condiciones"));

        $input= [
            "nombre_y_apellido"=>$nombre,
            "correo"=>$correo,
            "telefono"=>$telefono, 
        ];

        $rules = [
            "nombre_y_apellido"=>"required|min:7",
            "correo"=>"required|email|unique:usuarios,correo",
            "telefono"=>"required",
        ];

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            $response_estructure->set_response(false);

            $errors = $validator->errors();

            foreach ($errors->all() as $error) {
                $response_estructure->add_message_error($error);
            }
        }
        else
        {
            $response_estructure->set_response(true);

            if($terminos_y_condiciones !== true)
            {
                $response_estructure->add_message_error("Debe aceptar los terminos y condiciones");
                $response_estructure->set_response(false);
            }

            if($response_estructure->get_response() === TRUE)
            {
                $solicitud_registro_obj = new \App\SolicitudRegistro();

                $solicitud_registro_obj->nombre = $nombre;
                $solicitud_registro_obj->correo = $correo;
                $solicitud_registro_obj->telefono = $telefono;
                $solicitud_registro_obj->save();

                $response_estructure->set_response(true);
            }
        }

        $response = $response_estructure->get_response_array();

        $response["redirect"] = url('/');

        return response()->json($response);
    }

    public function forgot_password(Request $request)
    {
        $login_core = new LoginCore();
        return response()->json($login_core->forgot_password($request));
    }

    public function cerrar_sesion(Request $request)
    {
        $login_core = new LoginCore();
        $login_core->cerrar_sesion($request);

        return Redirect("/");
    }
}
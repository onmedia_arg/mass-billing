<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Mail\EmailSender;

use App\Library\GoogleRecaptchaV3;
use App\Library\ResponseEstructure;
use Illuminate\Support\Facades\Validator;

use App\Configuracion;

class ContactoController extends Controller
{
  public function index()
  {
    return View("frontend.contacto");
  }

  public function enviado()
  {
    return View("frontend.contacto_enviado");
  }

  public function enviar_mensaje(Request $request)
  {
    $response_estructure = new ResponseEstructure();
    $response_estructure->set_response(false);

    $token = $request->input("token");
    $nombre = $request->input("nombre");
    $correo = $request->input("correo");
    $telefono = $request->input("telefono");
    $mensaje = $request->input("mensaje");

    $input = [
      "nombre" => $nombre,
      "correo" => $correo,
      "telefono" => $telefono,
      "mensaje" => $mensaje,
    ];

    $rules = [
      "nombre" => "required|min:3",
      "correo" => "required|email",
      "telefono" => "required|min:6",
      "mensaje" => "required|min:10"
    ];

    $validator = Validator::make($input, $rules);

    if ($validator->fails()) {
        $response_estructure->set_response(false);

        $errors = $validator->errors();

        foreach ($errors->all() as $error) {
            $response_estructure->add_message_error($error);
        }
    }
    else
    {
      $GoogleRecaptchaV3 = new GoogleRecaptchaV3();

      if($GoogleRecaptchaV3->validate($token,0.6))
      {
        $email_sender = new \stdClass();
        $email_sender->title = "Nuevo Contacto desde la web";
        $email_sender->nombre = $nombre;
        $email_sender->correo =$correo;
        $email_sender->telefono = $telefono;
        $email_sender->mensaje = $mensaje;

        $email_notificacion_contacto = Configuracion::where("clave","MAIL_NOTIFICACION_CONTACTO")->first();

        if($email_notificacion_contacto)
        {
          Mail::to($email_notificacion_contacto->valor)->send(
              new EmailSender(
                  $email_sender,$email_sender->title,
                  "emails.nuevo_contacto_desde_la_web"
              )
          );
        }

        $response_estructure->set_response(true);
      }
      else
      {
        $response_estructure->set_response(false);
        $response_estructure->add_message_error("Ha ocurrido un error");
      }
    }

    return response()->json($response_estructure->get_response_array());
  }
}

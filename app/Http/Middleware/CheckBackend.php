<?php

namespace App\Http\Middleware;

use Closure;
use App\Library\SessionHelper;
use App\Library\ManagerJWT;

class CheckBackend
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $session_helper = new SessionHelper();

        try
        {
            $token_request = $request->input("codigo_acceso_rapido");

            if(trim($token_request) != "")
            {
                $ManagerJWT = new ManagerJWT();
                $response_estructure = $ManagerJWT->validateToken($token_request);

                if($response_estructure->get_login() == true)
                {
                    $id_usuario = $response_estructure->get_id_usuario();

                    if(trim($id_usuario) != "")
                    {
                        $request->session()->put("id",$id_usuario);
                        $session_helper->actualiza_session_usuario($request);
                    }
                }
            }
        }
        catch(\Exception $e)
        {
            
        }

        $respuesta = false;

        if($request->session()->get('ingreso_backend') === TRUE)
        {
            $session_helper->actualiza_session_usuario($request);
        }

        $respuesta = $request->session()->get('ingreso_backend');

        if(!$respuesta)
        {
          return redirect('/backend');
        }
        
        return $next($request);
    }
}

<?php
namespace App\Library\WebserviceAfip;

// incluyo la libreria con require y no en el composer,
// ya que le agregé funcionalidades que no tenía.
require(dirname(__FILE__).'/afipsdk/afip.php/src/Afip.php');
//20374206312
/**
 * 
 * WebserviceAfip
 * 
 */

use App\Library\ResponseEstructure;
use Illuminate\Support\Facades\DB;

use App\ConfiguracionAfip;
use App\PuntoDeVenta;

class WebserviceAfip
{
    private $afip = null;

    private $produccion = true; // default FALSE // TRUE para usar los Web Services en modo producción
    private $cuit = null; // El CUIT a usar en los Web Services
    private $cert = ""; //Ruta donde se encuentra el certificado, relativa a res_folder
    private $key = ""; //Ruta donde se encuentra el certificado, relativa a res_folder

    private $passphrase = ""; //Frase de contraseña para usar en el Web Service de Autenticación

    private $res_folder = ""; //Ruta de la carpeta donde se encuentran el cert y key
    private $ta_folder = ""; //Ruta de la carpeta donde se guardaran los Token

    // LISTADO MONEDAS ACEPTADAS
    private static $currencies_acepted = ["PES","DOL"];

    public function __construct($id_configuracion_afip)
    {
        $configuracion_afip = ConfiguracionAfip::find($id_configuracion_afip);

        if($configuracion_afip)
        { 
            $this->cuit = ((double) $configuracion_afip->cuit);

            $this->cert = "request_csr_afip_".$configuracion_afip->id.".csr";
            $this->key = "key_from_afip_".$configuracion_afip->id.".key";

            $data_cert = "";
            $data_key  = "";
           
            if($configuracion_afip->ambiente != "produccion")
            {
                $this->produccion = false;
                $this->res_folder = Config("filesystems.disks.local.root")."/certificados_ws/testing/";
                $this->ta_folder = Config("filesystems.disks.local.root")."/certificados_ws/testing/";
            
                $data_cert = $configuracion_afip->csr_afip_testing;
                $data_key = $configuracion_afip->key_afip_testing;
            }
            else
            {
                $this->produccion = true;
                $this->res_folder = Config("filesystems.disks.local.root")."/certificados_ws/produccion/";
                $this->ta_folder = Config("filesystems.disks.local.root")."/certificados_ws/produccion/";
                
                
                $data_cert = $configuracion_afip->csr_afip_produccion;
                $data_key = $configuracion_afip->key_afip_produccion;
            }

            // RES FOLDER TESTING
            if(!file_exists(Config("filesystems.disks.local.root")."/certificados_ws/testing/".$this->cert))
            {
                file_put_contents(Config("filesystems.disks.local.root")."/certificados_ws/testing/".$this->cert, $configuracion_afip->csr_afip_testing);
            }

            // TA FOLDER TESTING
            if(!file_exists(Config("filesystems.disks.local.root")."/certificados_ws/testing/".$this->key))
            {
                file_put_contents(Config("filesystems.disks.local.root")."/certificados_ws/testing/".$this->key, $configuracion_afip->key_afip_testing);
            }

            // RES FOLDER PRODUCCION
            if(!file_exists(Config("filesystems.disks.local.root")."/certificados_ws/produccion/".$this->cert))
            {
                file_put_contents(Config("filesystems.disks.local.root")."/certificados_ws/produccion/".$this->cert, $configuracion_afip->csr_afip_produccion);
            }

            // TA FOLDER PRODUCCION
            if(!file_exists(Config("filesystems.disks.local.root")."/certificados_ws/produccion/".$this->key))
            {
                file_put_contents(Config("filesystems.disks.local.root")."/certificados_ws/produccion/".$this->key, $configuracion_afip->key_afip_produccion);
            }

            $datos_ws = array();

            $datos_ws["CUIT"] = $this->cuit;
            $datos_ws["production"] = $this->produccion;
            $datos_ws["passphrase"] = $this->passphrase;
            $datos_ws["key"] = $this->key;
            $datos_ws["cert"] = $this->cert;
            $datos_ws["res_folder"] = $this->res_folder;
            $datos_ws["ta_folder"] = $this->ta_folder;

            $this->afip = new \Afip($datos_ws);
        }
    }

    public function isProduccion()
    {
        return $this->produccion;
    }
    /*
    Metodo en webservice: FEDummy
    Descripción: Metodo dummy para verificacion de funcionamiento
    Web: https://servicios1.afip.gob.ar/wsfev1/service.asmx?op=FEDummy
    */
    public function obtenerEstadoDeServidor()
    {
        return $this->afip->ElectronicBilling->GetServerStatus();
    }

    /*
    Metodo en webservice: FECompUltimoAutorizado
    Descripción: Retorna el ultimo comprobante autorizado para el tipo de comprobante 
    / cuit / punto de venta ingresado / Tipo de Emisión
    Web: https://servicios1.afip.gob.ar/wsfev1/service.asmx?op=FECompUltimoAutorizado
    */
    public function obtenerNumeroUltimoComprobante($punto_de_venta,$tipo_de_comprobante)
    {
        $punto_de_venta = (string)$punto_de_venta;
        $tipo_de_comprobante = (string)$tipo_de_comprobante;

        return $this->afip->ElectronicBilling->GetLastVoucher($punto_de_venta,$tipo_de_comprobante);
    }

    /*
    Metodo en webservice: FECAEAConsultar
    Descripción: Consultar CAEA emitidos.
    Web: https://servicios1.afip.gob.ar/wsfev1/service.asmx?op=FECAEAConsultar
    */
    public function ConsultarCAEAEmitidos($periodo,$orden)
    {
        return $this->afip->ElectronicBilling->ConsultarCAEAEmitidos($periodo,$orden);
    }

    /*
    Metodo en webservice: FECompConsultar
    Descripción: Consulta Comprobante emitido y su código..
    Web: https://servicios1.afip.gob.ar/wsfev1/service.asmx?op=FECompConsultar
    */
    public function consultarComprobante($tipo_de_comprobante,$numero_de_comprobante,$punto_de_venta)
    {
        $query = array(
			'FeCompConsReq'=> array(
				'CbteTipo'=>(int) $tipo_de_comprobante,
				'CbteNro'=>(int) $numero_de_comprobante,
				'PtoVta'=> (int) $punto_de_venta
			)
        );
        
        return $this->afip->ElectronicBilling->consultarComprobante($query);
    }

    public function ConsultarPuntosDeVentas()
    {
        $id_configuracion_afip = session("id_configuracion_afip");

        $puntos_de_venta_db = DB::table("puntos_de_venta")
        ->select(
            "puntos_de_venta.*"
        )
        ->where("puntos_de_venta.id_configuracion_afip",$id_configuracion_afip)
        ->get();

        if($this->produccion)
        {
            $response_ws = $this->afip->ElectronicBilling->ConsultarPuntosDeVentas();

            $con_modificacion = false;
            
            try
            {
                foreach($response_ws->ResultGet as $punto_de_venta_resp)
                {
                    $encontrado = false;

                    foreach($puntos_de_venta_db as $punto_de_venta_db_row)
                    {
                        if($punto_de_venta_db_row->numero == $punto_de_venta_resp->Nro)
                        {
                            $encontrado = true;

                            if(
                                ($punto_de_venta_db_row->EmisionTipo != $punto_de_venta_resp->EmisionTipo)
                                || ($punto_de_venta_db_row->Bloqueado != $punto_de_venta_resp->Bloqueado)
                                || ($punto_de_venta_db_row->FchBaja != $punto_de_venta_resp->FchBaja)
                            )
                            {
                                $punto_de_venta_obj = PuntoDeVenta::where("numero",$punto_de_venta_resp->Nro)->where("id_configuracion_afip",$id_configuracion_afip)->first();
                                $punto_de_venta_obj->EmisionTipo = $punto_de_venta_resp->EmisionTipo;
                                $punto_de_venta_obj->Bloqueado = $punto_de_venta_resp->Bloqueado;
                                $punto_de_venta_obj->FchBaja = $punto_de_venta_resp->FchBaja;
                                $punto_de_venta_obj->save();

                                $con_modificacion = true;
                            }

                            break;
                        }
                    }

                    // AGREGAR A DB
                    if($encontrado == false)
                    {
                        $new_punto_de_venta_obj = new PuntoDeVenta();
                        $new_punto_de_venta_obj->numero = $punto_de_venta_resp->Nro;
                        $new_punto_de_venta_obj->descripcion = "Punto de Venta #".$punto_de_venta_resp->Nro;
                        $new_punto_de_venta_obj->EmisionTipo = $punto_de_venta_resp->EmisionTipo;
                        $new_punto_de_venta_obj->Bloqueado = $punto_de_venta_resp->Bloqueado;
                        $new_punto_de_venta_obj->FchBaja = $punto_de_venta_resp->FchBaja;
                        $new_punto_de_venta_obj->id_configuracion_afip = $id_configuracion_afip;
                        $new_punto_de_venta_obj->save();

                        $con_modificacion = true;
                    }
                }
            }
            catch(Exception $e)
            {
                
            }

            if($con_modificacion)
            {
                $puntos_de_venta_db = DB::table("puntos_de_venta")
                ->select(
                    "puntos_de_venta.*"
                )
                ->where("puntos_de_venta.id_configuracion_afip",$id_configuracion_afip)
                ->get();
            }
        }
        else
        {
            // SIEMPRE PARA TESTING ES ASI :)
            $punto_de_venta = new \stdClass();
            $punto_de_venta->id = 1;
            $punto_de_venta->numero = '1';
            $punto_de_venta->descripcion = "PUNTO DE VENTA 1 TEST";
            $punto_de_venta->FchBaja = '';
            $punto_de_venta->Bloqueado = "N";
            $punto_de_venta->EmisionTipo = "CAE";
            
            $puntos_de_venta_db[] = $punto_de_venta;
        }

        return $puntos_de_venta_db;
    }

    public function crearCaeParaSiguienteComprobante($punto_de_venta,$tipo_de_comprobante)
    {
        /*

        COMPROBANTE ASOCIADO EJEMPLO

        <ar:CbtesAsoc>
            <ar:CbteAsoc>
                <ar:Tipo>short</ar:Tipo>
                <ar:PtoVta>int</ar:PtoVta>
                <ar:Nro>long</ar:Nro>
            </ar:CbteAsoc>
        </ar:CbtesAsoc>

        IVA EJEMPLO
        <ar:Iva>
            <ar:AlicIva>
                <ar:Id>short</ar:Id>
                <ar:BaseImp>double</ar:BaseImp>
                <ar:Importe>double</ar:Importe>
            </ar:AlicIva>
        </ar:Iva>

        */


        $data = array(
            'CantReg' 	=> 1,  // Cantidad de comprobantes a registrar
            'PtoVta' 	=> 1,  // Punto de venta
            'CbteTipo' 	=> 6,  // Tipo de comprobante (ver tipos disponibles) 
            'Concepto' 	=> 1,  // Concepto del Comprobante: (1)Productos, (2)Servicios, (3)Productos y Servicios
            'DocTipo' 	=> 99, // Tipo de documento del comprador (99 consumidor final, ver tipos disponibles)
            'DocNro' 	=> 0,  // Número de documento del comprador (0 consumidor final)
            'CbteDesde' 	=> 1,  // Número de comprobante o numero del primer comprobante en caso de ser mas de uno
            'CbteHasta' 	=> 1,  // Número de comprobante o numero del último comprobante en caso de ser mas de uno
            'CbteFch' 	=> intval(date('Ymd')), // (Opcional) Fecha del comprobante (yyyymmdd) o fecha actual si es nulo
            'ImpTotal' 	=> 121, // Importe total del comprobante
            'ImpTotConc' 	=> 0,   // Importe neto no grabado
            'ImpNeto' 	=> 100, // Importe neto grabado
            'ImpOpEx' 	=> 0,   // Importe exento de IVA
            'ImpIVA' 	=> 21,  //Importe total de IVA
            'ImpTrib' 	=> 0,   //Importe total de tributos
            'MonId' 	=> 'PES', //Tipo de moneda usada en el comprobante (ver tipos disponibles)('PES' para pesos argentinos) 
            'MonCotiz' 	=> 1,     // Cotización de la moneda usada (1 para pesos argentinos)  
            'Iva' 		=> array( // (Opcional) Alícuotas asociadas al comprobante
                array(
                    'Id' 		=> 5, // Id del tipo de IVA (5 para 21%)(ver tipos disponibles) 
                    'BaseImp' 	=> 100, // Base imponible
                    'Importe' 	=> 21 // Importe 
                )
            ), 
        );

        $res = $this->afip->ElectronicBilling->CreateNextVoucher($data);

        $res['CAE']; //CAE asignado el comprobante
        $res['CAEFchVto']; //Fecha de vencimiento del CAE (yyyy-mm-dd)
        $res['voucher_number']; //Número asignado al comprobante

        return $res;
    }

    public function crearFacturaWithCAE($comprobante_obj,$detalles_comprobantess_objs,$detalles_tributos_objs,$comprobantes_asociados)
    {
        $CbteFch = \DateTime::createFromFormat("Y-m-d",$comprobante_obj->fecha);
        $CbteFch = $CbteFch->format("Ymd");

        $ultima_factura = $this->obtenerNumeroUltimoComprobante($comprobante_obj->punto_de_venta,$comprobante_obj->id_tipo_de_comprobante);

        $ultima_factura++;

        $data = array(
            'CantReg' 	=> 1,  // Cantidad de comprobantes a registrar
            'PtoVta' 	=> (int)$comprobante_obj->punto_de_venta,  // Punto de venta
            'CbteTipo' 	=> (int)$comprobante_obj->id_tipo_de_comprobante,  // Tipo de comprobante (ver tipos disponibles) 
            'Concepto' 	=> (int)$comprobante_obj->id_concepto,  // Concepto del Comprobante: (1)Productos, (2)Servicios, (3)Productos y Servicios
            'DocTipo' 	=> (int)$comprobante_obj->tipo_documento_receptor, // Tipo de documento del comprador (99 consumidor final, ver tipos disponibles)
            'DocNro' 	=> (float)$comprobante_obj->num_documento_receptor,  // Número de documento del comprador (0 consumidor final)
            'CbteDesde' 	=> $ultima_factura,  // Número de comprobante o numero del primer comprobante en caso de ser mas de uno
            'CbteHasta' 	=> $ultima_factura,  // Número de comprobante o numero del último comprobante en caso de ser mas de uno
            'CbteFch' 	=> intval($CbteFch), // (Opcional) Fecha del comprobante (yyyymmdd) o fecha actual si es nulo
            'ImpTotal' 	=> (float)$comprobante_obj->ImpTotal, // Importe total del comprobante
            'ImpTotConc' 	=> (float)$comprobante_obj->ImpTotConc,   // Importe neto no grabado
            'ImpNeto' 	=> (float)$comprobante_obj->ImpNeto, // Importe neto grabado
            'ImpOpEx' 	=> (float)$comprobante_obj->ImpOpEx,   // Importe exento de IVA
            'ImpIVA' 	=> (float)$comprobante_obj->ImpIVA,  //Importe total de IVA
            'ImpTrib' 	=> (float)$comprobante_obj->ImpTrib,   //Importe total de tributos
            'MonId' 	=> (string)$comprobante_obj->id_moneda, //Tipo de moneda usada en el comprobante (ver tipos disponibles)('PES' para pesos argentinos) 
            'MonCotiz' 	=> (float)$comprobante_obj->cotizacion_de_la_moneda,     // Cotización de la moneda usada (1 para pesos argentinos)  
            /*'Iva' 		=> array( // (Opcional) Alícuotas asociadas al comprobante
                array(
                    'Id' 		=> 5, // Id del tipo de IVA (5 para 21%)(ver tipos disponibles) 
                    'BaseImp' 	=> 100, // Base imponible
                    'Importe' 	=> 21 // Importe 
                )
            ), */
        );

        if($comprobante_obj->id_concepto == 2 || $comprobante_obj->id_concepto == 3)
        {
            $periodo_facturado_desde = \DateTime::createFromFormat("Y-m-d",$comprobante_obj->periodo_facturado_desde);
            $periodo_facturado_desde = $periodo_facturado_desde->format("Ymd");

            $periodo_facturado_hasta = \DateTime::createFromFormat("Y-m-d",$comprobante_obj->periodo_facturado_hasta);
            $periodo_facturado_hasta = $periodo_facturado_hasta->format("Ymd");

            $vencimiento_del_pago = \DateTime::createFromFormat("Y-m-d",$comprobante_obj->vencimiento_del_pago);
            $vencimiento_del_pago = $vencimiento_del_pago->format("Ymd");
            
            $data["FchServDesde"] = intval($periodo_facturado_desde);
            $data["FchServHasta"] = intval($periodo_facturado_hasta);
            $data["FchVtoPago"] = intval($vencimiento_del_pago);
        }

        // IMPUESTOS NACIONALES EXAMPLE

        foreach($detalles_tributos_objs as $tributo_comprobante_obj)
        {
            $Tributo = array(
                "Id" => $tributo_comprobante_obj->id_tipo_tributo,
                "BaseImp"=>$tributo_comprobante_obj->base_imponible,
                "Alic"=>$tributo_comprobante_obj->alicuota,
                "Importe"=>$tributo_comprobante_obj->importe,
                "Desc" => $tributo_comprobante_obj->Desc,
                "FchDesde" => $tributo_comprobante_obj->FchDesde,
                "FchHasta" => $tributo_comprobante_obj->FchHasta
            ); 

            $data["Tributos"][] = $Tributo;
        }

        if(is_array($comprobantes_asociados) && count($comprobantes_asociados) > 0)
        {
            $CbtesAsoc = array();

            for($i=0; $i < count($comprobantes_asociados);$i++)
            {
                $CbteAsoc = array();
                $CbteAsoc["Tipo"] = $comprobantes_asociados[$i]["id_tipo_de_comprobante"];
                $CbteAsoc["PtoVta"] = $comprobantes_asociados[$i]["id_punto_de_venta"];
                $CbteAsoc["Nro"] = $comprobantes_asociados[$i]["comprobante"];

                $CbtesAsoc[] = $CbteAsoc;
            }  

            $data["CbtesAsoc"] = $CbtesAsoc;
        }
        
        $res = array();

        if($this->produccion != true)
        {
            $res['CAE'] = "70379861293051";
            $res['CAEFchVto'] = "2020-09-23";
        }
        else
        {
            $res = $this->afip->ElectronicBilling->CreateVoucher($data);
        }

        //$res['CAE']; //CAE asignado el comprobante
        //$res['CAEFchVto']; //Fecha de vencimiento del CAE (yyyy-mm-dd)
        
        if($res && isset($res['CAE']))
        {
            $comprobante_obj->CAE = $res['CAE'];
            $comprobante_obj->CAEFchVto = $res['CAEFchVto'];
        }
        
        $comprobante_obj->numero_afip = $ultima_factura;

        return $comprobante_obj;
    }

    public function crearFacturaWithCAEProcesoAPI($comprobante_obj)
    {
        $CbteFch = \DateTime::createFromFormat("Y-m-d",$comprobante_obj->fecha);
        $CbteFch = $CbteFch->format("Ymd");

        $numero_factura = $this->obtenerNumeroUltimoComprobante($comprobante_obj->punto_de_venta,$comprobante_obj->id_tipo_de_comprobante);

        $numero_factura++;

        $data = array(
            'CantReg' 	=> 1,  // Cantidad de comprobantes a registrar
            'PtoVta' 	=> (int)$comprobante_obj->punto_de_venta,  // Punto de venta
            'CbteTipo' 	=> (int)$comprobante_obj->id_tipo_de_comprobante,  // Tipo de comprobante (ver tipos disponibles) 
            'Concepto' 	=> (int)$comprobante_obj->id_concepto,  // Concepto del Comprobante: (1)Productos, (2)Servicios, (3)Productos y Servicios
            'DocTipo' 	=> (int)$comprobante_obj->tipo_documento_receptor, // Tipo de documento del comprador (99 consumidor final, ver tipos disponibles)
            'DocNro' 	=> (float)$comprobante_obj->num_documento_receptor,  // Número de documento del comprador (0 consumidor final)
            'CbteDesde' 	=> $numero_factura,  // Número de comprobante o numero del primer comprobante en caso de ser mas de uno
            'CbteHasta' 	=> $numero_factura,  // Número de comprobante o numero del último comprobante en caso de ser mas de uno
            'CbteFch' 	=> intval($CbteFch), // (Opcional) Fecha del comprobante (yyyymmdd) o fecha actual si es nulo
            'ImpTotal' 	=> (float)$comprobante_obj->ImpTotal, // Importe total del comprobante
            'ImpTotConc' 	=> (float)$comprobante_obj->ImpTotConc,   // Importe neto no grabado
            'ImpNeto' 	=> (float)$comprobante_obj->ImpNeto, // Importe neto grabado
            'ImpOpEx' 	=> (float)$comprobante_obj->ImpOpEx,   // Importe exento de IVA
            'ImpIVA' 	=> (float)$comprobante_obj->ImpIVA,  //Importe total de IVA
            'ImpTrib' 	=> (float)$comprobante_obj->ImpTrib,   //Importe total de tributos
            'MonId' 	=> (string)$comprobante_obj->id_moneda, //Tipo de moneda usada en el comprobante (ver tipos disponibles)('PES' para pesos argentinos) 
            'MonCotiz' 	=> (float)$comprobante_obj->cotizacion_de_la_moneda,     // Cotización de la moneda usada (1 para pesos argentinos)  
            /*'Iva' 		=> array( // (Opcional) Alícuotas asociadas al comprobante
                array(
                    'Id' 		=> 5, // Id del tipo de IVA (5 para 21%)(ver tipos disponibles) 
                    'BaseImp' 	=> 100, // Base imponible
                    'Importe' 	=> 21 // Importe 
                )
            ), */
        );

        if($comprobante_obj->id_concepto == 2 || $comprobante_obj->id_concepto == 3)
        {
            $periodo_facturado_desde = \DateTime::createFromFormat("Y-m-d",$comprobante_obj->periodo_facturado_desde);
            $periodo_facturado_desde = $periodo_facturado_desde->format("Ymd");

            $periodo_facturado_hasta = \DateTime::createFromFormat("Y-m-d",$comprobante_obj->periodo_facturado_hasta);
            $periodo_facturado_hasta = $periodo_facturado_hasta->format("Ymd");

            $vencimiento_del_pago = \DateTime::createFromFormat("Y-m-d",$comprobante_obj->vencimiento_del_pago);
            $vencimiento_del_pago = $vencimiento_del_pago->format("Ymd");
            
            $data["FchServDesde"] = intval($periodo_facturado_desde);
            $data["FchServHasta"] = intval($periodo_facturado_hasta);
            $data["FchVtoPago"] = intval($vencimiento_del_pago);
        }
        
        $res = $this->afip->ElectronicBilling->CreateVoucher($data);

        $res['CAE']; //CAE asignado el comprobante
        $res['CAEFchVto']; //Fecha de vencimiento del CAE (yyyy-mm-dd)
        
        if($res && isset($res['CAE']))
        {
            $comprobante_obj->CAE = $res['CAE'];
            $comprobante_obj->CAEFchVto = $res['CAEFchVto'];
        }
        
        $comprobante_obj->numero_afip = $numero_factura;
        $comprobante_obj->save();

        return $comprobante_obj;
    }

    public function getInfoComprobante($numero, $punto_de_venta, $tipo)
    {
        $voucher_info = $this->afip->ElectronicBilling->GetVoucherInfo($numero, $punto_de_venta, $tipo); //Devuelve la información del comprobante 1 para el punto de venta 1 y el tipo de comprobante 6 (Factura B)

        if($voucher_info === NULL){
            echo 'El comprobante no existe';
        }
        else{
            echo 'Esta es la información del comprobante:';
            echo '<pre>';
            print_r($voucher_info);
            echo '</pre>';
        }
    }

    public function getTiposDeComprobantesDisponibles()
    {
        $voucher_types = $this->afip->ElectronicBilling->GetVoucherTypes();

        $tipos_de_comprobantes = array();

        foreach($voucher_types as $tipo_comprobante_resp)
        {
            // FACTURA C || Nota de Débito C || Nota de Crédito C || Recibo C
            if($tipo_comprobante_resp->Id == 11 || $tipo_comprobante_resp->Id == 12 || $tipo_comprobante_resp->Id == 13 || $tipo_comprobante_resp->Id == 15)
            {
                $tipos_de_comprobantes[] = $tipo_comprobante_resp;
            }
        }

        return $tipos_de_comprobantes;
    }

    public function obtenerTiposDeDocumentosDisponibles()
    {
        return $this->afip->ElectronicBilling->GetDocumentTypes();
    }

    public function getTiposAlicuotas()
    {
        return $this->afip->ElectronicBilling->GetAliquotTypes();
    }

    public function geTiposMonedasDisponibles()
    {
        $currencies_types = $this->afip->ElectronicBilling->GetCurrenciesTypes();

        return $currencies_types;
    }

    public function getTiposOpcionesParaComprobante()
    {
        return $this->afip->ElectronicBilling->GetOptionsTypes();
    }

    public function getTiposDeTributosDisponibles()
    {
        return $this->afip->ElectronicBilling->GetTaxTypes();
    }

    public function getTiposDeConceptos()
    {
        $conceptos = $this->afip->ElectronicBilling->GetConceptTypes();
        return $conceptos;
    }

    public function get_condiciones_frente_al_iva()
    {
        $condiciones_frente_al_iva = \App\CondicionIva::all();          
        return $condiciones_frente_al_iva;
    }

    // OBTIENE LOS TIPOS DE DOCUMENTOS UNICOS, PASANDOLE LA CONDICION DEL IVA
    public function get_tipos_de_documentos_unicos($id_condicion_de_iva)
    {
        $condiciones_iva_tipo_doc = \App\CondicionIvaTipoDoc::where("id_condicion_iva",$id_condicion_de_iva)->get();

        $tipos_documentos = array();

        foreach($condiciones_iva_tipo_doc as $condicion_iva_tipo_doc_obj)
        {
            $tipo_documento_row = $condicion_iva_tipo_doc_obj->get_tipo_documento;

            if($tipo_documento_row)
            {
                $tipos_documentos[] = $tipo_documento_row;
            }
        }

        return $tipos_documentos;
    }

    public function get_condiciones_de_venta()
    {
        $condiciones_de_venta = \App\CondicionDeVenta::all();
        return $condiciones_de_venta;
    }

    public function get_cotizacion_moneda($MonId)
    {
        $respuesta = $this->afip->ElectronicBilling->FEParamGetCotizacion($MonId);

        return $respuesta;

    }

    public function get_datos_persona($cuit)
    {
        $datos = $this->afip->RegisterScopeFive->GetTaxpayerDetails($cuit);
        return $datos;
    }

    public function consultarComunicaciones($parametros)
    {
        $response = $this->afip->VentanillaElectronica->consultarComunicaciones($parametros);
        return $response;
    }

    public function consumirComunicacion($parametros)
    {
        $response = $this->afip->VentanillaElectronica->consumirComunicacion($parametros);
        return $response;
    }

    public static function isEnableCurrency($currency)
    {
        return in_array($currency,self::$currencies_acepted);
    }

    public static function isCurrencyIsDollarEEUU($currency)
    {
        return ($currency == "DOL" ? true : false);
    }

    public static function isCurrencyIsPesosArg($currency)
    {
        return ($currency == "PES" ? true : false);
    }
}
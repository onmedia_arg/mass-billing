<?php

/**
 * SDK for AFIP Ventanilla Electronica ()
 * 
 * @link http://www.afip.gob.ar/ws/WSCComu/vecuwsconcomunicaciones.pdf WS Specification
 *
 * @author 	Mario Olivera
 * @package Afip
 * @version 0.1
 **/

class VentanillaElectronica extends AfipWebService {

    var $soap_version 	= SOAP_1_2;
	var $WSDL 			= 'veconsumer.wsdl';
	var $URL 			= 'https://infraestructura.afip.gob.ar/ve-ws/services/veconsumer?wsdl';
	var $WSDL_TEST 		= 'veconsumer.wsdl';
	var $URL_TEST 		= 'https://infraestructura.afip.gob.ar/ve-ws/services/veconsumer?wsdl';

	 
    
    public function consultarComunicaciones($parametros)
    {
        $parametros_filter = 
		'<filter>';

			if(isset($parametros["estado"]) && trim($parametros["estado"] != "")){
				$parametros_filter .= '<estado>'.$parametros["estado"].'</estado>';
			}
			
			if(isset($parametros["fechaDesde"]) && trim($parametros["fechaDesde"] != "")){
				$parametros_filter .= '<fechaDesde>'.$parametros["fechaDesde"].'</fechaDesde>';
			}
			
			if(isset($parametros["fechaHasta"]) && trim($parametros["fechaHasta"] != "")){
				$parametros_filter .= '<fechaHasta>'.$parametros["fechaHasta"].'</fechaHasta>';
			}
			
			if(isset($parametros["comunicacionIdDesde"]) && trim($parametros["comunicacionIdDesde"] != "")){
				$parametros_filter .= '<comunicacionIdDesde>'.$parametros["comunicacionIdDesde"].'</comunicacionIdDesde>';
			}
			
			if(isset($parametros["comunicacionIdHasta"]) && trim($parametros["comunicacionIdHasta"] != "")){
				$parametros_filter .= '<comunicacionIdHasta>'.$parametros["comunicacionIdHasta"].'</comunicacionIdHasta>';
			}
			
			if(isset($parametros["tieneAdjunto"]) && trim($parametros["tieneAdjunto"] != "")){
				$parametros_filter .= '<tieneAdjunto>'.$parametros["tieneAdjunto"].'</tieneAdjunto>';
			}
			
			if(isset($parametros["sistemaPublicadorId"]) && trim($parametros["sistemaPublicadorId"] != "")){
				$parametros_filter .= '<sistemaPublicadorId>'.$parametros["sistemaPublicadorId"].'</sistemaPublicadorId>';
			}
			
			if(isset($parametros["pagina"]) && trim($parametros["pagina"] != "")){
				$parametros_filter .= '<pagina>'.$parametros["pagina"].'</pagina>';
			}
			
			if(isset($parametros["resultadosPorPagina"]) && trim($parametros["resultadosPorPagina"] != "")){
				$parametros_filter .= '<resultadosPorPagina>'.$parametros["resultadosPorPagina"].'</resultadosPorPagina>';
			}
			
			if(isset($parametros["referencia1"]) && trim($parametros["referencia1"] != "")){
				$parametros_filter .= '<referencia1>'.$parametros["referencia1"].'</referencia1>';
			}
			
			if(isset($parametros["referencia2"]) && trim($parametros["referencia2"] != "")){
				$parametros_filter .= '<referencia2>'.$parametros["referencia2"].'</referencia2>';
			}
		$parametros_filter .= 
		'</filter>';

		return $this->ejecutarConsulta("consultarComunicaciones",$parametros_filter);
	}
	
	public function consumirComunicacion($parametros)
	{
		$parametros_filter ='';

		if(isset($parametros["idComunicacion"]) && trim($parametros["idComunicacion"] != "")){
			$parametros_filter .= '<idComunicacion>'.$parametros["idComunicacion"].'</idComunicacion>';
		}
		
		if(isset($parametros["incluirAdjuntos"]) && trim($parametros["incluirAdjuntos"] != "")){
			$parametros_filter .= '<incluirAdjuntos>'.$parametros["incluirAdjuntos"].'</incluirAdjuntos>';
		}

		return $this->ejecutarConsulta("consumirComunicacion",$parametros_filter);
	}

	/**
	 * Sends request to AFIP servers
	 * 
	 * @since 0.7
	 *
	 * @param string 	$operation 	SOAP operation to do 
	 * @param array 	$params 	Parameters to send
	 *
	 * @return mixed Operation results 
	 **/
	public function ejecutarConsulta($metodo,$parametros)
	{
		$parametro_auth = $this->GetWSInitialRequest();

		$respuesta = array();

		$xml = 
		'<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:typ="http://ve.tecno.afip.gov.ar/domain/service/ws/types" xmlns:typ1="http://core.tecno.afip.gov.ar/model/ws/types">
		<soap:Header/>
		<soap:Body>
		<typ:'.$metodo.'>
			'.$parametro_auth.'
			'.$parametros.'
		</typ:'.$metodo.'>
		</soap:Body>
		</soap:Envelope>';
		
		$client = new SoapClient(null, array(
			'soap_version' 	=> $this->soap_version,
			'location' 		=> $this->URL,
			'uri' 		=> $this->URL,
			'trace' 		=> 1,
			'exceptions' 	=> 0
		));
			
		try{
			$search_result = $client->__doRequest($xml,$this->URL,$this->URL,$this->soap_version);

			$posicion_comienzo = strpos($search_result,"<soap:Envelope");

			if($posicion_comienzo !== FALSE)
			{
				$search_result = substr($search_result,$posicion_comienzo,strlen($search_result));
			}

			$posicion_final = strpos($search_result,"</soap:Envelope>");

			if($posicion_final !== FALSE)
			{
				$search_result = substr($search_result,0,$posicion_final+(strlen("</soap:Envelope>")));
			}

			$search_result = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $search_result);
			$xml = new SimpleXMLElement($search_result);
			$body = $xml->xpath('//soapBody')[0];
			$respuesta = json_decode(json_encode((array)$body), TRUE); 
		}
		catch (Exception $exception){
			
		}

		return $respuesta;
	}

	/**
	 * Make default request parameters for most of the operations
	 * 
	 * @since 0.7
	 *
	 * @param string $operation SOAP Operation to do 
	 *
	 * @return array Request parameters  
	 **/
	private function GetWSInitialRequest()
	{
		$ta = $this->afip->GetServiceTA('veconsumerws');

		return 
		'<authRequest>
			<typ1:token>'.$ta->token.'</typ1:token>
			<typ1:sign>'.$ta->sign.'</typ1:sign>
			<typ1:cuitRepresentada>'.((double)$this->afip->CUIT).'</typ1:cuitRepresentada>
		</authRequest>';
	}

}
<?php

namespace App\Library;

use Illuminate\Http\Request;
use App\Library\SessionHelper;
use Illuminate\Support\Facades\Hash;

use App\Library\ResponseEstructure;
use Illuminate\Support\Facades\Validator;

use App\Usuario;

class LoginCore
{
    public function ingresar(Request $request)
    {
        $response_estructure = new ResponseEstructure();
        $response_estructure->set_response(false);

        $correo = trim(strtolower($request->input("correo")));
        $password = $request->input("password");

        $input = [
            "correo"=>$correo,
            "password"=>$password
        ];

        $rules = [
            "correo"=>"required",
            "password"=>"required"
        ];

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            $response_estructure->set_response(false);

            $errors = $validator->errors();

            foreach ($errors->all() as $error) {
                $response_estructure->add_message_error($error);
            }
        }
        else
        {
            $response_estructure->set_response(true);

            $rules["correo"]="required|email";

            $validator = Validator::make($input, $rules);

            $usuario_obj = null;

            $es_correo = true;

            if ($validator->fails()) {

                $usuario_obj = Usuario::where("usuario",$correo)->where("eliminado",false)->first();
                $es_correo = false;
            }
            else
            {
                $usuario_obj = Usuario::where("correo",$correo)->where("eliminado",false)->first();
            }

            if($usuario_obj && $usuario_obj->eliminado){$usuario_obj = null;}

            if($usuario_obj && $usuario_obj->id_estado_usuario == 1)
            {
                if (Hash::check($password, $usuario_obj->password)) {

                    $request->session()->put("id",$usuario_obj->id);

                    $session_helper = new SessionHelper();
                    $session_helper->actualiza_session_usuario($request);

                    $response_estructure->set_response(true);
                }
                else
                {
                    $response_estructure->set_response(false);
                    $response_estructure->add_message_error("Contraseña incorrecta");
                }
            }
            else
            {

                $response_estructure->set_response(false);

                if($usuario_obj)
                {
                    if($usuario_obj->id_estado_usuario != 1)
                    {
                        $response_estructure->add_message_error("El usuario no está activado");
                    }
                }
                else
                {
                    if($es_correo)
                    {
                        $response_estructure->add_message_error("No se ha encontrado el correo ingresado");
                    }
                    else
                    {
                        $response_estructure->add_message_error("No se ha encontrado el usuario ingresado");
                    }
                }
            }
        }

        return $response_estructure->get_response_array();
    }

    public function forgot_password(Request $request)
    {
        $response_estructure = new ResponseEstructure();
        $response_estructure->set_response(false);

        $correo = trim(strtolower($request->input("correo")));

        $input = ["correo"=>$correo];
        $rules = ["correo"=>"required|email"];

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {

            $errors = $validator->errors();

            foreach ($errors->all() as $error) {
                $response_estructure->add_message_error($error);
            }
        }
        else
        {
            $user_obj = Usuario::where("correo",$correo)->first();

            if($user_obj->eliminado){$user_obj = null;}

            if($user_obj)
            {
                $user_obj->sendNewPassword();
                $response_estructure->set_response(true);
            }
            else{
                $response_estructure->add_message_error("No se ha encontrado la cuenta");
            }
        }

        return $response_estructure->get_response_array();
    }

    public function cerrar_sesion(Request $request)
    {
        $session_helper = new SessionHelper();
        $session_helper->destroy_session($request);
    }
}

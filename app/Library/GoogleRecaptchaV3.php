<?php

namespace App\Library;

use App\Library\ResponseEstructure;
use Illuminate\Support\Facades\Validator;

class GoogleRecaptchaV3
{
    public function validate($token,$score = null)
    {
        $client = new \GuzzleHttp\Client();
        $response = $client->post(
            'https://www.google.com/recaptcha/api/siteverify',
            array(
                'form_params' => array(
                    'secret' => '6LcWe9kZAAAAABodeXMuf9sx6vlIlzbpVdJ4EloC',
                    'response' => $token,
                )
            )
        );

        $response_google = json_decode($response->getBody());

        $respuesta = false;

        if(property_exists($response_google, 'success'))
        {
            if($response_google->success == true)
            {
                if($score != null)
                {
                    if($response_google->score >= $score)
                    {
                        $respuesta = true;
                    }
                }
                else
                {
                    $respuesta = true;
                }
            }
        }

        return $respuesta;
    }
}
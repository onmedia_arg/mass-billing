<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\Mail;
use App\Mail\EmailSender;

use Mpdf\Mpdf;
use DNS1D;

use App\ConfiguracionAfip;

class Comprobante extends Model
{
    public $table = "comprobantes";

    public static $FACTURA_C = 11;
    public static $NOTA_DE_DEBITO_C = 12;
    public static $NOTA_DE_CREDITO_C = 13;
    public static $RECIBO_C = 15;

    public static $REMITO = 91;

    public static $CONCEPTO_PRODUCTOS=1;
    public static $CONCEPTO_SERVICIOS=2;
    public static $CONCEPTO_PRODUCTOS_Y_SERVICIOS=3;

    public function get_unidad_de_medida()
    {
        return $this->hasOne("App\UnidadDeMedida","id","id_unidad_medida");
    }

    public function get_tipo_de_comprobante()
    {
        return $this->hasOne("App\TipoDeComprobante","id","id_tipo_de_comprobante");
    }

    public function get_detalles_comprobante()
    {
        return $this->hasMany("App\DetalleComprobante","id_comprobante","id");
    }

    public function get_tributos_comprobantes()
    {
        return $this->hasMany("App\TributoComprobante","id_comprobante","id");
    }

    public function get_tipo_documento_receptor()
    {
        return $this->hasOne("App\TipoDocumento","id","tipo_documento_receptor");
    }

    public function get_condicion_iva_receptor()
    {
        return $this->hasOne("App\CondicionIva","id","condicion_iva_receptor");
    }

    public function get_condicion_venta()
    {
        return $this->hasOne("App\CondicionDeVenta","id","id_condicion_venta");
    }

    public function get_numero_afip_formateado()
    {
        $numero_afip = $this->numero_afip;

        $longitud = strlen($numero_afip);

        if($longitud < 8)
        {
            for($i=$longitud-1; $i < 8;$i++)
            {
                $numero_afip = "0".$numero_afip;
            }
        }

        return $numero_afip;
    }

    public function get_punto_venta_formateado()
    {
        $punto_de_venta = $this->punto_de_venta;

        $longitud = strlen($punto_de_venta);

        if($longitud < 5)
        {
            for($i=$longitud-1; $i < 5;$i++)
            {
                $punto_de_venta = "0".$punto_de_venta;
            }
        }

        return $punto_de_venta;
    }

    public function comprobanteConDetalle($id_tipo_de_comprobante = null)
    {
        if($id_tipo_de_comprobante == null)
        {
            $id_tipo_de_comprobante = $this->id_tipo_de_comprobante;
        }

        if($id_tipo_de_comprobante == self::$FACTURA_C  
            || $id_tipo_de_comprobante == self::$NOTA_DE_DEBITO_C  
            || $id_tipo_de_comprobante == self::$NOTA_DE_CREDITO_C 
        )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function comprobanteConTributos($id_tipo_de_comprobante = null)
    {
        if($id_tipo_de_comprobante == null)
        {
            $id_tipo_de_comprobante = $this->id_tipo_de_comprobante;
        }

        if(
            $id_tipo_de_comprobante == self::$FACTURA_C  
            || $id_tipo_de_comprobante == self::$NOTA_DE_DEBITO_C  
            || $id_tipo_de_comprobante == self::$NOTA_DE_CREDITO_C 
            || $id_tipo_de_comprobante == self::$RECIBO_C 
        )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function comprobanteConComprobantesAsociados($id_tipo_de_comprobante = null)
    {
        if($id_tipo_de_comprobante == null)
        {
            $id_tipo_de_comprobante = $this->id_tipo_de_comprobante;
        }
        
        $ids_tipos_comprobantes_permitidos = [1, 2, 3, 6, 7, 8, 12, 13, 51, 52, 53, 201, 202, 203, 206, 207, 208, 211, 212, 213];

        if(in_array($id_tipo_de_comprobante,$ids_tipos_comprobantes_permitidos))
        {
          return true;  
        }
        else
        {
            return false;
        }
        /*arr
        if($id_tipo_de_comprobante == null)
        {
            $id_tipo_de_comprobante = $this->id_tipo_de_comprobante;
        }

        if( 
            $id_tipo_de_comprobante == self::$FACTURA_C   
            || $id_tipo_de_comprobante == self::$NOTA_DE_DEBITO_C  
            || $id_tipo_de_comprobante == self::$NOTA_DE_CREDITO_C
        )
        {
            return true;
        }
        else
        {
            return false;
        }*/
    }

    // DEVUELVE TRUE SI EL COMPROBANTE ESTÁ DISPONIBLE A REALIZAR
    public function comprobanteDisponible($id_tipo_de_comprobante = null)
    {
        if($id_tipo_de_comprobante == null)
        {
            $id_tipo_de_comprobante = $this->id_tipo_de_comprobante;
        }

        if( 
            $id_tipo_de_comprobante != self::$FACTURA_C   
            && $id_tipo_de_comprobante != self::$NOTA_DE_DEBITO_C  
            && $id_tipo_de_comprobante != self::$NOTA_DE_CREDITO_C
            && $id_tipo_de_comprobante != self::$RECIBO_C 
        )
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public function send_comprobante_creado()
    {
        if(filter_var($this->correo_receptor,FILTER_VALIDATE_EMAIL))
        {
            $email_sender = new \stdClass();
            $email_sender->comprobante = $this;
    
            $email_a_enviar = new \App\Mail\EmailSender($email_sender,"Tu Comprobante","emails.comprobante_generado_receptor");
            $email_a_enviar->add_files_64($this->get_base_64_comprobante(),"Comprobante.pdf");
    
            Mail::to($this->correo_receptor)->send($email_a_enviar);
        }
    }

    public function get_base_64_comprobante()
    {
        $detalles_comprobantes = $this->get_detalles_comprobante;
        $tributos_comprobantes = $this->get_tributos_comprobantes;

        $this->fecha = \DateTime::createFromFormat("Y-m-d",$this->fecha);
        $this->fecha = $this->fecha->format("d/m/Y");

        $this->periodo_facturado_desde = \DateTime::createFromFormat("Y-m-d",$this->periodo_facturado_desde);
        $this->periodo_facturado_desde = $this->periodo_facturado_desde->format("d/m/Y");

        $this->periodo_facturado_hasta = \DateTime::createFromFormat("Y-m-d",$this->periodo_facturado_hasta);
        $this->periodo_facturado_hasta = $this->periodo_facturado_hasta->format("d/m/Y");

        $this->vencimiento_del_pago = \DateTime::createFromFormat("Y-m-d",$this->vencimiento_del_pago);
        $this->vencimiento_del_pago = $this->vencimiento_del_pago->format("d/m/Y");

        $configuracion_afip = ConfiguracionAfip::first();

        $inicio_actividades = null;

        if($configuracion_afip)
        {
            if(trim($configuracion_afip->inicio_actividades) != "")
            {
                $inicio_actividades = \DateTime::createFromFormat("Y-m-d",$configuracion_afip->inicio_actividades);
                $inicio_actividades = $inicio_actividades->format("d/m/Y");
            }
        }

        $html = view('backend.comprobantes.comprobante_pdf',array(
            "comprobante_obj"=>$this,
            "detalles_factura"=>$detalles_comprobantes,
            "tributos_comprobantes"=>$tributos_comprobantes,
            "configuracion_afip"=>$configuracion_afip,
            "inicio_actividades" => $inicio_actividades
        ))->render();
    
        $namefile = 'pdf_comprobante_'.$this->numero_afip.'.pdf';

        $defaultConfig = (new \Mpdf\Config\ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];

        $defaultFontConfig = (new \Mpdf\Config\FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];

        $mpdf = new Mpdf([
            'fontDir' => array_merge($fontDirs, [
                public_path() . '/fonts',
            ]),
            /*'fontdata' => $fontData + [
                'arial' => [
                    //'R' => 'arial.ttf',
                ],
            ],*/
            'default_font' => 'arial',
            "format" => "A4",
            //"format" => [264.8,188.9],
        ]);

        $fecha_vencimiento_cae = "";

        if(trim($this->CAEFchVto) != "")
        {
            $fecha_vencimiento_cae = \DateTime::createFromFormat("Y-m-d",$this->CAEFchVto);
            $fecha_vencimiento_cae = $fecha_vencimiento_cae->format("d/m/Y");
        }

        $codigo_de_barras = $this->obtenerCodigoDeBarras();

        $mpdf->SetDisplayMode('fullpage');
        $mpdf->WriteHTML($html);
        $mpdf->SetFooter(
        '<div style="heitght: 300px;margin-top: 15px;">
            <p style="text-align: center;font-size: 10px;font-weight: bold;">Pág. {PAGENO}</p>

            <table width="100%">
                <tr>
                    <td width="50%" align="center">
                        <img src="data:image/png;base64,'.DNS1D::getBarcodePng($codigo_de_barras, "EAN13").'" alt="barcode"/>
                        <br>
                        <p>'.$codigo_de_barras.'</p>
                    </td>
                    <td width="50%" style="text-align: center;">
                        <p><b>CAE:</b> '.$this->CAE.'</p> 
                        <p><b>Fecha de Vto. de CAE:</b> '.$fecha_vencimiento_cae.'</p> 
                    </td>
                </tr>
            </table>
        </div>');

        return $mpdf->Output($namefile,"S");
    }

    public function obtenerCodigoDeBarras()
    {
        $configuracion_afip = ConfiguracionAfip::first();

        $CUIT = $configuracion_afip->cuit;
        $CAE = $this->CAE;
        $CAEFchVto = $this->CAEFchVto;
        $id_tipo_de_comprobante = $this->id_tipo_de_comprobante;
        $punto_de_venta = $this->punto_de_venta;

        while(strlen($id_tipo_de_comprobante) != 3 && strlen($id_tipo_de_comprobante) < 3)
        {
            $id_tipo_de_comprobante = "0".$id_tipo_de_comprobante;
        }

        while(strlen($punto_de_venta) != 5 && strlen($punto_de_venta) < 5)
        {
            $punto_de_venta = "0".$punto_de_venta;
        }
        
        while(strlen($CAE) != 14 && strlen($CAE) < 14)
        {
            $CAE = "0".$CAE;
        }

        $CAEFchVto = \DateTime::createFromFormat("Y-m-d",$CAEFchVto);
        $CAEFchVto = $CAEFchVto->format("Ymd");

        $codigo_de_barras = "".$CUIT.$id_tipo_de_comprobante.$punto_de_venta.$CAE.$CAEFchVto;
        $codigo_de_barras =  $codigo_de_barras.$this->obtenerDigitoVerificadorCodBarras($codigo_de_barras);

        return $codigo_de_barras;
    }

    private function obtenerDigitoVerificadorCodBarras($numero)
    {
        $j=strlen($numero);

        $par=0;$impar=0;
        for ($i=0; $i < $j ; $i++) { 
            if ($i%2==0){
                $par=$par+$numero[$i];
            }else{
                $impar=$impar+$numero[$i];

            }
        }

        $par=$par*3;
        $suma=$par+$impar;
        for ($i=0; $i < 9; $i++) {
            if ( fmod(($suma +$i),10) == 0) {
                $verificador=$i;
            }
        }

        $digito = 10 - ($suma - (intval($suma / 10) * 10));
        if ($digito == 10){
            $digito = 0;
        }
        
        return $digito;
    }
}

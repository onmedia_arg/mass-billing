<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConfiguracionSmtp extends Model
{
    public $table = "configuracion_smtp";
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PuntoDeVenta extends Model
{
    public $table = "puntos_de_venta";
}

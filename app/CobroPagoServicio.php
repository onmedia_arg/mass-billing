<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CobroPagoServicio extends Model
{
    public $table = "cobros_pagos_servicios";
}

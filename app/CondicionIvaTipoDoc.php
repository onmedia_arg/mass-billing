<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CondicionIvaTipoDoc extends Model
{
    public $table = "condiciones_iva_tipos_doc";

    public function get_tipo_documento()
    {
        return $this->hasOne("App\TipoDocumento","id","id_tipo_documento");
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImportacionEstadistica extends Model
{
    public $table = "importaciones_estadisticas";
}

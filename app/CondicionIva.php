<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CondicionIva extends Model
{
    public $table = "condiciones_iva";

    const IVA_RESPONSABLE_INSCRIPTO = 1;
    const IVA_SUJETO_EXENTO = 4;
    const CONSUMIDOR_FINAL = 5;
    const RESPONSABLE_MONOTRIBUTO = 6;
    const PROVEEDOR_DEL_EXTERIOR = 8;
    const CLIENTE_DEL_EXTERIOR = 9;
    const IVA_LIBERADO_LEY_19640 = 10;
    const IVA_RESPONSABLE_INSCRIPTO_AGENTE_DE_PERCEPCION = 11;
    const MONOTRIBUTISTA_SOCIAL = 13;
    const IVA_NO_ALCANZADO = 15;
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

use GuzzleHttp;

use App\ListadoProceso;
use App\EstadoImportacionComprobante;
use App\Comprobante;

class envioRespuestasImportaciones extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'envio:respuestas_importaciones';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envia las respuestas a GTP de cada proceso de importación de comprobantes';


    private $ENDPOINT = "http://motorplan.onmedia.com.ar/api/";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        set_time_limit(-1);

        $proceso_obj = ListadoProceso::where("proceso",$this->signature)->first();

        if($proceso_obj && $proceso_obj->trabajando == false)
        {
            $proceso_obj->trabajando = true;
            $proceso_obj->save();

            try
            {
                $importacion_row = DB::select(
                    "SELECT * FROM `importacion_comprobantes` where (id_estado = ? or id_estado = ?) AND respuesta_realizada = ? ORDER BY ID LIMIT 1",
                    [EstadoImportacionComprobante::FINALIZADO,EstadoImportacionComprobante::FINALIZADO_CON_ERRORES, 0]
                );

                if(count($importacion_row) > 0)
                {
                    $importacion_row = $importacion_row[0];

                    $http_client = new GuzzleHttp\Client(['base_uri' => $this->ENDPOINT,'verify' => false]);

                    $response_login = $http_client->post('login', [
                        'form_params' => [
                            "email" => "adm@gmail.com",
                            "password" => "123456"
                        ]
                    ]);

                    $response_login = json_decode($response_login->getBody()->getContents());

                    if(property_exists($response_login,"access_token") && trim($response_login->access_token) != "")
                    {
                        $data_to_send = array();

                        $comprobantes = Comprobante::where("id_importacion_comprobante",$importacion_row->id)->get();

                        foreach($comprobantes as $comprobante_obj)
                        {
                            $mensajes_errores = $comprobante_obj->errores;

                            $mensajes = array();

                            if(trim($mensajes_errores) != "")
                            {
                                $mensajes_errores = json_decode($mensajes_errores);

                                if(is_array($mensajes_errores) && count($mensajes_errores) > 0)
                                {
                                    $mensajes = $mensajes_errores;
                                }
                            }

                            $data_to_send[] = array(
                                "id_cobro" => $comprobante_obj->id_externo,
                                "nro_factura" => $comprobante_obj->id,
                                "estado" => $comprobante_obj->id_estado,
                                "mensaje" => $mensajes,
                                "key_access" => $comprobante_obj->key_access,
                            );
                        }

                        $headers = [
                            'Authorization' => 'Bearer '.$response_login->access_token,         
                        ];
                        
                        $response_send_data = $http_client->post('FacturaPago', [
                            'headers' => $headers,
                            'form_params' => [
                                "facturas" => json_encode($data_to_send)
                            ]
                        ]);

                        $response_send_data = $response_send_data->getBody()->getContents();

                        if($response_send_data == true)
                        {
                            DB::update("UPDATE importacion_comprobantes set respuesta_realizada = 1 where id = ?",[$importacion_row->id]);
                        }

                        echo "ENVIO:<br>";
                        echo "<pre>";
                        echo json_encode($data_to_send);
                        echo var_dump($response_send_data);
                        echo "<pre>";
                    }
                }
                else
                {
                    $proceso_obj->trabajando = false;
                    $proceso_obj->save();
                }
            }
            catch(\Exception $exception1)
            {
                echo $exception1->getMessage();
            }
            finally
            {
                $proceso_obj->trabajando = false;
                $proceso_obj->save();
            }
        }
    }
}

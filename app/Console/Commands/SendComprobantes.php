<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;

use App\Mail\EmailSender;
use App\Comprobante;
use App\ConfiguracionSmtp;
use App\ListadoProceso;
use App\EstadoComprobante;

class SendComprobantes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:comprobantes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envía los comprobantes que fueron importados y enviados a afip a los receptores';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        set_time_limit(-1);
        $tiempo_inicial = microtime(true);

        $proceso_obj = ListadoProceso::where("proceso",$this->signature)->first();

        if($proceso_obj && $proceso_obj->trabajando == false)
        {
            $proceso_obj->trabajando = true;
            $proceso_obj->save();

            $configuracion_smtp = ConfiguracionSmtp::first();

            if($configuracion_smtp)
            {
                /*
                Config::set('mail.driver','smtp');
                Config::set('mail.encryption',$configuracion_smtp->MAIL_ENCRYPTION);
                Config::set('mail.host',$configuracion_smtp->MAIL_HOST);
                Config::set('mail.port',$configuracion_smtp->MAIL_PORT);
                Config::set('mail.username',$configuracion_smtp->MAIL_USERNAME);
                Config::set('mail.password',$configuracion_smtp->MAIL_PASSWORD);
                Config::set('mail.from',  ['address' => $configuracion_smtp->MAIL_FROM_ADDRESS , 'name' => Config("app.name")]);
                */
                
                try
                {
                    $comprobantes_a_enviar = Comprobante::whereNotNull("id_importacion_comprobante")
                    ->where("id_importacion_comprobante",">",0)
                    ->where("enviar_por_correo",1)
                    ->where("enviado_por_correo",0)
                    ->where("id_estado",EstadoComprobante::REALIZADO)
                    ->get();

                    foreach($comprobantes_a_enviar as $comprobante_a_enviar_obj)
                    {
                        try
                        {  
                            $email_sender = new \stdClass();
                            $email_sender->comprobante_obj = $comprobante_a_enviar_obj;

                            Mail::to(["mario.olivera96@gmail.com","sm.blanco@hotmail.com","german@onmedia.com.ar"])
                            ->send(new EmailSender($email_sender,"Nuevo Comprobante Realizado","emails.envio_de_comprobante"));

                            $comprobante_a_enviar_obj->enviado_por_correo = true;
                            $comprobante_a_enviar_obj->save();
                        }
                        catch(\Exception $e)
                        {
                            //echo $e->getMessage();
                        }

                        // CODIGO ENVIA MENSAJE
                        $tiempo_actual = microtime(true);
                        $tiempo_de_ejecucion_actual = $tiempo_actual - $tiempo_inicial;

                        if(trim($proceso_obj->tiempo_maximo_ejecucion) != "" && $tiempo_de_ejecucion_actual >= $proceso_obj->tiempo_maximo_ejecucion)
                        {
                            break; // salgo del procesamiento
                        }
                    }
                }
                catch(\Exception $e)
                {
                    //echo $e->getMessage();
                }
                finally
                {
                    $proceso_obj->trabajando = false;
                    $proceso_obj->save();
                }
            }
            else
            {
                $proceso_obj->trabajando = false;
                $proceso_obj->save();
            }
        }
    }
}

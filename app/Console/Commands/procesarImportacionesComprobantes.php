<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Library\ResponseEstructure;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Artisan;

use App\Library\WebserviceAfip\WebserviceAfip;

use App\ImportacionComprobante;
use App\EstadoImportacionComprobante;
use App\Comprobante;
use App\EstadoComprobante;
use App\DetalleComprobante;

use App\ListadoProceso;

/*
CARGANDOSE
PENDIENTE
PROCESANDO
FINALIZADO
FINALIZADO_CON_ERRORES
*/

class procesarImportacionesComprobantes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'procesar:importaciones_comprobantes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Procesa las importaciones, generando los comprobantes en AFIP';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        set_time_limit(-1);

        $proceso_obj = ListadoProceso::where("proceso",$this->signature)->first();

        if($proceso_obj && $proceso_obj->trabajando == false)
        {
            $proceso_obj->trabajando = true;
            $proceso_obj->save();

            try
            {
                // CODIGO REALIZADO
                $tiempo_inicial = microtime(true);

                $importacion_a_realizar = DB::select(
                    "SELECT * FROM `importacion_comprobantes` where id_estado = ? or id_estado = ? ORDER BY ID LIMIT 1",
                    [EstadoImportacionComprobante::PENDIENTE,EstadoImportacionComprobante::PROCESANDO]
                );

                if(count($importacion_a_realizar) > 0)
                {
                    $importacion_a_realizar = $importacion_a_realizar[0];

                    /*** EMPEZANDO TRABAJO CON LIBRERIA AFIP ***/
                    $ws_afip = new WebserviceAfip($importacion_a_realizar->id_configuracion_afip);
                    $estado_de_servidor_afip = $ws_afip->obtenerEstadoDeServidor();
                    /*
                    if($estado_de_servidor_afip)
                    {
                        $AppServer = trim(strtoupper($estado_de_servidor_afip->AppServer));
                        $DbServer = trim(strtoupper($estado_de_servidor_afip->DbServer));
                        $AuthServer = trim(strtoupper($estado_de_servidor_afip->AuthServer));

                        if($AppServer != "OK" && $DbServer != "OK" && $AuthServer != "OK")
                        {
                            $proceso_obj->trabajando = false;
                            $proceso_obj->save();
                            die; // DIE MATO EL PROCESO, NO LLEGA A HACER NADA.
                        }
                    }
                    else
                    {
                        $proceso_obj->trabajando = false;
                        $proceso_obj->save();
                        die; // DIE MATO EL PROCESO, NO LLEGA A HACER NADA.
                    }

                    // EL ESTADO DE SERVIDORES DE AFIP ES CORRECTO, VERIFICO AUTORIZACION
                    try
                    {
                        $ws_afip->getTiposDeConceptos();
                    }
                    catch(\Exception $exception2)
                    {
                        $proceso_obj->trabajando = false;
                        $proceso_obj->save();
                        die; // DIE MATO EL PROCESO, NO LLEGA A HACER NADA.
                    }
                    */

                    // EMPEZO LAS VALIDACIONES DE CAMPOS, SETEO DE ERRORES Y LLAMADA A AFIP

                    $comprobantes_a_realizar = Comprobante::where("id_importacion_comprobante",$importacion_a_realizar->id)
                    ->where("id_estado",EstadoComprobante::PENDIENTE)
                    ->get();

                    foreach($comprobantes_a_realizar as $comprobante_a_realizar_obj)
                    {
                        // TRABAJANDO Y EVITANDO ERRORES
                        $punto_de_venta = trim($comprobante_a_realizar_obj->punto_de_venta);
                        $id_tipo_de_comprobante = trim($comprobante_a_realizar_obj->id_tipo_de_comprobante);
                        
                        $response_estructure = new ResponseEstructure();
                        $response_estructure->set_response(true);

                        // REPLICA PASO 1
                        if($punto_de_venta == ""){
                            $response_estructure->set_response(false);
                            $response_estructure->add_message_error("Seleccione un punto de venta");
                        }
            
                        if($id_tipo_de_comprobante == ""){
                            $response_estructure->set_response(false);
                            $response_estructure->add_message_error("Seleccione un id tipo de comprobante");
                        }
            
                        if($id_tipo_de_comprobante != "" && $comprobante_a_realizar_obj->comprobanteDisponible($id_tipo_de_comprobante) == FALSE){
                            $response_estructure->set_response(false);
                            $response_estructure->add_message_error("Solo se encuentran disponibles las Facturas C,Notas de Débito,Notas de Crédito C y Recibo C");
                        }

                        // REPLICA PASO 2
                        $fecha = trim($comprobante_a_realizar_obj->fecha);
                        $id_concepto = trim($comprobante_a_realizar_obj->id_concepto);
                        $id_moneda = strtoupper(trim($comprobante_a_realizar_obj->id_moneda));
                        $cotizacion_de_la_moneda = trim($comprobante_a_realizar_obj->cotizacion_de_la_moneda);
                        $periodo_facturado_desde = trim($comprobante_a_realizar_obj->periodo_facturado_desde);
                        $periodo_facturado_hasta = trim($comprobante_a_realizar_obj->periodo_facturado_hasta);
                        $periodo_facturado_vto = trim($comprobante_a_realizar_obj->vencimiento_del_pago);

                        $input = [
                            "fecha"=>$fecha,
                            "id_concepto"=>$id_concepto,
                            "id_moneda"=>$id_moneda,
                        ];
            
                        $rules = [
                            "fecha"=>"required|date_format:Y-m-d",
                            "id_concepto"=>"required",
                            "id_moneda"=>"required"
                        ];

                        if($id_concepto == $comprobante_a_realizar_obj::$CONCEPTO_SERVICIOS || $id_concepto == $comprobante_a_realizar_obj::$CONCEPTO_PRODUCTOS_Y_SERVICIOS)
                        {
                            $input["periodo_facturado_desde"] = $periodo_facturado_desde;
                            $input["periodo_facturado_hasta"] = $periodo_facturado_hasta;
                            $input["periodo_facturado_vto"] = $periodo_facturado_vto;

                            $rules["periodo_facturado_desde"] = "required|date_format:Y-m-d";
                            $rules["periodo_facturado_hasta"] = "required|date_format:Y-m-d";
                            $rules["periodo_facturado_vto"] = "required|date_format:Y-m-d";
                        }
                        else if($id_concepto != $comprobante_a_realizar_obj::$CONCEPTO_PRODUCTOS)
                        {
                            if(trim($id_concepto) != "")
                            {
                                $response_estructure->set_response(false);
                                $response_estructure->add_message_error("El campo id_concepto no es válido");
                            }
                        }

                        if($id_moneda != "")
                        {
                            if($id_moneda == "PES" || $id_moneda == "DOL")
                            {
                                if($id_moneda == "DOL")
                                {
                                    $input["cotizacion_de_la_moneda"] = $cotizacion_de_la_moneda;
                                    $rules["cotizacion_de_la_moneda"] = "required|regex:/^\d+(\.\d{1,2})?$/";
                                }
                            }
                            else
                            {
                                $response_estructure->set_response(false);
                                $response_estructure->add_message_error("El Valor de id_moneda no es correcto");
                            }
                        }

                        if(is_numeric($cotizacion_de_la_moneda))
                        {
                            if($id_moneda == "PES")
                            {
                                if($cotizacion_de_la_moneda != 1)
                                {
                                    $response_estructure->set_response(false);
                                    $response_estructure->add_message_error("Si la moneda es PES, el campo cotizacion de la moneda debe estar en 1");
                                }
                            }
                        }

                        $validator = Validator::make($input, $rules);

                        if ($validator->fails()) 
                        {
                            $response_estructure->set_response(false);
                            $errors = $validator->errors();

                            foreach ($errors->all() as $error) 
                            {
                                $response_estructure->add_message_error($error);
                            }
                        }
                        else
                        {
                            
                            $fecha_a_validar = $fecha;
                            $fecha_de_hoy = Date("Y-m-d");

                            $date1 = new \DateTime($fecha_de_hoy);
                            $date2 = new \DateTime($fecha_a_validar);
                            $diff = $date1->diff($date2);

                            $diferencia_en_dias = $diff->days;

                            // SERVICIOS
                            if($id_concepto == $comprobante_a_realizar_obj::$CONCEPTO_SERVICIOS || $id_concepto == $comprobante_a_realizar_obj::$CONCEPTO_PRODUCTOS_Y_SERVICIOS)
                            {
                                if($diferencia_en_dias > 10)
                                {
                                    $response_estructure->set_response(false);
                                    $response_estructure->add_message_error("La fecha para servicios debe ser 10 días para atrás o 10 para adelante de la fecha actual como máximo");
                                }

                                if($periodo_facturado_vto < Date("Y-m-d"))
                                {
                                    $response_estructure->set_response(false);
                                    $response_estructure->add_message_error("La fecha del vencimiento del pago no puede ser menor a la fecha actual");
                                }
                            }
                            else if($id_concepto == $comprobante_a_realizar_obj::$CONCEPTO_PRODUCTOS)
                            {
                                if($fecha_de_hoy < $fecha_a_validar)
                                {
                                    $response_estructure->set_response(false);
                                    $response_estructure->add_message_error("La fecha para productos debe ser igual a la fecha de hoy o puede tener 5 días para atrás como máximo");
                                }
                                else
                                {
                                    if($diferencia_en_dias > 5)
                                    {
                                        $response_estructure->set_response(false);
                                        $response_estructure->add_message_error("La fecha para productos debe ser igual a la fecha de hoy o puede tener 5 días para atrás como máximo");
                                    }
                                }
                            }
                        }
                        // FIN VALIDACION PASO 2
                        

                        // VALIDACION PASO 3

                        $condicion_iva_receptor = $comprobante_a_realizar_obj->condicion_iva_receptor;
                        $tipo_documento_receptor = $comprobante_a_realizar_obj->tipo_documento_receptor;
                        $num_documento_receptor = $comprobante_a_realizar_obj->num_documento_receptor;
                        $razon_social_receptor = $comprobante_a_realizar_obj->razon_social_receptor;
                        $correo_receptor = $comprobante_a_realizar_obj->correo_receptor;
                        $domicilio_receptor = $comprobante_a_realizar_obj->domicilio_receptor;
                        $id_condicion_venta = $comprobante_a_realizar_obj->id_condicion_venta;
                        $descripcion_servicio = $comprobante_a_realizar_obj->descripcion_servicio;

                        $ImpTotal = $comprobante_a_realizar_obj->ImpTotal;
                        $comprobante_a_realizar_obj->ImpNeto = $ImpTotal;
                        //$ImpTotConc = $comprobante_a_realizar_obj->ImpTotConc;
                        //$ImpNeto = $comprobante_a_realizar_obj->ImpNeto; // subtotal
                        //$ImpOpEx = $comprobante_a_realizar_obj->ImpOpEx;
                        //$ImpIVA = $comprobante_a_realizar_obj->ImpIVA;
                        //$ImpTrib = $comprobante_a_realizar_obj->ImpTrib; // total_tributos
                        $detalle_comprobante = $comprobante_a_realizar_obj->detalle_comprobante;
                        $id_externo = $comprobante_a_realizar_obj->id_externo;

                        if(trim($detalle_comprobante) != "")
                        {
                            $detalle_comprobante = json_decode($detalle_comprobante,true);

                            if(is_array($detalle_comprobante))
                            {
                                if(count($detalle_comprobante) >  0)
                                {
                                    // CREO EL DETALLE DE COMPROBANTE
                                    for($i=0; $i < count($detalle_comprobante);$i++)
                                    {
                                        $detalle_comprobante_new = new DetalleComprobante();
                                        $detalle_comprobante_new->codigo = $detalle_comprobante[$i]["codigo"];
                                        $detalle_comprobante_new->producto_servicio = $detalle_comprobante[$i]["producto_servicio"];;
                                        $detalle_comprobante_new->cantidad	= $detalle_comprobante[$i]["cantidad"];
                                        $detalle_comprobante_new->id_unidad_medida	= $detalle_comprobante[$i]["id_unidad_medida"];
                                        $detalle_comprobante_new->precio_unitario = (double) $detalle_comprobante[$i]["precio_unitario"];
                                        $detalle_comprobante_new->porcentaje_bonificacion = (double) $detalle_comprobante[$i]["porcentaje_bonificacion"];
                                        $detalle_comprobante_new->importe_bonificacion	= (double) $detalle_comprobante[$i]["importe_bonificacion"];
                                        $detalle_comprobante_new->subtotal	= (double) $detalle_comprobante[$i]["subtotal"];
                                        $detalle_comprobante_new->id_comprobante = $comprobante_a_realizar_obj->id;
                                        $detalle_comprobante_new->save();
                                    }
                                }
                                else
                                {
                                    $response_estructure->set_response(false);
                                    $response_estructure->add_message_error("El comprobante no tiene un detalle");
                                }
                            }
                            else
                            {
                                $response_estructure->set_response(false);
                                $response_estructure->add_message_error("El detalle del comprobante no es correcto");
                            }
                        }
                        else
                        {
                            $response_estructure->set_response(false);
                            $response_estructure->add_message_error("El comprobante no tiene un detalle");
                        }


                        if(trim($condicion_iva_receptor) == "")
                        {
                            $response_estructure->set_response(false);
                            $response_estructure->add_message_error("Condición iva receptor no válido");
                        }

                        if(trim($tipo_documento_receptor) == "")
                        {
                            $response_estructure->set_response(false);
                            $response_estructure->add_message_error("Tipo documento receptor no válido");
                        }

                        if(trim($num_documento_receptor) == "")
                        {
                            $response_estructure->set_response(false);
                            $response_estructure->add_message_error("Num documento receptor no válido");
                        }

                        if(trim($id_condicion_venta) == "")
                        {
                            $response_estructure->set_response(false);
                            $response_estructure->add_message_error("Id condicion venta no válido");
                        }

                        if(trim($correo_receptor) != "")
                        {
                            if(!filter_var($correo_receptor, FILTER_VALIDATE_EMAIL)) 
                            {
                                $response_estructure->set_response(false);
                                $response_estructure->add_message_error("El correo receptor ingresado no es válido");
                            }
                            else
                            {
                                $comprobante_a_realizar_obj->enviar_por_correo = true;
                            }
                        }

                        // IMPORTANTE: VALIDAR ID EXTERNO

                        if(trim($razon_social_receptor) == "" || trim($domicilio_receptor) == "")
                        {
                            // OBTENER CLIENTE CON AFIP

                            // setear datos que están vacíos

                        }

                        // VALIDANDO DETALLE
                        if(is_array($detalle_comprobante) && count($detalle_comprobante) > 0)
                        {
                            $subtotal_calculado = 0;

                            for($i=0; $i < count($detalle_comprobante);$i++)
                            {
                                //$codigo = $detalle_comprobante[$i]["codigo"];
                                //$producto_servicio = $detalle_comprobante[$i]["producto_servicio"];
                                $cantidad = $detalle_comprobante[$i]["cantidad"];
                                //$id_unidad_medida = $detalle_comprobante[$i]["id_unidad_medida"];
                                $precio_unitario = (double) $detalle_comprobante[$i]["precio_unitario"];
                                $porcentaje_bonificacion = (double) $detalle_comprobante[$i]["porcentaje_bonificacion"];
                                $importe_bonificacion = (double) $detalle_comprobante[$i]["importe_bonificacion"];
                                $subtotal = (double) $detalle_comprobante[$i]["subtotal"];

                                $total_calculando = $cantidad * $precio_unitario;
                                $importe_bonificacion_calculando =  ($total_calculando * $porcentaje_bonificacion) / 100;
                            
                                $subtotal_calculado += ($total_calculando - $importe_bonificacion_calculando);
                            }

                            if($ImpTotal != $subtotal_calculado)
                            {
                                $response_estructure->add_message_error("El ImpTotal pasado no es igual al calculado");
                                $response_estructure->set_response(false);
                            }
                        }

                        if(trim($id_externo) == "")
                        {
                            $response_estructure->add_message_error("El id_externo no es válido");
                            $response_estructure->set_response(false);
                        }
                        else
                        {
                            // VALIDAR QUE NO EXISTA EN DB
                        }
                        // FIN VALIDACION PASO 3

                        if($response_estructure->get_response() == true)
                        {
                            try
                            {
                                //$comprobante_a_realizar_obj = $ws_afip->crearFacturaWithCAEProcesoAPI($comprobante_a_realizar_obj);
                                $comprobante_a_realizar_obj->errores = json_encode(array());
                                $comprobante_a_realizar_obj->id_estado = EstadoComprobante::REALIZADO;
                                $comprobante_a_realizar_obj->CAE = "70399867750579";
                                $comprobante_a_realizar_obj->CAEFchVto = "2020-10-09";

                                $comprobante_a_realizar_obj->key_access = $this->generate_key_access();
                            }
                            catch(\Exception $exception3)
                            {
                                $mensaje_de_afip = $exception3->getMessage();

                                if(trim($mensaje_de_afip) == "")
                                {
                                    $mensaje_de_afip = "No se pudo encontrar el mensaje de error de afip";
                                }
                                
                                $response_estructure->add_message_error($mensaje_de_afip);
                                $response_estructure->set_response(false);

                                $comprobante_a_realizar_obj->errores = json_encode($response_estructure->get_messages_errors());
                                $comprobante_a_realizar_obj->id_estado = EstadoComprobante::ERROR_DE_AFIP;
                            }
                        }
                        else
                        {
                            $comprobante_a_realizar_obj->errores = json_encode($response_estructure->get_messages_errors());
                            $comprobante_a_realizar_obj->id_estado = EstadoComprobante::ERROR_DE_VALIDACION;
                        }

                        $comprobante_a_realizar_obj->save();

                        $tiempo_actual = microtime(true);
                        $tiempo_de_ejecucion_actual = $tiempo_actual - $tiempo_inicial;

                        if(trim($proceso_obj->tiempo_maximo_ejecucion) != "" && $tiempo_de_ejecucion_actual >= $proceso_obj->tiempo_maximo_ejecucion)
                        {
                            break; // salgo del procesamiento
                        }

                        $this->actualizarCantidadesImportacion($importacion_a_realizar);
                    }
                }
                // FIN CODIGO REALIZADO

                $proceso_obj->trabajando = false;
                $proceso_obj->save();
            }
            catch(\Exception $exception1)
            {
                echo $exception1->getMessage();
            }
            finally
            {
                $proceso_obj->trabajando = false;
                $proceso_obj->save();
            }
        }

        Artisan::call("envio:respuestas_importaciones");
    }

    private function actualizarCantidadesImportacion($importacion_a_realizar)
    {
        $cantidad_pendientes = DB::select(
            "SELECT count(id) as cantidad FROM `comprobantes` where id_importacion_comprobante = ? and id_estado = ?",
            [$importacion_a_realizar->id,EstadoComprobante::PENDIENTE]
        );
        $cantidad_pendientes = $cantidad_pendientes[0]->cantidad;

        $cantidad_realizados = DB::select(
            "SELECT count(id) as cantidad FROM `comprobantes` where id_importacion_comprobante = ? and id_estado = ?",
            [$importacion_a_realizar->id,EstadoComprobante::REALIZADO]
        );
        $cantidad_realizados = $cantidad_realizados[0]->cantidad;

        $cantidad_error_de_validacion = DB::select(
            "SELECT count(id) as cantidad FROM `comprobantes` where id_importacion_comprobante = ? and id_estado = ?",
            [$importacion_a_realizar->id,EstadoComprobante::ERROR_DE_VALIDACION]
        );
        $cantidad_error_de_validacion = $cantidad_error_de_validacion[0]->cantidad;

        $cantidad_error_de_afip = DB::select(
            "SELECT count(id) as cantidad FROM `comprobantes` where id_importacion_comprobante = ? and id_estado = ?",
            [$importacion_a_realizar->id,EstadoComprobante::ERROR_DE_AFIP]
        );
        $cantidad_error_de_afip = $cantidad_error_de_afip[0]->cantidad;
        

        $importacion_comprobante_obj = ImportacionComprobante::where("id",$importacion_a_realizar->id)->first();

        $importacion_comprobante_obj->cantidad_realizados = $cantidad_realizados;
        $importacion_comprobante_obj->cantidad_con_errores = ($cantidad_error_de_validacion + $cantidad_error_de_afip);
        $importacion_comprobante_obj->cantidad_pendientes = $cantidad_pendientes;

        if($cantidad_pendientes > 0)
        {
            $importacion_comprobante_obj->id_estado = EstadoImportacionComprobante::PROCESANDO;
        }
        else if($cantidad_error_de_validacion > 0 || $cantidad_error_de_afip > 0) 
        {
            $importacion_comprobante_obj->id_estado = EstadoImportacionComprobante::FINALIZADO_CON_ERRORES;
        }
        else
        {
            $importacion_comprobante_obj->id_estado = EstadoImportacionComprobante::FINALIZADO;
        }

        $importacion_comprobante_obj->save();
    }

    private function generate_key_access($length = 15){
		$characters = '1HajKpqOjsNAmkLHGA78fFWqLmGvDFSOQiIL653qQjGyY';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
    }
}

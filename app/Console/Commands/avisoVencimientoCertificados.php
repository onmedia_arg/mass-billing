<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Configuracion;
use App\Usuario;

use Illuminate\Support\Facades\Mail;
use App\Mail\EmailSender;

class avisoVencimientoCertificados extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'aviso:vencimientos_certificados';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $mail_aviso_vencimiento_afip = Configuracion::where("clave","MAIL_AVISO_VENCIMIENTO_AFIP")->first();

        if($mail_aviso_vencimiento_afip)
        {
            if(filter_var($mail_aviso_vencimiento_afip->valor, FILTER_VALIDATE_EMAIL)) 
            {
                $fecha_actual = date('Y-m-d');
                $fecha_vencimiento_a_buscar = strtotime('+7 day',strtotime($fecha_actual));
                $fecha_vencimiento_a_buscar = date ('Y-m-d',$fecha_vencimiento_a_buscar);

                $usuarios_certificados_a_vencer = Usuario::where("eliminado",0)
                ->where("vencimiento_certificado",$fecha_vencimiento_a_buscar)
                ->get();

                if(count($usuarios_certificados_a_vencer) > 0)
                {
                    $fecha_vencimiento_a_buscar = \DateTime::createFromFormat("Y-m-d",$fecha_vencimiento_a_buscar);
                    $fecha_vencimiento_a_buscar = $fecha_vencimiento_a_buscar->format("d/m/Y");

                    $email_sender = new \stdClass();
                    $email_sender->usuarios_certificados_a_vencer = $usuarios_certificados_a_vencer;
                    $email_sender->fecha_vencimiento_a_buscar = $fecha_vencimiento_a_buscar;

                    Mail::to($mail_aviso_vencimiento_afip->valor)
                    ->send(new EmailSender($email_sender,"Certificados AFIP a vencer","emails.certificados_a_vencer"));
                }
            }
        }
    }
}

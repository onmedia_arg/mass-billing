<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\EstadoCobroServicio;
use Illuminate\Support\Facades\DB;

class vencimientoPlanes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vencimiento:planes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::update(
            "UPDATE cobros_pagos_servicios 
            SET id_estado = ? 
            WHERE id IN (
                SELECT cobros_pagos_servicios.id
                FROM cobros_pagos_servicios 
                WHERE fecha_vencimiento <= ? 
                AND id_estado = ?
            )
        ",[EstadoCobroServicio::NO_ACTIVO,date("Y-m-d"),EstadoCobroServicio::ACTIVO]);
    }
}

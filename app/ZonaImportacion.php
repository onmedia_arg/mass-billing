<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ZonaImportacion extends Model
{
    public $table = "zonas_importaciones";

    const IMPORTACION_COMPROBANTES = 1;
}

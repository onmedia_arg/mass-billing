<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolicitudRegistro extends Model
{
    public $table = "solicitudes_registros";
}

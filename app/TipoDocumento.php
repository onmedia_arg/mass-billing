<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoDocumento extends Model
{
    public $table = "tipos_documentos";

    public static $CUIT = 80;
}

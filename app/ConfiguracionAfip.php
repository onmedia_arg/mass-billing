<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConfiguracionAfip extends Model
{
    public $table = "configuraciones_afip";

    public function get_condicion_iva()
    {
    	return $this->hasOne("App\CondicionIva","id","id_condicion_iva");
    }
}

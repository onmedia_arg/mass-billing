<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComprobanteAsociado extends Model
{
    public $table = "comprobantes_asociados";
}

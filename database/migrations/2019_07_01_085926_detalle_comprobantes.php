<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DetalleComprobantes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("detalle_comprobantes",function(Blueprint $table){
            $table->increments("id")->unsigned();

            $table->string("codigo");
            $table->string("producto_servicio");
            $table->double("cantidad");
            $table->integer("id_unidad_medida")->unsigned();
            $table->foreign("id_unidad_medida")->references("id")->on("unidades_de_medida");
            $table->double("precio_unitario");
            $table->double("porcentaje_bonificacion");
            $table->double("importe_bonificacion");
            $table->double("subtotal");
            $table->integer("id_comprobante")->unsigned();
            $table->foreign("id_comprobante")->references("id")->on("comprobantes");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::dropIfExists('detalle_comprobantes');
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}

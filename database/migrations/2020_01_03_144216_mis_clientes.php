<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MisClientes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("mis_clientes",function(Blueprint $table){

            $table->increments("id")->unsigned();

            $table->integer("id_tipo_documento")->unsigned();
            $table->foreign("id_tipo_documento")->references("id")->on("tipos_documentos");
            $table->string("nombre_razon_social");
            $table->string("documento");
            $table->string("correo")->nullable();
            $table->string("telefono")->nullable();
            $table->string("domicilio_comercial")->nullable();

            $table->integer("id_condicion_iva")->nullable()->unsigned();
            $table->foreign("id_condicion_iva")->references("id")->on("condiciones_iva");
            
            $table->boolean("eliminado")->default(0);

            $table->integer("id_configuracion_afip")->unsigned();
            $table->foreign("id_configuracion_afip")->references("id")->on("configuraciones_afip");

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    { 
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::dropIfExists('mis_clientes');
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}

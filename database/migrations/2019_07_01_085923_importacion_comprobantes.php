<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImportacionComprobantes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('importacion_comprobantes', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer("cantidad_total")->default(0);
            $table->integer("cantidad_realizados")->default(0);
            $table->integer("cantidad_con_errores")->default(0);
            $table->integer("cantidad_pendientes")->default(0);
            $table->integer("id_estado")->unsigned();
            $table->foreign("id_estado")->references("id")->on("estados_importaciones_comprobantes");
            $table->boolean("respuesta_realizada")->default(false);
            $table->integer("id_configuracion_afip")->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::dropIfExists('importacion_comprobantes');
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}

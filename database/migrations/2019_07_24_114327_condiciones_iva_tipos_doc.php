<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CondicionesIvaTiposDoc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("condiciones_iva_tipos_doc",function(Blueprint $table){
            $table->increments("id")->unsigned();
            $table->integer("id_condicion_iva")->unsigned();
            $table->foreign("id_condicion_iva")->references("id")->on("condiciones_iva");
            $table->integer("id_tipo_documento")->unsigned();
            $table->foreign("id_tipo_documento")->references("id")->on("tipos_documentos");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::dropIfExists('condiciones_iva_tipos_doc');
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}

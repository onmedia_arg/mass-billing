<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ZonasImportaciones extends Migration
{
    public function up()
    {
        Schema::create("zonas_importaciones",function(Blueprint $table){

            $table->increments("id")->unsigned();
            $table->string("zona_importacion");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::dropIfExists('zonas_importaciones');
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}

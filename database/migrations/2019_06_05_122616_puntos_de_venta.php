<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PuntosDeVenta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('puntos_de_venta', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('numero');
          $table->string("descripcion",150);
          $table->string("EmisionTipo");
          $table->string("Bloqueado");
          $table->string("FchBaja");
          $table->integer("id_configuracion_afip")->unsigned();
          $table->foreign("id_configuracion_afip")->references("id")->on("configuraciones_afip");

          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::dropIfExists('puntos_de_venta');
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}

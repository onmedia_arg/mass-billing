<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProductosServicios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("productos_servicios",function(Blueprint $table){

            $table->increments("id")->unsigned();

            $table->string("codigo");
            $table->string("descripcion");
            $table->double("precio")->nullable();
            $table->double("precio_dolar")->nullable();
            $table->enum("tipo",["producto","servicio"]);
            
            $table->integer("id_unidad_medida")->unsigned();
            $table->foreign("id_unidad_medida")->references("id")->on("unidades_de_medida");

            $table->boolean("eliminado")->default(0);

            $table->integer("id_configuracion_afip")->unsigned();
            $table->foreign("id_configuracion_afip")->references("id")->on("configuraciones_afip");

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::dropIfExists('productos_servicios');
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ConfiguracionesAfip extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configuraciones_afip', function (Blueprint $table) {
            $table->increments('id')->unsigned();

            $table->string('razon_social');
            $table->string('cuit');
            $table->string("domicilio_comercial");
            $table->string("ingresos_brutos");
            $table->date("inicio_actividades");
            $table->integer("id_condicion_iva")->unsigned();
            $table->foreign("id_condicion_iva")->references("id")->on("condiciones_iva");

            $table->longText('csr_afip_produccion')->nullable()->default(null);
            $table->longText('key_afip_produccion')->nullable()->default(null);

            $table->longText('csr_afip_testing')->nullable()->default(null);
            $table->longText('key_afip_testing')->nullable()->default(null);

            $table->enum('ambiente',['produccion','testing']);
            $table->string("logo_comprobante",255)->default("default.png");
            $table->boolean("eliminado")->default(false);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::dropIfExists('configuraciones_afip');
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TiposDeComprobantes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('tipos_de_comprobantes', function (Blueprint $table) {
          $table->integer('Id')->unsigned();
          $table->primary('Id');
          $table->string("Desc",150);
          $table->integer("FchDesde")->nullable();
          $table->integer("FchHasta")->nullable();
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::dropIfExists('tipos_de_comprobantes');
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}

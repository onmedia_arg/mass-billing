<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Usuarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('nombre',50);
            $table->string('apellido',50);
            $table->string('correo',150)->unique();
            $table->string('usuario',150)->nullable()->unique();
            $table->string('password');
            $table->string("foto_perfil",255)->default("default.png");
            $table->integer("id_rol")->nullable()->unsigned();
            $table->foreign("id_rol")->references("id")->on("roles")->onDelete("set Null");
            $table->integer("id_estado_usuario")->nullable()->unsigned();
            $table->foreign("id_estado_usuario")->references("id")->on("estados_usuarios");
            $table->integer("id_configuracion_afip")->unsigned();
            $table->foreign("id_configuracion_afip")->references("id")->on("configuraciones_afip");
            $table->boolean("eliminado")->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::dropIfExists('usuarios');
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ConfiguracionSmtp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configuracion_smtp', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('MAIL_FROM_ADDRESS',255);
            $table->string('MAIL_DRIVER',255);
            $table->string('MAIL_HOST',255);
            $table->string('MAIL_PORT',255);
            $table->string('MAIL_USERNAME',255);
            $table->string('MAIL_PASSWORD',255);
            $table->string('MAIL_ENCRYPTION',255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::dropIfExists('configuracion_smtp');
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ComprobantesAsociados extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("comprobantes_asociados",function(Blueprint $table){

            $table->increments("id");
            $table->integer("id_tipo_de_comprobante");
            $table->integer("id_punto_de_venta");
            $table->integer("comprobante");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::dropIfExists('comprobantes_asociados');
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}

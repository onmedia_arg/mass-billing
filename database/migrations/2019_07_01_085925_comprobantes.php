<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Comprobantes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('comprobantes', function(Blueprint $table){

        $table->increments("id");
        $table->integer("id_tipo_de_comprobante")->nullable();
        //$table->foreign("id_tipo_de_comprobante")->references("Id")->on("tipos_de_comprobantes");
        
        $table->integer("numero_afip")->nullable();
        $table->string("CAE")->nullable();
        $table->string("CAEFchVto")->nullable();

        $table->string("punto_de_venta")->nullable();

        $table->date("fecha")->nullable();
        $table->integer("id_concepto")->nullable();
        $table->string("id_moneda")->nullable();
        $table->double("cotizacion_de_la_moneda")->nullable();
        $table->double("cotizacion_del_dolar")->nullable();

        $table->date("periodo_facturado_desde")->nullable();
        $table->date("periodo_facturado_hasta")->nullable();
        $table->date("vencimiento_del_pago")->nullable();

        $table->integer("condicion_iva_receptor")->nullable();;
        //$table->foreign("condicion_iva_receptor")->references("id")->on("condiciones_iva");

        $table->integer("tipo_documento_receptor")->nullable();;
        //$table->foreign("tipo_documento_receptor")->references("id")->on("tipos_documentos");

        $table->bigInteger("num_documento_receptor")->nullable();;
        $table->string("razon_social_receptor")->nullable();
        $table->string("correo_receptor")->nullable();
        $table->string("domicilio_receptor")->nullable();

        $table->integer("id_condicion_venta")->nullable();
        //$table->foreign("id_condicion_venta")->references("id")->on("condiciones_de_venta");

        $table->double("ImpTotal")->nullable();
        $table->double("ImpTotConc")->nullable();
        $table->double("ImpNeto")->nullable();
        $table->double("ImpOpEx")->nullable();
        $table->double("ImpIVA")->nullable();
        $table->double("ImpTrib")->nullable();
        
        $table->integer("id_estado")->unsigned();
        $table->foreign("id_estado")->references("id")->on("estado_comprobante");
        
        $table->text("descripcion_servicio")->nullable();
        
        $table->text("aclaracion_usuario")->nullable();
        $table->text("codigo_de_barras",255)->nullable();


        $table->integer("id_externo")->nullable();

        $table->integer("id_configuracion_afip")->unsigned();
        //$table->foreign("id_configuracion_afip")->references("id")->on("configuraciones_afip");

        $table->integer("id_importacion_comprobante")->unsigned()->nullable();
        $table->foreign("id_importacion_comprobante")->references("id")->on("importacion_comprobantes");

        $table->longText("detalle_comprobante")->nullable();

        $table->boolean("enviar_por_correo")->default(false);
        $table->boolean("enviado_por_correo")->default(false);
        $table->string("key_access")->nullable();

        $table->longText("errores")->nullable();

        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::dropIfExists('comprobantes');
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}

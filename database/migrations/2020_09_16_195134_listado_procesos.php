<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ListadoProcesos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("listado_procesos",function(Blueprint $table){
            $table->increments("id")->unsigned();
            $table->string("proceso",100)->unique();
            $table->boolean("trabajando")->default(false);
            $table->integer("tiempo_maximo_ejecucion");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    { 
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::dropIfExists('listado_procesos');
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CondicionesIva extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('condiciones_iva', function (Blueprint $table) {
          $table->integer('id')->unsigned();
          $table->primary('id');
          $table->string("descripcion",150);
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::dropIfExists('condiciones_iva');
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}

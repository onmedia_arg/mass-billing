<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Importaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("importaciones",function(Blueprint $table){

            $table->increments("id");

            $table->string("archivo");
            $table->integer("id_zona_importacion")->unsigned();
            $table->foreign("id_zona_importacion")->references("id")->on("zonas_importaciones");
            $table->integer("ultimo_row_leido")->nullable();
            $table->integer("cant_total_row");
            $table->string("nombre_pagina");

            $table->integer("id_configuracion_afip")->unsigned();
            $table->foreign("id_configuracion_afip")->references("id")->on("configuraciones_afip");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::dropIfExists('importaciones');
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}

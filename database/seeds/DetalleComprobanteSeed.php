<?php

use Illuminate\Database\Seeder;
use App\DetalleComprobante;

class DetalleComprobanteSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DetalleComprobante::truncate();
    }
}

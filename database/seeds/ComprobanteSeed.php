<?php

use Illuminate\Database\Seeder;
use App\Comprobante;

class ComprobanteSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Comprobante:: truncate();
    }
}

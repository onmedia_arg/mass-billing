<?php

use Illuminate\Database\Seeder;
use App\UnidadDeMedida;

class UnidadDeMedidaSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UnidadDeMedida::truncate();

        $unidad_obj = new UnidadDeMedida();
        $unidad_obj->id = "1";
        $unidad_obj->descripcion = "kilogramos";
        $unidad_obj->save();


        $unidad_obj = new UnidadDeMedida();
        $unidad_obj->id = "2";
        $unidad_obj->descripcion = "metros";
        $unidad_obj->save();


        $unidad_obj = new UnidadDeMedida();
        $unidad_obj->id = "3";
        $unidad_obj->descripcion = "metros cuadrados";
        $unidad_obj->save();


        $unidad_obj = new UnidadDeMedida();
        $unidad_obj->id = "4";
        $unidad_obj->descripcion = "metros cúbicos";
        $unidad_obj->save();


        $unidad_obj = new UnidadDeMedida();
        $unidad_obj->id = "5";
        $unidad_obj->descripcion = "litros";
        $unidad_obj->save();


        $unidad_obj = new UnidadDeMedida();
        $unidad_obj->id = "6";
        $unidad_obj->descripcion = "1000 kWh";
        $unidad_obj->save();


        $unidad_obj = new UnidadDeMedida();
        $unidad_obj->id = "7";
        $unidad_obj->descripcion = "unidades";
        $unidad_obj->save();


        $unidad_obj = new UnidadDeMedida();
        $unidad_obj->id = "8";
        $unidad_obj->descripcion = "pares";
        $unidad_obj->save();


        $unidad_obj = new UnidadDeMedida();
        $unidad_obj->id = "9";
        $unidad_obj->descripcion = "docenas";
        $unidad_obj->save();


        $unidad_obj = new UnidadDeMedida();
        $unidad_obj->id = "10";
        $unidad_obj->descripcion = "quilates";
        $unidad_obj->save();


        $unidad_obj = new UnidadDeMedida();
        $unidad_obj->id = "11";
        $unidad_obj->descripcion = "millares";
        $unidad_obj->save();


        $unidad_obj = new UnidadDeMedida();
        $unidad_obj->id = "14";
        $unidad_obj->descripcion = "gramos";
        $unidad_obj->save();


        $unidad_obj = new UnidadDeMedida();
        $unidad_obj->id = "15";
        $unidad_obj->descripcion = "milimetros";
        $unidad_obj->save();


        $unidad_obj = new UnidadDeMedida();
        $unidad_obj->id = "16";
        $unidad_obj->descripcion = "mm cúbicos";
        $unidad_obj->save();


        $unidad_obj = new UnidadDeMedida();
        $unidad_obj->id = "17";
        $unidad_obj->descripcion = "kilómetros";
        $unidad_obj->save();


        $unidad_obj = new UnidadDeMedida();
        $unidad_obj->id = "18";
        $unidad_obj->descripcion = "hectolitros";
        $unidad_obj->save();


        $unidad_obj = new UnidadDeMedida();
        $unidad_obj->id = "20";
        $unidad_obj->descripcion = "centímetros";
        $unidad_obj->save();


        $unidad_obj = new UnidadDeMedida();
        $unidad_obj->id = "25";
        $unidad_obj->descripcion = "jgo. pqt. mazo naipes";
        $unidad_obj->save();


        $unidad_obj = new UnidadDeMedida();
        $unidad_obj->id = "27";
        $unidad_obj->descripcion = "cm cúbicos";
        $unidad_obj->save();


        $unidad_obj = new UnidadDeMedida();
        $unidad_obj->id = "29";
        $unidad_obj->descripcion = "toneladas";
        $unidad_obj->save();


        $unidad_obj = new UnidadDeMedida();
        $unidad_obj->id = "30";
        $unidad_obj->descripcion = "dam cúbicos";
        $unidad_obj->save();


        $unidad_obj = new UnidadDeMedida();
        $unidad_obj->id = "31";
        $unidad_obj->descripcion = "hm cúbicos";
        $unidad_obj->save();


        $unidad_obj = new UnidadDeMedida();
        $unidad_obj->id = "32";
        $unidad_obj->descripcion = "km cúbicos";
        $unidad_obj->save();


        $unidad_obj = new UnidadDeMedida();
        $unidad_obj->id = "33";
        $unidad_obj->descripcion = "microgramos";
        $unidad_obj->save();


        $unidad_obj = new UnidadDeMedida();
        $unidad_obj->id = "34";
        $unidad_obj->descripcion = "nanogramos";
        $unidad_obj->save();


        $unidad_obj = new UnidadDeMedida();
        $unidad_obj->id = "35";
        $unidad_obj->descripcion = "picogramos";
        $unidad_obj->save();


        $unidad_obj = new UnidadDeMedida();
        $unidad_obj->id = "41";
        $unidad_obj->descripcion = "miligramos";
        $unidad_obj->save();


        $unidad_obj = new UnidadDeMedida();
        $unidad_obj->id = "47";
        $unidad_obj->descripcion = "mililitros";
        $unidad_obj->save();


        $unidad_obj = new UnidadDeMedida();
        $unidad_obj->id = "48";
        $unidad_obj->descripcion = "curie";
        $unidad_obj->save();


        $unidad_obj = new UnidadDeMedida();
        $unidad_obj->id = "49";
        $unidad_obj->descripcion = "milicurie";
        $unidad_obj->save();


        $unidad_obj = new UnidadDeMedida();
        $unidad_obj->id = "50";
        $unidad_obj->descripcion = "microcurie";
        $unidad_obj->save();


        $unidad_obj = new UnidadDeMedida();
        $unidad_obj->id = "51";
        $unidad_obj->descripcion = "uiacthor";
        $unidad_obj->save();


        $unidad_obj = new UnidadDeMedida();
        $unidad_obj->id = "52";
        $unidad_obj->descripcion = "muiacthor";
        $unidad_obj->save();


        $unidad_obj = new UnidadDeMedida();
        $unidad_obj->id = "53";
        $unidad_obj->descripcion = "kg base";
        $unidad_obj->save();


        $unidad_obj = new UnidadDeMedida();
        $unidad_obj->id = "54";
        $unidad_obj->descripcion = "gruesa";
        $unidad_obj->save();


        $unidad_obj = new UnidadDeMedida();
        $unidad_obj->id = "61";
        $unidad_obj->descripcion = "kg bruto";
        $unidad_obj->save();


        $unidad_obj = new UnidadDeMedida();
        $unidad_obj->id = "62";
        $unidad_obj->descripcion = "uiactant";
        $unidad_obj->save();


        $unidad_obj = new UnidadDeMedida();
        $unidad_obj->id = "63";
        $unidad_obj->descripcion = "muiactant";
        $unidad_obj->save();


        $unidad_obj = new UnidadDeMedida();
        $unidad_obj->id = "64";
        $unidad_obj->descripcion = "uiactig";
        $unidad_obj->save();


        $unidad_obj = new UnidadDeMedida();
        $unidad_obj->id = "65";
        $unidad_obj->descripcion = "muiactig";
        $unidad_obj->save();


        $unidad_obj = new UnidadDeMedida();
        $unidad_obj->id = "66";
        $unidad_obj->descripcion = "kg activo";
        $unidad_obj->save();


        $unidad_obj = new UnidadDeMedida();
        $unidad_obj->id = "67";
        $unidad_obj->descripcion = "gramo activo";
        $unidad_obj->save();


        $unidad_obj = new UnidadDeMedida();
        $unidad_obj->id = "68";
        $unidad_obj->descripcion = "gramo base";
        $unidad_obj->save();


        $unidad_obj = new UnidadDeMedida();
        $unidad_obj->id = "96";
        $unidad_obj->descripcion = "packs";
        $unidad_obj->save();


        $unidad_obj = new UnidadDeMedida();
        $unidad_obj->id = "98";
        $unidad_obj->descripcion = "otras unidades";
        $unidad_obj->save();
    }
}

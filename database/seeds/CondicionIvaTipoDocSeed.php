<?php

use Illuminate\Database\Seeder;
use App\CondicionIvaTipoDoc;

class CondicionIvaTipoDocSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CondicionIvaTipoDoc::truncate();

        // IVA Responsable Inscripto
        $condicion_iva_tipo_doc = new CondicionIvaTipoDoc();
        $condicion_iva_tipo_doc->id_condicion_iva = 1;
        $condicion_iva_tipo_doc->id_tipo_documento = 80; // CUIT
        $condicion_iva_tipo_doc->save();

        // IVA Sujeto Exento
        $condicion_iva_tipo_doc = new CondicionIvaTipoDoc();
        $condicion_iva_tipo_doc->id_condicion_iva = 4;
        $condicion_iva_tipo_doc->id_tipo_documento = 80; // CUIT
        $condicion_iva_tipo_doc->save();

        // Responsable Monotributo
        $condicion_iva_tipo_doc = new CondicionIvaTipoDoc();
        $condicion_iva_tipo_doc->id_condicion_iva = 6;
        $condicion_iva_tipo_doc->id_tipo_documento = 80; // CUIT
        $condicion_iva_tipo_doc->save();

        // IVA Responsable Inscripto - Agente de Percepción
        $condicion_iva_tipo_doc = new CondicionIvaTipoDoc();
        $condicion_iva_tipo_doc->id_condicion_iva = 11;
        $condicion_iva_tipo_doc->id_tipo_documento = 80; // CUIT
        $condicion_iva_tipo_doc->save();

        // Monotributista Social
        $condicion_iva_tipo_doc = new CondicionIvaTipoDoc();
        $condicion_iva_tipo_doc->id_condicion_iva = 13;
        $condicion_iva_tipo_doc->id_tipo_documento = 80; // CUIT
        $condicion_iva_tipo_doc->save();

        // IVA No Alcanzado
        $condicion_iva_tipo_doc = new CondicionIvaTipoDoc();
        $condicion_iva_tipo_doc->id_condicion_iva = 15;
        $condicion_iva_tipo_doc->id_tipo_documento = 80; // CUIT
        $condicion_iva_tipo_doc->save();

        // Consumidor Final
        $condicion_iva_tipo_doc = new CondicionIvaTipoDoc();
        $condicion_iva_tipo_doc->id_condicion_iva = 5;
        $condicion_iva_tipo_doc->id_tipo_documento = 80; // CUIT
        $condicion_iva_tipo_doc->save();

        $condicion_iva_tipo_doc = new CondicionIvaTipoDoc();
        $condicion_iva_tipo_doc->id_condicion_iva = 5;
        $condicion_iva_tipo_doc->id_tipo_documento = 87; // CDI
        $condicion_iva_tipo_doc->save();

        $condicion_iva_tipo_doc = new CondicionIvaTipoDoc();
        $condicion_iva_tipo_doc->id_condicion_iva = 5;
        $condicion_iva_tipo_doc->id_tipo_documento = 89; // LE
        $condicion_iva_tipo_doc->save();

        $condicion_iva_tipo_doc = new CondicionIvaTipoDoc();
        $condicion_iva_tipo_doc->id_condicion_iva = 5;
        $condicion_iva_tipo_doc->id_tipo_documento = 90; // LC
        $condicion_iva_tipo_doc->save();

        $condicion_iva_tipo_doc = new CondicionIvaTipoDoc();
        $condicion_iva_tipo_doc->id_condicion_iva = 5;
        $condicion_iva_tipo_doc->id_tipo_documento = 91; // CI Extranjera
        $condicion_iva_tipo_doc->save();

        $condicion_iva_tipo_doc = new CondicionIvaTipoDoc();
        $condicion_iva_tipo_doc->id_condicion_iva = 5;
        $condicion_iva_tipo_doc->id_tipo_documento = 96; // DNI
        $condicion_iva_tipo_doc->save();

        $condicion_iva_tipo_doc = new CondicionIvaTipoDoc();
        $condicion_iva_tipo_doc->id_condicion_iva = 5;
        $condicion_iva_tipo_doc->id_tipo_documento = 94; // Pasaporte
        $condicion_iva_tipo_doc->save();

        $condicion_iva_tipo_doc = new CondicionIvaTipoDoc();
        $condicion_iva_tipo_doc->id_condicion_iva = 5;
        $condicion_iva_tipo_doc->id_tipo_documento = 00; // CI Policía Federal
        $condicion_iva_tipo_doc->save();

        $condicion_iva_tipo_doc = new CondicionIvaTipoDoc();
        $condicion_iva_tipo_doc->id_condicion_iva = 5;
        $condicion_iva_tipo_doc->id_tipo_documento = 30; // Certificado de Migración
        $condicion_iva_tipo_doc->save();

        // Proveedor del Exterior

        $condicion_iva_tipo_doc = new CondicionIvaTipoDoc();
        $condicion_iva_tipo_doc->id_condicion_iva = 8;
        $condicion_iva_tipo_doc->id_tipo_documento = 80; // CUIT
        $condicion_iva_tipo_doc->save();

        $condicion_iva_tipo_doc = new CondicionIvaTipoDoc();
        $condicion_iva_tipo_doc->id_condicion_iva = 8;
        $condicion_iva_tipo_doc->id_tipo_documento = 91; // CI Extranjera
        $condicion_iva_tipo_doc->save();

        $condicion_iva_tipo_doc = new CondicionIvaTipoDoc();
        $condicion_iva_tipo_doc->id_condicion_iva = 8;
        $condicion_iva_tipo_doc->id_tipo_documento = 94; // Pasaporte
        $condicion_iva_tipo_doc->save();

        $condicion_iva_tipo_doc = new CondicionIvaTipoDoc();
        $condicion_iva_tipo_doc->id_condicion_iva = 8;
        $condicion_iva_tipo_doc->id_tipo_documento = 99; // Doc. (Otro)
        $condicion_iva_tipo_doc->save();

        // Cliente del Exterior

        $condicion_iva_tipo_doc = new CondicionIvaTipoDoc();
        $condicion_iva_tipo_doc->id_condicion_iva = 9;
        $condicion_iva_tipo_doc->id_tipo_documento = 80; // CUIT
        $condicion_iva_tipo_doc->save();

        $condicion_iva_tipo_doc = new CondicionIvaTipoDoc();
        $condicion_iva_tipo_doc->id_condicion_iva = 9;
        $condicion_iva_tipo_doc->id_tipo_documento = 91; // CI Extranjera
        $condicion_iva_tipo_doc->save();

        $condicion_iva_tipo_doc = new CondicionIvaTipoDoc();
        $condicion_iva_tipo_doc->id_condicion_iva = 9;
        $condicion_iva_tipo_doc->id_tipo_documento = 94; // Pasaporte
        $condicion_iva_tipo_doc->save();

        $condicion_iva_tipo_doc = new CondicionIvaTipoDoc();
        $condicion_iva_tipo_doc->id_condicion_iva = 9;
        $condicion_iva_tipo_doc->id_tipo_documento = 99; // Doc. (Otro)
        $condicion_iva_tipo_doc->save();

        // IVA Liberado - Ley Nº 19.640

        $condicion_iva_tipo_doc = new CondicionIvaTipoDoc();
        $condicion_iva_tipo_doc->id_condicion_iva = 10;
        $condicion_iva_tipo_doc->id_tipo_documento = 80; // CUIT
        $condicion_iva_tipo_doc->save();

        $condicion_iva_tipo_doc = new CondicionIvaTipoDoc();
        $condicion_iva_tipo_doc->id_condicion_iva = 10;
        $condicion_iva_tipo_doc->id_tipo_documento = 91; // CI Extranjera
        $condicion_iva_tipo_doc->save();

        $condicion_iva_tipo_doc = new CondicionIvaTipoDoc();
        $condicion_iva_tipo_doc->id_condicion_iva = 10;
        $condicion_iva_tipo_doc->id_tipo_documento = 94; // Pasaporte
        $condicion_iva_tipo_doc->save();

        $condicion_iva_tipo_doc = new CondicionIvaTipoDoc();
        $condicion_iva_tipo_doc->id_condicion_iva = 10;
        $condicion_iva_tipo_doc->id_tipo_documento = 99; // Doc. (Otro)
        $condicion_iva_tipo_doc->save();
    }
}

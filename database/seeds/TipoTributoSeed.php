<?php

use Illuminate\Database\Seeder;
use App\TipoTributo;

class TipoTributoSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TipoTributo::truncate();

        $tipo_tributo_obj = new TipoTributo();
        $tipo_tributo_obj->Id = 1;
        $tipo_tributo_obj->Desc = "Impuestos nacionales";
        $tipo_tributo_obj->FchDesde = "20100917";
        $tipo_tributo_obj->FchHasta = null;
        $tipo_tributo_obj->save(); 


        $tipo_tributo_obj = new TipoTributo();
        $tipo_tributo_obj->Id = 2;
        $tipo_tributo_obj->Desc = "Impuestos provinciales";
        $tipo_tributo_obj->FchDesde = "20100917";
        $tipo_tributo_obj->FchHasta = null;
        $tipo_tributo_obj->save(); 


        $tipo_tributo_obj = new TipoTributo();
        $tipo_tributo_obj->Id = 3;
        $tipo_tributo_obj->Desc = "Impuestos municipales";
        $tipo_tributo_obj->FchDesde = "20100917";
        $tipo_tributo_obj->FchHasta = null;
        $tipo_tributo_obj->save(); 


        $tipo_tributo_obj = new TipoTributo();
        $tipo_tributo_obj->Id = 4;
        $tipo_tributo_obj->Desc = "Impuestos Internos";
        $tipo_tributo_obj->FchDesde = "20100917";
        $tipo_tributo_obj->FchHasta = null;
        $tipo_tributo_obj->save(); 


        $tipo_tributo_obj = new TipoTributo();
        $tipo_tributo_obj->Id = 99;
        $tipo_tributo_obj->Desc = "Otro";
        $tipo_tributo_obj->FchDesde = "20100917";
        $tipo_tributo_obj->FchHasta = null;
        $tipo_tributo_obj->save(); 


        $tipo_tributo_obj = new TipoTributo();
        $tipo_tributo_obj->Id = 5;
        $tipo_tributo_obj->Desc = "IIBB";
        $tipo_tributo_obj->FchDesde = "20170719";
        $tipo_tributo_obj->FchHasta = null;
        $tipo_tributo_obj->save(); 


        $tipo_tributo_obj = new TipoTributo();
        $tipo_tributo_obj->Id = 6;
        $tipo_tributo_obj->Desc = "Percepción de IVA";
        $tipo_tributo_obj->FchDesde = "20170719";
        $tipo_tributo_obj->FchHasta = null;
        $tipo_tributo_obj->save(); 


        $tipo_tributo_obj = new TipoTributo();
        $tipo_tributo_obj->Id = 7;
        $tipo_tributo_obj->Desc = "Percepción de IIBB";
        $tipo_tributo_obj->FchDesde = "20170719";
        $tipo_tributo_obj->FchHasta = null;
        $tipo_tributo_obj->save(); 


        $tipo_tributo_obj = new TipoTributo();
        $tipo_tributo_obj->Id = 8;
        $tipo_tributo_obj->Desc = "Percepciones por Impuestos Municipales";
        $tipo_tributo_obj->FchDesde = "20170719";
        $tipo_tributo_obj->FchHasta = null;
        $tipo_tributo_obj->save(); 


        $tipo_tributo_obj = new TipoTributo();
        $tipo_tributo_obj->Id = 9;
        $tipo_tributo_obj->Desc = "Otras Percepciones";
        $tipo_tributo_obj->FchDesde = "20170719";
        $tipo_tributo_obj->FchHasta = null;
        $tipo_tributo_obj->save(); 


        $tipo_tributo_obj = new TipoTributo();
        $tipo_tributo_obj->Id = 13;
        $tipo_tributo_obj->Desc = "Percepción de IVA a no Categorizado";
        $tipo_tributo_obj->FchDesde = "20170719";
        $tipo_tributo_obj->FchHasta = null;
        $tipo_tributo_obj->save(); 
    }
}

<?php

use Illuminate\Database\Seeder;
use App\ListadoProceso;

class ListadoProcesoSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ListadoProceso::truncate();

        $row_obj = new ListadoProceso();
        $row_obj->proceso = "procesar:importaciones_comprobantes";
        $row_obj->tiempo_maximo_ejecucion = 1 * 60; // un minuto
        $row_obj->save();

        $row_obj = new ListadoProceso();
        $row_obj->proceso = "envio:respuestas_importaciones";
        $row_obj->tiempo_maximo_ejecucion = 1 * 60; // un minuto
        $row_obj->save();

        $row_obj = new ListadoProceso();
        $row_obj->proceso = "send:comprobantes";
        $row_obj->tiempo_maximo_ejecucion = 1 * 60; // un minuto
        $row_obj->save();
    }
}

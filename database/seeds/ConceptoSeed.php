<?php

use Illuminate\Database\Seeder;
use App\Concepto;
use Illuminate\Support\Facades\DB;

class ConceptoSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Concepto::truncate();


        DB::statement("INSERT INTO `conceptos` (`Id`, `Desc`, `FchDesde`, `FchHasta`, `created_at`, `updated_at`) VALUES
        (1, 'Producto', 20100917, 0, '2019-06-18 19:06:07', '2019-06-18 19:06:07'),
        (2, 'Servicios', 20100917, 0, '2019-06-18 19:06:07', '2019-06-18 19:06:07'),
        (3, 'Productos y Servicios', 20100917, 0, '2019-06-18 19:06:07', '2019-06-18 19:06:07');");

    }
}

<?php

use Illuminate\Database\Seeder;
use App\TributoComprobante;

class TributoComprobanteSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TributoComprobante::truncate();
    }
}

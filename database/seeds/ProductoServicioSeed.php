<?php

use Illuminate\Database\Seeder;
use App\ProductoServicio;

class ProductoServicioSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProductoServicio::truncate();

        $producto_servicio_obj = new ProductoServicio();
        $producto_servicio_obj->codigo = "001";
        $producto_servicio_obj->descripcion = "Login web";
        $producto_servicio_obj->precio = 3000;
        $producto_servicio_obj->tipo = "servicio";
        $producto_servicio_obj->id_configuracion_afip = 1;
        $producto_servicio_obj->id_unidad_medida = 7;
        $producto_servicio_obj->save();

        $producto_servicio_obj = new ProductoServicio();
        $producto_servicio_obj->codigo = "002";
        $producto_servicio_obj->descripcion = "sistema facturador";
        $producto_servicio_obj->precio = 4500;
        $producto_servicio_obj->tipo = "producto";
        $producto_servicio_obj->id_configuracion_afip = 1;
        $producto_servicio_obj->id_unidad_medida = 7;
        $producto_servicio_obj->save();

        $producto_servicio_obj = new ProductoServicio();
        $producto_servicio_obj->codigo = "003";
        $producto_servicio_obj->descripcion = "servidor intel i7";
        $producto_servicio_obj->precio = 15000;
        $producto_servicio_obj->tipo = "producto";
        $producto_servicio_obj->id_configuracion_afip = 1;
        $producto_servicio_obj->id_unidad_medida = 7;
        $producto_servicio_obj->save();
    }
}

<?php

use Illuminate\Database\Seeder;
use App\ImportacionEstadistica;

class ImportacionEstadisticaSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ImportacionEstadistica::truncate();
    }
}

<?php

use Illuminate\Database\Seeder;
use App\ComprobanteAsociado;

class ComprobanteAsociadoSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ComprobanteAsociado::truncate();
    }
}

<?php

use Illuminate\Database\Seeder;
use App\CondicionIva;

class CondicionIvaSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CondicionIva::truncate();

        $condicion_obj = new CondicionIva();
        $condicion_obj->id = 1;
        $condicion_obj->descripcion = "IVA Responsable Inscripto";
        $condicion_obj->save();

        $condicion_obj = new CondicionIva();
        $condicion_obj->id = 4;
        $condicion_obj->descripcion = "IVA Sujeto Exento";
        $condicion_obj->save();

        $condicion_obj = new CondicionIva();
        $condicion_obj->id = 5;
        $condicion_obj->descripcion = "Consumidor Final";
        $condicion_obj->save();

        $condicion_obj = new CondicionIva();
        $condicion_obj->id = 6;
        $condicion_obj->descripcion = "Responsable Monotributo";
        $condicion_obj->save();

        $condicion_obj = new CondicionIva();
        $condicion_obj->id = 8;
        $condicion_obj->descripcion = "Proveedor del Exterior";
        $condicion_obj->save();

        $condicion_obj = new CondicionIva();
        $condicion_obj->id = 9;
        $condicion_obj->descripcion = "Cliente del Exterior";
        $condicion_obj->save();

        $condicion_obj = new CondicionIva();
        $condicion_obj->id = 10;
        $condicion_obj->descripcion = "IVA Liberado - Ley Nº 19.640";
        $condicion_obj->save();

        $condicion_obj = new CondicionIva();
        $condicion_obj->id = 11;
        $condicion_obj->descripcion = "IVA Responsable Inscripto - Agente de Percepción";
        $condicion_obj->save();

        $condicion_obj = new CondicionIva();
        $condicion_obj->id = 13;
        $condicion_obj->descripcion = "Monotributista Social";
        $condicion_obj->save();

        $condicion_obj = new CondicionIva();
        $condicion_obj->id = 15;
        $condicion_obj->descripcion = "IVA No Alcanzado";
        $condicion_obj->save();
    }
}

<?php

use Illuminate\Database\Seeder;
use App\TipoDocumento;

class TipoDocumentoSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TipoDocumento::truncate();

        $tipo_documento_obj = new TipoDocumento();
        $tipo_documento_obj->id = 80;
        $tipo_documento_obj->descripcion = "CUIT";
        $tipo_documento_obj->save();

        $tipo_documento_obj = new TipoDocumento();
        $tipo_documento_obj->id = 86;
        $tipo_documento_obj->descripcion = "CUIL";
        $tipo_documento_obj->save();

        $tipo_documento_obj = new TipoDocumento();
        $tipo_documento_obj->id = 87;
        $tipo_documento_obj->descripcion = "CDI";
        $tipo_documento_obj->save();

        $tipo_documento_obj = new TipoDocumento();
        $tipo_documento_obj->id = 89;
        $tipo_documento_obj->descripcion = "LE";
        $tipo_documento_obj->save();

        $tipo_documento_obj = new TipoDocumento();
        $tipo_documento_obj->id = 90;
        $tipo_documento_obj->descripcion = "LC";
        $tipo_documento_obj->save();

        $tipo_documento_obj = new TipoDocumento();
        $tipo_documento_obj->id = 91;
        $tipo_documento_obj->descripcion = "CI Extranjera";
        $tipo_documento_obj->save();

        $tipo_documento_obj = new TipoDocumento();
        $tipo_documento_obj->id = 92;
        $tipo_documento_obj->descripcion = "en trámite";
        $tipo_documento_obj->save();

        $tipo_documento_obj = new TipoDocumento();
        $tipo_documento_obj->id = 93;
        $tipo_documento_obj->descripcion = "Acta Nacimiento";
        $tipo_documento_obj->save();

        $tipo_documento_obj = new TipoDocumento();
        $tipo_documento_obj->id = 95;
        $tipo_documento_obj->descripcion = "CI Bs. As. RNP";
        $tipo_documento_obj->save();

        $tipo_documento_obj = new TipoDocumento();
        $tipo_documento_obj->id = 96;
        $tipo_documento_obj->descripcion = "DNI";
        $tipo_documento_obj->save();

        $tipo_documento_obj = new TipoDocumento();
        $tipo_documento_obj->id = 94;
        $tipo_documento_obj->descripcion = "Pasaporte";
        $tipo_documento_obj->save();

        $tipo_documento_obj = new TipoDocumento();
        $tipo_documento_obj->id = 0;
        $tipo_documento_obj->descripcion = "CI Policía Federal";
        $tipo_documento_obj->save();

        $tipo_documento_obj = new TipoDocumento();
        $tipo_documento_obj->id = 1;
        $tipo_documento_obj->descripcion = "CI Buenos Aires";
        $tipo_documento_obj->save();

        $tipo_documento_obj = new TipoDocumento();
        $tipo_documento_obj->id = 2;
        $tipo_documento_obj->descripcion = "CI Catamarca";
        $tipo_documento_obj->save();

        $tipo_documento_obj = new TipoDocumento();
        $tipo_documento_obj->id = 3;
        $tipo_documento_obj->descripcion = "CI Córdoba";
        $tipo_documento_obj->save();

        $tipo_documento_obj = new TipoDocumento();
        $tipo_documento_obj->id = 4;
        $tipo_documento_obj->descripcion = "CI Corrientes";
        $tipo_documento_obj->save();

        $tipo_documento_obj = new TipoDocumento();
        $tipo_documento_obj->id = 5;
        $tipo_documento_obj->descripcion = "CI Entre Ríos";
        $tipo_documento_obj->save();

        $tipo_documento_obj = new TipoDocumento();
        $tipo_documento_obj->id = 6;
        $tipo_documento_obj->descripcion = "CI Jujuy";
        $tipo_documento_obj->save();

        $tipo_documento_obj = new TipoDocumento();
        $tipo_documento_obj->id = 7;
        $tipo_documento_obj->descripcion = "CI Mendoza";
        $tipo_documento_obj->save();

        $tipo_documento_obj = new TipoDocumento();
        $tipo_documento_obj->id = 8;
        $tipo_documento_obj->descripcion = "CI La Rioja";
        $tipo_documento_obj->save();

        $tipo_documento_obj = new TipoDocumento();
        $tipo_documento_obj->id = 9;
        $tipo_documento_obj->descripcion = "CI Salta";
        $tipo_documento_obj->save();

        $tipo_documento_obj = new TipoDocumento();
        $tipo_documento_obj->id = 10;
        $tipo_documento_obj->descripcion = "CI San Juan";
        $tipo_documento_obj->save();

        $tipo_documento_obj = new TipoDocumento();
        $tipo_documento_obj->id = 11;
        $tipo_documento_obj->descripcion = "CI San Luis";
        $tipo_documento_obj->save();

        $tipo_documento_obj = new TipoDocumento();
        $tipo_documento_obj->id = 12;
        $tipo_documento_obj->descripcion = "CI Santa Fe";
        $tipo_documento_obj->save();

        $tipo_documento_obj = new TipoDocumento();
        $tipo_documento_obj->id = 13;
        $tipo_documento_obj->descripcion = "CI Santiago del Estero";
        $tipo_documento_obj->save();

        $tipo_documento_obj = new TipoDocumento();
        $tipo_documento_obj->id = 14;
        $tipo_documento_obj->descripcion = "CI Tucumán";
        $tipo_documento_obj->save();

        $tipo_documento_obj = new TipoDocumento();
        $tipo_documento_obj->id = 16;
        $tipo_documento_obj->descripcion = "CI Chaco";
        $tipo_documento_obj->save();

        $tipo_documento_obj = new TipoDocumento();
        $tipo_documento_obj->id = 17;
        $tipo_documento_obj->descripcion = "CI Chubut";
        $tipo_documento_obj->save();

        $tipo_documento_obj = new TipoDocumento();
        $tipo_documento_obj->id = 18;
        $tipo_documento_obj->descripcion = "CI Formosa";
        $tipo_documento_obj->save();

        $tipo_documento_obj = new TipoDocumento();
        $tipo_documento_obj->id = 19;
        $tipo_documento_obj->descripcion = "CI Misiones";
        $tipo_documento_obj->save();

        $tipo_documento_obj = new TipoDocumento();
        $tipo_documento_obj->id = 20;
        $tipo_documento_obj->descripcion = "CI Neuquén";
        $tipo_documento_obj->save();

        $tipo_documento_obj = new TipoDocumento();
        $tipo_documento_obj->id = 21;
        $tipo_documento_obj->descripcion = "CI La Pampa";
        $tipo_documento_obj->save();

        $tipo_documento_obj = new TipoDocumento();
        $tipo_documento_obj->id = 22;
        $tipo_documento_obj->descripcion = "CI Río Negro";
        $tipo_documento_obj->save();

        $tipo_documento_obj = new TipoDocumento();
        $tipo_documento_obj->id = 23;
        $tipo_documento_obj->descripcion = "CI Santa Cruz";
        $tipo_documento_obj->save();

        $tipo_documento_obj = new TipoDocumento();
        $tipo_documento_obj->id = 24;
        $tipo_documento_obj->descripcion = "CI Tierra del Fuego";
        $tipo_documento_obj->save();

        $tipo_documento_obj = new TipoDocumento();
        $tipo_documento_obj->id = 99;
        $tipo_documento_obj->descripcion = "Doc. (Otro)";
        $tipo_documento_obj->save();
    }
}

<?php

use Illuminate\Database\Seeder;

use App\ImportacionComprobante;
use App\EstadoImportacionComprobante;

class ImportacionComprobanteSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ImportacionComprobante::truncate();
    }
}

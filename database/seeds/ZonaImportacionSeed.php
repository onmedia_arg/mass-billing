<?php

use Illuminate\Database\Seeder;
use App\ZonaImportacion;

class ZonaImportacionSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ZonaImportacion::truncate();

        $zona_importacion_obj = new ZonaImportacion();
        $zona_importacion_obj->zona_importacion = "carga_datos_stats";
        $zona_importacion_obj->save();
    }
}

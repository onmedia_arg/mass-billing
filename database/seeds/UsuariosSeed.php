<?php

use Illuminate\Database\Seeder;
use App\Usuario;
use App\Rol;
use App\EstadoUsuario;

class UsuariosSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Usuario::truncate();

        $usuario = new Usuario();
        $usuario->nombre = "Mario";
        $usuario->apellido = "Olivera";
        $usuario->correo = "mario.olivera96@gmail.com";
        $usuario->usuario = "admin";
        $usuario->password = bcrypt("admin");
        $usuario->id_rol = Rol::ADMINISTRADOR;
        $usuario->id_estado_usuario = EstadoUsuario::ACTIVO;
        $usuario->id_configuracion_afip = 1;
        $usuario->save();

        /*
        $usuario = new Usuario();
        $usuario->nombre = "Mario";
        $usuario->apellido = "Olivera";
        $usuario->correo = "administrator@gmail.com";
        $usuario->usuario = "admin2";
        $usuario->password = bcrypt("admin2");
        $usuario->id_rol = Rol::ADMINISTRADOR;
        $usuario->id_estado_usuario = EstadoUsuario::ACTIVO;
        $usuario->id_configuracion_afip = 2;
        $usuario->save();
        */
    }
}

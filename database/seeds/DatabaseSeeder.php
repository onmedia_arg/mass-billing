<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        $this->call(RolSeed::class);
        $this->call(EstadoUsuarioSeed::class);
        $this->call(UsuariosSeed::class);
        $this->call(ConfiguracionAfipSeed::class);
        $this->call(RolSeed::class);
        $this->call(EstadoUsuarioSeed::class);
        $this->call(UsuariosSeed::class);
        $this->call(TipoDeComprobanteSeed::class);
        $this->call(ConceptoSeed::class);
        $this->call(CondicionIvaSeed::class);
        $this->call(UnidadDeMedidaSeed::class);
        $this->call(DetalleComprobanteSeed::class);
        $this->call(ComprobanteSeed::class);
        $this->call(TributoComprobanteSeed::class);
        $this->call(TipoDocumentoSeed::class);
        $this->call(CondicionDeVentaSeed::class);
        $this->call(TipoTributoSeed::class);
        $this->call(CondicionIvaTipoDocSeed::class);
        $this->call(EstadoComprobanteSeed::class);
        $this->call(ComprobanteAsociadoSeed::class);
        $this->call(ZonaImportacionSeed::class);
        $this->call(ImportacionSeed::class);
        $this->call(MisClientesSeed::class);
        $this->call(ProductoServicioSeed::class);
        $this->call(EstadoImportacionComprobanteSeed::class);
        $this->call(ImportacionComprobanteSeed::class);
        $this->call(ListadoProcesoSeed::class);
        $this->call(ConfiguracionSmtpSeed::class);
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}

<?php

use Illuminate\Database\Seeder;
use App\MisClientes;

class MisClientesSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MisClientes::truncate();

        $mis_clientes_obj = new MisClientes();

        $mis_clientes_obj->id_tipo_documento = 96;
        $mis_clientes_obj->documento = "20376548291";
        $mis_clientes_obj->nombre_razon_social = "PRUEBA 1";
        $mis_clientes_obj->correo = "prueba@gmail.com";
        $mis_clientes_obj->telefono = "3446609723";
        $mis_clientes_obj->domicilio_comercial = "Luis N Palma 3000";
        $mis_clientes_obj->id_condicion_iva = 6;
        $mis_clientes_obj->eliminado = 0;
        $mis_clientes_obj->id_configuracion_afip = 1;
        $mis_clientes_obj->save(); 
    }
}

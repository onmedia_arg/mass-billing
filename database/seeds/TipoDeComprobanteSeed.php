<?php

use Illuminate\Database\Seeder;
use App\TipoDeComprobante;
use Illuminate\Support\Facades\DB;

class TipoDeComprobanteSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TipoDeComprobante::truncate();

        DB::statement("INSERT INTO `tipos_de_comprobantes` (`Id`, `Desc`, `FchDesde`, `FchHasta`, `created_at`, `updated_at`) VALUES
        (1, 'Factura A', 20100917, 0, '2019-06-18 18:46:03', '2019-06-18 18:46:03'),
        (2, 'Nota de Debito A', 20100917, 0, '2019-06-18 18:46:03', '2019-06-18 18:46:03'),
        (3, 'Nota de Credito A', 20100917, 0, '2019-06-18 18:46:03', '2019-06-18 18:46:03'),
        (4, 'Recibos A', 20100917, 0, '2019-06-18 18:46:04', '2019-06-18 18:46:04'),
        (5, 'Notas de Venta al contado A', 20100917, 0, '2019-06-18 18:46:04', '2019-06-18 18:46:04'),
        (6, 'Factura B', 20100917, 0, '2019-06-18 18:46:03', '2019-06-18 18:46:03'),
        (7, 'Nota de Debito B', 20100917, 0, '2019-06-18 18:46:03', '2019-06-18 18:46:03'),
        (8, 'Nota de Credito B', 20100917, 0, '2019-06-18 18:46:04', '2019-06-18 18:46:04'),
        (9, 'Recibos B', 20100917, 0, '2019-06-18 18:46:04', '2019-06-18 18:46:04'),
        (10, 'Notas de Venta al contado B', 20100917, 0, '2019-06-18 18:46:04', '2019-06-18 18:46:04'),
        (11, 'Factura C', 20110330, 0, '2019-06-18 18:46:04', '2019-06-18 18:46:04'),
        (12, 'Nota de Debito C', 20110330, 0, '2019-06-18 18:46:04', '2019-06-18 18:46:04'),
        (13, 'Nota de Credito C', 20110330, 0, '2019-06-18 18:46:04', '2019-06-18 18:46:04'),
        (15, 'Recibo C', 20110330, 0, '2019-06-18 18:46:04', '2019-06-18 18:46:04'),
        (34, 'Cbtes. A del Anexo I, Apartado A,inc.f),R.G.Nro. 1415', 20100917, 0, '2019-06-18 18:46:04', '2019-06-18 18:46:04'),
        (35, 'Cbtes. B del Anexo I,Apartado A,inc. f),R.G. Nro. 1415', 20100917, 0, '2019-06-18 18:46:04', '2019-06-18 18:46:04'),
        (39, 'Otros comprobantes A que cumplan con R.G.Nro. 1415', 20100917, 0, '2019-06-18 18:46:04', '2019-06-18 18:46:04'),
        (40, 'Otros comprobantes B que cumplan con R.G.Nro. 1415', 20100917, 0, '2019-06-18 18:46:04', '2019-06-18 18:46:04'),
        (49, 'Comprobante de Compra de Bienes Usados a Consumidor Final', 20130401, 0, '2019-06-18 18:46:04', '2019-06-18 18:46:04'),
        (51, 'Factura M', 20150522, 0, '2019-06-18 18:46:04', '2019-06-18 18:46:04'),
        (52, 'Nota de Debito M', 20150522, 0, '2019-06-18 18:46:04', '2019-06-18 18:46:04'),
        (53, 'Nota de Credito M', 20150522, 0, '2019-06-18 18:46:04', '2019-06-18 18:46:04'),
        (54, 'Recibo M', 20150522, 0, '2019-06-18 18:46:04', '2019-06-18 18:46:04'),
        (60, 'Cta de Vta y Liquido prod. A', 20100917, 0, '2019-06-18 18:46:04', '2019-06-18 18:46:04'),
        (61, 'Cta de Vta y Liquido prod. B', 20100917, 0, '2019-06-18 18:46:04', '2019-06-18 18:46:04'),
        (63, 'Liquidacion A', 20100917, 0, '2019-06-18 18:46:04', '2019-06-18 18:46:04'),
        (64, 'Liquidacion B', 20100917, 0, '2019-06-18 18:46:04', '2019-06-18 18:46:04'),
        (201, 'Factura de Credito electronica MiPyMEs (FCE) A', 20181226, 0, '2019-06-18 18:46:04', '2019-06-18 18:46:04'),
        (202, 'Nota de Débito electronica MiPyMEs (FCE) A', 20181226, 0, '2019-06-18 18:46:04', '2019-06-18 18:46:04'),
        (203, 'Nota de Credito electronica MiPyMEs (FCE) A', 20181226, 0, '2019-06-18 18:46:04', '2019-06-18 18:46:04'),
        (206, 'Factura de Credito electronica MiPyMEs (FCE) B', 20181226, 0, '2019-06-18 18:46:05', '2019-06-18 18:46:05'),
        (207, 'Nota de Debito electronica MiPyMEs (FCE) B', 20181226, 0, '2019-06-18 18:46:05', '2019-06-18 18:46:05'),
        (208, 'Nota de Credito electronica MiPyMEs (FCE) B', 20181226, 0, '2019-06-18 18:46:05', '2019-06-18 18:46:05'),
        (211, 'Factura de Credito electronica MiPyMEs (FCE) C', 20181226, 0, '2019-06-18 18:46:05', '2019-06-18 18:46:05'),
        (212, 'Nota de Debito electronica MiPyMEs (FCE) C', 20181226, 0, '2019-06-18 18:46:05', '2019-06-18 18:46:05'),
        (213, 'Nota de Credito electronica MiPyMEs (FCE) C', 20181226, 0, '2019-06-18 18:46:05', '2019-06-18 18:46:05');");
    }
}

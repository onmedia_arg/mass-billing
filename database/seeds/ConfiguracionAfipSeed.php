<?php

use Illuminate\Database\Seeder;
use App\ConfiguracionAfip;

use App\CondicionIva;

class ConfiguracionAfipSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ConfiguracionAfip::truncate();

        $configuracion_afip = new ConfiguracionAfip();

        $configuracion_afip->razon_social = "Mario Ernesto Olivera Iribarren";
        $configuracion_afip->cuit = "20397182088";
        $configuracion_afip->domicilio_comercial = "Luis N Palma 3448";
        $configuracion_afip->ingresos_brutos = "0";
        $configuracion_afip->inicio_actividades = "2020-04-06";
        $configuracion_afip->id_condicion_iva = CondicionIva::RESPONSABLE_MONOTRIBUTO;

        $configuracion_afip->csr_afip_produccion = "-----BEGIN CERTIFICATE-----
MIIDUDCCAjigAwIBAgIINIhxbM+PgY0wDQYJKoZIhvcNAQENBQAwMzEVMBMGA1UEAwwMQ29tcHV0
YWRvcmVzMQ0wCwYDVQQKDARBRklQMQswCQYDVQQGEwJBUjAeFw0xOTEwMjQxMzAyNTRaFw0yMTEw
MjMxMzAyNTRaMDsxHjAcBgNVBAMMFU1pUGVkaWRvQ1NSUHJvZHVjY2lvbjEZMBcGA1UEBRMQQ1VJ
VCAyMDM5NzE4MjA4OTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMU880EChSoVcvYO
txrtFfpWSr5fxXMDoctgQILNcBrmy0e8cAFaxhaKPXcbh5IonEzUis3C5w9xm26/CAfzB6qoBgoj
7P7bnudNj5mzGOc9xkmDJgCipsWM7PGYUzcEGwVUQ05Znx52NeV1rePHqKATjKPAg+W+4m2LdGsG
NIR4zk9RdEnl2Ymg+xvkfN3yclzzbcHv4IYhjrKUETHGry1q1mIf3DgLGwZghzoBp3SeOvfO6syM
35roCFJPcxTcKmJsKSFhsIIi+QWm9NxBf4zEc1gQ4kdMhlismO4piNQ1OgwpInAbbv5kTgeNyk5j
C7n1ubVuNUNFB9rx4Cv6cCkCAwEAAaNgMF4wDAYDVR0TAQH/BAIwADAfBgNVHSMEGDAWgBRbQFEw
58MqtpxWnJOF40KAsFDr9jAdBgNVHQ4EFgQUWkTOr8ZOwWDiSFkdWY4DMVSP90YwDgYDVR0PAQH/
BAQDAgXgMA0GCSqGSIb3DQEBDQUAA4IBAQBExJWvuCfJmnf7q/25sM2G3gfFXVpJt9wbJX51Nn9O
pGyaM3oT8XZUk0eellZVOq8Uuyhkbykdes83+LWqC+pKJyuOiVtlVCYxjBO/RR5Qccb1v4K4ju85
QeZ0WbAzCD7tbjH1dfgvqQ8KPC0DqBtM13N50kMZVqB9sYfwtKpenik5jtQQ3pUbIj2aj+aPkJW+
jPUbRFzu9zQYhSUiAsYFh6c5Rxh69vykELnaReTMfCjp595WCxBh2cBITULse0JrQHqDBpE2P7LT
ThiLroUbqVCOygw2yoBTD5LlCt7AB0Hoif7PYT0s5N/FB2DdUYId67xkcrAmy4VGkmHKv1g9
-----END CERTIFICATE-----";

        $configuracion_afip->key_afip_produccion = "-----BEGIN RSA PRIVATE KEY-----
MIIEpQIBAAKCAQEAxTzzQQKFKhVy9g63Gu0V+lZKvl/FcwOhy2BAgs1wGubLR7xw
AVrGFoo9dxuHkiicTNSKzcLnD3Gbbr8IB/MHqqgGCiPs/tue502PmbMY5z3GSYMm
AKKmxYzs8ZhTNwQbBVRDTlmfHnY15XWt48eooBOMo8CD5b7ibYt0awY0hHjOT1F0
SeXZiaD7G+R83fJyXPNtwe/ghiGOspQRMcavLWrWYh/cOAsbBmCHOgGndJ46987q
zIzfmugIUk9zFNwqYmwpIWGwgiL5Bab03EF/jMRzWBDiR0yGWKyY7imI1DU6DCki
cBtu/mROB43KTmMLufW5tW41Q0UH2vHgK/pwKQIDAQABAoIBAQCGPFwl44wzvxNV
qWAvICp0DOSjulSMWW11F9Xv79ebR3YUJYhpM5lbX8xDUuYqsB7oH0M0lLBjD2PE
tJjHqDyQhfhOUHMMZPY538yaw4xwXNel0m87wXcVPjtIle4UpGOrddS070PfHH7D
4xpsysbV41g1XmGmLAq4mU+rQzbc7JVEIFTyE1Fz3DBzFwHOL8Eua0zoaGyk7aOC
iOcjQ/US18OFdsPIQ57KWEtbD9P/u1srO6e4b2UBnJTazIWKm+fvzRG3z/+vLK2s
nb9xJWiW9WfIw549a4jwLkma0Ypr7Hbpq63/FnQZrKWbBuYk1p2I0sbjaCBsxxiG
xTDs77NdAoGBAOLMMmOT7vEScIcPxsNA/Ib23mn2EE2TV0ahSMad/iArKCq06jjL
4et7RKFH0dOVTTYzp+MpVUeU50hgNaEMiYh6eSLoQ1WX357tdbVS3IZOTRYpWo/7
9uKjulWFF77QSe63PjeeH+C65cOCJF91JSmO65vStH3UDrdAfgmNbjzXAoGBAN6i
ZfiP8cG2Fzzgno+NgylPXL7a2gGiEtgDijLJNk1YFZ/upmJY/RJIk2/gUZHnnpmv
r7Mcb2znAZFffnj3QDkALnogtEqnbhPr4AWo8gkQ1kqdN/un64rVVsN8v94VcUpG
XVM6sSLOuxpbAVpjn3/Iz79X2JoVQmUSPOyhMhr/AoGBAN8DAY1vOuasRiBZAA6h
SW+BMuUow5E0j/5wu7oqQ2foZ//Ok/4zZTyDotGgaJBQYHMqyiySSTwxDFfb2wA4
7shX/eO4VfXFeGuzfipFh3k9Z4miMl4g2fy0GJbdX6MO95QXdcOE5j26KhJcV13S
OC+sTLuVrkuIiMvjS8wi2UJTAoGBALmIDoBovKiYrZ7e9WReItrPN1QZn6nSve9k
a8vM4MLtz/+a6u90h1mwP6XxAykbI5Gh1kLmRHk1vZm/Qe9e5Rdbbl/6VyULiHFz
lRpCY4lojZxajqfalTD6frakhGR37BRgtS9B7xI5w7S/+E2yTd44o+AqPeiSJOWD
oF/2KYJTAoGAGLs8ZdiKGnnD0hepo0Ic4wPG1bcJNsX0YZv+MZkQlQbazRaLUhXP
pgFH+WgTWTvq+wuXzjZdnXeazJFlu9tRx0bc0u4cmw/BzYr7jfeabV+32nKOQdxf
Vpw+kGe3jg5et/avigHl62gwg9r5ybvc3ztH9Ku0+yY6Vx3XrrKgjSY=
-----END RSA PRIVATE KEY-----";

$configuracion_afip->csr_afip_testing = "-----BEGIN CERTIFICATE-----
MIIDTDCCAjSgAwIBAgIIfO4CSTi+j4gwDQYJKoZIhvcNAQENBQAwODEaMBgGA1UEAwwRQ29tcHV0
YWRvcmVzIFRlc3QxDTALBgNVBAoMBEFGSVAxCzAJBgNVBAYTAkFSMB4XDTE5MDYwNTEyMjgxNloX
DTIxMDYwNDEyMjgxNlowMjEVMBMGA1UEAwwMdGVzdGluZ2xvY2FsMRkwFwYDVQQFExBDVUlUIDIw
Mzk3MTgyMDg5MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA7Mtattrla7YSQsWd70W5
MqXhlQ7R9XcbJb48JbSwcSLVGUgBwiXicAyHFjnNH8QchEk02tC5OHTwrQI200ZK+PS6lHBLGHXV
xJ63LGb28hNFsEeK79IJ14iJXrfO/seBSWJfAQuFdrkZRrrOASBV3C0iWZVgYfNbhxof1oySK3+c
OhxNWb+GgGzdiT4SzgP/kOIXvYJs9glkAeKgTICFQXDxmyOflTesXCiVpi5BwC73JmIaks8psMFO
s15JjiDAztruSqQG+ysoPCDEVTy8wC1bR7Dwg2O+asvEnY+Ayo0MGqvolTa8r65t5eXY60Ri0gTl
4iP2w7cilCpBBtWl5QIDAQABo2AwXjAMBgNVHRMBAf8EAjAAMB8GA1UdIwQYMBaAFLOy0//96bre
3o2vESGc1iB98k9vMB0GA1UdDgQWBBQkGFz6W5le+cyMUS+/0XxYNoHgbjAOBgNVHQ8BAf8EBAMC
BeAwDQYJKoZIhvcNAQENBQADggEBAA6fz6fhFC6ESavAcAqS6SQ/JoWVKH5K9fWxsXZuM+/ZyQej
r2DjkToXQtQCBnh7pa7XvF2g2OpLnoU+cF/UOAXPEW0I/ZVNcJQogx88csPLTZrAd//uHli7y3Fe
CB1BVF5HIV1+d+N6aY/hggTe+GAKtZXxugSnUkd2eNZ90t6MnDjONmo0tV6pmO3QuQl9qp9JI92+
fXLbnXblw2cFmpnpntz40dI4B3K6E9KlRyKhL1hNEHOPgDIOpzlMnfNmY0YKlHfrvxW52gKbal3j
blf5MnhS9oid+m/7kKShs2AZRruGHTpaA/AvZYslJx0FzfUs6Y8Do99aC2ZOKb9STMQ=
-----END CERTIFICATE-----";

$configuracion_afip->key_afip_testing = "-----BEGIN RSA PRIVATE KEY-----
MIIEpQIBAAKCAQEA7Mtattrla7YSQsWd70W5MqXhlQ7R9XcbJb48JbSwcSLVGUgB
wiXicAyHFjnNH8QchEk02tC5OHTwrQI200ZK+PS6lHBLGHXVxJ63LGb28hNFsEeK
79IJ14iJXrfO/seBSWJfAQuFdrkZRrrOASBV3C0iWZVgYfNbhxof1oySK3+cOhxN
Wb+GgGzdiT4SzgP/kOIXvYJs9glkAeKgTICFQXDxmyOflTesXCiVpi5BwC73JmIa
ks8psMFOs15JjiDAztruSqQG+ysoPCDEVTy8wC1bR7Dwg2O+asvEnY+Ayo0MGqvo
lTa8r65t5eXY60Ri0gTl4iP2w7cilCpBBtWl5QIDAQABAoIBAQDgHagCITO6ulyW
LgvJByi9sxrKxXga51weYOr0lQvaSKmAgBKJ+ulSO8kvVA283xROcCtCXwKvWJ7i
YJIbbRBQiPB+xbmNsRleOwoLUHixrWPr8Aqj6Ru/j07VAws7QQAwwJZCmvD66T3v
H6cSkdQxy9YTC7pperdKK4OuwOUnnJG2s30mK50LFS+1hkji3RxxR6g1ajPE+PPt
Zi8Yn/SlCWUM0TZszQKUfqE5onK4nJpwXGokLTkCSEHsuIN/Jj9WXpfgxvvVRa7B
UW2+NDi2yq/+3m/8mvo/1xA+1NleZd7/lHuDcUSxOxp3sS3xBR8PRdN6h+rXvDjr
dyvcXmmRAoGBAP0o2lRZINqLUXr3mHyVmEikYw92HthVhQe2JXcsLV3TYWGOWYQb
A4rRXfH1iyc2MWXGYDMnwk5uT5lTGbh5I0HoEBIPR2OUvKFk1M2ywcfE58U573Eg
/UmQfkYpGDr/Hc4sIPaxHvfPLiEEDWjMhnqRrhF+C7h+DwXiu4bQ0hbvAoGBAO9z
fvCXuHYlAYVbp71O5bds+5nD793L/3ZFUygKpyDfM+w1cOXIaFWBV/aLwhKNfj2K
PcmAg51vxZJba7U4iu6H19Oqnlccy/tyJbd5WV3EIqRtJkE3dybFh4WHgyywJhpM
03AuwiNUSznm9vBzDjSA1byqo4VxQoxLAyje2vBrAoGBANN1yljmUfD6qkbChhaa
hic3WJ9TlRQy6vm0BbtzpTlYFWF9m/wi7GSJ2aT/6woH1F8kwro4Ha+RqN9zcGqM
lQy6IKJVLNZbKOzO4oonJIIPUWGGsSCX47oM/czxCpJNAsJyTd6BuFJaNjfRwLmn
DTVYdkOaO4QRl87cwrMrvjD1AoGAFuPqmaNXXevxtkbpxR4atas0biGd94z90deU
uHo0Od88fnMpZL4Aw1PHDemUVuOx0r45Y97mNhGuSGx24m8SUPUpe7GOy3MUSOhD
lMOD+IO8cA2DH67mtA0kBG7M6mO4cMExYUMHCguP2Wwr5RaKoBhE1nFVincmTnUJ
S0nqkhsCgYEAi7LkzsNoIGIYL6dzFhlXrsqnsDgHUty6tyrIUkCK8eQpBCx6Lkoh
wqD8Ox2IVdg/SA4TTiVLY0mQHpIzTqpUaQdiN5GRfXS9/VTkrDE1Wh6nF4oZRSJS
AEd2KZrmcXq1Cu8+H0FKYDKCkgBPGWQ0XZkFUEwlIffAZtP8YC/Ow2o=
-----END RSA PRIVATE KEY-----";

        $configuracion_afip->ambiente = "testing";
        $configuracion_afip->save();

        /*
        $configuracion_afip = new ConfiguracionAfip();

        $configuracion_afip->razon_social = "Mario Ernesto Olivera Iribarren";
        $configuracion_afip->cuit = "20397182088";
        $configuracion_afip->domicilio_comercial = "Luis N Palma 3448";
        $configuracion_afip->ingresos_brutos = "0";
        $configuracion_afip->inicio_actividades = "2020-04-06";
        $configuracion_afip->id_condicion_iva = CondicionIva::RESPONSABLE_MONOTRIBUTO;

        $configuracion_afip->csr_afip_produccion = "";

        $configuracion_afip->key_afip_produccion = "";

        $configuracion_afip->csr_afip_testing = "";

        $configuracion_afip->key_afip_testing = "";

        $configuracion_afip->ambiente = "testing";
        $configuracion_afip->save();
        */
    }
}

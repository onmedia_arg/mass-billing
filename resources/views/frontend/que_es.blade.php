@extends('frontend.template.master')

@section('title', 'Que es '.Config("app.name"))

@section('contenido')

<div class="container mt-5 mb-5" style="margin-top:">
    <div class="row mt-5">
        <div class="col-md-12">
            <h2>Que es {{Config("app.name") }}</h2>    
        </div>
    </div>
</div>
@endsection

@section("js_code")
<script type="text/javascript">

$(document).ready(function(){
  $(".navegacion_menu a").removeClass("active");
  $(".navegacion_menu .is_option_que_es").addClass("active");
});

</script>
@endsection

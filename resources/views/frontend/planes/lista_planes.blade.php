<?php 
$planes = \App\Plan::where("eliminado",0)->where("activo",1)->orderBy("precio")->get();
?>

<section class="section-precios">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h3>¡Obtené tu contador online ahora!</h3>
      </div>

      @foreach($planes as $plan_row)
      <div class="col-lg-3 col-md-6">
        <div class="item">
          <div class="item-title" style="background-color: #a3a3a3">{{$plan_row->nombre}}</div>
          <?php
          $items_plan = explode("\n",$plan_row->descripcion)
          ?>

          @if(is_array($items_plan) && count($items_plan))
            @foreach($items_plan as $item_plan_row)
              @if(trim($item_plan_row) != "")
                <div class="item-row">{{$item_plan_row}}</div>
              @endif
            @endforeach
          @endif
          <div class="item-row">PRECIO DESDE: $ {{number_format($plan_row->precio,0,",",".")}}</div>
          <a href="{{url('/planes/adquirir/'.$plan_row->id)}}" class="item-btn">Seleccionar</a>
        </div>
      </div>
      @endforeach
    </div>
  </div>
</section>
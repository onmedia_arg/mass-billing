@extends('frontend.template.master')

@section('title', 'Adquirir plan')

@section('contenido')

<div class="container mt-5 mb-5">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <h2 class="text-center">{{$plan_obj->nombre}}</h2>
            <h4 class="text-center">$ {{number_format($plan_obj->precio,0,",",".")}}</h4>
            <form id="formulario_plan">
                <div class="form-group">
                    <label for="cuit">cuit</label>
                    <input type="text" class="form-control" id="cuit">
                </div>
                <div class="form-group">
                    <label for="nombre">Nombre</label>
                    <input type="text" class="form-control" id="nombre">
                </div>
                <div class="form-group">
                    <label for="apellido">Apellido</label>
                    <input type="text" class="form-control" id="apellido">
                </div>
                <div class="form-group">
                    <label for="correo">Correo</label>
                    <input type="text" class="form-control" id="correo">
                </div>
                <div class="form-group">
                    <label for="telefono">Teléfono</label>
                    <input type="text" class="form-control" id="telefono">
                </div>
                <div style="text-align: center;">
                    <button type="submit" class="btn btn-primary">
                        Enviar
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section("js_code")

<script src="https://www.google.com/recaptcha/api.js?render={{env('RECAPTCHAV3_SITEKEY')}}"></script>

<script>
    
    $("#formulario_plan").submit(function(e){
        e.preventDefault();

        grecaptcha.ready(function() {
          grecaptcha.execute("{{env('RECAPTCHAV3_SITEKEY')}}", {action: 'submit'}).then(function(token) {
              
            console.log(token);
            sendFormularioPlan(token);
          });
        });
    });

    function sendFormularioPlan(token)
    {
        let cuit = $.trim($("#cuit").val());
        let nombre = $.trim($("#nombre").val());
        let apellido =$.trim($("#apellido").val());
        let correo = $.trim($("#correo").val());
        let telefono = $.trim($("#telefono").val());
        
        $.ajax({
            url: "{{url('/planes/saveSolicitudRegistro')}}",
            type: "POST",
            data: {
                cuit,
                token,
                nombre,
                apellido,
                correo,
                telefono,
                id_plan: {{$plan_obj->id}}
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend: function(data)
            {
                abrir_loading();
            },
            success: function(data)
            {
                cerrar_loading();

                try
                {
                    if(data["response"] == true)
                    {
                        mostrar_mensajes_success(
                            "Solicitud realizada",
                            "Se ha realizado la solicitud correctamente, nos pondremos en contacto a la brevedad",
                            "{{url('/')}}"
                        )
                    }
                    else
                    {
                        mostrar_mensajes_errores(data["messages_errors"]);
                    }
                }
                catch(e)
                {
                    mostrar_mensajes_errores();
                }
            },
            error: function(error)
            {
                mostrar_mensajes_errores();
                cerrar_loading();
            }
        });
    }

 </script>


@endsection

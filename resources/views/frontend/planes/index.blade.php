@extends('frontend.template.master')

@section('title', 'Quienes somos')

@section('contenido')

<div class="container " style="margin-top:">
    @include('frontend.planes.lista_planes')
</div>
@endsection

@section("js_code")
<script type="text/javascript">

$(document).ready(function(){
  $(".navegacion_menu a").removeClass("active");
  $(".navegacion_menu .is_option_planes").addClass("active");
});

</script>
@endsection

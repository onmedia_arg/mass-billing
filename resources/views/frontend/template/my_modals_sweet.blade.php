
<div class="modal" tabindex="-1" role="dialog" id="modal_messages_errors" data-backdrop="false" data-keyboard="false">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title">Ha ocurrido un error</h5>
            </div>
        <div class="modal-body" style="text-align: center;">
            <div class="row">
                <div class="col-md-12" id="contenedor_messages_errors">
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
    </div>
  </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="modal_messages_success" data-backdrop="false" data-keyboard="false">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="title_messages_success">Ha ocurrido un error</h5>
            </div>
            <div class="modal-body" style="text-align: center;">
                <div class="row">
                    <div class="col-md-12">
                        <p id="message_messages_success"></p>
                    </div>
                    <input type="hidden" id="redirect_messages_success">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" onClick="cerrar_modal_mensajes_success()">
                    Ok
                </button>
            </div>
        </div>
    </div>
</div>

<script>

	function mostrar_mensajes_errores(mensajes = null){

		var html_content = "<p>Ha ocurrido un error</p>";

		if(mensajes != null)
		{
			html_content ="";

			for(var i=0; i < mensajes.length;i++)
			{
				html_content += "<p>"+mensajes[i]+"</p>";
			}
		}

		$("#contenedor_messages_errors").html(html_content);
		$("#modal_messages_errors").modal("show");
	}

	function mostrar_mensajes_success(title,descripcion,redirect = null)
	{
		$("#title_messages_success").text(title);
		$("#message_messages_success").text(descripcion);

		if(redirect == null)
		{
			redirect= "";
		}

		$("#redirect_messages_success").val(redirect);

		$("#modal_messages_success").modal("show");
	}

	function cerrar_modal_mensajes_success()
	{
		var redirect = $("#redirect_messages_success").val();

		if(redirect != "")
		{
			location.href=redirect;
		}
		else{
			$("#modal_messages_success").modal("hide");
		}
	}

	</script>
<!doctype html>
<html lang="es">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="csrf-token" content="<?php echo csrf_token() ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Styles -->
  <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css">
  <link rel="stylesheet" href="{{ asset('/frontend/vendor/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/frontend/assets/css/style.css') }}">

  <!-- Favicon -->
  <link rel="apple-touch-icon" sizes="180x180" href="/{{ asset('/frontend/assets/img/favicon/apple-touch-icon.png') }}">
  <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('/frontend/assets/img/favicon/favicon-32x32.png') }}">
  <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/frontend/assets/img/favicon/favicon-16x16.png') }}">
  <link rel="manifest" href="{{ asset('/frontend/assets/img/favicon/site.webmanifest') }}">
  <link rel="mask-icon" href="{{ asset('/frontend/assets/img/favicon/safari-pinned-tab.svg') }}" color="#ff8e60">
  <link rel="shortcut icon" href="{{ asset('/frontend/assets/img/favicon/favicon.ico') }}">
  <meta name="msapplication-TileColor" content="#ff8e60">
  <meta name="msapplication-config" content="{{ asset('/frontend/assets/img/favicon/browserconfig.xml') }}">
  <meta name="theme-color" content="#ff8e60">

  <!-- Meta -->
  <title>@yield("title") | {{Config("app.name")}}</title>
  <meta name="title" content="econta - Tu contador Online">
  <meta name="description" content="No importa donde vayas, tu contador online te sigue.">

  <!-- Open Graph / Facebook -->
  <meta property="og:type" content="website">
  <meta property="og:url" content="">
  <meta property="og:title" content="econta - Tu contador Online">
  <meta property="og:description" content="No importa donde vayas, tu contador online te sigue.">
  <meta property="og:image" content="{{ asset('/frontend/assets/img/og.jpg') }}">

  <!-- Twitter -->
  <meta property="twitter:card" content="summary_large_image">
  <meta property="twitter:url" content="">
  <meta property="twitter:title" content="econta - Tu contador Online">
  <meta property="twitter:description" content="No importa donde vayas, tu contador online te sigue.">
  <meta property="twitter:image" content="{{ asset('/frontend/assets/img/og.jpg') }}">



  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css2?family=Cabin:ital,wght@0,400;0,500;0,600;0,700;1,400;1,500;1,600;1,700&family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">

  <link rel="preload" as="image" href="{{ asset('/frontend/assets/img/hero.jpg') }}">

  @section("style")
  @show
</head>

<body>

    <div id="Top"></div>


    <header id="main-header">
      <div class="glow"></div>
        <a href="{{url('/')}}" class="logo"><img src="{{ asset('/frontend/assets/img/econta.svg') }}" alt="econta"></a>
      <nav class="navegacion_menu">
        <a href="{{url('/')}}" class="is_option_home">Home</a>
        <a href="{{url('/quienes_somos')}}" class="is_option_quienes_somos">Quiénes Somos</a>
        <a href="{{url('/servicios')}}" class="is_option_servicios">Servicios</a>
        <a href="{{url('/planes')}}"  class="is_option_planes">Planes</a>
      </nav>
      <button class="btn-menu">
        <span></span>
        <span></span>
        <span></span>
      </button>
    </header>
    <div class="menu">
      <nav class="navegacion_menu">
        <a href="{{url('/backend')}}">Login</a>
        <a href="{{url('/')}}" class="is_option_home">Home</a>
        <a href="{{url('/que_es')}}" class="is_option_que_es">Qué es econta</a>
        <a href="{{url('/servicios')}}" class="is_option_servicios">Servicios</a>
        <a href="#">Packs disponibles</a>
        <a href="{{url('/contacto')}}"  class="is_option_contacto">Contacto</a>
      </nav>
      <footer>
        <img src="{{ asset('/frontend/assets/img/econta.svg') }}" alt="econta">
        <p>Powered by JS & Asociados</p>
      </footer>
    </div>
    
  <main class="main-wrapper">
    @section('contenido')
    @show
  </main>

    <footer class="main-footer">
      <div class="glow"></div>
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="content">

              <img src="{{ asset('/frontend/assets/img/econta-white.svg') }}" alt="econta">
              <p>Powered by JS & Asociados</p>

            </div>

          </div>
        </div>
      </div>
    </footer>





  <!-- Scripts -->
  <script src="{{ asset('/frontend/vendor/jquery/jquery-3.5.1.min.js') }}"></script>
  <script src="{{ asset('/frontend/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
  <script src="{{ asset('/frontend/assets/js/app.js') }}"></script>

  @section("modals")
  @show

  @include("frontend.template.my_loading_modal")
  @include("frontend.template.my_modals_sweet")


  @section("js_code")
  @show
</body>

</html>
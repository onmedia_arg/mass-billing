@extends('frontend.template.master')

@section('title', 'Contactanos')

@section('contenido')

<div class="container mt-5 mb-5">
  <div class="row">
    <div class="col-md-12 mb-2">
      <h2>Contactanos</h2>
      <p class="mt-2">Completa el formulario a continuación o <a class="c-orange" href="mailto:mario.olivera@diblet.com">contacta directamente con nosotros vía email</a></p>
    </div>

    <div class="col-md-12">
      <form action="#" id="formulario_contacto">
        <div class="form-group">
            <label for="nombre">Nombre y apellido</label>
            <input type="text" class="form-control" id="nombre" name="nombre">
        </div>
        <div class="form-group">
            <label for="correo">Correo</label>
            <input type="text" class="form-control" id="correo" name="correo">
        </div>
        <div class="form-group">
            <label for="telefono">Teléfono</label>
            <input type="text" class="form-control" id="telefono" name="telefono">
        </div>
        <div class="form-group">
            <label for="mensaje">Mensaje</label>
            <textarea class="form-control" rows="5" id="mensaje" name="mensaje"></textarea>
        </div>

        <div style="text-align: center;">
            <button type="submit" class="btn-rounded black">
                Enviar
            </button>
        </div>

      </form>
    </div>
  </div>
</div>
@endsection

@section("js_code")

<script src="https://www.google.com/recaptcha/api.js?render={{env('RECAPTCHAV3_SITEKEY')}}"></script>

<script type="text/javascript">

  $(document).ready(function(){
    $(".navegacion_menu a").removeClass("active");
    $(".navegacion_menu .is_option_contacto").addClass("active");
  });

  $("#formulario_contacto").submit(function(e){
      e.preventDefault();

      grecaptcha.ready(function() {
        grecaptcha.execute("{{env('RECAPTCHAV3_SITEKEY')}}", {action: 'submit'}).then(function(token) {
          sendFormularioContacto(token);
        });
      });
  });

  function sendFormularioContacto(token)
  {
    let nombre = $.trim($("#formulario_contacto [name=nombre]").val());
    let correo = $.trim($("#formulario_contacto [name=correo]").val());
    let telefono = $.trim($("#formulario_contacto [name=telefono]").val());
    let mensaje = $.trim($("#formulario_contacto [name=mensaje]").val());

    $.ajax({
      url : "{{url('/contacto/enviar_mensaje')}}",
      type:"POST",
      data:{
        token,
        nombre,
        correo,
        telefono,
        mensaje
      },
      headers:
      {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      beforeSend: function(data){
        abrir_loading();
      },
      success: function(data)
      {
        cerrar_loading();

        try
        {
          if(data["response"] == true)
          {
            $("#formulario_contacto [name=nombre]").val("");
            $("#formulario_contacto [name=correo]").val("");
            $("#formulario_contacto [name=telefono]").val("");
            $("#formulario_contacto [name=mensaje]").val("");

            mostrar_mensajes_success(
              "Mensaje Enviado!",
              "Se ha enviado el mensaje correctamente, pronto nos pondremos en contacto con usted"
            );
          }
          else 
          {
            mostrar_mensajes_errores(data["messages_errors"]);
          }
        }
        catch(e)
        {
          mostrar_mensajes_errores();
        }
      },
      error: function(data)
      {
        cerrar_loading();
        mostrar_mensajes_errores();
      }
    });
  }
</script>

@endsection

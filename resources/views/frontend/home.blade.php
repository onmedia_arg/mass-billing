@extends('frontend.template.master')

@section('title', 'Inicio')

@section('contenido')

<section class="hero-wrapper">
  <div class="hero-wrapper--container">

    <div class="content">
      <h1>Llevá el control de tu contabilidad, estés donde estés.</h1>
      <div class="content-button">

        <a href="#" class="btn-rounded">
          Crea tu cuenta gratis
        </a>
        <p>¡No requiere tarjeta de crédito!</p>
      </div>
    </div>

  </div>
  <img src="{{ asset('/frontend/assets/img/hero.jpg') }}" alt="">
</section>

<section class="section-bienvenido">
  <div class="swiper-container swiper-bienvenido">
    <div class="swiper-wrapper">
      <div class="swiper-slide">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-lg-4">

              <h3>ES NEGOCIO CUANDO SABÉS ADMINISTRARTE.</h3>
              <p>Dedícale poco tiempo y mejorá los resultados de tu economía con Econta.<br>
              Como contadores nos encargaremos de que estés al día con tus impuestos y obligaciones, brindándote nuestra asesoría constante de manera online.
              Encontrarás información, novedades y herramientas que potenciarán tu
              actividad. Bienvenido.
              </p>

              <figure class="d-flex d-lg-none">
                <img src="{{ asset('/frontend/assets/img/welcome-2.svg') }}" alt="">
                <img src="{{ asset('/frontend/assets/img/welcome-1.svg') }}" alt="">
              </figure>

              <div class="text-center d-inline-block">
                <a href="#" class="btn-rounded black">Botón</a><br>
                <small>No requiere tarjeta de crédito</small>
              </div>

            </div>
            <div class="col-lg-8">

              <figure class="d-none d-lg-flex">
                <img src="{{ asset('/frontend/assets/img/welcome-2.svg') }}" alt="">
                <img src="{{ asset('/frontend/assets/img/welcome-1.svg') }}" alt="">
              </figure>

            </div>
          </div>
        </div>
      </div>
      <div class="swiper-slide">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-lg-4">

              <h3>Facturación eConta</h3>
              <p>Emití tus facturas dondequiera que estés, de un modo sencillo y rápido. O mejor aún, deléganos la tarea.</p>
              
              <figure class="d-flex d-lg-none">
                <img src="{{ asset('/frontend/assets/img/welcome-2.svg') }}" alt="">
                <img src="{{ asset('/frontend/assets/img/welcome-1.svg') }}" alt="">
              </figure>

              <div class="text-center d-inline-block">
                <a href="#" class="btn-rounded black">Botón</a><br>
                <small>No requiere tarjeta de crédito</small>
              </div>

            </div>
            <div class="col-lg-8">

              <figure class="d-none d-lg-flex">
                <img src="{{ asset('/frontend/assets/img/welcome-2.svg') }}" alt="">
                <img src="{{ asset('/frontend/assets/img/welcome-1.svg') }}" alt="">
              </figure>

            </div>
          </div>
        </div>
      </div>

      

      <div class="swiper-slide">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-lg-4">

              <!--<h3>Facturación eConta</h3>-->
              <p>Lleva un control de tus ventas y cobranzas. Con nuestros reportes podrás observar tus movimientos y compararlos de una forma clara para ayudarte a mejorar.</p>
              
              <figure class="d-flex d-lg-none">
                <img src="{{ asset('/frontend/assets/img/welcome-2.svg') }}" alt="">
                <img src="{{ asset('/frontend/assets/img/welcome-1.svg') }}" alt="">
              </figure>

              <div class="text-center d-inline-block">
                <a href="#" class="btn-rounded black">Botón</a><br>
                <small>No requiere tarjeta de crédito</small>
              </div>

            </div>
            <div class="col-lg-8">

              <figure class="d-none d-lg-flex">
                <img src="{{ asset('/frontend/assets/img/welcome-2.svg') }}" alt="">
                <img src="{{ asset('/frontend/assets/img/welcome-1.svg') }}" alt="">
              </figure>

            </div>
          </div>
        </div>
      </div>
      
    </div>
    <div class="swiper-pagination swiper-bienvenido--pagination"></div>
  </div>
</section>

<section class="section-contador">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="content">

          <h3>Un contador que <br> te la hace fácil</h3>
          <p>No pierdas tiempo, podés realizarnos consultas y conocer tu situación desde cualquier lugar</p>
        </div>
      </div>
    </div>
  </div>
  <img src="{{ asset('/frontend/assets/img/contador-cut.jpg') }}" alt="">
</section>

<section class="section-beneficios">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="content">
        
          <div class="item">
            <img src="{{ asset('/frontend/assets/img/beneficio-1.svg') }}" alt="">
            <p>Facturación <br> eConta </p>
          </div>
          <div class="item">
            <img src="{{ asset('/frontend/assets/img/beneficio-1.svg') }}" alt="">
            <p>Impuestos <br>  al dia </p>
          </div>
          <div class="item">
            <img src="{{ asset('/frontend/assets/img/beneficio-1.svg') }}" alt="">
            <p>crea <br>reportes</p>
          </div>
          <div class="item">
            <img src="{{ asset('/frontend/assets/img/beneficio-2.svg') }}" alt="">
            <p>es fácil <br>e intuitivo</p>
          </div>
          <div class="item">
            <img src="{{ asset('/frontend/assets/img/beneficio-3.svg') }}" alt="">
            <p>contador <br>remoto</p>
          </div>
          <div class="item">
            <img src="{{ asset('/frontend/assets/img/beneficio-4.svg') }}" alt="">
            <p>resumen <br>mensual</p>
          </div>
          <div class="item">
            <img src="{{ asset('/frontend/assets/img/beneficio-5.svg') }}" alt="">
            <p>todo en <br>la nube</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

@if(count($comentarios_clientes))
<section class="section-testimonios">
  <div class="container">
    <div class="row">
      <div class="col-12">

        

  <div class="swiper-container swiper-testimonios">
    <div class="swiper-wrapper">
      @foreach($comentarios_clientes as $comentario_cliente_obj)
      <div class="swiper-slide"><div class="quote">
        <div class="avatar"><img src="{{ asset('/storage/imagenes/comentarios_clientes/'.$comentario_cliente_obj->imagen) }}" alt=""></div>
        <blockquote>
          {{$comentario_cliente_obj->texto}}
          <div class="stars">
            @for($i=1; $i <= 5;$i++)
              @if($i <= $comentario_cliente_obj->calificacion)
                <div class="star active"></div>
              @else
                <div class="star"></div>
              @endif
            @endfor
          </div>
        </blockquote>
      </div>
    </div>
    @endforeach
  </div>
    <div class="swiper-pagination swiper-testimonios--pagination"></div>
  </div>




        
        
      </div>
    </div>
  </div>
</section>
@endif


<!-- AQUI PLANES -->
@include('frontend.planes.lista_planes')
<!-- FIN PLANES -->

<footer class="footer-top">
  <div class="container">
    <div class="row">
      <div class="col-12 text-right">
        <a href="#Top" class="btn-volver"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
            <path d="M0 16.67l2.829 2.83 9.175-9.339 9.167 9.339 2.829-2.83-11.996-12.17z" /></svg> Volver arriba</a>

      </div>
    </div>
  </div>
</footer>
@endsection

@section("js_code")


<script type="text/javascript">

$(document).ready(function(){
  $(".navegacion_menu a").removeClass("active");
  $(".navegacion_menu .is_option_home").addClass("active");
});

$("#formulario_registro").submit(function(){

  var nombre = $.trim($("#formulario_registro [name=nombre]").val());
  var correo = $.trim($("#formulario_registro [name=correo]").val());
  var telefono = $.trim($("#formulario_registro [name=telefono]").val());
  var terminos_y_condiciones = $("#formulario_registro [name=terminos_y_condiciones]").prop("checked");

  $.ajax({
    url : "{{url('/registro')}}",
    type:"POST",
    data:{
      nombre:nombre,
      correo:correo,
      telefono:telefono,
      terminos_y_condiciones:terminos_y_condiciones
    },
    headers:
    {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    beforeSend: function(data){
      abrir_loading();
    },
    success: function(data)
    {
      cerrar_loading();

      try{
        if(data["response"] == true)
        {
          mostrar_mensajes_success("Registrado!!","Se registrado correctamente, pronto nos pondremos en contacto con usted","{{url('/')}}");
          
        }
        else {
          mostrar_mensajes_errores(data["messages_errors"]);
        }
      }
      catch(e)
      {
        mostrar_mensajes_errores();
      }
    },
    error: function(data)
    {
      cerrar_loading();
      mostrar_mensajes_errores();
    }
  });

  return false;

});




</script>

@endsection

@extends('frontend.template.master')

@section('title', 'Contactanos')

@section('contenido')

<main class="page-contacto">
    <header class="page-header wow fadeIn" style="background-image: url('{{ asset('assets/img/bg-listado.jpg')}}')"></header>
  <div class="page-contacto--content">
    <div class="container">
      <div class="row">
        <div class="col-12 col-title">
          <h2 class="wow fadeUp"><span>¡Muchas gracias!</span></h2>
          <p class="wow fadeUp" data-wow-delay=".1s">Recibimos tus datos, en breve nos pondremos en contacto con vos.</p>
        </div>
      </div>
    </div>
  </div>
</main>



@endsection

@section("js_code")

@endsection

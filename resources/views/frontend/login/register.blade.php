<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
  	<meta name="csrf-token" content="<?php echo csrf_token() ?>">
	<title>Registrarse | {{ Config("app.name") }}</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('/admin/login/bootstrap/css/bootstrap.min.css')}}">
</head>
<body>
	<section>
		<div class="container">
			<div class="row">
                <div class="col-md-12">
                    <div class="card" style="margin-top: 20px;">
                        <div class="card-body">
                            <h4 class="card-title">Registrarse</h4>
                            
                                <div class="row">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="correo">Correo <span class="text-danger">*</span></label>
                                            <input type="text" type="correo" class="form-control" name="correo" id="correo" value="" required>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="password">Contraseña <span class="text-danger">*</span></label>
                                            <input type="password" type="password" class="form-control" name="password" id="password" value="" required>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="nombre">Nombre <span class="text-danger">*</span></label>
                                            <input type="text" type="nombre" class="form-control" name="nombre" id="nombre" value="" required>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="apellido">Apellido <span class="text-danger">*</span></label>
                                            <input type="text" type="apellido" class="form-control" name="apellido" id="apellido" value="" required>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="caracteristica_telefono">Caracteristica Teléfono <span class="text-danger">*</span></label>
                                            <input type="number" type="caracteristica_telefono" class="form-control" name="caracteristica_telefono" id="caracteristica_telefono" value="" required>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="telefono">Teléfono <span class="text-danger">*</span></label>
                                            <input type="number" type="telefono" class="form-control" name="telefono" id="telefono" value="" required>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="pais">País <span class="text-danger">*</span></label>
                                            <input type="text" type="pais" class="form-control" name="pais" id="pais" value="" required>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="provincia">Provincia <span class="text-danger">*</span></label>
                                            <input type="text" type="provincia" class="form-control" name="provincia" id="provincia" required data-eye>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="ciudad">Ciudad <span class="text-danger">*</span></label>
                                            <input type="text" type="ciudad" class="form-control" name="ciudad" id="ciudad" required data-eye>
                                        </div>
                                    </div>


                                </div>

                                <div class="form-group" style="margin-top: 40px;">
                                    <button onclick="registrarse()" class="btn btn-primary btn-block">
                                        Registrarse
                                    </button>
                                </div>
                        </div>
                        <div class="footer">
                        </div>
                    </div>
                </div>
			</div>
		</div>
	</section>

	<script src="{{ asset('/admin/login/js/jquery.min.js')}}"></script>
	<!-- Bootstrap 4 -->
	<script src="{{ asset('/admin/assets/js/core/bootstrap.min.js') }}"></script>

	@include("frontend.template.my_loading_modal")
    @include("frontend.template.my_modals_sweet")
	
	<script>

	  
	  function registrarse(){


	    var correo = $.trim($("#correo").val());
        var password = $.trim($("#password").val());
        var nombre = $.trim($("#nombre").val());
        var apellido = $.trim($("#apellido").val());
        var caracteristica_telefono = $.trim($("#caracteristica_telefono").val());
        var telefono = $.trim($("#telefono").val());
        var pais = $.trim($("#pais").val());
        var provincia = $.trim($("#provincia").val());
        var ciudad = $.trim($("#ciudad").val());

	    $.ajax({
	      url: "{{url('/registro')}}",
	      type: "POST",
	      data: {
	        correo:correo,
            password:password,
            nombre:nombre,
            apellido:apellido,
            caracteristica_telefono:caracteristica_telefono,
            telefono:telefono,
            pais:pais,
            provincia:provincia,
            ciudad:ciudad
	      },
	      headers:
	      {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	      },

	      beforeSend: function(e)
	      {
	        abrir_loading();
	      },
	      success: function(data)
	      {
	        cerrar_loading();

	        try
	        {
	          if(data["response"] == true)
	          {
	            location.href= data["redirect"];
	          }
	          else
	          {
	            mostrar_mensajes_errores(data["messages_errors"]);
	          }
	        }
	        catch(e)
	        {
	          mostrar_mensajes_errores();
	        }
	      },
	      error: function(e)
	      {
	        mostrar_mensajes_errores();
	        cerrar_loading();
	      }
	    });

	    return false;

	}
</script>

</body>
</html>
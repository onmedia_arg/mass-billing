@extends('emails.template.master')

@section('title', "Tu Factura - ".Config("app.name"))

@section('content')
<table width="100%" align="center">
	<tr>
		<td colspan="3" align="center">
			<p>Hola, ha recibido una nueva factura desde la web <a href="{{url('/')}}">{{Config("app.name")}}</a>:</p>
		</td>
	</tr>
</table>
@endsection

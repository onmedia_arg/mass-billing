@extends('emails.template.master')

@section('title', "Certificados AFIP a vencer - ".Config("app.name"))

@section('content')
<table width="100%">
	<tr>
		<td colspan="3">
			<p>Estos son los usuarios que tienen los certificados que vencen en la fecha: {{$email_sender->fecha_vencimiento_a_buscar}}</p>
		</td>
	</tr>
    
</table>
<br><br>

<hr>
@foreach($email_sender->usuarios_certificados_a_vencer as $usuario_a_vencer)
<div>
    <p><b>Usuario:</b> {{$usuario_a_vencer->usuario}}</p>
    <p><b>Correo:</b> {{$usuario_a_vencer->correo}}</p>
</div>
<hr>
@endforeach
<br><br><br>
<table width="100%">
	<tr>
		<td colspan="3">
			<a href="{{url('/backend')}}" class="btn btn-primary" style="color: #fff">INICIAR SESIÓN</a>
		</td>
	</tr>
</table>
@endsection
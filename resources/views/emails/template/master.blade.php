<!DOCTYPE html>
<html>
<head>
	<title>@yield("title")</title>

	<style type="text/css">

	.btn:not(:disabled):not(.disabled) {
        cursor: pointer;
    }
    .demo .btn, .demo .progress {
        margin-bottom: 15px !important;
    }
    .btn-round {
        border-radius: 100px !important;
    }
    .btn-primary {
        background: #d82b1d !important;
        border-color: #d82b1d !important;
    }
    .btn {
        padding: .65rem 1.4rem;
        font-size: 14px;
        opacity: 1;
        border-radius: 3px;
    }
    .btn-primary {
        color: #fff;
        background-color: #d82b1d;
        border-color: #d82b1d;
    }
    .btn {
        display: inline-block;
        font-weight: 400;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        border: 1px solid transparent;
            border-top-color: transparent;
            border-right-color: transparent;
            border-bottom-color: transparent;
            border-left-color: transparent;
        padding: .375rem .75rem;
        font-size: 1rem;
        line-height: 1.5;
        border-radius: .25rem;
        transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
    }
	</style>
</head>
<body>

<div width="100%" style="height: 40px;">
	<table width="100%" >
		<tr>
			<td width="100%" >
				<img src="{{ asset('/admin/assets/img/logo.png')}}" width="250">
			</td>
		</tr>
	</table>
</div>
<div style="height: 5px;background-color: #d82b1d"></div>
<table width="100%"  align="center">
	<tr>
		<td colspan="3" align="center">
			<h2 style="color: #d82b1d">@yield("title")</h2>
		</td>
	</tr>
</table>

@section("content")
@show
</body>
</html>

@extends('emails.template.master')

@section('title', "Comprobante - ".Config("app.name"))

@section('content')
<table width="100%" align="center">
	<tr>
		<td colspan="3" align="center">
			<p>Hola <b>{{$email_sender->comprobante_obj->razon_social_receptor}}</b> le enviamos este correo para informarle sobre el nuevo comprobante realizado</p>
		</td>
	</tr>
	<tr>
		<td colspan="3" align="center">
			<a href="{{url('/api/comprobantes/show/'.$email_sender->comprobante_obj->id.'/'.$email_sender->comprobante_obj->key_access)}}" class="btn btn-primary" style="color: #fff">VER COMPROBANTE</a>
		</td>
	</tr>
</table>
@endsection
@extends('emails.template.master')

@section('title', "Nueva Reserva desde la Web - ".Config("app.name"))

@section('content')
<table width="100%" align="center">
	<tr>
		<td colspan="3" align="center">
			<p>Hola, ha recibido una nueva reserva desde la web {{Config("app.name")}}:</p>
		</td>
	</tr>
	<tr>
		<td colspan="3" align="center">
			<p style="font-size: 17px;"><b>Nombre:</b> {{$email_sender->nombre}}</p>
		</td>
	</tr>
	<tr>
		<td colspan="3" align="center">
				<p style="font-size: 17px;"><b>Teléfono:</b> {{$email_sender->telefono}}</p>
		</td>
	</tr>
	<tr>
		<td colspan="3" align="center">
				<p style="font-size: 17px;"><b>Correo:</b> {{$email_sender->correo}}</p>
		</td>
	</tr>
	<tr>
		<td colspan="3" align="center">
				<p style="font-size: 17px;"><b>Llegada:</b> {{$email_sender->llegada}}</p>
		</td>
	</tr>
	<tr>
		<td colspan="3" align="center">
				<p style="font-size: 17px;"><b>Salida:</b> {{$email_sender->salida}}</p>
		</td>
	</tr>
	<tr>
		<td colspan="3" align="center">
				<p style="font-size: 17px;"><b>Cantidad de Huespedes:</b> {{$email_sender->cantidad_huespedes}}</p>
		</td>
	</tr>

	<tr>
		<td colspan="3" align="center">
				<p style="font-size: 17px;"><b>Propiedad:</b> <a href="{{$email_sender->url_propiedad}}"></a> {{$email_sender->nombre_propiedad}}</p>
		</td>
	</tr>

	<tr>
		<td colspan="3" align="center">
      <br><br>
			<a href="{{$email_sender->url_propiedad}}" class="btn btn-primary" style="color: #fff;">Ver Propiedad</a>
		</td>
	</tr>
</table>
@endsection

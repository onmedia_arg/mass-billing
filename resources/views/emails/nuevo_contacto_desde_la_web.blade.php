@extends('emails.template.master')

@section('title', "Nuevo Contacto desde la Web - ".Config("app.name"))

@section('content')
<table width="100%" align="center">
	<tr>
		<td colspan="3" align="center">
			<p>Hola, ha recibido un mensaje desde la sección de contacto de {{Config("app.name")}}:</p>
		</td>
	</tr>
	<tr>
		<td colspan="3" align="center">
			<p style="font-size: 17px;"><b>Nombre:</b> {{$email_sender->nombre}}</p>
		</td>
	</tr>
	<tr>
		<td colspan="3" align="center">
				<p style="font-size: 17px;"><b>Correo:</b> {{$email_sender->correo}}</p>
		</td>
	</tr>
	<tr>
		<td colspan="3" align="center">
				<p style="font-size: 17px;"><b>Teléfono:</b> {{$email_sender->telefono}}</p>
		</td>
	</tr>
	<tr>
		<td colspan="3" align="center">
				<p style="font-size: 17px;"><b>Mensaje</p>
		</td>
	</tr>
	<tr>
		<td colspan="3" align="center">
				<p style="font-size: 17px;">{{$email_sender->mensaje}}</p>
		</td>
	</tr>
	<tr>
		<td colspan="3" align="center">
      <br><br>
			<a href="{{url('/backend')}}" class="btn btn-primary" style="color: #fff;">INICIAR SESIÓN</a>
		</td>
	</tr>
</table>
@endsection

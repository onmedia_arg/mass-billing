@extends('emails.template.master')

@section('title', "Datos de su cuenta - ".Config("app.name"))

@section('content')
<table width="100%" align="center">
	<tr>
		<td colspan="3" align="center">
			<p>Hola <b>{{ $email_sender->apellido." ".$email_sender->nombre }}</b>, hemos recibido una solcitud de reestablecimiento de tu contraseña, creamos una nueva contraseña, a continuación te dejamos los datos de acceso, no te olvides que puedes cambiarla ingresando al sistema:</p>
		</td>
	</tr>
	<tr>
		<td colspan="3" align="center">
			<p style="font-size: 17px;"><b>Correo:</b> {{$email_sender->correo}}</p>
		</td>
	</tr>
	<tr>
		<td colspan="3" align="center">
			<p style="font-size: 17px;"><b>Contraseña:</b> {{$email_sender->password}}</p>
		</td>
	</tr>
	<tr>
		<td colspan="3" align="center">
			<a href="{{url('/backend')}}" class="btn btn-primary" style="color: #fff">INICIAR SESIÓN</a>
		</td>
	</tr>
</table>
@endsection
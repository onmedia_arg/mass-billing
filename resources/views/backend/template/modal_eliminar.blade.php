<div class="modal" tabindex="-1" role="dialog" id="modal_eliminar">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header bg-danger">
        <h4 class="text-white">{{ $titulo }}</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="row">
              <div class="col-md-12">
                <h4 class="text-center">{{ $mensaje }}</h4>
              </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" onclick="eliminar()">
            <i class="fa fa-times"></i> {{$confirmButtonText}}
        </button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">
            <i class="fa fa-close"></i>{{$cancelButtonText}}
        </button>
      </div>
    </div>
  </div>
</div>
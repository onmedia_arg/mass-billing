<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="csrf-token" content="<?php echo csrf_token() ?>">
  <title>@yield("title") | {{Config("app.name")}}</title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
  <link rel="icon" href="{{ asset('/admin/assets/img/icon.ico')}}" type="image/x-icon"/>

  <!-- Fonts and icons -->
  <script src="{{ asset('/admin/assets/js/plugin/webfont/webfont.min.js')}}"></script>
  <script>
    WebFont.load({
      google: {"families":["Lato:300,400,700,900"]},
      custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ['{{ asset('/admin/assets/css/fonts.min.css')}}']},
      active: function() {
        sessionStorage.fonts = true;
      }
    });
  </script>

  <!-- CSS Files -->
  <link rel="stylesheet" href="{{ asset('/admin/assets/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{ asset('/admin/assets/css/atlantis.min.css')}}">

  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link rel="stylesheet" href="{{ asset('/admin/assets/css/demo.css')}}">

  <link rel="stylesheet" href="{{ asset('/admin/assets/plugins/datetimepicker/jquery.datetimepicker.css') }}">

  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">

  <link rel="stylesheet" href="{{asset('/admin/assets/plugins/select2/dist/css/select2.min.css')}}">

  <link rel="stylesheet" href="{{asset('/admin/assets/plugins/dropzone/basic.css')}}">
  <link rel="stylesheet" href="{{asset('/admin/assets/plugins/dropzone.css')}}>">

  <style>
  .modal{
    background-color: rgba(0,0,0,0.6) !important;
  }

  /* EDITION WIDTH CONTENT*/
  .main-panel{
    width: 100% !important;
  }

  .main-panel>.content {
    margin-top: 0px !important;
}
  </style>
  @section("style")
  @show
</head>
<body>
  <div class="wrapper">
  @include('backend.template.navbar')

  <?php
  //@include('backend.template.menu_lateral')
  ?>

  @section("contenido")
  @show


  </div>
  <!--   Core JS Files   -->
  <script src="{{ asset('/admin/assets/js/core/jquery.3.2.1.min.js')}}"></script>
  <script src="{{ asset('/admin/assets/js/core/popper.min.js')}}"></script>
  <script src="{{ asset('/admin/assets/js/core/bootstrap.min.js')}}"></script>
  <!-- jQuery UI -->
  <script src="{{ asset('/admin/assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js')}}"></script>
  <script src="{{ asset('/admin/assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js')}}"></script>
  <!-- jQuery Scrollbar -->
  <script src="{{ asset('/admin/assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js')}}"></script>
  <!-- jQuery Sparkline -->
  <script src="{{ asset('/admin/assets/js/plugin/jquery.sparkline/jquery.sparkline.min.js')}}"></script>
  <!-- Datatables -->
  <script src="{{ asset('/admin/assets/js/plugin/datatables/datatables.min.js')}}"></script>
  <!-- Bootstrap Notify -->
  <script src="{{ asset('/admin/assets/js/plugin/bootstrap-notify/bootstrap-notify.min.js')}}"></script>
  <!-- jQuery Vector Maps -->
  <script src="{{ asset('/admin/assets/js/plugin/jqvmap/jquery.vmap.min.js')}}"></script>
  <script src="{{ asset('/admin/assets/js/plugin/jqvmap/maps/jquery.vmap.world.js')}}"></script>
  <!-- Sweet Alert -->
	<script src="{{ asset('/admin/assets/js/plugin/sweetalert/sweetalert.min.js') }}"></script>
  <!-- Atlantis JS -->
  <script src="{{ asset('/admin/assets/js/atlantis.min.js')}}"></script>
  <!-- MOMENT JS -->
  <script src="{{ asset('/admin/assets/plugins/moment/moment.min.js') }}"></script>
  <script src="{{ asset('/admin/assets/plugins/moment/moment-with-locales.min.js') }}"></script>
  <script>moment.locale('es');</script>
  <!-- DATE/TIME PICKERS -->
  <script src="{{ asset('/admin/assets/plugins/datetimepicker/jquery.datetimepicker.js') }}"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
  <!-- SELECT2 -->
  <script src="{{asset('/admin/assets/plugins/select2/dist/js/select2.full.min.js')}}"></script>
  <!-- DROPZONE -->
  <script src="{{asset('/admin/assets/plugins/dropzone/dropzone.min.js')}}"></script>

  @section("modals")
  @show

  @include("backend.template.my_loading_modal")
  @include("backend.template.my_modals_sweet")

  <script>

    $(document).ready(function(){

      $.datetimepicker.setLocale('es');

      $('.datepicker').datetimepicker({
          timepicker: false,
          closeOnDateSelect: true,
          format: 'd/m/Y',
          scrollMonth : false,
      });

      $('.timepicker').datetimepicker({
            timepicker: true,
            datepicker:false,
            closeOnDateSelect: true,
            format: 'H:i',
      });

      $(".select2").select2();
      
      $(".select2_add").select2({
        tags: true
      });

    });

  </script>

  <script>
    // FIX MODALS BOOTSTRAP 4
    $(document).on('hidden.bs.modal', function (event) {
      if ($('.modal:visible').length) {
          $('body').addClass('modal-open');
      }
    });
    // END FIX
  </script>
  @section("js_code")
  @show

</body>
</html>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark"  style="background-color: #293a50 !important;">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">
      <img class="logo horizontal-logo" src="{{asset('back.png')}}" style="height: 40px;">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a class="nav-link" href="{{url('/backend/desktop')}}"><i class="fas fa-desktop"></i> Escritorio</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{url('backend/ventanilla_electronica')}}"><i class="fas fa-bell"></i> Ventanilla Electronica</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{url('/backend/mis_clientes')}}"><i class="fas fa-users"></i> Mis Clientes</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="dropdownComprobantes" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		  	<i class="fas fa-list"></i> Comprobantes
          </a>
          <div class="dropdown-menu" aria-labelledby="dropdownComprobantes">
            <a class="dropdown-item" href="{{url('/backend/comprobantes')}}">Mis comprobantes</a>
            <a class="dropdown-item" href="{{url('/backend/comprobantes/nuevo')}}">Nuevo</a>
            <a class="dropdown-item" href="{{url('/backend/comprobantes/importaciones')}}">Importaciones</a>
            <a class="dropdown-item" href="{{url('/backend/comprobantes/mis_puntos_de_venta')}}">Mis Puntos de venta</a>
        </li>
      </ul>
    </div>
  </div>
</nav>

<?php
/*
<div class="main-header">
	<!-- Logo Header -->
	<div class="logo-header" data-background-color="white"  style="background-color: #293a50 !important;">
		
		<a href="{{url('backend/desktop')}}" class="logo" style="text-align: center;">
			<img src="{{ asset('/admin/assets/img/logo.png')}}" alt="navbar brand" class="navbar-brand img-fluid" width="150px"></span>
		</a>
		<button class="navbar-toggler sidenav-toggler ml-auto" type="button" data-toggle="collapse" data-target="collapse" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon">
				<i class="icon-menu"></i>
			</span>
		</button>
		<button class="topbar-toggler more"><i class="icon-options-vertical"></i></button>
		<div class="nav-toggle">
			<button class="btn btn-toggle toggle-sidebar">
				<i class="icon-menu"></i>
			</button>
		</div>
	</div>
	<!-- End Logo Header -->

	<!-- Navbar Header -->
	<nav class="navbar navbar-header navbar-expand-lg" data-background-color="white" style="background-color: #293a50 !important;">
		
		<div class="container-fluid">
			<!--<div class="collapse" id="search-nav">
				<form class="navbar-left navbar-form nav-search mr-md-3">
					<div class="input-group">
						<div class="input-group-prepend">
							<button type="submit" class="btn btn-search pr-1">
								<i class="fa fa-search search-icon"></i>
							</button>
						</div>
						<input type="text" placeholder="Search ..." class="form-control">
					</div>
				</form>
			</div>-->
			<ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
				<li class="nav-item toggle-nav-search hidden-caret">
					<a class="nav-link" data-toggle="collapse" href="#search-nav" role="button" aria-expanded="false" aria-controls="search-nav">
						<i class="fa fa-search"></i>
					</a>
				</li>

				
				<!--
				<li class="nav-item dropdown hidden-caret">
					<a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#" aria-expanded="false">
						<div class="avatar-sm">
							<img src="{{ url('storage/imagenes/usuarios/'.session('foto_perfil'))}}" alt="..." class="avatar-img rounded-circle">
						</div>
					</a>
					<ul class="dropdown-menu dropdown-user animated fadeIn">
						<div class="dropdown-user-scroll scrollbar-outer">
							<li>
								<div class="user-box">
									<div class="avatar-lg"><img src="{{ url('storage/imagenes/usuarios/'.session('foto_perfil'))}}" alt="image profile" class="avatar-img rounded"></div>
									<div class="u-text">
										<h4>{{session("apellido")." ".session("nombre")}}</h4>
										<p class="text-muted">{{session("correo")}}</p><a href="{{url('/backend/perfil')}}" class="btn btn-xs btn-secondary btn-sm">Editar Perfil</a>
									</div>
								</div>
							</li>
							<li>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item" href="{{url('/backend/cerrar_sesion')}}">Cerrar Sesión</a>
							</li>
						</div>
					</ul>
				</li>
				-->
			</ul>
		</div>
	</nav>
	<!-- End Navbar -->
</div>
*/
?>
@extends('backend.template.master')

@section('title', $title_page)

@section('style')
@endsection

@section('contenido')
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">{{$title_page}}</h4>
            </div>

            <div class="row">
                
                <div class="col-md-12">
                    <div class="card">

                        <div class="card-header">
                            <div class="d-flex align-items-center">
                                <a class='{{$config_buttons["go_back"]["class"]}}' href="{{ $link_controlador }}">
                                    <i class='{{$config_buttons["go_back"]["icon"]}}'></i>
                                    {{$config_buttons["go_back"]["title"]}}
                                </a>
                            </div>
                        </div>

                        <form action="#" id="formulario_editar">
                            <div class="card-body">
                                
                                <div class="row">
                                    <div class="col-md-2" style="text-align:center;">
                                        <img id="preview_foto_perfil" src="{{asset('/storage/imagenes/usuarios/'.$row_obj->foto_perfil)}}" class="img-fluid">
                                        <div id="foto_perfil_dropzone" style="display:none;"></div>
                                        <input type="text" hidden="true" id="foto_perfil" name="foto_perfil" value="{{$row_obj->foto_perfil}}">
                                        <button type="button" class="btn btn-sm btn-primary" style="margin-top: 2px;" onClick="cargarImagenDePerfil()">
                                            Cambiar
                                        </button>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-md-3 mt-2">
                                                <label for="correo">Correo: <span class="text-danger">*</span></label>
                                                <input type="text" name="correo" id="correo" class="form-control" value="{{$row_obj->correo}}">
                                            </div>

                                            <div class="col-md-3 mt-2">
                                                <label for="usuario">Usuario: <span class="text-danger">*</span></label>
                                                <input type="text" name="usuario" id="usuario" class="form-control" value="{{$row_obj->usuario}}">
                                            </div>
                                            
                                            <div class="col-md-3 mt-2">
                                                <label for="password_new">Cambiar Contraseña: <span class="text-danger">*</span></label>
                                                <input type="password" name="password_new" id="password_new" class="form-control" value="">
                                            </div>

                                            <div class="col-md-3 mt-2">
                                                <label for="password_new2">Repetir Contraseña: <span class="text-danger">*</span></label>
                                                <input type="password" name="password_new2" id="password_new2" class="form-control" value="">
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3 mt-2">
                                                <label for="nombre">Nombre: <span class="text-danger">*</span></label>
                                                <input type="text" name="nombre" id="nombre" class="form-control" value="{{$row_obj->nombre}}">
                                            </div>
                                            <div class="col-md-3 mt-2">
                                                <label for="apellido">Apellido: <span class="text-danger">*</span></label>
                                                <input type="text" name="apellido" id="apellido" class="form-control" value="{{$row_obj->apellido}}">
                                            </div>
                                        </div>
                                        
                                        <div class="row"> 

                                            <div class="col-md-3 mt-2">
                                                <label for="id_estado_usuario" >Estado: <span class="text-danger">*</span></label>
                                                <select name="id_estado_usuario" id="id_estado_usuario"  class="form-control"  value="{{$row_obj->id_estado_usuario}}">
                                                    <option value="">Seleccionar</option>

                                                    @foreach($estados_usuarios as $estado_usuario)
                                                        <option value="{{$estado_usuario->id}}">{{$estado_usuario->estado}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 mt-4" style="text-align: center;">
                                        <button class='{{$config_buttons["edit"]["class"]}}'>
                                            <i class='{{$config_buttons["edit"]["icon"]}}'></i>
                                            {{$config_buttons["edit"]["title"]}} {{$entity}}
                                        </button>
                                    </div>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
			</div>
		</div>
    </div>
</div>
@endsection


@section("js_code")

<script type="text/javascript">

$(document).ready(function(){

    $("#id_estado_usuario").val(<?php echo $row_obj->id_estado_usuario?>);

    Dropzone.autoDiscover = false;

    $("#foto_perfil_dropzone").dropzone({
        autoProcessQueue: true,
        url: "{{url('/backend/usuarios/upload_foto_perfil')}}",
        acceptedFiles: 'image/*',
        paramName: "file",
        uploadMultiple: false,
        chunking: true,
        chunkSize: 1000000,
        addRemoveLinks: true,
        dictRemoveFile : "Eliminar",

        init: function() {

            this.on("sending", function(file, xhr, formData) {
            abrir_loading();

            xhr.onreadystatechange = function() {

                if (xhr.readyState == XMLHttpRequest.DONE) {

                    var respuesta = JSON.parse(xhr.responseText);

                    if(respuesta["done"] == undefined)
                    {
                        var file_name =  respuesta["name"];

                        $("#preview_foto_perfil").attr("src","{{asset('storage/imagenes/usuarios')}}/"+file_name);
                        $("#formulario_editar [name=foto_perfil]").val(file_name);
                        cerrar_loading();
                    }
                }
            }
        });

        this.on("removedfile", function (file) {
        });

        },
        success: function(file, response){
            cerrar_loading();
        },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
    });
    
});

function cargarImagenDePerfil()
{
    $("#foto_perfil_dropzone").click();
}

$("#formulario_editar").submit(function(){
    editar();
    return false;
});

function editar()
{
    var formdata = new FormData();

    formdata.append("id",<?php echo $row_obj->id?>);
    formdata.append("correo",$("#correo").val());
    formdata.append("usuario",$("#usuario").val());
    formdata.append("nombre",$("#nombre").val());
    formdata.append("apellido",$("#apellido").val());
    formdata.append("password_new",$("#password_new").val());
    formdata.append("password_new2",$("#password_new2").val());
    formdata.append("id_estado_usuario",$("#id_estado_usuario").val());
    formdata.append("foto_perfil",$("#foto_perfil").val());

    $.ajax({
        url: "{{$link_controlador}}update",
        type: "POST",
        contentType: false,
        cache: false,
        processData:false,
        data: formdata,
        headers:
        {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function(event){
          abrir_loading();
        },
        success: function(data)
        {
            cerrar_loading();

            try
            {
              if(data["response"] == true)
              {
                $("#modal_editar").modal("hide");

                mostrar_mensajes_success(
                    "{{$abm_messages['success_edit']['title']}}",
                    "{{$abm_messages['success_edit']['description']}}",
                    "{{$link_controlador}}"
                );

              }
              else
              {
                mostrar_mensajes_errores(data["messages_errors"]);
              }
            }
            catch(e)
            {
                mostrar_mensajes_errores();
            }

        },
        error: function(error){
          cerrar_loading();
          mostrar_mensajes_errores();
        },
    });
}
</script>

@endsection
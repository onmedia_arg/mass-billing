@extends('backend.template.master')

@section('title', $title_page)

@section('contenido')
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">{{$title_page}}</h4>
            </div>
            
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">

                            <div class="card-header">
                                <div class="d-flex align-items-center">
                                    <a class='{{$config_buttons["go_back"]["class"]}}' href="{{ $link_controlador }}">
                                        <i class='{{$config_buttons["go_back"]["icon"]}}'></i>
                                        {{$config_buttons["go_back"]["title"]}}
                                    </a>
                                </div>
                            </div>
                            <form action="#" id="formulario_agregar">
                                <div class="card-body">
                                    
                                    <div class="row">
                                        <div class="col-md-2" style="text-align:center;">
                                            <img id="preview_foto_perfil" src="{{asset('/storage/imagenes/usuarios/default.png')}}" class="img-fluid">
                                            <div id="foto_perfil_dropzone" style="display:none;"></div>
                                            <input type="text" hidden="true" id="foto_perfil" name="foto_perfil" value="default.png">
                                            <button type="button" class="btn btn-sm btn-primary" style="margin-top: 2px;" onClick="cargarImagenDePerfil()">
                                                Cambiar
                                            </button>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row">
                                                <div class="col-md-3 mt-2">
                                                    <label for="correo">Correo: <span class="text-danger">*</span></label>
                                                    <input type="text" name="correo" id="correo" class="form-control">
                                                </div>

                                                <div class="col-md-3 mt-2">
                                                    <label for="usuario">Usuario: <span class="text-danger">*</span></label>
                                                    <input type="text" name="usuario" id="usuario" class="form-control">
                                                </div>
                                                
                                                <div class="col-md-3 mt-2">
                                                    <label for="password">Contraseña: <span class="text-danger">*</span></label>
                                                    <input type="password" name="password" id="password" class="form-control">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3 mt-2">
                                                    <label for="nombre">Nombre: <span class="text-danger">*</span></label>
                                                    <input type="text" name="nombre" id="nombre" class="form-control">
                                                </div>
                                                <div class="col-md-3 mt-2">
                                                    <label for="apellido">Apellido: <span class="text-danger">*</span></label>
                                                    <input type="text" name="apellido" id="apellido" class="form-control">
                                                </div>
                                            </div>
                                            
                                            <div class="row">    

                                                <div class="col-md-3 mt-2">
                                                    <label for="id_estado_usuario" >Estado: <span class="text-danger">*</span></label>
                                                    <select name="id_estado_usuario" id="id_estado_usuario"  class="form-control">
                                                        @foreach($estados_usuarios as $estado_usuario)
                                                            <option value="{{$estado_usuario->id}}">{{$estado_usuario->estado}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>  

                                                <div class="col-md-3 mt-2">
                                                    <label for="id_configuracion_afip" >Configuracion AFIP: <span class="text-danger">*</span></label>
                                                    <select name="id_configuracion_afip" id="id_configuracion_afip"  class="form-control">
                                                        @foreach($configuraciones_afip as $configuracion_afip_row)
                                                            <option value="{{$configuracion_afip_row->id}}">{{$configuracion_afip_row->razon_social}} - {{$configuracion_afip_row->cuit}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                
                                            </div>

                                            
                                        </div>

                                        

                                        <div class="col-md-12 mt-4" style="text-align: center;">
                                            <button class='{{$config_buttons["save"]["class"]}}'>
                                                <i class='{{$config_buttons["save"]["icon"]}}'></i>
                                                {{$config_buttons["save"]["title"]}} {{$entity}}
                                            </button>
                                        </div>

                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </form>
		</div>
    </div>
</div>
@endsection



@section("js_code")

<script type="text/javascript">

$(document).ready(function(){

    $("#id_estado_usuario").val(1);

    Dropzone.autoDiscover = false;

    $("#foto_perfil_dropzone").dropzone({
        autoProcessQueue: true,
        url: "{{url('/backend/usuarios/upload_foto_perfil')}}",
        acceptedFiles: 'image/*',
        paramName: "file",
        uploadMultiple: false,
        chunking: true,
        chunkSize: 1000000,
        addRemoveLinks: true,
        dictRemoveFile : "Eliminar",

        init: function() {

            this.on("sending", function(file, xhr, formData) {
            abrir_loading();

            try{
                xhr.onreadystatechange = function() {

                    if (xhr.readyState == XMLHttpRequest.DONE) {

                        var respuesta = JSON.parse(xhr.responseText);

                        if(respuesta["done"] == undefined)
                        {
                            var file_name =  respuesta["name"];

                            $("#preview_foto_perfil").attr("src","{{asset('storage/imagenes/usuarios')}}/"+file_name);
                            $("#formulario_agregar [name=foto_perfil]").val(file_name);
                            cerrar_loading();
                        }
                    }
                }
            }
            catch(e)
            {
            }
            
        });

        this.on("dictResponseError",function(error){
            alert("ERROR!");
        });

        this.on("removedfile", function (file) {
        });

        },
        success: function(file, response){
            cerrar_loading();
        },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
    });
});

function cargarImagenDePerfil()
{
    $("#foto_perfil_dropzone").click();
}

$("#formulario_agregar").submit(function(){
    agregar();
    return false;
});

function agregar()
{
    var formdata = new FormData();

    
    formdata.append("correo",$("#correo").val());
    formdata.append("usuario",$("#usuario").val());
    formdata.append("nombre",$("#nombre").val());
    formdata.append("apellido",$("#apellido").val());
    formdata.append("password",$("#password").val());
    formdata.append("id_estado_usuario",$("#id_estado_usuario").val());
    formdata.append("id_configuracion_afip",$("#id_configuracion_afip").val());
    formdata.append("foto_perfil",$("#foto_perfil").val());

    $.ajax({
        url: "{{$link_controlador}}store",
        type: "POST",
        contentType: false,
        cache: false,
        processData:false,
        data: formdata,
        headers:
        {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function(event){
          abrir_loading();
        },
        success: function(data)
        {
            cerrar_loading();

            try
            {
              if(data["response"] == true)
              {
                $("#modal_agregar").modal("hide");

                mostrar_mensajes_success(
                    "{{$abm_messages['success_add']['title']}}",
                    "{{$abm_messages['success_add']['description']}}",
                    "{{$link_controlador}}"
                );
              }
              else
              {
                mostrar_mensajes_errores(data["messages_errors"]);
              }
            }
            catch(e)
            {
                mostrar_mensajes_errores();
            }

        },
        error: function(error){
          cerrar_loading();
          mostrar_mensajes_errores();
        },
    });
}
</script>

@endsection
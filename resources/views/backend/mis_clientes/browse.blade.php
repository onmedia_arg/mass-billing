@extends('backend.template.master')

@section('title', $title_page)

@section('contenido')
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">{{$title_page}}</h4>
            </div>

            <div class="row">
                
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            @if($add_active === true)
                            <div class="d-flex align-items-center">
                                @if($is_ajax == true)
                                <button class='{{$config_buttons["add"]["class"]}}' onClick="abrir_modal_agregar_cliente()">
                                    <i class='{{$config_buttons["add"]["icon"]}}'></i>
                                    {{$config_buttons["add"]["title"]}} {{$entity}}
                                </button>
                                @else
                                <a href="{{$link_controlador.'nuevo'}}" class='{{$config_buttons["add"]["class"]}}'>
                                    <i class='{{$config_buttons["add"]["icon"]}}'></i>
                                    {{$config_buttons["add"]["title"]}} {{$entity}}
                                </a>
                                @endif
                            </div>
                            @endif
                        </div>
                        <div class="card-body">
                            
                            <div class="table-responsive">
                                <table id="tabla_listado" class="display table table-striped table-hover" >
                                    <thead>
                                        <tr>
                                            @foreach($columns as $column)
                                            <th>{{$column["name"]}}</th>
                                            @endforeach
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>
    </div>
</div>
@endsection


@section("modals")

    @include('backend.mis_clientes.modal_agregar_cliente')

    @component('backend.template.modal_editar')
        @slot('entidad')
            {{$entity}}
        @endslot

        @slot('inputs')
        <div class="row">
            <div class="col-md-6">
                <label for="id_condicion_iva_editar">Condición Iva: <span class="text-danger">*</span></label>
                <select class="form-control" id="id_condicion_iva_editar">
                    <option value="">Seleccionar</option>
                    
                    @foreach($condiciones_iva as $condicion_iva_row)
                    <option value="{{$condicion_iva_row->id}}">{{$condicion_iva_row->descripcion}}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-md-6">
                <label for="id_tipo_documento_editar_cliente">Tipo documento: <span class="text-danger">*</span></label>
                <select class="form-control" id="id_tipo_documento_editar_cliente">
                </select>
            </div>
        </div>

        <div class="row mt-2">
            <div class="col-md-12">
                <label for="documento_editar_cliente">Documento: <span class="text-danger">*</span></label>
                <input type="text" class="form-control" id="documento_editar_cliente" oninput="soloNumeros(this)">
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <label for="nombre_razon_social_editar">Nombre/Razón Social: <span class="text-danger">*</span></label>
                <input type="text" class="form-control" id="nombre_razon_social_editar">
            </div>
        </div>

        <div class="row mt-2">
            <div class="col-md-6">
                <label for="correo_editar">Correo:</label>
                <input type="text" class="form-control" id="correo_editar">
            </div>

            <div class="col-md-6">
                <label for="telefono_editar">Teléfono:</label>
                <input type="text" class="form-control" id="telefono_editar">
            </div>
        </div>

        <div class="row mt-2">
            <div class="col-md-12">
                <label for="domicilio_comercial_editar">Domicilio comercial: <span class="text-danger">*</span></label>
                <input type="text" class="form-control" id="domicilio_comercial_editar">
            </div>
        </div>
        @endslot
    @endcomponent

@endsection

@section("js_code")

<script type="text/javascript">
var listado = null;
var id_trabajando = 0;

$(document).ready( function () {
    listado= $('#tabla_listado').DataTable( {
        "processing": true,
        "serverSide": true,
        "responsive":false,
        "ajax":{
        url : "{{$link_controlador}}get_listado_dt", // json datasource
        type: "post",
        headers:
        {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        error: function(error){
            $(".employee-grid-error").html("");
            $("#employee-grid").append('<tbody class="employee-grid-error"><tr><th colspan="3">No hay datos</th></tr></tbody>');
            $("#employee-grid_processing").css("display","none");
        }
        }
    });

    setRefTablaListadoClientesAddCode(listado);
});

$("#documento_editar_cliente").change(function(){

    var numero_documento = $.trim($("#documento_editar_cliente").val());
    var id_tipo_documento = $.trim($("#id_tipo_documento_editar_cliente").val());

    if(numero_documento != "" && id_tipo_documento != "")
    {
        $.ajax({
            url: "{{url('/backend/afip/get_datos_persona')}}",
            type: "POST",
            data: {
                id_tipo_documento:id_tipo_documento,
                numero_documento:numero_documento
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend: function(data)
            {
                abrir_loading();
            },
            success: function(data)
            {
                cerrar_loading();

                try
                {
                    if(data["response"])
                    {
                        if(data["data"])
                        {
                            $("#nombre_razon_social_editar").val(data["data"]["razon_social"]);
                            $("#domicilio_comercial_editar").val(data["data"]["domicilio_comercial"]);
                        }
                        else
                        {
                            $("#nombre_razon_social_editar").val("");
                            $("#domicilio_comercial_editar").val("");
                        }
                    }
                    else
                    {
                        mostrar_mensajes_errores(data["messages_errors"]);
                    }
                }
                catch(e)
                {
                    $("#nombre_razon_social_editar").val("");
                    $("#domicilio_comercial_editar").val("");
                }
            },
            error: function(error)
            {
                cerrar_loading();
                $("#nombre_razon_social_editar").val("");
                $("#domicilio_comercial_editar").val("");
            }
        });
    }
    else
    {
        $("#nombre_razon_social_editar").val("");
        $("#domicilio_comercial_editar").val("");
    }
});

function abrir_modal_editar(id)
{
    $.ajax({
        url: "{{$link_controlador}}get",
        type: "POST",
        data: {id:id},
        headers:
        {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function(event){
          abrir_loading();
        },
        success: function(data)
        {
            cerrar_loading();

            try
            {
              if(data["response"] == true)
              {
                id_trabajando = id;

                $("#nombre_razon_social_editar").val(data["data"]["nombre_razon_social"]);
                $("#documento_editar_cliente").val(data["data"]["documento"]);
                $("#correo_editar").val(data["data"]["correo"]);
                $("#telefono_editar").val(data["data"]["telefono"]);
                $("#domicilio_comercial_editar").val(data["data"]["domicilio_comercial"]);

                $("#id_tipo_documento_editar_cliente").html("<option value=''>Selected</option>");

                for(var i=0; i < data["data"]["tipos_de_documentos"].length;i++)
                {
                    $("#id_tipo_documento_editar_cliente").append("<option value='"+data["data"]["tipos_de_documentos"][i]["id"]+"'>"+data["data"]["tipos_de_documentos"][i]["descripcion"]+"</option>");
                }

                $("#id_tipo_documento_editar_cliente").val(data["data"]["id_tipo_documento"]);

                $("#id_condicion_iva_editar").val(data["data"]["id_condicion_iva"]);

                $("#modal_editar").modal("show");
              }
              else
              {
                mostrar_mensajes_errores(data["messages_errors"]);
              }
            }
            catch(e)
            {
              mostrar_mensajes_errores();
            }
        },
        error: function(error){
          cerrar_loading();
          mostrar_mensajes_errores();
        },
    });
}



function editar()
{
    var formdata = new FormData();

    formdata.append("id_tipo_documento",$("#id_tipo_documento_editar_cliente").val());
    formdata.append("id",id_trabajando);
    formdata.append("nombre_razon_social",$("#nombre_razon_social_editar").val());
    formdata.append("documento",$("#documento_editar_cliente").val());
    formdata.append("correo",$("#correo_editar").val());
    formdata.append("telefono",$("#telefono_editar").val());
    formdata.append("domicilio_comercial",$("#domicilio_comercial_editar").val());
    formdata.append("id_condicion_iva",$("#id_condicion_iva_editar").val());

    $.ajax({
        url: "{{$link_controlador}}update",
        type: "POST",
        contentType: false,
        cache: false,
        processData:false,
        data: formdata,
        headers:
        {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function(event){
          abrir_loading();
        },
        success: function(data)
        {
            cerrar_loading();

            try
            {
              if(data["response"] == true)
              {
                $("#modal_editar").modal("hide");

                mostrar_mensajes_success(
                    "{{$abm_messages['success_edit']['title']}}",
                    "{{$abm_messages['success_edit']['description']}}"
                );

                listado.draw();
              }
              else
              {
                mostrar_mensajes_errores(data["messages_errors"]);
              }
            }
            catch(e)
            {
              mostrar_mensajes_errores();
            }

        },
        error: function(error){
          cerrar_loading();
          mostrar_mensajes_errores();
        },
    });
}

function eliminar(id)
{
    swal({
        title: "{{$abm_messages['delete']['title']}}",
        text: "{{$abm_messages['delete']['text']}}",
        icon: "warning",
        buttons: ["Cancelar", "Aceptar"],
        dangerMode: true,
    })
    .then((willDelete) => {

        if (willDelete) 
        {
            $.ajax({
                url: "{{$link_controlador}}delete",
                type: "POST",
                data: {id:id},
                headers:
                {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                beforeSend: function(event){
                    abrir_loading();
                },
                success: function(data)
                {
                    cerrar_loading();

                    try{

                        if(data["response"])
                        {
                            mostrar_mensajes_success(
                                "{{$abm_messages['delete']['success_text']}}",
                                "{{$abm_messages['delete']['success_description']}}"
                            );

                            listado.draw();
                        }
                        else
                        {
                            mostrar_mensajes_errores(data["messages_errors"]);
                        }
                        }
                        catch(e)
                        {
                        mostrar_mensajes_errores();
                        }
                },
                error: function(error){
                    cerrar_loading();
                    mostrar_mensajes_errores();
                },
            });
        }
    });
}
</script>

@endsection
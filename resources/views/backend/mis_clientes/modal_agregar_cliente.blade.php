<?php
    $condiciones_iva = \App\CondicionIva::orderBy("descripcion")->get();
?>

<div class="modal" tabindex="-1" role="dialog" id="modal_agregar_cliente">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <h4 class="text-white">Agregar Cliente</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="#" id="formulario_agregar">

            <div class="row">

                <div class="col-md-6">
                    <label for="id_condicion_iva_agregar_cliente">Condición Iva: <span class="text-danger">*</span></label>
                    <select class="form-control" id="id_condicion_iva_agregar_cliente">
                        <option value="">Seleccionar</option>
                        
                        @foreach($condiciones_iva as $condicion_iva_row)
                        <option value="{{$condicion_iva_row->id}}">{{$condicion_iva_row->descripcion}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-md-6">
                    <label for="id_tipo_documento_agregar_cliente">Tipo documento: <span class="text-danger">*</span></label>
                    <select class="form-control" id="id_tipo_documento_agregar_cliente">
                    </select>
                </div>
            </div>

            <div class="row mt-2">
                <div class="col-md-12">
                    <label for="documento_agregar_cliente">Documento: <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" id="documento_agregar_cliente" oninput="soloNumeros(this)">
                </div>
            </div>

            <div class="row mt-2">
                <div class="col-md-12">
                    <label for="nombre_razon_social_agregar_cliente">Nombre/Razón Social: <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" id="nombre_razon_social_agregar_cliente">
                </div>
            </div>

            <div class="row mt-2">
                <div class="col-md-6">
                    <label for="correo_agregar_cliente">Correo:</label>
                    <input type="text" class="form-control" id="correo_agregar_cliente">
                </div>

                <div class="col-md-6">
                    <label for="telefono_agregar_cliente">Teléfono:</label>
                    <input type="text" class="form-control" id="telefono_agregar_cliente">
                </div>
            </div>

            <div class="row mt-2">

                <div class="col-md-12">
                    <label for="domicilio_comercial_agregar_cliente">Domicilio comercial: <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" id="domicilio_comercial_agregar_cliente">
                </div>
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" onclick="agregar_cliente()">
            <i class="fa fa-save"></i> Guardar
        </button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">
            <i class="fa fa-close"></i>Cancelar
        </button>
      </div>
    </div>
  </div>
</div>

<script>

var ref_tabla_listado_clientes_add_code = null;

function setRefTablaListadoClientesAddCode(listado)
{
    ref_tabla_listado_clientes_add_code = listado;
}

$("#documento_agregar_cliente").change(function(){

    var numero_documento = $.trim($("#documento_agregar_cliente").val());
    var id_tipo_documento = $.trim($("#id_tipo_documento_agregar_cliente").val());

    if(numero_documento != "" && id_tipo_documento != "")
    {
        $.ajax({
            url: "{{url('/backend/afip/get_datos_persona')}}",
            type: "POST",
            data: {
                id_tipo_documento:id_tipo_documento,
                numero_documento:numero_documento
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend: function(data)
            {
                abrir_loading();
            },
            success: function(data)
            {
                cerrar_loading();

                try
                {
                    if(data["response"])
                    {
                        if(data["data"])
                        {
                            $("#nombre_razon_social_agregar_cliente").val(data["data"]["razon_social"]);
                            $("#domicilio_comercial_agregar_cliente").val(data["data"]["domicilio_comercial"]);
                        }
                        else
                        {
                            $("#nombre_razon_social_agregar_cliente").val("");
                            $("#domicilio_comercial_agregar_cliente").val("");
                        }
                    }
                    else
                    {
                        mostrar_mensajes_errores(data["messages_errors"]);
                    }
                }
                catch(e)
                {
                    $("#nombre_razon_social_agregar_cliente").val("");
                    $("#domicilio_comercial_agregar_cliente").val("");
                }
            },
            error: function(error)
            {
                cerrar_loading();
                $("#nombre_razon_social_agregar_cliente").val("");
                $("#domicilio_comercial_agregar_cliente").val("");
            }
        });
    }
    else
    {
        $("#nombre_razon_social_agregar_cliente").val("");
        $("#domicilio_comercial_agregar_cliente").val("");
    }
});

$("#id_condicion_iva_agregar_cliente").change(function(){

    var id_condicion_frente_al_iva = $.trim($("#id_condicion_iva_agregar_cliente").val());
    
    if(id_condicion_frente_al_iva != "")
    {
        $.ajax({
            url: "{{url('/backend/comprobantes/get_tipos_de_documentos_unicos')}}",
            type: "POST",
            data: {
                id_condicion_frente_al_iva:id_condicion_frente_al_iva
            },
            headers:
            {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend: function(event){
                abrir_loading();
            },
            success: function(data)
            {
                cerrar_loading();

                try
                {
                    if(data["response"] == true)
                    {
                        var tipos_documentos_permitidos = data["data"];

                        $("#id_tipo_documento_agregar_cliente").html("<option value='' selected>Seleccionar</option>");

                        for(var i=0; i < tipos_documentos_permitidos.length;i++)
                        {
                            $("#id_tipo_documento_agregar_cliente").append("<option value='"+tipos_documentos_permitidos[i]["id"]+"'>"+tipos_documentos_permitidos[i]["descripcion"]+"</option>");
                        }
                    }
                    else
                    {
                        mostrar_mensajes_errores(data["messages_errors"]);
                    }
                }
                catch(e)
                {
                    mostrar_mensajes_errores();
                }
            },
            error: function(error){
                cerrar_loading();
                mostrar_mensajes_errores();
            },
        });
    }

});

function abrir_modal_agregar_cliente()
{
    $("#nombre_razon_social_agregar_cliente").val("");
    $("#documento_agregar_cliente").val("");
    $("#correo_agregar_cliente").val("");
    $("#telefono_agregar_cliente").val("");
    $("#domicilio_comercial_agregar_cliente").val("");
    $("#id_condicion_iva_agregar_cliente").val("");

    $("#modal_agregar_cliente").modal("show");
}

function agregar_cliente()
{
    var formdata = new FormData();

    formdata.append("id_tipo_documento",$("#id_tipo_documento_agregar_cliente").val());
    formdata.append("nombre_razon_social",$("#nombre_razon_social_agregar_cliente").val());
    formdata.append("documento",$("#documento_agregar_cliente").val());
    formdata.append("correo",$("#correo_agregar_cliente").val());
    formdata.append("telefono",$("#telefono_agregar_cliente").val());
    formdata.append("domicilio_comercial",$("#domicilio_comercial_agregar_cliente").val());
    formdata.append("id_condicion_iva",$("#id_condicion_iva_agregar_cliente").val());

    $.ajax({
        url: "{{url('/backend/mis_clientes/store')}}",
        type: "POST",
        contentType: false,
        cache: false,
        processData:false,
        data: formdata,
        headers:
        {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function(event){
          abrir_loading();
        },
        success: function(data)
        {
            cerrar_loading();

            try
            {
              if(data["response"] == true)
              {
                $("#modal_agregar_cliente").modal("hide");

                mostrar_mensajes_success(
                    "{{$abm_messages['success_add']['title']}}",
                    "{{$abm_messages['success_add']['description']}}"
                );

                if(ref_tabla_listado_clientes_add_code != null)
                {
                    ref_tabla_listado_clientes_add_code.draw();
                }
              }
              else
              {
                mostrar_mensajes_errores(data["messages_errors"]);
              }
            }
            catch(e)
            {
              mostrar_mensajes_errores();
            }

        },
        error: function(error){
          cerrar_loading();
          mostrar_mensajes_errores();
        },
    });
}

</script>
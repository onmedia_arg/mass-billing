@extends('backend.template.master')

@section('title', 'Escritorio')

@section('contenido')
<div class="main-panel">
    <div class="content">
        <div class="panel-header bg-primary-gradient">
            <div class="page-inner py-5">
                <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                    <div>
                        <h2 class="text-white pb-2 fw-bold">Escritorio</h2>
                        <h5 class="text-white op-7 mb-2">Panel de inicio de {{Config("app.name")}}</h5>
                    </div>
                    <div class="ml-md-auto py-2 py-md-0">
                        <a href="{{url('/backend/comprobantes')}}" class="btn btn-white btn-border btn-round mr-2">Mis Comprobantes</a>

                    </div>
                </div>
            </div>
        </div>

        <div class="page-inner mt--5">
            <div class="row">
                <div class="col-md-12">
                    <div class="card full-height">
                        <div class="card-body">
                            <div class="card-title">Bienvenido {{ session("nombre") }}</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mt-2">
                <div class="col-md-6" style="text-align: center;">
                    <div class="card full-height">
                        <div class="card-body">
                            <h2 class="text-info">Total facturado ultimos 12 meses (en {{Config("app.name")}})</h2>
                            <h2 class="text-success">$ {{number_format($total_facturado,2,",",".")}}</h2>
                            @if($usuario_obj->margen_facturacion_categoria > 0)
                            <h2 class="text-danger">Margen facturación para no salir de categoría: <br>$ {{number_format($usuario_obj->margen_facturacion_categoria,2,",",".")}}</h2>
                            @endif
                        </div>
                    </div>
                </div>
                

                @if(count($data_plan_usando))
                    <div class="col-md-6" style="text-align: center;">
                        <div class="card full-height">
                            <div class="card-body">
                                <h2 class="text-info">Está usando el plan {{$data_plan_usando[0]->nombre}}</h2>
                                <h2 class="text-danger">Fecha de vencimiento: {{$data_plan_usando[0]->fecha_vencimiento}}</h2>
                                
                            </div>
                        </div>
                    </div>
                @endif
            </div>

            <div class="row mt--2">
                <div class="col-md-4">
                    <div class="card full-height">
                        <div class="card-body">
                            <div class="card-title">Estado servicio AFIP</div>

                            <div class="row" style="margin-top: 20px;">
                                @if($estado_de_servidor)
                                <div class="col-md-12">
                                    <p>Servidor: {{$estado_de_servidor->AppServer}}</p>
                                    <p>Base de Datos: {{$estado_de_servidor->DbServer}}</p>
                                    <p>Autorización: {{$estado_de_servidor->AuthServer}}</p>
                                </div>
                                @else
                                <div class="row" style="margin-top: 20px;">
                                    <div class="col-md-12">
                                        <p>Servidor: <span class="text-danger">NO</span></p>
                                        <p>Base de Datos: <span class="text-danger">NO</span></p>
                                        <p>Autorización: <span class="text-danger">NO</span></p>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="card full-height">
                        <div class="card-body">
                            <div class="card-title">Estado de tu certificado AFIP</div>

                            <div class="row" style="margin-top: 20px;">
                                
                                @if($estado_certificado)
                                    <div class="col-md-12 text-center">
                                        <img src="{{asset('/admin/assets/img/checking.png')}}" alt="" width="200px">
                                    </div>
                                    <div class="col-md-12 text-center mt-2">
                                        <p style="font-size: 16px" class="text-success"><strong>Tu certificado de AFIP es válido</strong></p>
                                    </div>
                                @else
                                    <div class="col-md-12 text-center">
                                        <img src="{{asset('/admin/assets/img/error.png')}}" alt="" width="200px">
                                    </div>
                                    <div class="col-md-12 text-center mt-2">
                                        <p style="font-size: 16px" class="text-danger"><strong>Tu certificado de AFIP no es válido, contactate con nosotros</strong></p>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                @if(trim($usuario_obj->tus_impuestos) != "")
                <div class="col-md-4">
                    <div class="card full-height">
                        <div class="card-body">
                            <div class="card-title">Tus impuestos</div>

                            <div class="row" style="margin-top: 20px;">
                                
                                <div class="col-md-12">
                                    <p>{!!str_replace("\n","<br/>",$usuario_obj->tus_impuestos)!!}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>

    </div>
    <footer class="footer">
        <div class="container-fluid">
            <div class="copyright ml-auto">
                {{Date("Y")}}, Desarrollado por <a href="{{Config('app.link_empresa_software')}}">{{ Config('app.nombre_empresa_software') }}</a>
            </div>
        </div>
    </footer>
</div>
@endsection

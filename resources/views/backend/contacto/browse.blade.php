@extends('backend.template.master')

@section('title', $title_page)

@section('contenido')
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">{{$title_page}}</h4>
            </div>

            <div class="row">
                
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            
                            <form id="formulario_contacto">
                                <div class="row">
                                    <div class="col-md-12">
                                        <p>En esta sección podrá ponerse en contacto con nosotros, le estaremos respondiendo a la brevedad</p>
                                    </div>
                                    <div class="col-md-12">
                                        <label for="mensaje">Mensaje</label>
                                        <textarea id="mensaje" class="form-control" rows="10"></textarea>
                                    </div>
                                    <div class="col-md-12 mt-5" style="text-align: center">
                                        <button class="btn btn-primary btn-lg">
                                            <i class="fa fa-envelope"></i> Enviar mensaje
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
			</div>
		</div>
    </div>
</div>
@endsection


@section("modals")

@endsection

@section("js_code")

<script type="text/javascript">

$("#formulario_contacto").submit(function(){

    var formdata = new FormData();

    formdata.append("mensaje",$("#mensaje").val());

    $.ajax({
        url: "{{$link_controlador}}enviarMensaje",
        type: "POST",
        contentType: false,
        cache: false,
        processData:false,
        data: formdata,
        headers:
        {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function(event){
          abrir_loading();
        },
        success: function(data)
        {
            cerrar_loading();

            try
            {
              if(data["response"] == true)
              { 
                mostrar_mensajes_success(
                    "Enviado",
                    "Se ha enviado el mensaje correctamente, nos pondremos en contacto pronto",
                    "{{$link_controlador}}"
                );
              }
              else
              {
                mostrar_mensajes_errores(data["messages_errors"]);
              }
            }
            catch(e)
            {
                mostrar_mensajes_errores();
            }

        },
        error: function(error){
          cerrar_loading();
          mostrar_mensajes_errores();
        },
    });

    return false;
});
</script>

@endsection
@extends('backend.template.master')

@section('title', $title_page)

@section('contenido')
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">{{$title_page}}</h4>
            </div>

            <div class="row">

                <div class="col-md-12 mt-3" >
                    <div class="card">
                        <div class="card-header">
                            @if($add_active === true)
                            <div class="d-flex align-items-center">
                                @if($is_ajax == true)
                                <button class='{{$config_buttons["add"]["class"]}}' onClick="abrir_modal_importar()">
                                    <i class='{{$config_buttons["add"]["icon"]}}'></i>
                                    {{$config_buttons["add"]["title"]}} {{$entity}}
                                </button>
                                @else
                                <a href="{{$link_controlador.'nuevo'}}" class='{{$config_buttons["add"]["class"]}}'>
                                    <i class='{{$config_buttons["add"]["icon"]}}'></i>
                                    {{$config_buttons["add"]["title"]}} {{$entity}}
                                </a>
                                @endif
                            </div>
                            @endif


                        </div>
                        <div class="card-body">

                            <div class="table-responsive">
                                <table id="tabla_listado" class="display table table-striped table-hover" >
                                    <thead>
                                        <tr>
                                            @foreach($columns as $column)
                                            <th>{{$column["name"]}}</th>
                                            @endforeach
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>
    </div>
</div>

<div id="formulario_subida_archivo_importacion" style="display: none;">
</div>

@endsection


@section("modals")

<div class="modal" role="dialog" id="modal_importar">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Importar Comprobantes</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

	  	<form id="form_import">
		  	<div class="row">
				<div class="col-md-12">
					<div class="alert alert-info" role="alert">
						La primera fila del archivo siempre será tomada como la descripción de las columnas
					</div>
				</div>
				<div class="col-md-12">
					<div class="alert alert-info" role="alert">
						Puede descargar una <strong>plantilla base para cargar comprobantes</strong> haciendo <a href="{{$link_controlador}}descargarTemplateImportacion">Click Aqui</a>
					</div>
				</div>
			</div>
		</form>
        
      </div>
      <div class="modal-footer">
        <button type="button" class='btn btn-success'  style="background-color: #37882e !important;" onclick="subirArchivoImportacion({{\App\ZonaImportacion::IMPORTACION_COMPROBANTES}})">
            <i class='fa fa-file-excel'></i> Cargar Archivo
        </button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="modal_upload_file" data-backdrop="false" data-keyboard="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Subiendo archivo....</h5>
      </div>
      <div class="modal-body">
        <div class="row">
            <div clasS="col-md-12">
            <div class="progress">
                <div id="barra_progreso" class="progress-bar" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">0%</div>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

    @component('backend.template.modal_eliminar')
        @slot('titulo')
            {{$abm_messages["delete"]["title"]}}
        @endslot

        @slot('mensaje')
            {{$abm_messages["delete"]["text"]}}
        @endslot

        @slot('confirmButtonText')
            {{$abm_messages["delete"]["confirmButtonText"]}}
        @endslot

        @slot('cancelButtonText')
            {{$abm_messages["delete"]["cancelButtonText"]}}
        @endslot

    @endcomponent

@endsection

@section("js_code")

<script type="text/javascript">
var listado = null;
var id_trabajando = 0;

var id_zona_a_importar = null;

$(document).ready( function () {

    listado= $('#tabla_listado').DataTable( {
        "processing": true,
        "serverSide": true,
        "responsive":false,
        "ordering": true,
        "order": [[ 0, "desc" ]],
        "ajax":{
        url : "{{$link_controlador}}get_listado_dt", // json datasource
        type: "post",
        data: {
            
        },
        headers:
        {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        error: function(error){
            $(".employee-grid-error").html("");
            $("#employee-grid").append('<tbody class="employee-grid-error"><tr><th colspan="3">No hay datos</th></tr></tbody>');
            $("#employee-grid_processing").css("display","none");
        }
        }
    });

    $("#formulario_subida_archivo_importacion").dropzone({
        autoProcessQueue: true,
        url: "{{ url('/backend/upload_file_excel_importaciones') }}",
        acceptedFiles: '.xlsx',
        paramName: "file",
        uploadMultiple: false,
        chunking: true,
        chunkSize: 10000, // original: 1000000
        addRemoveLinks: true,
        dictRemoveFile : "Eliminar",

        init: function() {

            this.on("sending", function(file, xhr, formData) {

            formData.append("id_zona_importacion", id_zona_a_importar);

            $("#modal_upload_file").modal("show");

            try{
                xhr.onreadystatechange = function() {

                    if (xhr.readyState == XMLHttpRequest.DONE) {

                        var respuesta = JSON.parse(xhr.responseText);

                        if(respuesta["done"] == undefined)
                        {
                            cambiarPorcentajeBarraProgresoUpload(0);
                            $("#modal_upload_file").modal("hide");
                            $("#modal_importar").modal("hide");
                            $("#contenedor_importacion").css("display","block");
                            
                            var file_name =  respuesta["name"];

                            getPorcentajesImportaciones();
                        }
                        else
                        {
                            cambiarPorcentajeBarraProgresoUpload(respuesta["done"]);
                        }
                    }
                }
            }
            catch(e)
            {

            }
        });

        this.on("dictResponseError",function(error){
            cambiarPorcentajeBarraProgresoUpload(0);
            $("#modal_upload_file").modal("hide");
        });

        this.on("removedfile", function (file) {
        });

        },
        success: function(file, response){
            cambiarPorcentajeBarraProgresoUpload(0);
            $("#modal_upload_file").modal("hide");
        },
        error: function(error) {
            cambiarPorcentajeBarraProgresoUpload(0);
            $("#modal_upload_file").modal("hide");
        },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
    });
});

function abrir_modal_importar()
{
    $("#modal_importar").modal("show");
}

function subirArchivoImportacion(id_zona_importacion)
{
    id_zona_a_importar = id_zona_importacion;
    $("#formulario_subida_archivo_importacion").click();
}

function cambiarPorcentajeBarraProgresoUpload(porcentaje)
{
    if(!isNaN(porcentaje))
    {
        porcentaje = porcentaje.toFixed(2);
    }

    if(porcentaje <= 100)
    {
        $("#barra_progreso").attr("aria-valuenow",porcentaje);
        $("#barra_progreso").css("width",porcentaje+"%");
        $("#barra_progreso").text(porcentaje+"%");
    }
}


function abrir_modal_eliminar(id)
{
    id_trabajando = id;
    $("#modal_eliminar").modal("show");
}

function eliminar()
{
    $("#modal_eliminar").modal("hide");

    $.ajax({
        url: "{{$link_controlador}}delete",
        type: "POST",
        data: {id:id_trabajando},
        headers:
        {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function(event){
            abrir_loading();
        },
        success: function(data)
        {
            cerrar_loading();

            try{

                if(data["response"])
                {
                    mostrar_mensajes_success(
                        "{{$abm_messages['delete']['success_text']}}",
                        "{{$abm_messages['delete']['success_description']}}"
                    );

                    listado.draw();
                }
                else
                {
                    mostrar_mensajes_errores(data["messages_errors"]);
                }
                }
                catch(e)
                {
                mostrar_mensajes_errores();
                }
        },
        error: function(error){
            cerrar_loading();
            mostrar_mensajes_errores();
        },
    });
}
</script>

@endsection

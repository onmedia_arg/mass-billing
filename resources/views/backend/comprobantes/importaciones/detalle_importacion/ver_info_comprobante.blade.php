@extends('backend.template.master')

@section('title', $title_page)

@section('contenido')
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">{{$title_page}}</h4>
            </div>
            
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">

                            <div class="card-header">
                                <div class="d-flex align-items-center">
                                    <a class='{{$config_buttons["go_back"]["class"]}}' href="{{ $link_controlador.$comprobante_obj->id }}">
                                        <i class='{{$config_buttons["go_back"]["icon"]}}'></i>
                                        {{$config_buttons["go_back"]["title"]}}
                                    </a>
                                </div>
                            </div>

                            <div class="card-body">

                                <div class="row mt-3">
                                    <div class="col-md-4">
                                        <label for="id_tipo_de_comprobante">id_tipo_de_comprobante</label>
                                        <input type="text" class="form-control" id="id_tipo_de_comprobante" readonly style="background-color: #fff !important;color: #000" value="{{$comprobante_obj->id_tipo_de_comprobante}}">
                                    </div>
                                    
                                    <div class="col-md-4">
                                        <label for="punto_de_venta">punto_de_venta</label>
                                        <input type="text" class="form-control" id="punto_de_venta" readonly style="background-color: #fff !important;color: #000" value="{{$comprobante_obj->punto_de_venta}}">
                                    </div>

                                    @php
                                    $fecha_valid_format = \DateTime::createFromFormat("Y-m-d",$comprobante_obj->fecha);

                                    if(!$fecha_valid_format)
                                    {
                                        $fecha = $comprobante_obj->fecha;
                                    }
                                    else
                                    {
                                        $fecha = $fecha_valid_format->format("d/m/Y");
                                    }
                                    @endphp
                                    <div class="col-md-4">
                                        <label for="fecha">fecha</label>
                                        <input type="text" class="form-control" id="fecha" readonly style="background-color: #fff !important;color: #000" value="{{$fecha}}">
                                    </div>
                                </div>

                                <div class="row mt-3">
                                    <div class="col-md-4">
                                        <label for="id_concepto">id_concepto</label>
                                        <input type="text" class="form-control" id="id_concepto" readonly style="background-color: #fff !important;color: #000" value="{{$comprobante_obj->id_concepto}}">
                                    </div>

                                    <div class="col-md-4">
                                        <label for="id_moneda">id_moneda</label>
                                        <input type="text" class="form-control" id="id_moneda" readonly style="background-color: #fff !important;color: #000" value="{{$comprobante_obj->id_moneda}}">
                                    </div>

                                    <div class="col-md-4">
                                        <label for="cotizacion_de_la_moneda">cotizacion_de_la_moneda</label>
                                        <input type="text" class="form-control" id="cotizacion_de_la_moneda" readonly style="background-color: #fff !important;color: #000" value="{{$comprobante_obj->cotizacion_de_la_moneda}}">
                                    </div>
                                </div>

                                <div class="row mt-3">
                                    <div class="col-md-4">
                                        <label for="cotizacion_del_dolar">cotizacion_del_dolar</label>
                                        <input type="text" class="form-control" id="cotizacion_del_dolar" readonly style="background-color: #fff !important;color: #000" value="{{$comprobante_obj->cotizacion_del_dolar}}">
                                    </div>

                                    @php
                                    $fecha_valid_format = \DateTime::createFromFormat("Y-m-d",$comprobante_obj->periodo_facturado_desde);

                                    if(!$fecha_valid_format)
                                    {
                                        $fecha = $comprobante_obj->periodo_facturado_desde;
                                    }
                                    else
                                    {
                                        $fecha = $fecha_valid_format->format("d/m/Y");
                                    }
                                    @endphp

                                    <div class="col-md-4">
                                        <label for="periodo_facturado_desde">periodo_facturado_desde</label>
                                        <input type="text" class="form-control" id="periodo_facturado_desde" readonly style="background-color: #fff !important;color: #000" value="{{$fecha}}">
                                    </div>
                                    @php
                                    $fecha_valid_format = \DateTime::createFromFormat("Y-m-d",$comprobante_obj->periodo_facturado_hasta);

                                    if(!$fecha_valid_format)
                                    {
                                        $fecha = $comprobante_obj->periodo_facturado_hasta;
                                    }
                                    else
                                    {
                                        $fecha = $fecha_valid_format->format("d/m/Y");
                                    }
                                    @endphp
                                    <div class="col-md-4">
                                        <label for="periodo_facturado_hasta">periodo_facturado_hasta</label>
                                        <input type="text" class="form-control" id="periodo_facturado_hasta" readonly style="background-color: #fff !important;color: #000" value="{{$fecha}}">
                                    </div>
                                
                                </div>

                                <div class="row mt-3">
                                    
                                    @php
                                    $fecha_valid_format = \DateTime::createFromFormat("Y-m-d",$comprobante_obj->vencimiento_del_pago);

                                    if(!$fecha_valid_format)
                                    {
                                        $fecha = $comprobante_obj->vencimiento_del_pago;
                                    }
                                    else
                                    {
                                        $fecha = $fecha_valid_format->format("d/m/Y");
                                    }
                                    @endphp

                                    <div class="col-md-4">
                                        <label for="vencimiento_del_pago">vencimiento_del_pago</label>
                                        <input type="text" class="form-control" id="vencimiento_del_pago" readonly style="background-color: #fff !important;color: #000" value="{{$fecha}}">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="condicion_iva_receptor">condicion_iva_receptor</label>
                                        <input type="text" class="form-control" id="condicion_iva_receptor" readonly style="background-color: #fff !important;color: #000" value="{{$comprobante_obj->condicion_iva_receptor}}">
                                    </div>

                                    <div class="col-md-4">
                                        <label for="num_documento_receptor">num_documento_receptor</label>
                                        <input type="text" class="form-control" id="num_documento_receptor" readonly style="background-color: #fff !important;color: #000" value="{{$comprobante_obj->num_documento_receptor}}">
                                    </div>
                                </div>

                                <div class="row mt-3">
                                    <div class="col-md-4">
                                        <label for="razon_social_receptor">razon_social_receptor</label>
                                        <input type="text" class="form-control" id="razon_social_receptor" readonly style="background-color: #fff !important;color: #000" value="{{$comprobante_obj->razon_social_receptor}}">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="correo_receptor">correo_receptor</label>
                                        <input type="text" class="form-control" id="correo_receptor" readonly style="background-color: #fff !important;color: #000" value="{{$comprobante_obj->correo_receptor}}">
                                    </div>

                                    <div class="col-md-4">
                                        <label for="domicilio_receptor">domicilio_receptor</label>
                                        <input type="text" class="form-control" id="domicilio_receptor" readonly style="background-color: #fff !important;color: #000" value="{{$comprobante_obj->domicilio_receptor}}">
                                    </div>
                                </div>

                                <div class="row mt-3">
                                    <div class="col-md-4">
                                        <label for="id_condicion_venta">id_condicion_venta</label>
                                        <input type="text" class="form-control" id="id_condicion_venta" readonly style="background-color: #fff !important;color: #000" value="{{$comprobante_obj->id_condicion_venta}}">
                                    </div>

                                    <div class="col-md-4">
                                        <label for="ImpTotal">ImpTotal</label>
                                        <input type="text" class="form-control" id="ImpTotal" readonly style="background-color: #fff !important;color: #000" value="{{$comprobante_obj->ImpTotal}}">
                                    </div>

                                    <div class="col-md-4">
                                        <label for="id_externo">id_externo</label>
                                        <input type="text" class="form-control" id="id_externo" readonly style="background-color: #fff !important;color: #000" value="{{$comprobante_obj->id_externo}}">
                                    </div>
                                </div>

                            </div>
                            
                        </div>
                    </div>
                </div>

                <div class="row mt-4">
                    <div class="col-md-12">
                        <h3>Detalle comprobante</h3>
                    </div>

                    <div class="col-md-12 mt-3">

                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>CODIGO</th>
                                    <th>DESCRIPCION</th>
                                    <th>CANTIDAD</th>
                                    <th>UNIDAD MEDIDA</th>
                                    <th>PRECIO UNITARIO</th>
                                    <th>PORCENTAJE BONIFICACION</th>
                                    <th>IMPORTE BONIFICACION</th>
                                    <th>SUBTOTAL</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                $detalles_comprobante = $comprobante_obj->get_detalles_comprobante;

                                    $simbolo_moneda = "$";

                                    if($comprobante_obj->id_moneda == "DOL")
                                    {
                                        $simbolo_moneda = "US$";
                                    }
                                @endphp

                                @foreach($detalles_comprobante as $detalle_comprobante_obj)
                                <tr>
                                    <td>{{$detalle_comprobante_obj->codigo}}</td>
                                    <td>{{$detalle_comprobante_obj->producto_servicio}}</td>
                                    <td>{{$detalle_comprobante_obj->cantidad}}</td>
                                    <td>{{$detalle_comprobante_obj->id_unidad_medida}}</td>
                                    <td>{{$simbolo_moneda}} {{$detalle_comprobante_obj->precio_unitario}}</td>
                                    <td>{{$detalle_comprobante_obj->porcentaje_bonificacion}} %</td>
                                    <td>{{$simbolo_moneda}}  {{$detalle_comprobante_obj->importe_bonificacion}}</td>
                                    <td>{{$simbolo_moneda}} {{$detalle_comprobante_obj->subtotal}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>   
                    </div>
                </div>

                <div class="row mt-4">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h3>Errores</h3>
                            </div>
                            <div class="card-body">
                                    <?php 
                                    $errores = array();

                                    if(trim($comprobante_obj->id_externo) != "")
                                    {
                                        $errores_json = json_decode($comprobante_obj->errores,true);

                                        if(is_array($errores_json))
                                        {
                                            $errores = $errores_json;
                                        } 
                                    }
                                    ?>

                                    <div class="col-md-12">
                                        <div style="color: #f00;">
                                            @foreach($errores as $error_row)
                                            <p>• {{$error_row}}</p>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
		</div>
    </div>
</div>
@endsection



@section("js_code")
@endsection
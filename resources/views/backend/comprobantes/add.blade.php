@extends('backend.template.master')

@section('title', $title_page)

@section('style')
<link rel="stylesheet" href="<?php echo url("/assets/plugins/dropzone/basic.css")?>">
<link rel="stylesheet" href="<?php echo url("/assets/plugins/dropzone/dropzone.css")?>">
@endsection

@section('contenido')
<div class="main-panel">
  <div class="content">
    <div class="page-inner">

      <div class="page-header">
          <h4 class="page-title">{{$title_page}}</h4>
      </div>

      <div class="row">

          <div class="col-md-12">

            <!-- COMIENZO DE CARTA -->
            <div class="card">
              <div class="card-header">
                <div class="d-flex align-items-center">
                  <a class='{{$config_buttons["go_back"]["class"]}}' href="{{ $link_controlador }}">
                      <i class='{{$config_buttons["go_back"]["icon"]}}'></i>
                      {{$config_buttons["go_back"]["title"]}}
                  </a>
                </div>
              </div>
                    
              <!-- COMIENZO CUERPO CARTA -->
              <div class="card-body">

                <form id="formulario_paso" method="post">
                  <div class="row ">

                    <div class="col-md-12">
                      <h2 class="text-center">Puntos de Ventas y Tipos de Comprobantes habilitados para impresión</h2>
                      
                      <div class="row mt-4 justify-content-center">

                        <div class="col-md-3">
                          <label for="punto_de_venta">Punto De Venta: <span class="text-danger">*</span></label>
                          <select name="punto_de_venta" id="punto_de_venta" class="form-control">
                            <option value="">Seleccionar</option>

                            @foreach($puntos_de_venta as $punto_de_venta_row)
                            
                            @if((trim($punto_de_venta_row->FchBaja) == "" || strtolower(trim($punto_de_venta_row->FchBaja)) == "null") && trim(strtolower($punto_de_venta_row->Bloqueado)) == "n" && strpos(strtolower($punto_de_venta_row->EmisionTipo),"cae") !== FALSE)
                              <option value="{{$punto_de_venta_row->numero}}">{{$punto_de_venta_row->descripcion}}</option>
                              @endif

                            @endforeach
                          </select>
                        </div>

                        <div class="col-md-3">
                          <label for="tipo_de_comprobante">Tipo de Comprobante: <span class="text-danger">*</span></label>
                          <select name="tipo_de_comprobante" id="tipo_de_comprobante" class="form-control">
                            <option value="">Seleccionar</option>
                            @foreach($tipos_de_comprobantes as $tipo_de_comprobante)
                              @if($tipo_de_comprobante->Id == 11)
                              <option value="{{$tipo_de_comprobante->Id}}">{{$tipo_de_comprobante->Desc}}</option>
                              @endif
                            @endforeach
                          </select>
                        </div>

                      </div>

                      <div class="row mt-4 justify-content-center">

                        <div class="col-md-12 m-5" style="text-align: center;">
                          <button type="submit" class="btn btn-primary">
                            Siguiente <i class="fa fa-chevron-circle-right"></i>
                          </button>
                        </div>

                      </div>

                    </div>
                  </div>
                </form>
              </div>
              <!-- CIERRE DE CUERPO DE CARTA -->
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- CIERRE DE PAGE INNER -->

                                
@endsection



@section("js_code")

<script src="{{asset('/assets/plugins/dropzone/dropzone.min.js')}}"></script>

<script type="text/javascript" src='https://maps.google.com/maps/api/js?key={{Config("app.key_google_map")}}&sensor=true&libraries=places'></script>

<script src="<?php echo url("assets/plugins/jquery-locationpicker/dist/locationpicker.jquery.min.js")?>"></script>

<script type="text/javascript">

  $("#formulario_paso").submit(function(){
    
    var punto_de_venta = $.trim($("#punto_de_venta").val());
    var tipo_de_comprobante = $.trim($("#tipo_de_comprobante").val());

    $.ajax({
      url: "{{$link_controlador.'nuevo'}}",
      type: "POST",
      data: {
        punto_de_venta:punto_de_venta,
        tipo_de_comprobante:tipo_de_comprobante
      },
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      beforeSend: function(data)
      {
        abrir_loading();
      },
      success: function(data)
      {
        cerrar_loading();

        try
        {
          if(data["response"] == true)
          {
            location.href="{{$link_controlador.'nuevo2'}}";
          }
          else
          {
            mostrar_mensajes_errores(data["messages_errors"]);
          }
        }
        catch(e)
        {
          mostrar_mensajes_errores(e);
        }
      },
      error: function(error)
      {
        mostrar_mensajes_errores();
        cerrar_loading();
      }
    });

    return false;
  });

</script>

@endsection

@extends('backend.template.master')

@section('title', $title_page)

@section('style')
<link rel="stylesheet" href="<?php echo url("/assets/plugins/dropzone/basic.css")?>">
<link rel="stylesheet" href="<?php echo url("/assets/plugins/dropzone/dropzone.css")?>">
@endsection

@section('contenido')
<div class="main-panel">
  <div class="content">
    <div class="page-inner">

      <div class="page-header">
          <h4 class="page-title">{{$title_page}}</h4>
      </div>

      <div class="row">

          @if($comprobante_ayuda_obj->con_ayuda == true)
            @if($id_rol == 1)
            <div class="col-md-12">
              <div class="alert alert-info" role="alert">
                <strong><span class="text-danger">IMPORTANTE:</span> Este comprobante fue realizado solicitando ayuda, al confirmarlo generará el comprobante en AFIP</strong>
              </div>
            </div>
            @else
            <div class="col-md-12">
              <div class="alert alert-info" role="alert">
                <strong><span class="text-danger">IMPORTANTE:</span> Este comprobante se cargará como pendiente para que lo revise un administrador, ya que usted ha seleccionado la ayuda</strong>
              </div>
            </div>
            @endif
          @endif

          

          <div class="col-md-12" style="text-align: right;">
            <button type="button" onclick="ver_dudas({{$comprobante_ayuda_obj->id}})" class="btn btn-primary btn-round">
              <i class="fa fa-eye"></i> Ver dudas/aclaraciones
            </button>
          </div>

          <div class="col-md-12">

            <!-- COMIENZO DE CARTA -->
            <div class="card">
              <div class="card-header">
                <div class="d-flex align-items-center">
                  <a class='{{$config_buttons["go_back"]["class"]}}' href="{{ $link_controlador }}">
                      <i class='{{$config_buttons["go_back"]["icon"]}}'></i>
                      {{$config_buttons["go_back"]["title"]}}
                  </a>
                </div>
              </div>
                    
              <!-- COMIENZO CUERPO CARTA -->
              <div class="card-body">

                <form id="formulario_paso" method="post">

                    <div class="row mt-4 justify-content-center">

                        <div class="col-md-2">
                            <label for="fecha_del_comprobante">Fecha: <span class="text-danger">*</span></label>
                            <input type="text" name="fecha_del_comprobante" id="fecha_del_comprobante" class="form-control datepicker" value="">
                        </div>

                        @if($comprobante_ayuda_obj->id_tipo_de_comprobante == $comprobante_ayuda_obj::$RECIBO_C)
                        <div class="col-md-3">
                            <label for="concepto">Concepto: <span class="text-danger">*</span></label>
                            <select name="concepto" id="concepto" class="form-control">
                            @foreach($conceptos as $concepto)
                              @if($concepto->Id == $comprobante_obj::$CONCEPTO_SERVICIOS)
                              <option value="{{$concepto->Id}}">{{$concepto->Desc}}</option>
                              @endif
                            @endforeach
                            </select>
                        </div>
                        @else
                        <div class="col-md-3">
                            <label for="concepto">Concepto: <span class="text-danger">*</span></label>
                            <select name="concepto" id="concepto" class="form-control">

                            @foreach($conceptos as $concepto)
                            <option value="{{$concepto->Id}}">{{$concepto->Desc}}</option>
                            @endforeach
                            </select>
                        </div>
                        @endif

                        <div class="col-md-3">
                            <label for="moneda">Moneda: <span class="text-danger">*</span></label>
                            <select name="moneda" id="moneda" class="form-control">

                            @foreach($tipos_de_monedas as $tipo_de_moneda)
                                <option value="{{$tipo_de_moneda->Id}}">{{$tipo_de_moneda->Desc}}</option>
                            @endforeach
                            </select>
                        </div>

                        <div class="col-md-3" id="contenedor_selector_cotizacion" style="display: none;">
                            <label for="MonCotiz">Cotización de la moneda: <span class="text-danger">*</span></label>
                            <input type="text" name="MonCotiz" id="MonCotiz" class="form-control">
                        </div>
                    </div>

                    <div class="row mt-4" id="contenedor_periodo_facturado" style="display: none;">

                        <div class="col-md-12" style="margin-top: 15px;">
                          <h2>Periodo facturado</h2>
                        </div>

                          <div class="row">
                            <div class="col-md-2 mt-2">
                                <label for="periodo_facturado_desde">Desde: <span class="text-danger">*</span></label>
                                <input type="text" name="periodo_facturado_desde" id="periodo_facturado_desde" class="form-control datepicker"  value="{{ Date('d/m/Y') }}">
                            </div>

                            <div class="col-md-2 mt-2">
                                <label for="periodo_facturado_hasta">Hasta: <span class="text-danger">*</span></label>
                                <input type="text" name="periodo_facturado_hasta" id="periodo_facturado_hasta" class="form-control datepicker"  value="{{ Date('d/m/Y') }}">
                            </div>

                            <div class="col-md-3 mt-2">
                                <label for="periodo_facturado_vto">Vencimiento del Pago: <span class="text-danger">*</span></label>
                                <input type="text" name="periodo_facturado_vto" id="periodo_facturado_vto" class="form-control datepicker"  value="{{ Date('d/m/Y') }}">
                            </div>
                          </div>

                        </div>

                        
                    </div>

                    <div class="row">
                        <div class="col-md-12 m-5" style="text-align: center;">
                            <a href="{{ url('/backend/comprobantes/nuevo') }}" class="btn btn-primary">
                                <i class="fa fa-chevron-circle-left"></i> Anterior
                            </a>

                            <button type="submit" class="btn btn-primary">
                                Siguiente <i class="fa fa-chevron-circle-right"></i>
                            </button>
                        </div>
                    </div>

                </form>
              </div>
              <!-- CIERRE DE CUERPO DE CARTA -->
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- CIERRE DE PAGE INNER -->

                                
@endsection

@section("modals")

  @component('backend.comprobantes.solicitud_asistencia.modal_ver_dudas')
  @endcomponent

@endsection

@section("js_code")

<script src="{{asset('/assets/plugins/dropzone/dropzone.min.js')}}"></script>

<script type="text/javascript" src='https://maps.google.com/maps/api/js?key={{Config("app.key_google_map")}}&sensor=true&libraries=places'></script>

<script src="<?php echo url("assets/plugins/jquery-locationpicker/dist/locationpicker.jquery.min.js")?>"></script>

<script type="text/javascript">

    $(document).ready(function(){

        $("#fecha_del_comprobante").val("{{$fecha_del_comprobante}}");
        $("#concepto").val("{{$comprobante_ayuda_obj->id_concepto}}").trigger("change");
        $("#moneda").val("{{$comprobante_ayuda_obj->id_moneda}}").trigger("change");

        $("#MonCotiz").val("{{$comprobante_ayuda_obj->cotizacion_de_la_moneda}}");

        $("#periodo_facturado_desde").val("{{$periodo_facturado_desde}}");
        $("#periodo_facturado_hasta").val("{{$periodo_facturado_hasta}}");
        $("#periodo_facturado_vto").val("{{$vencimiento_del_pago}}");

    });
    
    $("#MonCotiz").on("input",function(){

      var cadena = $(this).val();

      if(cadena.substr((cadena.length -1 ),1) != "."){

        cadena = parseFloat(cadena);

        if(cadena == "" || isNaN(cadena))
        {
          cadena = 0;
        }
        
        $(this).val(cadena);
      }


    });

    $("#moneda").change(function(){
        
        var valor_moneda = $.trim($(this).val());

        if(valor_moneda == "" || valor_moneda == "PES")
        {
            $("#contenedor_selector_cotizacion").css("display","none");
        }
        else
        {
          
          $.ajax({
            url: "{{url('/backend/comprobantes/get_cotizacion_moneda')}}",
            type: "POST",
            data: {
              moneda_id:valor_moneda
            },
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend: function(data)
            {
              abrir_loading();
            },
            success: function(data)
            {
              cerrar_loading();

              try
              {
                if(data["response"] == true)
                {
                  $("#MonCotiz").val(data["data"]["cotizacion"].toFixed(2));
                  $("#contenedor_selector_cotizacion").css("display","block");
                }
                else
                {
                  $("#contenedor_selector_cotizacion").css("display","block");
                  $("#MonCotiz").val("0");
                }
              }
              catch(e)
              {
                $("#contenedor_selector_cotizacion").css("display","block");
                $("#MonCotiz").val("0");
              }
            },
            error: function(error)
            {
              cerrar_loading();
              $("#contenedor_selector_cotizacion").css("display","block");
              $("#MonCotiz").val("0");
            }
          });
            
        }
    });

    $("#concepto").change(function(){

        var id_concepto = $(this).val();

        if(id_concepto == 3 || id_concepto == 2)
        {
            $("#contenedor_periodo_facturado").css("display","block");
        }
        else
        {
          $.ajax({
            url: "{{$link_controlador.'/nuevo2'}}",
            type: "POST",
            data: {
              fecha_del_comprobante:fecha_del_comprobante,
              concepto:concepto,
              moneda:moneda,
              MonCotiz:MonCotiz,
              periodo_facturado_desde:periodo_facturado_desde,
              periodo_facturado_hasta:periodo_facturado_hasta,
              periodo_facturado_vto:periodo_facturado_vto
            },
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend: function(data)
            {
              abrir_loading();
            },
            success: function(data)
            {
              cerrar_loading();

              try
              {
                if(data["response"] == true)
                {
                  location.href="<?php echo $link_controlador.'/nuevo3'?>";
                }
                else
                {
                  $("#contenedor_periodo_facturado").css("display","none");
                }
              }
              catch(e)
              {
                $("#contenedor_periodo_facturado").css("display","none");
              }
            },
            error: function(error)
            {
              cerrar_loading();
              $("#contenedor_periodo_facturado").css("display","none");
            }
          });
            
        }
    });

  $("#formulario_paso").submit(function(){
    
    var fecha_del_comprobante = $.trim($("#fecha_del_comprobante").val());
    var concepto = $.trim($("#concepto").val());
    var moneda = $.trim($("#moneda").val());
    var MonCotiz = $.trim($("#MonCotiz").val());
    var periodo_facturado_desde = $.trim($("#periodo_facturado_desde").val());
    var periodo_facturado_hasta = $.trim($("#periodo_facturado_hasta").val());
    var periodo_facturado_vto = $.trim($("#periodo_facturado_vto").val());

    $.ajax({
      url: "{{$link_controlador.'/nuevo2'}}",
      type: "POST",
      data: {
        fecha_del_comprobante:fecha_del_comprobante,
        concepto:concepto,
        moneda:moneda,
        MonCotiz:MonCotiz,
        periodo_facturado_desde:periodo_facturado_desde,
        periodo_facturado_hasta:periodo_facturado_hasta,
        periodo_facturado_vto:periodo_facturado_vto
      },
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      beforeSend: function(data)
      {
        abrir_loading();
      },
      success: function(data)
      {
        cerrar_loading();
        
        try
        {
          if(data["response"] == true)
          {
            location.href="<?php echo $link_controlador.'/nuevo3'?>";
          }
          else
          {
            mostrar_mensajes_errores(data["messages_errors"]);
          }
        }
        catch(e)
        {
          mostrar_mensajes_errores(e);
        }
      },
      error: function(error)
      {
        mostrar_mensajes_errores();
        cerrar_loading();
      }
    });

    return false;
  });

</script>

@endsection

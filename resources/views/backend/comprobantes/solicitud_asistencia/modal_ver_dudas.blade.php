<div class="modal" tabindex="-1" role="dialog" id="modal_ver_dudas">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <h4 class="text-white">Dudas / Aclaraciones</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <label for="">Duda / Aclaración</label>
            <textarea name="" id="area_mostrar_dudas_aclaraciones" class="form-control" rows="10" readonly></textarea>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">
            <i class="fa fa-close"></i> Cerrar
        </button>
      </div>
    </div>
  </div>
</div>

<script>
function ver_dudas(id_comprobante)
  {
    $.ajax({
        url: "{{url('/backend/comprobantes/getDudas')}}",
        type: "POST",
        data: {id:id_comprobante},
        headers:
        {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function(event){
          abrir_loading();
        },
        success: function(data)
        {
            cerrar_loading();

            try
            {
              if(data["response"] == true)
              {
                $("#area_mostrar_dudas_aclaraciones").val(data["data"]);
                $("#modal_ver_dudas").modal("show");
              }
              else
              {
                mostrar_mensajes_errores(data["messages_errors"]);
              }
            }
            catch(e)
            {
              mostrar_mensajes_errores();
            }
        },
        error: function(error){
          cerrar_loading();
          mostrar_mensajes_errores();
        },
    });
  }
</script>
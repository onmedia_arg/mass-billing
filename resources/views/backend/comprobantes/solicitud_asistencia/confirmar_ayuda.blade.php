@extends('backend.template.master')

@section('title', $title_page)

@section('contenido')
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">{{$title_page}}</h4>
            </div>

            <div class="row">
                
                
                <div class="col-md-12">
                    <div class="alert alert-info" role="alert">
                        <strong><span class="text-danger">IMPORTANTE:</span> Este comprobante fue realizado solicitando ayuda, al confirmarlo generará el comprobante en AFIP</strong>
                    </div>
                </div>

                <div class="col-md-12" style="text-align: right;">
                    <button type="button" onclick="ver_dudas({{$comprobante_ayuda_obj->id}})" class="btn btn-primary btn-round">
                    <i class="fa fa-eye"></i> Ver dudas/aclaraciones
                    </button>
                </div>

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            
                            <div class="row">

                                <div class="col-md-12">
                                    <h3 class="text-center">Por favor controle los datos ingresados:</h3>
                                </div>

                                <div class="col-md-12">
                                    <h2>Datos generales</h2>
                                    <p><strong>Fecha:</strong> {{$fecha_del_comprobante}}</p>
                                    <p><strong>Punto de Venta:</strong>	{{$comprobante_ayuda_obj->punto_de_venta}}</p>
                                    <p><strong>Domicilio:</strong> {{session("domicilio_comercial")}}</p>
                                    <p><strong>Conceptos a Incluír:</strong>	
                                    <?php
                                    if($concepto_obj)
                                    {
                                        echo e($concepto_obj->Desc);
                                    }
                                    ?>
                                    </p>
                                    @if($comprobante_ayuda_obj->id_concepto == 2 || $comprobante_ayuda_obj->id_concepto == 3)
                                    <p><strong>Período Facturado:</strong>	desde: {{$periodo_facturado_desde}}   hasta: {{$periodo_facturado_hasta}}</p>
                                    <p><strong>Vto. para el Pago:</strong>	{{$periodo_facturado_vto}}</p>
                                    @endif
                                    <p><strong>Moneda:</strong> {{$comprobante_ayuda_obj->id_moneda}}</p>
                                    @if($comprobante_ayuda_obj->id_moneda != "PES")
                                    <p><strong>Cotización Moneda:</strong> {{ $comprobante_ayuda_obj->cotizacion_de_la_moneda }}</p>
                                    @endif
                                </div>
                            </div>
                            
                            <div class="row  mt-3">
                                <div class="col-md-12">
                                    <h2>Datos del Receptor</h2>
                                    <p><strong>
                                    <?php
                                    for($i=0; $i < count($tipos_de_documentos);$i++)
                                    {
                                        if($comprobante_ayuda_obj->tipo_documento_receptor == $tipos_de_documentos[$i]->Id )
                                        {
                                            echo e($tipos_de_documentos[$i]->Desc).":";
                                        }
                                    }
                                    ?>
                                    </strong>	{{$comprobante_ayuda_obj->num_documento_receptor}}</p>
                                    <p><strong>Razón Social:</strong>	{{$comprobante_ayuda_obj->razon_social_receptor}}</p>
                                    <p><strong>Domicilio Comercial:</strong>	{{$comprobante_ayuda_obj->domicilio_receptor}}</p>
                                    <p><strong>Email:</strong> {{$comprobante_ayuda_obj->correo_receptor}}</p>	
                                    <p><strong>Condición frente al IVA:</strong>
                                    @if($condicion_iva_obj)

                                        {{$condicion_iva_obj->descripcion}}

                                    @endif
                                    </p>
                                    <p><strong>Condiciones de Venta:</strong>
                                    @if($condicion_venta_obj)
                                        {{$condicion_venta_obj->descripcion}}
                                    @endif
                                    </p>
                                </div>
                            </div>
                            
                            <?php
                            //$subtotal =  0;
                            ?>

                            @if($comprobante_ayuda_obj->id_tipo_de_comprobante == 11 || $comprobante_ayuda_obj->id_tipo_de_comprobante == 12 || $comprobante_ayuda_obj->id_tipo_de_comprobante == 13)
                            

                            <div class="row  mt-3">
                                <div class="col-md-12">
                                    <h2>Detalle de la Operación</h2>
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Código</th>
                                                <th>Producto/Servicio</th>
                                                <th>Cant.</th>
                                                <th>U. Medida</th>
                                                <th>Prec. Unitario</th>
                                                <th>% Bon.</th>
                                                <th>Importe Bon.</th>
                                                <th>Subtotal</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($detalle_comprobante as $detalle_comprobante_row)
                                            <tr>
                                                <td>{{$detalle_comprobante_row["codigo"]}}</td>
                                                <td>{{$detalle_comprobante_row["producto_servicio"]}}</td>
                                                <td>{{$detalle_comprobante_row["cantidad"]}}</td>
                                                <td>{{$detalle_comprobante_row["nombre_unidad"]}}</td>
                                                <td>$ {{$detalle_comprobante_row["precio_unitario"]}}</td>
                                                <td>{{ (float)$detalle_comprobante_row["porcentaje_bonificacion"]}} %</td>
                                                <td>$ {{$detalle_comprobante_row["importe_bonificacion"]}}</td>
                                                <td>$ {{$detalle_comprobante_row["subtotal"]}}</td>

                                                <?php
                                                $total_del_row = $detalle_comprobante_row["cantidad"] * $detalle_comprobante_row["precio_unitario"];

                                                $detalle_comprobante_row["porcentaje_bonificacion"] = (float) $detalle_comprobante_row["porcentaje_bonificacion"];

                                                $importe_bonif = ($total_del_row  * $detalle_comprobante_row["porcentaje_bonificacion"]) / 100;

                                                //$subtotal += ($total_del_row - $importe_bonif);

                                                ?>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            @endif

                            @if($comprobante_ayuda_obj->id_tipo_de_comprobante == 15)

                            

                            <div class="row  mt-3">
                                <div class="col-md-12">
                                    <h2>Descripción del servicio</h2>
                                    <div>
                                    {{$comprobante_ayuda_obj->descripcion_servicio}}
                                    </div>
                                </div>
                            </div>

                            @endif

                            @if($comprobante_ayuda_obj->id_tipo_de_comprobante == 11 || $comprobante_ayuda_obj->id_tipo_de_comprobante == 12 || $comprobante_ayuda_obj->id_tipo_de_comprobante == 13 || $comprobante_ayuda_obj->id_tipo_de_comprobante == 15)
                            
                                @if(count($detalle_tributos) > 0)
                                <div class="row  mt-3">
                                    <div class="col-md-12">
                                        <h2>Otros Tributos:</h2>
                                    </div>

                                    <div class="col-md-12">	 			
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Descripción</th>
                                                    <th>Detalle</th>
                                                    <th>Base Imponible.</th>
                                                    <th>Alícuota %</th>
                                                    <th>Importe</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($detalle_tributos as $detalle_tributos_row)
                                                <tr>
                                                    <td>{{$detalle_tributos_row["descripcion"]}}</td>
                                                    <td>{{$detalle_tributos_row["detalle"]}}</td>
                                                    <td>$ {{$detalle_tributos_row["base_imponible"]}}</td>
                                                    <td>{{$detalle_tributos_row["alicuota"]}} %</td>
                                                    <td>$ {{$detalle_tributos_row["importe"]}}</td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                @endif
                            @endif

                            <div class="row mt-3">

                                <div class="col-md-12" style="text-align: right;">
                                    <p><strong>Subtotal</strong>: $	{{$comprobante_ayuda_obj->ImpNeto}}</p>
                                    <p><strong>Importe Otros Tributos</strong>: $ {{$comprobante_ayuda_obj->ImpTrib}}</p>
                                    <p><strong>Importe Total</strong>: $	{{ $comprobante_ayuda_obj->ImpTotal }}</p>
                                </div>

                            </div>

                            
                            @if($comprobante_ayuda_obj->id_tipo_de_comprobante == 11 || $comprobante_ayuda_obj->id_tipo_de_comprobante == 12 || $comprobante_ayuda_obj->id_tipo_de_comprobante == 13)
                                @if(count($comprobantes_asociados) > 0)
                                <div class="row mt-3">
                                    <div class="col-md-12">
                                        <h2>Comprobantes Asociados:</h2>
                                    </div>

                                    <div class="col-md-12">	 			
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                <th>Tipo de Comprobante</th>
                                                <th>Punto de venta</th>
                                                <th>Comprobante</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($comprobantes_asociados as $comprobantes_asociados_row)
                                                <tr>
                                                    <td>{{$comprobantes_asociados_row["tipo_de_comprobante"]}}</td>
                                                    <td>{{$comprobantes_asociados_row["id_punto_de_venta"]}}</td>
                                                    <td>{{$comprobantes_asociados_row["comprobante"]}}</td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                @endif
                            @endif

                            <div class="row mt-5" style="text-align: center;">
                                <div class="col-md-12 mt-2" >
                                    <button type="button" class="btn btn-success" onclick="confirmar()">
                                        <i class="fa fa-check"></i> Confirmar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>
    </div>
</div>
@endsection


@section("modals")
    @component('backend.comprobantes.solicitud_asistencia.modal_ver_dudas')
    @endcomponent
@endsection

@section("js_code")

<script type="text/javascript">

function confirmar()
{
    $.ajax({
      url: "{{$link_controlador.'/confirmar_nuevo'}}",
      type: "POST",
      data: {},
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      beforeSend: function(data)
      {
        abrir_loading();
      },
      success: function(data)
      {
        cerrar_loading();

        try
        {
            if(data["response"] == true)
            {
                mostrar_mensajes_success("{{$tipo_de_comprobante_obj->Desc}} realizada/o!","Se ha realizado correctamente el comprobante: {{$tipo_de_comprobante_obj->Desc}}!","{{url('/backend/comprobantes')}}");
            }
            else
            {
                mostrar_mensajes_errores(data["messages_errors"]);
            }
        }
        catch(e)
        {
          mostrar_mensajes_errores([]);
        }
      },
      error: function(error)
      {
        mostrar_mensajes_errores();
        cerrar_loading();
      }
    });
}

</script>

@endsection

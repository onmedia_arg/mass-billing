@extends('backend.template.master')

@section('title', $title_page)

@section('style')
<link rel="stylesheet" href="<?php echo url("/assets/plugins/dropzone/basic.css")?>">
<link rel="stylesheet" href="<?php echo url("/assets/plugins/dropzone/dropzone.css")?>">
@endsection

@section('contenido')
<div class="main-panel">
  <div class="content">
    <div class="page-inner">

      <div class="page-header">
          <h4 class="page-title">{{$title_page}}</h4>
      </div>

      <div class="row">
      
        <div class="col-md-12">
            <div class="alert alert-info" role="alert">
              <strong><span class="text-danger">IMPORTANTE:</span> Este comprobante fue realizado solicitando ayuda, al confirmarlo generará el comprobante en AFIP</strong>
            </div>
        </div>

          <div class="col-md-12">

            <!-- COMIENZO DE CARTA -->
            <div class="card">
              <div class="card-header">
                <div class="d-flex align-items-center">
                  <a class='{{$config_buttons["go_back"]["class"]}}' href="{{ $link_controlador }}">
                      <i class='{{$config_buttons["go_back"]["icon"]}}'></i>
                      {{$config_buttons["go_back"]["title"]}}
                  </a>
                </div>
              </div>
                    
              <!-- COMIENZO CUERPO CARTA -->
              <div class="card-body">

                <form id="formulario_paso" method="post">

                    <div class="row mt-4">

                        <div class="col-md-12" style="text-align: right;">
                            <button type="button" onclick="ver_dudas({{$comprobante_ayuda_obj->id}})" class="btn btn-primary btn-round">
                            <i class="fa fa-eye"></i> Ver dudas/aclaraciones
                            </button>
                        </div>

                        <div class="col-md-12">
                            <h2>Datos del receptor</h2>
                        </div>

                        <div class="col-md-5 mt-2">
                            <label for="condicion_frente_al_iva">Condición frente al Iva: <span class="text-danger">*</span></label>
                            <select name="condicion_frente_al_iva" id="condicion_frente_al_iva" class="form-control">
                                @foreach($condiciones_frente_al_iva as $condicion_frente_al_iva_row)
                                <option value='{{$condicion_frente_al_iva_row->id}}'>{{$condicion_frente_al_iva_row->descripcion}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-md-6 mt-2">

                            <div class="row">

                                <div class="col-md-6">
                                    <label for="tipo_y_numero_doc">Tipo y número de documento: <span class="text-danger">*</span></label>
                                    <select class="form-control" name="idIVAReceptor" id="idivareceptor">
                                        
                                        @foreach($tipos_de_documentos_unicos as $tipo_documento_unico_row_obj)
                                            <option value="{{$tipo_documento_unico_row_obj->id}}">{{$tipo_documento_unico_row_obj->descripcion}}</option>
                                        @endforeach

                                        
                                    </select>
                                </div>

                                <div class="col-md-6">
                                    <label for="nrodocreceptor">Número: <span class="text-danger">*</span></label>
                                    <input type="text" name="nroDocReceptor" id="nrodocreceptor" class="form-control">
                                </div>

                            </div>
                        </div>

                        <div class="col-md-4 mt-2">
                            <label for="razon_social">Razon Social: <span class="text-danger">*</span></label>
                            <input type="text" name="razon_social" id="razon_social" class="form-control">
                        </div>

                        <div class="col-md-4 mt-2">
                            <label for="domicilio_comercial">Domicilio Comercial: <span class="text-danger">*</span></label>
                            <input type="text" name="domicilio_comercial" id="domicilio_comercial" class="form-control">
                        </div>

                        <div class="col-md-4 mt-2">
                            <label for="correo">Correo: </label>
                            <input type="text" name="correo" id="correo" class="form-control">
                        </div>
                    </div>

                    <div class="row mt-4">

                        <div class="col-md-3">
                            <label for="id_condicion_de_venta">Condición de Venta   <span class="text-danger">*</span></label>

                            <select name="id_condicion_de_venta" id="id_condicion_de_venta" class="form-control">
                                <option value=""></option>
                                @foreach($condiciones_de_venta as $condicion_de_venta_row)
                                    <option value="{{$condicion_de_venta_row['id']}}">{{$condicion_de_venta_row['descripcion']}}</option>
                                @endforeach
                            </select>
                        </div>


                        @if(session('id_condicion_iva') == 1)

                        <div class="col-md-12">
                            <h2>Detalle</h2>
                        </div>

                        <div class="col-md-3">
                            <label for="id_tipo_de_iva">Tipo de Iva</label>
                            <select name="id_tipo_de_iva" id="id_tipo_de_iva" class="form-control">
                                <option value=""></option>
                                @foreach($tipos_de_iva as $tipo_de_iva)
                                    <option value="{{$tipo_de_iva->Id}}">{{$tipo_de_iva->Desc}}</option>
                                @endforeach
                            </select>   
                        </div>

                        <div class="col-md-3">
                            <label for="ImpTotConc">Importe neto no grabado</label>
                            <input type="text" name="ImpTotConc" id="ImpTotConc" class="form-control">   
                        </div>
                        
                        <div class="col-md-3">
                            <label for="ImpNeto">Importe neto grabado</label>
                            <input type="text" name="ImpNeto" id="ImpNeto" class="form-control">   
                        </div>
                        
                        <div class="col-md-3">
                            <label for="ImpOpEx">Importe exento de IVA</label>
                            <input type="text" name="ImpOpEx" id="ImpOpEx" class="form-control">   
                        </div>
                        
                        <div class="col-md-3">
                            <label for="ImpIVA">Importe total de IVA</label>
                            <input type="text" name="ImpIVA" id="ImpIVA" class="form-control">   
                        </div>
                        
                        <div class="col-md-3">
                            <label for="ImpTrib">Importe total de tributos</label>
                            <input type="text" name="ImpTrib" id="ImpTrib" class="form-control">   
                        </div>
                        @endif
                    </div>

                    
                    @if($comprobante_ayuda_obj->comprobanteConDetalle())
                    <div class="row mt-4">
                        <div class="col-md-12">
                            <h2>Detalle</h2>
                        </div>
                        <div class="col-md-12">
                            <button type="button" class="btn btn-sm btn-primary" onclick="abrir_modal_agregar_detalle()">
                                <i class="fa fa-plus"></i>
                            </button>
                        </div>

                        <div class="col-md-12 mt-2">     
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Código</th>
                                        <th>Producto/Servicio</th>
                                        <th>Cant</th>
                                        <th>U. Medida</th>
                                        <th>Prec. <br> Unitario</th>
                                        <th>% <br> Bon.</th>
                                        <th>Importe <br> Bon.</th>
                                        <th>Subtotal</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody id="cuerpo_detalle_comprobante">
                                </tbody>
                            </table>
                        </div>
                    </div>
                    @endif
                    

                    <div class="row mt-4" style="display: <?php if($comprobante_ayuda_obj->id_tipo_de_comprobante == $comprobante_ayuda_obj::$RECIBO_C){ echo "flex"; }else{ echo "none"; } ?>">
                        <div class="col-md-12">
                            <h2>Descripción del servicio</h2>
                        </div>
                        <div class="col-md-8">
                            <textarea id="descripcion_del_servicio" name="descripcion_del_servicio" class="form-control" rows="7"></textarea>
                        </div>
                        <div class="col-md-4">
                            <label for="precio_servicio">Precio</label>
                            <input type="text" class="form-control" id="precio_servicio" name="precio_servicio">
                        </div>
                    </div>

                    @if($comprobante_ayuda_obj->comprobanteConTributos())
                    <div class="row mt-4">
                        <div class="col-md-12">
                            <h2>Otros Tributos</h2>
                        </div>
                        <div class="col-md-12">
                            <button type="button" class="btn btn-sm btn-primary" onclick="abrir_modal_agregar_tributo()">
                                <i class="fa fa-plus"></i>
                            </button>
                        </div>

                        <div class="col-md-12 mt-2">     
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Tipo</th>
                                        <th>Descripción</th>
                                        <th>Detalle</th>
                                        <th>Base Imponible</th>
                                        <th>Alicuota <br> %</th>
                                        <th>Importe</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody id="cuerpo_detalle_tributos">
                                </tbody>
                            </table>
                        </div>
                    </div>
                    @endif

                    <div class="row mt-4">
                        <div class="col-md-12" style="text-align: right;">
                            <p><span style="font-weight: bold;font-size: 15px;">Subtotal: $</span> <span style="font-size: 22px;" id="subtotal">0</span></p>
                            @if($comprobante_ayuda_obj->comprobanteConTributos())
                            <p><span style="font-weight: bold;font-size: 15px;">Total Tributos: $</span> <span style="font-size: 22px;" id="total_tributos">0</span></p>
                            @endif
                            <p><span style="font-weight: bold;font-size: 15px;">Total: $</span> <span style="font-size: 22px;" id="total">0</span></p>
                        </div>
                    </div>
                    
                    @if($comprobante_ayuda_obj->comprobanteConComprobantesAsociados())
                    <div class="row mt-4">
                        <div class="col-md-12">
                            <h2>Comprobantes Asociados</h2>
                        </div>
                        <div class="col-md-12">
                            <button type="button" class="btn btn-sm btn-primary" onclick="abrir_modal_agregar_comprobante_asociado()">
                                <i class="fa fa-plus"></i>
                            </button>
                        </div>

                        <div class="col-md-12 mt-2">     
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Tipo de Comprobante</th>
                                        <th>Punto de venta</th>
                                        <th>Comprobante</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody id="cuerpo_detalle_comprobantes_asociados">
                                </tbody>
                            </table>
                        </div>
                    </div>
                    @endif

                    <div class="row">
                        <div class="col-md-12 m-5" style="text-align: center;">
                            <a href="{{ url('/backend/comprobantes/nuevo') }}" class="btn btn-primary">
                                <i class="fa fa-chevron-circle-left"></i> Anterior
                            </a>

                            <button type="submit" class="btn btn-primary">
                                Siguiente <i class="fa fa-chevron-circle-right"></i>
                            </button>
                        </div>
                    </div>

                    
                    <!-- FIN TEST -->

                </form>
              </div>
              <!-- CIERRE DE CUERPO DE CARTA -->
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- CIERRE DE PAGE INNER -->

                                
@endsection

@section("modals")

@if($comprobante_ayuda_obj->comprobanteConDetalle())
<div class="modal" tabindex="-1" role="dialog" id="modal_agregar_detalle">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Agregar detalle</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="formulario_agregar_detalle" action="">

            <div class="row">
                <div class="col-md-6">
                    <label for="codigo_agregar_detalle">Código</label>
                    <input type="text" class="form-control" name="codigo" id="codigo_agregar_detalle" >
                </div>
                <div class="col-md-6">
                    <label for="producto_servicio_agregar_detalle">Producto/servicio</label>
                    <input type="text" class="form-control" name="producto_servicio" id="producto_servicio_agregar_detalle" >
                </div>
                <div class="col-md-6">
                    <label for="cantidad_agregar_detalle">Cantidad</label>
                    <input type="text" class="form-control" name="cantidad" id="cantidad_agregar_detalle" >
                </div>
                
                <div class="col-md-6">
                    <label for="id_unidad_medida_agregar_detalle">Unidad de medida</label>
                    <select class="form-control" name="id_unidad_medida" id="id_unidad_medida_agregar_detalle" >
                        <option value=""></option>

                        @foreach($unidades_de_medida as $unidad_de_medida)
                            <option value="{{$unidad_de_medida->id}}">{{$unidad_de_medida->descripcion}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-6">
                    <label for="precio_unitario_agregar_detalle">Precio Unitario</label>
                    <input type="text" class="form-control" name="precio_unitario" id="precio_unitario_agregar_detalle" >
                </div>
                <div class="col-md-6">
                    <label for="porcentaje_bonificacion_agregar_detalle">% Bonificación</label>
                    <input type="text" class="form-control" name="porcentaje_bonificacion" id="porcentaje_bonificacion_agregar_detalle" >
                </div>
                <div class="col-md-6">
                    <label for="importe_bonificacion_agregar_detalle">Importe Bonificación</label>
                    <input type="text" class="form-control" name="importe_bonificacion" id="importe_bonificacion_agregar_detalle">
                </div>
                
                <div class="col-md-6">
                    <label for="subtotal_agregar_detalle">Subtotal</label>
                    <input type="text" class="form-control" name="subtotal" id="subtotal_agregar_detalle" readonly="true" style="background-color: #fff !important;">
                </div>

            </div>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" onclick="agregar_detalle()">Agregar</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="modal_editar_detalle">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Editar detalle</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="formulario_editar_detalle" action="">

            <div class="row">
                <div class="col-md-6">
                    <label for="codigo_editar_detalle">Código</label>
                    <input type="text" class="form-control" name="codigo" id="codigo_editar_detalle" >
                </div>
                <div class="col-md-6">
                    <label for="producto_servicio_editar_detalle">Producto/servicio</label>
                    <input type="text" class="form-control" name="producto_servicio" id="producto_servicio_editar_detalle" >
                </div>
                <div class="col-md-6">
                    <label for="cantidad_editar_detalle">Cantidad</label>
                    <input type="text" class="form-control" name="cantidad" id="cantidad_editar_detalle" >
                </div>
                
                <div class="col-md-6">
                    <label for="id_unidad_medida_editar_detalle">Unidad de medida</label>
                    <select class="form-control" name="id_unidad_medida" id="id_unidad_medida_editar_detalle" >
                        <option value=""></option>

                        @foreach($unidades_de_medida as $unidad_de_medida)
                            <option value="{{$unidad_de_medida->id}}">{{$unidad_de_medida->descripcion}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-6">
                    <label for="precio_unitario_editar_detalle">Precio Unitario</label>
                    <input type="text" class="form-control" name="precio_unitario" id="precio_unitario_editar_detalle" >
                </div>
                <div class="col-md-6">
                    <label for="porcentaje_bonificacion_editar_detalle">% Bonificación</label>
                    <input type="text" class="form-control" name="porcentaje_bonificacion" id="porcentaje_bonificacion_editar_detalle" >
                </div>
                <div class="col-md-6">
                    <label for="importe_bonificacion_editar_detalle">Importe Bonificación</label>
                    <input type="text" class="form-control" name="importe_bonificacion" id="importe_bonificacion_editar_detalle">
                </div>
                
                <div class="col-md-6">
                    <label for="subtotal_editar_detalle">Subtotal</label>
                    <input type="text" class="form-control" name="subtotal" id="subtotal_editar_detalle"  readonly="true"  style="background-color: #fff;" >
                </div>

            </div>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" onclick="editar_detalle()">Guardar</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>

@endif

@if($comprobante_ayuda_obj->comprobanteConTributos())
<div class="modal" tabindex="-1" role="dialog" id="modal_agregar_tributo">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Agregar tributo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="formulario_agregar_tributo" action="">

            <div class="row">

                <div class="col-md-12">
                    <label for="id_tipo_agregar_tributo">Tipo de Tributo</label>
                    <select name="id_tipo" id="id_tipo_agregar_tributo" class="form-control">
                        <option value="">Seleccionar</option>

                        @foreach($tipos_de_tributos as $tipo_tributo_row)
                        <option value="{{$tipo_tributo_row->Id}}">{{$tipo_tributo_row->Desc}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-md-6">
                    <label for="descripcion_agregar_tributo">Descripción</label>
                    <input type="text" class="form-control" name="descripcion" id="descripcion_agregar_tributo" >
                </div>

                <div class="col-md-6">
                    <label for="detalle_agregar_tributo">Detalle</label>
                    <input type="text" class="form-control" name="detalle" id="detalle_agregar_tributo" >
                </div>

                <div class="col-md-6">
                    <label for="base_imponible_agregar_tributo">Base Imponible</label>
                    <input type="text" class="form-control" name="base_imponible" id="base_imponible_agregar_tributo" >
                </div>

                <div class="col-md-6">
                    <label for="alicuota_agregar_tributo">alicuota</label>
                    <input type="text" class="form-control" name="alicuota" id="alicuota_agregar_tributo" >
                </div>

                <div class="col-md-6">
                    <label for="importe_agregar_tributo">Importe</label>
                    <input type="text" class="form-control" name="importe" id="importe_agregar_tributo" readonly="true" style="background-color: #fff">
                </div>
                
            </div>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" onclick="agregar_tributo()">Agregar</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="modal_editar_tributo">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Editar tributo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="formulario_editar_tributo" action="">

            <div class="row">

                <div class="col-md-12">
                    <label for="id_tipo_editar_tributo">Tipo de Tributo</label>
                    <select name="id_tipo" id="id_tipo_editar_tributo" class="form-control">
                        @foreach($tipos_de_tributos as $tipo_tributo_row)
                        <option value="{{$tipo_tributo_row->Id}}">{{$tipo_tributo_row->Desc}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-md-6">
                    <label for="descripcion_editar_tributo">Descripción</label>
                    <input type="text" class="form-control" name="descripcion" id="descripcion_editar_tributo" >
                </div>

                <div class="col-md-6">
                    <label for="detalle_editar_tributo">Detalle</label>
                    <input type="text" class="form-control" name="detalle" id="detalle_editar_tributo" >
                </div>

                <div class="col-md-6">
                    <label for="base_imponible_editar_tributo">Base Imponible</label>
                    <input type="text" class="form-control" name="base_imponible" id="base_imponible_editar_tributo" >
                </div>

                <div class="col-md-6">
                    <label for="alicuota_editar_tributo">alicuota</label>
                    <input type="text" class="form-control" name="alicuota" id="alicuota_editar_tributo" >
                </div>

                <div class="col-md-6">
                    <label for="importe_editar_tributo">Importe</label>
                    <input type="text" class="form-control" name="importe" id="importe_editar_tributo" readonly="true" style="background-color: #fff">
                </div>
                
            </div>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" onclick="editar_tributo()">Guardar</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>
@endif


@if($comprobante_ayuda_obj->comprobanteConComprobantesAsociados())
<div class="modal" tabindex="-1" role="dialog" id="modal_agregar_comprobante_asociado">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Agregar comprobante asociado</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="formulario_agregar_comprobante_asociado" action="">

            <div class="row">

                <div class="col-md-12">
                    <label for="id_tipo_de_comprobante_agregar_ca">Tipo de comprobante <span class="text-danger">*</span></label>
                    <select name="id_tipo_de_comprobante" id="id_tipo_de_comprobante_agregar_ca" class="form-control">
                        <option value="">Seleccionar</option>
                        <?php 
                        // comprobante C
                        if($comprobante_ayuda_obj->id_tipo_de_comprobante == 11)
                        {
                            echo '<option value="91">Remito</option>';
                        }
                        // NOTA DE DÉBITO C o NOTA DE CRÉDITO C
                        else if($comprobante_ayuda_obj->id_tipo_de_comprobante == 12 || $comprobante_ayuda_obj->id_tipo_de_comprobante == 13)
                        {
                            echo 
                            '<option value="91">Remito</option>
                            <option value="11">Factura C</option>
                            <option value="12">Nota de Débito C</option>
                            <option value="13">Nota de Crédito C</option>
                            <option value="15">Recibo C</option>';
                        }
                        ?>
                    </select>
                </div>

                <div class="col-md-12  mt-3">
                    <label for="id_punto_de_venta_agregar_ca">Punto de venta <span class="text-danger">*</span></label>
                    <select name="id_punto_de_venta" id="id_punto_de_venta_agregar_ca" class="form-control">
                        <option value="">Seleccionar</option>

                        @foreach($puntos_de_venta as $punto_de_venta_row)
                        <option value="{{$punto_de_venta_row['Nro']}}">Punto de venta #{{$punto_de_venta_row['Nro']}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-md-12 mt-3">
                    <label for="comprobante_agregar_ca">Comprobante <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="comprobante" id="comprobante_agregar_ca" >
                </div>
                
            </div>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" onclick="agregar_comprobante_asociado()">Agregar</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="modal_editar_comprobante_asociado">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Editar comprobante asociado</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="formulario_editar_comprobante_asociado" action="">

            <div class="row">

                <div class="col-md-12">
                    <label for="id_tipo_de_comprobante_editar_ca">Tipo de comprobante <span class="text-danger">*</span></label>
                    <select name="id_tipo_de_comprobante" id="id_tipo_de_comprobante_editar_ca" class="form-control">
                        <option value="">Seleccionar</option>

                        <?php 
                        // comprobante C
                        if($comprobante_ayuda_obj->id_tipo_de_comprobante == 11)
                        {
                            echo '<option value="91">Remito</option>';
                        }
                        // NOTA DE DÉBITO C
                        else if($comprobante_ayuda_obj->id_tipo_de_comprobante == 12)
                        {
                            echo 
                            '<option value="91">Remito</option>
                            <option value="11">comprobante C</option>
                            <option value="12">Nota de Débito C</option>
                            <option value="13">Nota de Crédito C</option>
                            <option value="15">Recibo C</option>';
                        }
                        ?>
                    </select>
                </div>

                <div class="col-md-12  mt-3">
                    <label for="id_punto_de_venta_editar_ca">Punto de venta <span class="text-danger">*</span></label>
                    <select name="id_punto_de_venta" id="id_punto_de_venta_editar_ca" class="form-control">
                        <option value="">Seleccionar</option>

                        @foreach($puntos_de_venta as $punto_de_venta_row)
                        <option value="{{$punto_de_venta_row['Nro']}}">Punto de venta #{{$punto_de_venta_row['Nro']}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-md-12 mt-3">
                    <label for="comprobante_editar_ca">Comprobante <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="comprobante" id="comprobante_editar_ca" >
                </div>
                
            </div>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" onclick="editar_comprobante_asociado()">Guardar</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>
@endif

  @component('backend.comprobantes.solicitud_asistencia.modal_ver_dudas')
  @endcomponent

@endsection

@section("js_code")


<script type="text/javascript">

    var posicion_trabajando = null;
    var detalle_comprobante = new Array();
    var detalle_tributos = new Array();

    var comprobantes_asociados = new Array();

    var ID_FACTURA_C = {{$comprobante_ayuda_obj::$FACTURA_C}};
    var ID_NOTA_DEBITO_C = {{$comprobante_ayuda_obj::$NOTA_DE_DEBITO_C}};
    var ID_NOTA_CREDITO_c = {{$comprobante_ayuda_obj::$NOTA_DE_CREDITO_C}};
    var ID_RECIBO_C = {{$comprobante_ayuda_obj::$RECIBO_C}};

    var ID_TIPO_DE_COMPROBANTE = {{$comprobante_ayuda_obj->id_tipo_de_comprobante}};

    $(document).ready(function(){

        // DE AYUDA

        $("#condicion_frente_al_iva").val("{{$comprobante_ayuda_obj->condicion_iva_receptor}}");
        $("#idivareceptor").val("{{$comprobante_ayuda_obj->tipo_documento_receptor}}");
        $("#nrodocreceptor").val("{{$comprobante_ayuda_obj->num_documento_receptor}}");
        $("#razon_social").val("{{$comprobante_ayuda_obj->razon_social_receptor}}");
        $("#domicilio_comercial").val("{{$comprobante_ayuda_obj->domicilio_receptor}}");
        $("#correo").val("{{$comprobante_ayuda_obj->correo_receptor}}");
        $("#id_condicion_de_venta").val("{{$comprobante_ayuda_obj->id_condicion_venta}}");

        // AGREGANDO DETALLE CARGADO

        <?php

        foreach($comprobante_ayuda_obj->get_detalles_comprobante as $detalle_comprobante)
        {
            $nombre_unidad = $detalle_comprobante->get_unidad_de_medida;

            if($nombre_unidad)
            {
                $nombre_unidad = $nombre_unidad->descripcion;
            }
            else
            {
                $nombre_unidad = "";
            }

            echo 'var nombre_unidad = "'.addslashes($nombre_unidad).'";';

            echo 'var row = {
                codigo: "'.addslashes($detalle_comprobante->codigo).'",
                producto_servicio:"'.addslashes($detalle_comprobante->producto_servicio).'",
                cantidad:"'.((double) $detalle_comprobante->cantidad).'",
                id_unidad_medida:"'.addslashes($detalle_comprobante->id_unidad_medida).'",
                nombre_unidad:nombre_unidad,
                precio_unitario:"'.((double)$detalle_comprobante->precio_unitario).'",
                porcentaje_bonificacion:"'.($detalle_comprobante->porcentaje_bonificacion).'",
                importe_bonificacion:"'.((double)$detalle_comprobante->importe_bonificacion).'",
                subtotal:"'.((double)$detalle_comprobante->subtotal).'"
            };

            detalle_comprobante.push(row);
            actualizar_tabla_detalles();
            actualizar_totales();';
        }

        ?>

        // FIN AGREGANDO DETALLE CARGADO

        // AGREGANDO TRIBUTOS SI LOS HAY
        <?php

        foreach($comprobante_ayuda_obj->get_tributos_comprobantes as $tributo_comprobante_obj)
        {
            $nombre_tipo = $tributo_comprobante_obj->get_tipo_tributo;

            if($nombre_tipo){
                $nombre_tipo = $nombre_tipo->Desc;
            }
            else{$nombre_tipo = "";}

            echo 'var nombre_tipo = "'.addslashes($nombre_tipo).'";';
            
            echo '
            var row = {
                descripcion:"'.addslashes($tributo_comprobante_obj->descripcion).'",
                detalle:"'.addslashes($tributo_comprobante_obj->detalle).'",
                base_imponible:'.((double)$tributo_comprobante_obj->base_imponible).',
                alicuota:'.((double)$tributo_comprobante_obj->alicuota).',
                importe:'.((double)$tributo_comprobante_obj->importe).',
                id_tipo:'.addslashes($tributo_comprobante_obj->id_tipo_tributo).',
                nombre_tipo:nombre_tipo
            };

            detalle_tributos.push(row);
            actualizar_tabla_tributos();
            actualizar_totales();
            ';
        }
        ?>

        // FIN AGREGANDO TRIBUTOS SI LOS HAY

        // FIN DE AYUDA

        <?php
        
        $zonas = ["agregar","editar"];

        
        foreach($zonas as $zona_row): ?>

        // SI CAMBIAN LA CANTIDAD
        $("#cantidad_<?php echo $zona_row ?>_detalle").on("input",function(){

            var cadena = $.trim($("#cantidad_<?php echo $zona_row ?>_detalle").val());

            if(cadena.substr((cadena.length-1),1) != ".")
            {
                calcular_<?php echo $zona_row ?>_detalle_por_porcentaje();
            }
        });

        // SI CAMBIAN EL PRECIO
        $("#precio_unitario_<?php echo $zona_row ?>_detalle").on("input",function(){
            var cadena = $.trim($("#precio_unitario_<?php echo $zona_row ?>_detalle").val());

            if(cadena.substr((cadena.length-1),1) != ".")
            {
                calcular_<?php echo $zona_row ?>_detalle_por_porcentaje();
            }
        });

        // SI CAMBIAN EL PORCENTAJE
        $("#porcentaje_bonificacion_<?php echo $zona_row ?>_detalle").on("input",function(){
            var cadena = $.trim($("#porcentaje_bonificacion_<?php echo $zona_row ?>_detalle").val());

            if(cadena.substr((cadena.length-1),1) != ".")
            {
                calcular_<?php echo $zona_row ?>_detalle_por_porcentaje();
            }
        });

        // SI CAMBIAN EL IMPORTE DE BONIFICACION
        $("#importe_bonificacion_<?php echo $zona_row ?>_detalle").on("input",function(){
            var cadena = $.trim($("#importe_bonificacion_<?php echo $zona_row ?>_detalle").val());

            if(cadena.substr((cadena.length-1),1) != ".")
            {
                calcular_<?php echo $zona_row ?>_detalle_por_importe_bonificacion();
            }
        });

        function calcular_<?php echo $zona_row ?>_detalle_por_porcentaje()
        {
            // OBTENIENDO, VALIDANDO Y SETEANDO VALORES
            var cantidad = parseFloat($("#cantidad_<?php echo $zona_row ?>_detalle").val());

            if(isNaN(cantidad))
            {
                cantidad = 0;

                if($("#cantidad_<?php echo $zona_row ?>_detalle").val() != "")
                {
                    $("#cantidad_<?php echo $zona_row ?>_detalle").val(cantidad);
                }
            }
            else
            {
                $("#cantidad_<?php echo $zona_row ?>_detalle").val(cantidad);
            }

            var precio_unitario = parseFloat($("#precio_unitario_<?php echo $zona_row ?>_detalle").val());

            if(isNaN(precio_unitario))
            {
                precio_unitario = 0;

                if($("#precio_unitario_<?php echo $zona_row ?>_detalle").val() != "")
                {
                    $("#precio_unitario_<?php echo $zona_row ?>_detalle").val(precio_unitario);
                }
            }
            else
            {
                $("#precio_unitario_<?php echo $zona_row ?>_detalle").val(precio_unitario);
            }

            var porcentaje_bonificacion = parseFloat($("#porcentaje_bonificacion_<?php echo $zona_row ?>_detalle").val());

            if(isNaN(porcentaje_bonificacion))
            {
                porcentaje_bonificacion = 0;

                if($("#porcentaje_bonificacion_<?php echo $zona_row ?>_detalle").val() != "")
                {
                    $("#porcentaje_bonificacion_<?php echo $zona_row ?>_detalle").val(porcentaje_bonificacion);
                }
            }
            else
            {
                $("#porcentaje_bonificacion_<?php echo $zona_row ?>_detalle").val(porcentaje_bonificacion);
            }

            // CALCULANDO IMPORTE BONIFICACION Y SUBTOTAL

            var total = cantidad * precio_unitario;
            var importe_bonificacion_a_mostrar = (total * porcentaje_bonificacion) / 100;
            var subtotal_a_mostrar = total - importe_bonificacion_a_mostrar;

            $("#importe_bonificacion_<?php echo $zona_row ?>_detalle").val(importe_bonificacion_a_mostrar.toFixed(2));
            $("#subtotal_<?php echo $zona_row ?>_detalle").val(subtotal_a_mostrar.toFixed(2));
        }

        function calcular_<?php echo $zona_row ?>_detalle_por_importe_bonificacion()
        {
            // OBTENIENDO, VALIDANDO Y SETEANDO VALORES
            var cantidad = parseFloat($("#cantidad_<?php echo $zona_row ?>_detalle").val());

            if(isNaN(cantidad))
            {
                cantidad = 0;

                if($("#cantidad_<?php echo $zona_row ?>_detalle").val() != "")
                {
                    $("#cantidad_<?php echo $zona_row ?>_detalle").val(cantidad);
                }
            }
            else
            {
                $("#cantidad_<?php echo $zona_row ?>_detalle").val(cantidad);
            }

            var precio_unitario = parseFloat($("#precio_unitario_<?php echo $zona_row ?>_detalle").val());

            if(isNaN(precio_unitario))
            {
                precio_unitario = 0;

                if($("#precio_unitario_<?php echo $zona_row ?>_detalle").val() != "")
                {
                    $("#precio_unitario_<?php echo $zona_row ?>_detalle").val(precio_unitario);
                }
            }
            else
            {
                $("#precio_unitario_<?php echo $zona_row ?>_detalle").val(precio_unitario);
            }

            var importe_bonificacion = parseFloat($("#importe_bonificacion_<?php echo $zona_row ?>_detalle").val());

            if(isNaN(importe_bonificacion))
            {
                importe_bonificacion = 0;

                if($("#importe_bonificacion_<?php echo $zona_row ?>_detalle").val() != "")
                {
                    $("#importe_bonificacion_<?php echo $zona_row ?>_detalle").val(importe_bonificacion);
                }
            }
            else
            {
                $("#importe_bonificacion_<?php echo $zona_row ?>_detalle").val(importe_bonificacion);
            }

            // CALCULANDO IMPORTE BONIFICACION Y SUBTOTAL

            var precio_base = cantidad * precio_unitario;
            var total_con_importe = precio_base + importe_bonificacion;
            var porcentaje_calculado = ((total_con_importe * 100) / precio_base) -100;

            var subtotal_a_mostrar = precio_base - importe_bonificacion;

            $("#porcentaje_bonificacion_<?php echo $zona_row ?>_detalle").val(porcentaje_calculado.toFixed(2));
            $("#subtotal_<?php echo $zona_row ?>_detalle").val(subtotal_a_mostrar.toFixed(2));
        }


        $("#base_imponible_<?php echo $zona_row ?>_tributo").on("input",function(e){

            var cadena = $.trim($("#base_imponible_<?php echo $zona_row ?>_tributo").val());

            if(cadena.substr((cadena.length-1),1) != ".")
            {
                calcular_<?php echo $zona_row ?>_tributo();
            }

        });

        $("#alicuota_<?php echo $zona_row ?>_tributo").on("input",function(){

            var cadena = $.trim($("#alicuota_<?php echo $zona_row ?>_tributo").val());

            if(cadena.substr((cadena.length-1),1) != ".")
            {
                calcular_<?php echo $zona_row ?>_tributo();
            }

        });

        function calcular_<?php echo $zona_row ?>_tributo()
        {
            // OBTENIENDO, VALIDANDO Y SETEANDO VALORES
            var base_imponible = parseFloat($("#base_imponible_<?php echo $zona_row ?>_tributo").val());

            if(isNaN(base_imponible))
            {
                base_imponible = 0;

                if($("#base_imponible_<?php echo $zona_row ?>_tributo").val() != "")
                {
                    $("#base_imponible_<?php echo $zona_row ?>_tributo").val(base_imponible);
                }
            }
            else
            {
                $("#base_imponible_<?php echo $zona_row ?>_tributo").val(base_imponible);
            }

            var alicuota = parseFloat($("#alicuota_<?php echo $zona_row ?>_tributo").val());

            if(isNaN(alicuota))
            {
                alicuota = 0;

                if($("#alicuota_<?php echo $zona_row ?>_tributo").val() != "")
                {
                    $("#alicuota_<?php echo $zona_row ?>_tributo").val(alicuota);
                }
            }
            else
            {
                $("#alicuota_<?php echo $zona_row ?>_tributo").val(alicuota);
            }
            // CALCULANDO IMPORTE BONIFICACION Y SUBTOTAL

            var importe_alicuota = (base_imponible * alicuota) / 100;
            var importe =  importe_alicuota;

            $("#importe_<?php echo $zona_row ?>_tributo").val(importe.toFixed(2));
        }

        <?php endforeach; ?>

    });

    // SI CAMBIAN LA CONDICION FRENTE AL IVA DEL RECEPTOR
    $("#condicion_frente_al_iva").change(function(){

        // pongo en vacio el tipo de documento
        $("#idivareceptor").html("<option value=''></option>");
        $("#idivareceptor").val("");

        // obtengo el valor
        var id_condicion_frente_al_iva = $(this).val();

        $.ajax({
            url: "{{url('/backend/comprobantes/get_tipos_de_documentos_unicos')}}",
            type: "POST",
            data: {
                id_condicion_frente_al_iva:id_condicion_frente_al_iva
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend: function(data)
            {
                abrir_loading();
            },
            success: function(data)
            {
                cerrar_loading();

                if(data["response"])
                {
                    if(data["data"].length > 0)
                    {
                        $("#idivareceptor").html("");

                        for(var i=0; i < data["data"].length;i++)
                        {
                            $("#idivareceptor").append("<option value='"+data["data"][i]["id"]+"'>"+data["data"][i]["descripcion"]+"</option>");
                        }

                        $("#idivareceptor").val(data["data"][0]["id"]);
                    }
                }
                else
                {
                    mostrar_mensajes_errores(data["messages_errors"]);
                }
            },
            error: function(error)
            {
                mostrar_mensajes_errores();
                cerrar_loading();
            }
        });


    });

    $("#precio_servicio").on("blur",function(){

        var precio_servicio = parseFloat($(this).val());

        if(isNaN(precio_servicio))
        {
            precio_servicio = 0;
            alert("Ingrese un precio de servicio válido");
        }

        $(this).val(precio_servicio);

        calcular_totales();
    })

    $("#formulario_paso").submit(function(){

        var condicion_frente_al_iva = $("#condicion_frente_al_iva").val();
        var idIVAReceptor = $("#idivareceptor").val();
        var nrodocreceptor = $("#nrodocreceptor").val();
        var razon_social = $("#razon_social").val();
        var domicilio_comercial = $("#domicilio_comercial").val();
        var correo = $("#correo").val();
        var id_condicion_de_venta = $("#id_condicion_de_venta").val();
        var id_tipo_de_iva = $("#id_tipo_de_iva").val();

        var ImpTotal = $.trim($("#ImpTotal").val());
        var ImpTotConc = $.trim($("#ImpTotConc").val());
        var ImpNeto = $.trim($("#ImpNeto").val());
        var ImpOpEx = $.trim($("#ImpOpEx").val());
        var ImpIVA = $.trim($("#ImpIVA").val());
        var ImpTrib = $.trim($("#ImpTrib").val());

        var subtotal = parseFloat($.trim($("#subtotal").text()));
        var total_tributos = parseFloat($.trim($("#total_tributos").text()));
        
        $.ajax({
            url: "{{$link_controlador.'/nuevo3'}}",
            type: "POST",
            data: {
                condicion_frente_al_iva:condicion_frente_al_iva,
                idIVAReceptor:idIVAReceptor,
                nrodocreceptor:nrodocreceptor,
                razon_social:razon_social,
                domicilio_comercial:domicilio_comercial,
                correo:correo,
                id_condicion_de_venta:id_condicion_de_venta,
                id_tipo_de_iva:id_tipo_de_iva,
                ImpTotal:ImpTotal,
                ImpTotConc:ImpTotConc,
                ImpNeto:ImpNeto,
                ImpOpEx:ImpOpEx,
                ImpIVA:ImpIVA,
                ImpTrib:ImpTrib,
                detalle_comprobante:JSON.stringify(detalle_comprobante),
                detalle_tributos:JSON.stringify(detalle_tributos),
                subtotal:subtotal,
                total_tributos:total_tributos,
                comprobantes_asociados: JSON.stringify(comprobantes_asociados),
                descripcion_servicio: $("#descripcion_del_servicio").val()
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend: function(data)
            {
                abrir_loading();
            },
            success: function(data)
            {
                cerrar_loading();

                if(data["response"])
                {
                    location.href="{{$link_controlador.'/confirmar_nuevo'}}"
                }
                else
                {
                    mostrar_mensajes_errores(data["messages_errors"]);
                }
            },
            error: function(error)
            {
                mostrar_mensajes_errores();
                cerrar_loading();
            }
        });

        return false;
    });

    function abrir_modal_agregar_tributo()
    {
        $("#formulario_agregar_tributo select").val("").trigger("change");
        $("#formulario_agregar_tributo input[type=text]").val("");
        $("#formulario_agregar_tributo textarea").val("");
        $("#modal_agregar_tributo").modal("show");
    }

    $("#formulario_agregar_tributo").submit(function(){
        agregar_tributo();
        return false;
    });

    function agregar_tributo()
    {
        var descripcion = $.trim($("#formulario_agregar_tributo [name=descripcion]").val());
        var detalle = $.trim($("#formulario_agregar_tributo [name=detalle]").val());
        var base_imponible = parseFloat($.trim($("#formulario_agregar_tributo [name=base_imponible]").val()));
        var alicuota = parseFloat($.trim($("#formulario_agregar_tributo [name=alicuota]").val()));
        var importe = parseFloat($.trim($("#formulario_agregar_tributo [name=importe]").val()));
        var id_tipo = $.trim($("#formulario_agregar_tributo [name=id_tipo]").val());

        var mensajes_validacion = new Array();

        if(id_tipo == ""){
            mensajes_validacion.push("Seleccione un tipo de tributo");
        }

        if(descripcion == ""){
            mensajes_validacion.push("Ingrese una descripcion");
        }
        
        if(detalle == ""){
            mensajes_validacion.push("Ingrese un detalle");
        }

        if(base_imponible == "" || isNaN(base_imponible) || base_imponible == 0){
            mensajes_validacion.push("Ingrese una base imponible correcta");
        }

        if(alicuota == "" || isNaN(alicuota) || alicuota == 0){
            mensajes_validacion.push("Ingrese una alicuota correcta");
        }

        if(importe == "" || isNaN(importe) || importe == 0){
            mensajes_validacion.push("Seleccione un importe");
        }

        if(mensajes_validacion.length == 0)
        {
            var nombre_tipo = $("#id_tipo_agregar_tributo option:selected").text();

            var row = {
                descripcion:descripcion,
                detalle:detalle,
                base_imponible:base_imponible,
                alicuota:alicuota,
                importe:importe,
                id_tipo:id_tipo,
                nombre_tipo:nombre_tipo
            };

            detalle_tributos.push(row);
            actualizar_tabla_tributos();
            actualizar_totales();
            $("#modal_agregar_tributo").modal("hide");
        }
        else
        {
            mostrar_mensajes_errores(mensajes_validacion);
        }
    }

    function actualizar_tabla_tributos()
    {
        $("#cuerpo_detalle_tributos").html("");

        for(var i=0; i < detalle_tributos.length;i++)
        {
            var row = detalle_tributos[i];
            
            $("#cuerpo_detalle_tributos").append(
                `<tr>
                    <td>`+row["nombre_tipo"]+`</td>
                    <td>`+row["descripcion"]+`</td>
                    <td>`+row["detalle"]+`</td>
                    <td>$ `+row["base_imponible"]+`</td>
                    <td>`+row["alicuota"]+` %</td>
                    <td>$ `+row["importe"]+`</td>
                    <td>
                        <button type="button" class="btn btn-sm btn-info btn-round" onclick="abrir_modal_editar_tributo(`+i+`)">
                            <i class="fa fa-edit"></i>
                        </button>
                        <button type="button" class="btn btn-sm btn-danger btn-round" onclick="eliminar_tributo(`+i+`)">
                            <i class="fa fa-times"></i>
                        </button>
                    </td>
                </tr>`
            );
        }
    }

    function abrir_modal_editar_tributo(position)
    {
        posicion_trabajando = position;

        var row = detalle_tributos[position];

        $("#formulario_editar_tributo [name=descripcion]").val(row["descripcion"]);
        $("#formulario_editar_tributo [name=detalle]").val(row["detalle"]);
        $("#formulario_editar_tributo [name=base_imponible]").val(row["base_imponible"]);
        $("#formulario_editar_tributo [name=alicuota]").val(row["alicuota"]);
        $("#formulario_editar_tributo [name=importe]").val(row["importe"]);

        $("#modal_editar_tributo").modal("show");
    }

    $("#formulario_editar_tributo").submit(function(){
        editar_detalle();
        return false;
    });

    function editar_tributo()
    { 
        var descripcion = $.trim($("#formulario_editar_tributo [name=descripcion]").val());
        var detalle = $.trim($("#formulario_editar_tributo [name=detalle]").val());
        var base_imponible = parseFloat($("#formulario_editar_tributo [name=base_imponible]").val());
        var alicuota = parseInt($("#formulario_editar_tributo [name=alicuota]").val());
        var importe = parseFloat($("#formulario_editar_tributo [name=importe]").val());
        var id_tipo = $.trim($("#formulario_editar_tributo [name=id_tipo]").val());

        var mensajes_validacion = new Array();

        if(id_tipo == ""){
            mensajes_validacion.push("Seleccione un tipo de tributo");
        }

        if(descripcion == ""){
            mensajes_validacion.push("Ingrese una descripcion");
        }

        if(detalle == ""){
            mensajes_validacion.push("Ingrese un detalle");
        }

        if(base_imponible == "" || isNaN(base_imponible) || base_imponible == 0){
            mensajes_validacion.push("Ingrese una base imponible correcta");
        }

        if(alicuota == "" || isNaN(alicuota) || alicuota == 0){
            mensajes_validacion.push("Ingrese una alicuota correcta");
        }

        if(importe == "" || isNaN(importe) || importe == 0){
            mensajes_validacion.push("Seleccione un importe");
        }

        if(mensajes_validacion.length == 0)
        {
            var nombre_tipo = $("#id_tipo_editar_tributo option:selected").text();

            var row = detalle_tributos[posicion_trabajando];

            row["descripcion"] = descripcion;
            row["detalle"] = detalle;
            row["base_imponible"] = base_imponible;
            row["alicuota"] = alicuota;
            row["importe"] = importe;
            row["id_tipo"] = id_tipo;
            row["nombre_tipo"] = nombre_tipo;
            
            detalle_tributos[posicion_trabajando]= row;
            actualizar_tabla_tributos();
            actualizar_totales();
            $("#modal_editar_tributo").modal("hide");
        }
        else
        {
            mostrar_mensajes_errores(mensajes_validacion);
        }
    }

    function eliminar_tributo(position)
    {
        swal({
            title: "{{$abm_messages['delete']['title']}}",
            text: "{{$abm_messages['delete']['text']}}",
            type: 'error',
            buttons:{
                confirm: {
                    text : "{{$abm_messages['delete']['confirmButtonText']}}",
                    className : "{{$config_buttons['delete']['class']}}",
                },
                cancel: {
                    visible: true,
                    text : "{{$abm_messages['delete']['cancelButtonText']}}",
                    className: "{{$config_buttons['cancel']['class']}}",
                }
            }
        }).then((Delete) => {
            if (Delete) {

                delete detalle_tributos[position];
                clean_detalle_tributos_array();

            } else {
                swal.close();
            }
        });
    }

    function clean_detalle_tributos_array()
    {
        var aux_detalle_tributos = new Array();

        for(var i=0; i < detalle_tributos.length;i++)
        {
            if(detalle_tributos[i] != undefined)
            {
                aux_detalle_tributos.push(detalle_tributos[i]);
            }
        }

        detalle_tributos = aux_detalle_tributos;
        actualizar_tabla_tributos();
        actualizar_totales();
    }

    function abrir_modal_agregar_detalle()
    {
        $("#formulario_agregar_detalle select").val(0).trigger("change");
        $("#formulario_agregar_detalle input[type=text]").val("");
        $("#formulario_agregar_detalle textarea").val("");
        $("#modal_agregar_detalle").modal("show");
    }

    $("#formulario_agregar_detalle").submit(function(){
        agregar_detalle();
        return false;
    });

    function agregar_detalle()
    { 
        var codigo = $.trim($("#formulario_agregar_detalle [name=codigo]").val());
        var producto_servicio = $.trim($("#formulario_agregar_detalle [name=producto_servicio]").val());
        var cantidad = parseFloat($("#formulario_agregar_detalle [name=cantidad]").val());
        var id_unidad_medida = parseInt($("#formulario_agregar_detalle [name=id_unidad_medida]").val());
        var precio_unitario = parseFloat($("#formulario_agregar_detalle [name=precio_unitario]").val());
        var porcentaje_bonificacion = $("#formulario_agregar_detalle [name=porcentaje_bonificacion]").val();
        var importe_bonificacion = $("#formulario_agregar_detalle [name=importe_bonificacion]").val();
        var subtotal = $("#formulario_agregar_detalle [name=subtotal]").val();

        var mensajes_validacion = new Array();

        if(codigo == ""){
            mensajes_validacion.push("Ingrese un código");
        }

        if(producto_servicio == ""){
            mensajes_validacion.push("Ingrese un producto o servicio");
        }

        if(cantidad == "" || isNaN(cantidad) || cantidad == 0){
            mensajes_validacion.push("Ingrese una cantidad correcta");
        }

        if(id_unidad_medida == "" || isNaN(id_unidad_medida) || id_unidad_medida == 0){
            mensajes_validacion.push("Seleccione un tipo de unidad");
        }

        if(precio_unitario == "" || isNaN(precio_unitario) || precio_unitario == 0){
            mensajes_validacion.push("Ingrese un precio unitario correcto");
        }

        if(porcentaje_bonificacion != "" && isNaN(porcentaje_bonificacion)){
            mensajes_validacion.push("Ingrese un porcentaje de bonificacion correcto");
        }

        if(mensajes_validacion.length == 0)
        {
            var nombre_unidad = $("#id_unidad_medida_agregar_detalle option:selected").text();

            var row = {
                codigo:codigo,
                producto_servicio:producto_servicio,
                cantidad:cantidad,
                id_unidad_medida:id_unidad_medida,
                nombre_unidad:nombre_unidad,
                precio_unitario:precio_unitario,
                porcentaje_bonificacion:porcentaje_bonificacion,
                importe_bonificacion:importe_bonificacion,
                subtotal:subtotal
            };

            detalle_comprobante.push(row);
            actualizar_tabla_detalles();
            actualizar_totales();
            $("#modal_agregar_detalle").modal("hide");
        }
        else
        {
            mostrar_mensajes_errores(mensajes_validacion);
        }
    }

    function actualizar_tabla_detalles()
    {
        $("#cuerpo_detalle_comprobante").html("");

        for(var i=0; i < detalle_comprobante.length;i++)
        {
            var row = detalle_comprobante[i];

            $("#cuerpo_detalle_comprobante").append(
                `<tr>
                    <td>`+row["codigo"]+`</td>
                    <td>`+row["producto_servicio"]+`</td>
                    <td>`+row["cantidad"]+`</td>
                    <td>`+row["nombre_unidad"]+`</td>
                    <td>$ `+row["precio_unitario"]+`</td>
                    <td>`+row["porcentaje_bonificacion"]+` %</td>
                    <td>$ `+row["importe_bonificacion"]+`</td>
                    <td>$ `+row["subtotal"]+`</td>
                    <td>
                        <button type="button" class="btn btn-sm btn-info btn-round" onclick="abrir_modal_editar_detalle(`+i+`)">
                            <i class="fa fa-edit"></i>
                        </button>
                        <button type="button" class="btn btn-sm btn-danger btn-round" onclick="eliminar_detalle(`+i+`)">
                            <i class="fa fa-times"></i>
                        </button>
                    </td>
                </tr>`
            );
        }
    }

    function abrir_modal_editar_detalle(position)
    {
        posicion_trabajando = position;

        var row = detalle_comprobante[position];

        $("#formulario_editar_detalle [name=codigo]").val(row["codigo"]);
        $("#formulario_editar_detalle [name=producto_servicio]").val(row["producto_servicio"]);
        $("#formulario_editar_detalle [name=cantidad]").val(row["cantidad"]);
        $("#formulario_editar_detalle [name=id_unidad_medida]").val(row["id_unidad_medida"]);
        $("#formulario_editar_detalle [name=precio_unitario]").val(row["precio_unitario"]);
        $("#formulario_editar_detalle [name=porcentaje_bonificacion]").val(row["porcentaje_bonificacion"]);
        $("#formulario_editar_detalle [name=importe_bonificacion]").val(row["importe_bonificacion"]);
        $("#formulario_editar_detalle [name=subtotal]").val(row["subtotal"]);

        $("#modal_editar_detalle").modal("show");
    }

    $("#formulario_editar_detalle").submit(function(){
        editar_detalle();
        return false;
    });

    function editar_detalle(position)
    { 
        var codigo = $.trim($("#formulario_editar_detalle [name=codigo]").val());
        var producto_servicio = $.trim($("#formulario_editar_detalle [name=producto_servicio]").val());
        var cantidad = parseFloat($("#formulario_editar_detalle [name=cantidad]").val());
        var id_unidad_medida = parseInt($("#formulario_editar_detalle [name=id_unidad_medida]").val());
        var precio_unitario = parseFloat($("#formulario_editar_detalle [name=precio_unitario]").val());
        var porcentaje_bonificacion = $("#formulario_editar_detalle [name=porcentaje_bonificacion]").val();
        var importe_bonificacion = $("#formulario_editar_detalle [name=importe_bonificacion]").val();
        var subtotal = $("#formulario_editar_detalle [name=subtotal]").val();

        var mensajes_validacion = new Array();

        if(codigo == ""){
            mensajes_validacion.push("Ingrese un código");
        }

        if(producto_servicio == ""){
            mensajes_validacion.push("Ingrese un producto o servicio");
        }

        if(cantidad == "" || isNaN(cantidad) || cantidad == 0){
            mensajes_validacion.push("Ingrese una cantidad correcta");
        }

        if(id_unidad_medida == "" || isNaN(id_unidad_medida) || id_unidad_medida == 0){
            mensajes_validacion.push("Seleccione un tipo de unidad");
        }

        if(precio_unitario == "" || isNaN(precio_unitario) || precio_unitario == 0){
            mensajes_validacion.push("Ingrese un precio unitario correcto");
        }

        if(porcentaje_bonificacion == "" || isNaN(porcentaje_bonificacion) || porcentaje_bonificacion == 0){
            mensajes_validacion.push("Ingrese un porcentaje de bonificacion correcto");
        }

        if(mensajes_validacion.length == 0)
        {
            var nombre_unidad = $("#id_unidad_medida_editar_detalle option:selected").text();

            var row = detalle_comprobante[posicion_trabajando];

            row["codigo"] = codigo;
            row["producto_servicio"] = producto_servicio;
            row["cantidad"] = cantidad;
            row["id_unidad_medida"] = id_unidad_medida;
            row["nombre_unidad"] = nombre_unidad;
            row["precio_unitario"] = precio_unitario;
            row["porcentaje_bonificacion"] = porcentaje_bonificacion;
            row["importe_bonificacion"] = importe_bonificacion;
            row["subtotal"] = subtotal;
            
            detalle_comprobante[posicion_trabajando]= row;
            actualizar_tabla_detalles();
            actualizar_totales();
            $("#modal_editar_detalle").modal("hide");
        }
        else
        {
            mostrar_mensajes_errores(mensajes_validacion);
        }
    }

    function eliminar_detalle(position)
    {
        swal({
            title: "{{$abm_messages['delete']['title']}}",
            text: "{{$abm_messages['delete']['text']}}",
            type: 'error',
            buttons:{
                confirm: {
                    text : "{{$abm_messages['delete']['confirmButtonText']}}",
                    className : "{{$config_buttons['delete']['class']}}",
                },
                cancel: {
                    visible: true,
                    text : "{{$abm_messages['delete']['cancelButtonText']}}",
                    className: "{{$config_buttons['cancel']['class']}}",
                }
            }
        }).then((Delete) => {
            if (Delete) {

                delete detalle_comprobante[position];
                clean_detalle_comprobante_array();

            } else {
                swal.close();
            }
        });
    }

    function clean_detalle_comprobante_array()
    {
        var aux_detalle_comprobante = new Array();

        for(var i=0; i < detalle_comprobante.length;i++)
        {
            if(detalle_comprobante[i] != undefined)
            {
                aux_detalle_comprobante.push(detalle_comprobante[i]);
            }
        }

        detalle_comprobante = aux_detalle_comprobante;

        actualizar_tabla_detalles();
        actualizar_totales();
    }

    function actualizar_totales()
    {
        var subtotal = 0;

        switch(ID_TIPO_DE_COMPROBANTE)
        {
            case ID_FACTURA_C:
            case ID_NOTA_DEBITO_C:
            case ID_NOTA_CREDITO_c:
                // EL SUBTOTAL SE CALCULA DESDE EL DETALLE AGREGADO
                for(var i=0; i < detalle_comprobante.length;i++)
                {
                    if(detalle_comprobante[i] != undefined)
                    {
                        subtotal += parseFloat(detalle_comprobante[i]["subtotal"]);
                    }
                }
            break;

            case ID_RECIBO_C:
                // EL SUBTOTAL ES EL PRECIO QUE PUSIERON AL SERVICIO
                subtotal = parseFloat($("#precio_servicio").val());

            break;
        }

        if(isNaN(subtotal))
        {
            subtotal = 0;
        }

        $("#subtotal").text(subtotal);

        var total_tributos = 0;

        for(var i=0; i < detalle_tributos.length;i++)
        {
            if(detalle_tributos[i] != undefined)
            {
                total_tributos += parseFloat(detalle_tributos[i]["importe"]);
            }
        }

        $("#total_tributos").text(total_tributos);

        $("#total").text((subtotal+total_tributos));
    }

    function abrir_modal_agregar_comprobante_asociado()
    {
        $("#formulario_agregar_comprobante_asociado select").val("").trigger("change");
        $("#formulario_agregar_comprobante_asociado input[type=text]").val("");
        $("#formulario_agregar_comprobante_asociado textarea").val("");
        $("#modal_agregar_comprobante_asociado").modal("show");
    }

    function agregar_comprobante_asociado()
    {
        var id_tipo_de_comprobante = $.trim($("#id_tipo_de_comprobante_agregar_ca").val());
        var id_punto_de_venta = $.trim($("#id_punto_de_venta_agregar_ca").val());
        var comprobante = $.trim($("#comprobante_agregar_ca").val());

        var mensajes_validacion = new Array();

        if(isNaN(id_tipo_de_comprobante) || id_tipo_de_comprobante == "" || id_tipo_de_comprobante <= 0)
        {
            mensajes_validacion.push("Seleccione un tipo de comprobante");
        }

        if(id_punto_de_venta == "")
        {
            mensajes_validacion.push("Seleccione un punto de venta");
        }

        if(isNaN(comprobante) || comprobante == "" || comprobante <= 0)
        {
            mensajes_validacion.push("Ingrese un número de comprobante");
        }

        if(mensajes_validacion.length == 0)
        {
            var tipo_de_comprobante = $("#id_tipo_de_comprobante_agregar_ca option:selected").text();

            comprobantes_asociados.push({
                tipo_de_comprobante:tipo_de_comprobante,
                id_tipo_de_comprobante:id_tipo_de_comprobante,
                id_punto_de_venta:id_punto_de_venta,
                comprobante:comprobante
            });

            actualizar_tabla_comprobantes_asociados();
            $("#modal_agregar_comprobante_asociado").modal("hide");
        }
        else
        {
            mostrar_mensajes_errores(mensajes_validacion);
        }
    }

    function actualizar_tabla_comprobantes_asociados()
    {
        $("#cuerpo_detalle_comprobantes_asociados").html("");

        for(var i=0; i < comprobantes_asociados.length;i++)
        {
            var tr_file = $("<tr></tr>");

                var td_tipo_de_comprobante = $("<td></td>");
                td_tipo_de_comprobante.text(comprobantes_asociados[i]["tipo_de_comprobante"]);
                
                var td_punto_de_venta = $("<td></td>");
                td_punto_de_venta.text(comprobantes_asociados[i]["id_punto_de_venta"]);
                
                var td_comprobante = $("<td></td>");
                td_comprobante.text(comprobantes_asociados[i]["comprobante"]);
                
                
                var td_buttons = $("<td></td>");

                    var button_edit = $("<button></button>");
                    button_edit.attr("type","button");
                    button_edit.addClass("btn");
                    button_edit.addClass("btn-sm");
                    button_edit.addClass("btn-primary");
                    button_edit.addClass("btn-round");
                    button_edit.attr("onclick","abrir_modal_comprobante_asociado("+i+")");
                    button_edit.html("<span class='fa fa-edit'></span>");

                    var button_remove = $("<button></button>");
                    button_remove.attr("type","button");
                    button_remove.addClass("btn");
                    button_remove.addClass("btn-sm");
                    button_remove.addClass("btn-danger");
                    button_remove.addClass("btn-round");
                    button_remove.attr("onclick","eliminar_comprobante_asociado("+i+")");
                    button_remove.html("<span class='fa fa-times'></span>");

                td_buttons.append(button_edit);
                td_buttons.append("&nbsp;")
                td_buttons.append(button_remove);

            tr_file.append(td_tipo_de_comprobante);
            tr_file.append(td_punto_de_venta);
            tr_file.append(td_comprobante);
            tr_file.append(td_buttons);

            $("#cuerpo_detalle_comprobantes_asociados").append(tr_file);
        }
    }

    function abrir_modal_comprobante_asociado(position)
    {
        posicion_trabajando = position;

        var row = comprobantes_asociados[position];

        $("#formulario_editar_comprobante_asociado [name=id_tipo_de_comprobante]").val(row["id_tipo_de_comprobante"]);
        $("#formulario_editar_comprobante_asociado [name=id_punto_de_venta]").val(row["id_punto_de_venta"]);
        $("#formulario_editar_comprobante_asociado [name=comprobante]").val(row["comprobante"]);

        $("#modal_editar_comprobante_asociado").modal("show");
    }

    function editar_comprobante_asociado(position)
    { 
        var id_tipo_de_comprobante = $.trim($("#id_tipo_de_comprobante_editar_ca").val());
        var id_punto_de_venta = $.trim($("#id_punto_de_venta_editar_ca").val());
        var comprobante = $.trim($("#comprobante_editar_ca").val());

        var mensajes_validacion = new Array();

        if(isNaN(id_tipo_de_comprobante) || id_tipo_de_comprobante == "" || id_tipo_de_comprobante <= 0)
        {
            mensajes_validacion.push("Seleccione un tipo de comprobante");
        }

        if(id_punto_de_venta == "")
        {
            mensajes_validacion.push("Seleccione un punto de venta");
        }

        if(isNaN(comprobante) || comprobante == "" || comprobante <= 0)
        {
            mensajes_validacion.push("Ingrese un número de comprobante");
        }

        if(mensajes_validacion.length == 0)
        {
            var tipo_de_comprobante = $("#id_tipo_de_comprobante_editar_ca option:selected").text();

            var row = comprobantes_asociados[posicion_trabajando];

            row["tipo_de_comprobante"] = tipo_de_comprobante;
            row["id_tipo_de_comprobante"] = id_tipo_de_comprobante;
            row["id_punto_de_venta"] = id_punto_de_venta;
            row["comprobante"] = comprobante;
            
            comprobantes_asociados[posicion_trabajando]= row;
            actualizar_tabla_comprobantes_asociados();

            $("#modal_editar_comprobante_asociado").modal("hide");
        }
        else
        {
            mostrar_mensajes_errores(mensajes_validacion);
        }
    }

    function eliminar_comprobante_asociado(position)
    {
        swal({
            title: "{{$abm_messages['delete']['title']}}",
            text: "{{$abm_messages['delete']['text']}}",
            type: 'error',
            buttons:{
                confirm: {
                    text : "{{$abm_messages['delete']['confirmButtonText']}}",
                    className : "{{$config_buttons['delete']['class']}}",
                },
                cancel: {
                    visible: true,
                    text : "{{$abm_messages['delete']['cancelButtonText']}}",
                    className: "{{$config_buttons['cancel']['class']}}",
                }
            }
        }).then((Delete) => {
            if (Delete) {

                delete comprobantes_asociados[position];
                clean_comprobantes_asociados_array();

            } else {
                swal.close();
            }
        });
    }

    function clean_comprobantes_asociados_array()
    {
        var aux_comprobantes_asociados = new Array();

        for(var i=0; i < comprobantes_asociados.length;i++)
        {
            if(comprobantes_asociados[i] != undefined)
            {
                aux_comprobantes_asociados.push(comprobantes_asociados[i]);
            }
        }

        comprobantes_asociados = aux_comprobantes_asociados;

        actualizar_tabla_comprobantes_asociados();
    }

    function get_datos_persona()
    {
        var id_tipo_documento = $.trim($("#idivareceptor").val());
        var numero_documento = $.trim($("#nrodocreceptor").val());

        $.ajax({
            url: "{{url('/backend/afip/get_datos_persona')}}",
            type: "POST",
            data: {
                id_tipo_documento:id_tipo_documento,
                numero_documento:numero_documento
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend: function(data)
            {
                abrir_loading();
            },
            success: function(data)
            {
                cerrar_loading();

                if(data["response"])
                {
                    $("#razon_social").val(data["data"]["razon_social"]);
                    $("#domicilio_comercial").val(data["data"]["domicilio_comercial"]);
                }
            },
            error: function(error)
            {
                cerrar_loading();
            }
        });
    }

    $("#idivareceptor").change(function(){
        get_datos_persona();
    });

    $("#nrodocreceptor").change(function(){
        get_datos_persona();
    });
</script>

@endsection

@extends('backend.template.master')

@section('title', $title_page)

@section('style')
<link rel="stylesheet" href="<?php echo url("/assets/plugins/dropzone/basic.css")?>">
<link rel="stylesheet" href="<?php echo url("/assets/plugins/dropzone/dropzone.css")?>">

<style >
.dz-image img{
  border-radius: 20px;
  overflow: hidden;
  width: 120px;
  height: 120px;
  position: relative;
  display: block;
  z-index: 10;
}
</style>

@endsection

@section('contenido')
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">{{$title_page}}</h4>
            </div>

            <div class="row">

                <div class="col-md-12">
                    
                    <div class="d-flex align-items-center">
                        <a class='{{$config_buttons["go_back"]["class"]}}' href="{{ $link_controlador }}?ver=pendientes_ayuda">
                            <i class='{{$config_buttons["go_back"]["icon"]}}'></i>
                            {{$config_buttons["go_back"]["title"]}}
                        </a>
                    </div>

                    <div class="card mt-3">

                        <div class="card-body"> 

                          <div class="row">

                              <div class="col-md-12">
                                  <h3>Datos del usuario</h3>

                                  <div class="row mt-3">

                                    <div class="col-md-4">
                                      <label>Nombre y apellido:</label> 
                                      <p class="form-control">{{$usuario_obj->nombre}} {{$usuario_obj->apellido}}</p>
                                    </div>

                                    <div class="col-md-4">
                                      <label>DNI:</label> 
                                      <p class="form-control">{{$usuario_obj->dni}}</p>
                                    </div>

                                    <div class="col-md-4">
                                      <label>CUIT:</label> 
                                      <p class="form-control">{{$usuario_obj->cuit}}</p>
                                    </div>

                                    <div class="col-md-4">
                                      <label>Condición frente al IVA:</label> 
                                      <p class="form-control">{{$usuario_obj->get_condicion_iva->descripcion}}</p>
                                    </div>
                                    
                                    <div class="col-md-4">
                                      <label>Ingresos Brutos:</label> 
                                      <p class="form-control">{{$usuario_obj->ingresos_brutos}}</p>
                                    </div>
                                    
                                    <div class="col-md-4">
                                      <label>Razon Social:</label> 
                                      <p class="form-control">{{$usuario_obj->razon_social}}</p>
                                    </div>
                                    
                                    <div class="col-md-4">
                                      <label>Inicio de actividades:</label> 
                                      <?php
                                      if(trim($usuario_obj->inicio_actividades) != "")
                                      {
                                        $usuario_obj->inicio_actividades = \DateTime::createFromFormat("Y-m-d",$usuario_obj->inicio_actividades);
                                        $usuario_obj->inicio_actividades = $usuario_obj->inicio_actividades->format("d/m/Y");
                                      }
                                      ?>
                                      <p class="form-control">{{$usuario_obj->inicio_actividades}}</p>
                                    </div>

                                  </div>

                              </div>

                             

                          </div>

                        </div>
                    </div>
                    
                    @if(trim($comprobante_obj->aclaracion_usuario) != "")
                    <div class="card mt-3">

                        <div class="card-body"> 

                          <div class="row">

                              <div class="col-md-12">
                                  <h3>Aclaración usuario</h3>

                                  <div class="row mt-3">

                                    <div class="col-md-12">
                                      <textarea readonly="" class="form-control" rows="10"  style="background-color: #fff !important;color: #000;" >{{$comprobante_obj->aclaracion_usuario}}</textarea>

                                    </div>
                                  </div>
                              </div>
                          </div>
                        </div>
                    </div>
                    @endif

                    <div class="row">
                      
                      <div class="col-md-12" style="text-align: center;">
                          <a class="btn btn-primary" href="">
                             Ir a la factura
                          </a>
                      </div>

                    </div>

                </div>
			        </div>
		        </div>
    </div>
</div>
@endsection



@section("js_code")

<script type="text/javascript">

$(document).ready( function () {

});
</script>

@endsection

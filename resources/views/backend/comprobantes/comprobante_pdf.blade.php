<!DOCTYPE html>
<html>
<head>
<title>{{ strtoupper($comprobante_obj->get_tipo_de_comprobante->Desc) }}</title>

<!--<link href="{{asset('/assets/css/reset.css')}}" rel="stylesheet"> -->
   
<style type="text/css">

@page {
     margin-top: 10px;
     margin-left: 10px;
     margin-right: 10px;
    }

body{
    padding: 0px 0px 0px 0px;
    margin: 0px 0px 0px 0px;
    font-size: 11px;
} 

.tabla_bordeada{
    border-width: 1px;
    border-color: #000;
    border-style: solid;
}

.texto_dato_header{
    font-size: 13px;
    margin-top: 10px;
    padding-left: 50px;
}

.texto_dato_izquierdo_header{
    font-size: 12px;
    padding-top: 25px;
    padding-left: 20px;
}


.tx_resaltado_header{
    font-weight: bold;
}

.texto_original{
    font-size: 22px;
    font-weight: bold;
    text-align: center;
    padding-top: 10px;
    padding-bottom: 10px;
}

.tabla_sin_bordeado_superior
{
    border-top: 0px;
}

.titulo_nombre_header{
    font-size: 20px;
    font-weight: bold;
    padding-left: 20px;
    padding-top: 20px;
    padding-bottom: 20px;
}

.titulo_factura_header
{
    font-size: 20px;
    font-weight: bold;
    padding-left: 50px;
    padding-top: 20px;
    padding-bottom: 20px;
}

.tabla_detalles{
    font-size: 12px;
}

.th_gris{
    background-color: #cccccc;
    font-weight: bold;
}

#contenedor_totales{
    right: 0px;
    bottom: 200px;
    position: absolute;
}

</style>
</head>
<body>

    <table width="100%" class="tabla_header tabla_bordeada">
        <tr width="100%" style="text-align:center;">
            <td width="100%" style="text-align: center;">
                <span class="texto_original">ORIGINAL<span>
            </td>
        </tr>
    </table>
    <table width="100%" class="tabla_bordeada tabla_sin_bordeado_superior">
        <tr width="100%">
            <td style="padding-top: 10px;">
                <img src="{{asset('/storage/imagenes/logo_comprobante/'.$configuracion_afip->logo_comprobante)}}" width="290">
            </td>
            <td>
                <h1 class='titulo_factura_header'>{{ strtoupper($comprobante_obj->get_tipo_de_comprobante->Desc) }}</h1>
            </td>
        </tr>
        <tr width="100%">
            <td width="50%">
                <table width="100%">
                    <tr width="100%">
                        <td  style="padding-top: 32px;">
                            <p class="texto_dato_izquierdo_header">
                                <span class="tx_resaltado_header">Razón Social:</span> {{ ucwords($configuracion_afip->razon_social) }}
                            </p>
                        </td>
                    </tr>
                    <tr width="100%">
                        <td  style="padding-top: 32px;">
                            <p class="texto_dato_izquierdo_header">
                                <span class="tx_resaltado_header">Domicilio Comercial:</span> {{ ucwords($configuracion_afip->domicilio_comercial) }}
                            </p>
                        </td>
                    </tr>
                    <tr width="100%">
                        <td  style="padding-top: 32px;">
                            <p class="texto_dato_izquierdo_header">
                                <span class="tx_resaltado_header">
                                Condición frente al IVA:  
                                <?php
                                $condicion_frente_al_iva_obj = $configuracion_afip->get_condicion_iva;

                                if($condicion_frente_al_iva_obj)
                                {
                                    echo e($condicion_frente_al_iva_obj->descripcion);
                                }
                                ?>
                                </span>
                            </p>
                        </td>
                    </tr>
                </table>
            </td>
            <td width="50%">

                <table width="100%">
                    <tr width="100%">
                        <td style="padding-top: 10px;">
                            <p class="texto_dato_header"><span class="tx_resaltado_header">Punto de Venta:</span> {{ $comprobante_obj->get_punto_venta_formateado() }}</p>
                        </td>
                        <td style="padding-top: 10px;">
                            <p class="texto_dato_header"><span class="tx_resaltado_header">Núm. de Comprobante:</span> {{ $comprobante_obj->get_numero_afip_formateado() }}</p>
                        </td>
                    </tr>
                    <tr width="100%">
                        <td colspan=2>
                            <p class="texto_dato_header"><span class="tx_resaltado_header">Fecha de Emisión:</span> {{ $comprobante_obj->fecha }}</p>   
                        </td>
                    </tr>
                    <tr width="100%">
                        <td colspan="2" style="padding-top: 20px;">
                            <p class="texto_dato_header"><span class="tx_resaltado_header">CUIT:</span> {{ $configuracion_afip->cuit }}</p>   
                        </td>
                    </tr>
                    <tr width="100%">
                        <td colspan="2">
                            <p class="texto_dato_header"><span class="tx_resaltado_header">Ingresos Brutos:</span> {{ $configuracion_afip->ingresos_brutos }}</p>   
                        </td>
                    </tr>
                    <tr width="100%">
                        <td colspan="2">
                            <p class="texto_dato_header"><span class="tx_resaltado_header">Fecha de inicio de actividades:</span> {{ $inicio_actividades }}</p>   
                        </td>
                    </tr>
                </table>

            </td>
        </tr>
    </table>

    @if($comprobante_obj->id_concepto == $comprobante_obj::$CONCEPTO_SERVICIOS || $comprobante_obj->id_concepto == $comprobante_obj::$CONCEPTO_PRODUCTOS_Y_SERVICIOS)
    <table width="100%" class="tabla_bordeada tabla_sin_bordeado_superior">
        <tr width="100%" >
            <td style="padding-left: 10px;padding-top: 5px;padding-bottom: 5px;">
                <p class="texto_dato_header"><span class="tx_resaltado_header">Período Facturado desde:</span> {{$comprobante_obj->periodo_facturado_desde}}</p>
            </td>
            <td style="padding-left: 10px;padding-top: 5px;padding-bottom: 5px;">
                <p class="texto_dato_header"><span class="tx_resaltado_header">Hasta:</span> {{$comprobante_obj->periodo_facturado_hasta}}</p>
            </td>
            <td style="padding-left: 10px;padding-top: 5px;padding-bottom: 5px;">
                <p class="texto_dato_header"><span class="tx_resaltado_header">Fecha de Vto. para el pago:</span> {{$comprobante_obj->vencimiento_del_pago}}</p>
            </td>
        </tr>
    </table>
    @endif

    <table width="100%" class="tabla_bordeada" style="margin-top: 3px;">
        <tr width="100%">
            <td style="padding-left: 10px;padding-top: 5px;padding-bottom: 5px;">
                <p class="texto_dato_header" ><span class="tx_resaltado_header">{{$comprobante_obj->get_tipo_documento_receptor->descripcion}}:</span> {{$comprobante_obj->num_documento_receptor}}</p>
            </td>
            <td>
                <p class="texto_dato_header" style="padding-left: 10px;"><span class="tx_resaltado_header">Apellido y Nombre / Razón Social:</span> {{$comprobante_obj->razon_social_receptor}}</p>
            </td>
        </tr>
        <tr width="100%">
            <td style="padding-left: 10px;padding-top: 5px;padding-bottom: 5px;">
                <p class="texto_dato_header"><span class="tx_resaltado_header">Condición frente al IVA:</span> {{$comprobante_obj->get_condicion_iva_receptor->descripcion}}</p>
            </td>
            <td>
                <p class="texto_dato_header"><span class="tx_resaltado_header">Domicilio:</span> {{$comprobante_obj->domicilio_receptor}}</p>
            </td>
        </tr>
        <tr width="100%">
            <td colspan="2" style="padding-left: 10px;padding-top: 5px;padding-bottom: 5px;">
                <p class="texto_dato_header"><span class="tx_resaltado_header">Condición de venta:</span> {{$comprobante_obj->get_condicion_venta->descripcion}}</p>
            </td>
        </tr>
    </table>

    @if($comprobante_obj->comprobanteConDetalle() && count($detalles_factura) > 0)
    <table width="100%" class="tabla_bordeada tabla_detalles" style="margin-top: 3px;padding-top: 5px;padding-left: 5px;">
        <thead>
            <tr>
                <th class="th_gris">Código</th>
                <th class="th_gris">Producto/Servicio</th>
                <th class="th_gris">Cantidad</th>
                <th class="th_gris">U. Medida</th>
                <th class="th_gris">Precio. Unit</th>
                <th class="th_gris">% Bonif</th>
                <th class="th_gris">Imp. Bonif</th>
                <th class="th_gris">Subtotal</th>
            </tr>
        </thead>
        <tbody>
            <tbody>

                @foreach($detalles_factura as $detalle_factura_row)
                <tr>
                    <td >{{$detalle_factura_row->codigo}}</td>
                    <td >{{$detalle_factura_row->producto_servicio}}</td>
                    <td >{{$detalle_factura_row->cantidad}}</td>
                    <td >
                    <?php
                    $unidad_de_medida = $detalle_factura_row->get_unidad_de_medida;
                    ?>
                    @if($unidad_de_medida)
                        {{$unidad_de_medida->descripcion}}
                    @else
                        {{$detalle_factura_row->id_unidad_medida}}
                    @endif
                    </td>
                    <td >$ {{ number_format($detalle_factura_row->precio_unitario,2,",",".")}}</td>
                    <td >{{ number_format($detalle_factura_row->porcentaje_bonificacion,2,",",".")}} %</td>
                    <td >$ {{ number_format($detalle_factura_row->importe_bonificacion,2,",",".")}}</td>
                    <td >$ {{ number_format($detalle_factura_row->subtotal,2,",",".")}}</td>
                </tr>
                @endforeach
            </tbody>
        </tbody>
    </table>
    @elseif($comprobante_obj->id_tipo_de_comprobante == $comprobante_obj::$RECIBO_C)
    <div width="100%" style="font-size: 14px;">
        <p><span class="tx_resaltado_header">Recibi(mos) la suma de: $3000</span></p>
        <p><span class="tx_resaltado_header">En concepto de:</span></p>
        <p>{{$comprobante_obj->descripcion_servicio}}</p>
    </div>
    @endif

    @if($comprobante_obj->comprobanteConTributos() && count($tributos_comprobantes) > 0)
    <div>
        <h2>Tributos</h2>
        <table width="100%" class="tabla_bordeada tabla_tributos" style="margin-top: 3px;padding-top: 5px;padding-left: 5px;">
            <thead>
                <tr>
                    <th class="th_gris">Tipo</th>
                    <th class="th_gris">Descripción</th>
                    <th class="th_gris">Detalle</th>
                    <th class="th_gris">Base Imponible</th>
                    <th class="th_gris">alicuota</th>
                    <th class="th_gris">Importe</th>
                </tr>
            </thead>
            <tbody>
                <tbody>

                    @foreach($tributos_comprobantes as $tributo_factura_row)
                    <tr>
                        <td >{{ $tributo_factura_row->get_tipo_tributo->Desc }}</td>
                        <td >{{ $tributo_factura_row->descripcion }}</td>
                        <td >{{ $tributo_factura_row->detalle }}</td>
                        <td >$ {{ number_format($tributo_factura_row->base_imponible,2,",",".") }}</td>
                        <td >{{ $tributo_factura_row->alicuota }} %</td>
                        <td >$ {{ number_format($tributo_factura_row->importe,2,",",".") }}</td>
                        
                    </tr>
                    @endforeach

                </tbody>
            </tbody>
        </table>
    </div>
    @endif

    @if($comprobante_obj->id_moneda == "PES")
    <div id="contenedor_totales">
        <table style="font-size: 13px;">
            <tr>
                <td><b>Subtotal:</b></td>
                <td>$ {{ number_format($comprobante_obj->ImpNeto,2,",",".") }}</td>
            </tr>
            @if($comprobante_obj->comprobanteConTributos())
            <tr>
                <td><b>Total Tributos:</b></td>
                <td>$ {{ number_format($comprobante_obj->ImpTrib,2,",",".") }}</td>
            </tr>
            @endif
            <tr>
                <td><b>Total:</b></td>
                <td>$ {{ number_format($comprobante_obj->ImpTotal,2,",",".") }}</td>
            </tr>
        </table>
    </div>
    @elseif($comprobante_obj->id_moneda == "DOL")
    <div id="contenedor_totales">
        <table width="100%">
            <tr>
                <td width="65%">&nbsp;</td>
                <td width="35%">
                    
                    <p>Moneda: USD - Dólar Estadounidense</p>
                    <table width="100%">
                        <tr>
                            <td><b>Subtotal:</b>USD</td>
                            <td>$ {{ number_format($comprobante_obj->ImpNeto,2,",",".") }}</td>
                        </tr>
                        <tr>
                            <td><b>Importe Otros Tributos:</b>USD</td>
                            <td>{{ number_format($comprobante_obj->ImpTrib,2,",",".") }}</td>
                        </tr>
                        <tr>
                            <td><b>Importe Total:</b>USD</td>
                            <td>{{ number_format($comprobante_obj->ImpTotal,2,",",".") }}</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

        <table width="100%" class="tabla_bordeada">
            <tr>
                <td>El total de este comprobante en moneda de curso legal - Pesos Argentinos - considerándose un tipo de cambio consignado de {{ number_format($comprobante_obj->cotizacion_del_dolar,2,",",".") }} asciende a: $</td>
                <td><b>{{ number_format($comprobante_obj->cotizacion_del_dolar * $comprobante_obj->ImpTotal,2,",",".") }}</b></td>
            </tr>

        </table>
    </div>

    @endif

</body>
</html>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="csrf-token" content="<?php echo csrf_token() ?>">
        <title>Pago Correcto | {{Config("app.name")}}</title>
        <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
        <link rel="icon" href="{{ asset('/admin/assets/img/icon.ico')}}" type="image/x-icon"/>

        <!-- Fonts and icons -->
        <script src="{{ asset('/admin/assets/js/plugin/webfont/webfont.min.js')}}"></script>
        <script>
            WebFont.load({
            google: {"families":["Lato:300,400,700,900"]},
            custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ['{{ asset('/admin/assets/css/fonts.min.css')}}']},
            active: function() {
                sessionStorage.fonts = true;
            }
            });
        </script>

        <!-- CSS Files -->
        <link rel="stylesheet" href="{{ asset('/admin/assets/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{ asset('/admin/assets/css/atlantis.min.css')}}">

        <!-- CSS Just for demo purpose, don't include it in your project -->
        <link rel="stylesheet" href="{{ asset('/admin/assets/css/demo.css')}}">
    </head>

    <body style="background-color: #1572e8!important;">
        <div class="row justify-content-center " style="margin-top: 120px;">
            <div class="col-md-6">
                <h1 class="text-center"><span class="badge badge-success" style="font-size: 35px !important;">Pago Correcto!</span></h1>
                <div class="card mt-5">
                    <div class="card-body bg-success text-white">
                        
                        <div class="text-center">
                            <h4>Su pago a traves de MercadoPago ha sido realizado correctamente</h4>
                        </div>
                    </div>
                    <div class="card-footer" style="text-align: center;color: #000 !important;">
                        <div>
                            <h1>Pago # {{$cobro_pago_servicio_obj->id}}</h1>
                            <h4>Monto: $ {{number_format($cobro_pago_servicio_obj->precio,2,",",".")}}</h4>
                        </div>
                        <div class="mt-5">
                            <a href="{{url('/backend/desktop')}}" class="btn btn-primary btn-lg btn-mercado-pago">
                                <i></i> Ingresar al Sistema
                            </a>    
                        </div> 
                    </div>
                </div>
            </div>
        </div>

        <!-- jQuery  -->
        <script src="{{asset('assets/js/jquery.min.js')}}"></script>
        <script src="{{asset('assets/js/bootstrap.bundle.min.js')}}"></script>
    </body>
</html>
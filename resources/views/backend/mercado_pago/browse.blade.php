@extends('backend.template.master')

@section('title', $title_page)

@section('contenido')
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">{{$title_page}}</h4>
            </div>

            <div class="row">
                
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            @if($add_active === true)
                            <div class="d-flex align-items-center">
                                @if($is_ajax == true)
                                <button class='{{$config_buttons["add"]["class"]}}' onClick="abrir_modal_agregar()">
                                    <i class='{{$config_buttons["add"]["icon"]}}'></i>
                                    {{$config_buttons["add"]["title"]}} {{$entity}}
                                </button>
                                @else
                                <a href="{{$link_controlador.'nuevo'}}" class='{{$config_buttons["add"]["class"]}}'>
                                    <i class='{{$config_buttons["add"]["icon"]}}'></i>
                                    {{$config_buttons["add"]["title"]}} {{$entity}}
                                </a>
                                @endif
                            </div>
                            @endif
                        </div>
                        <div class="card-body">

                            <form action="#" id="formulario_editar" enctype="multipart/form-data">

                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="EN_PRODUCCION">AMBIENTE <span class="text-danger">*</span></label>
                                        <select id="EN_PRODUCCION" name="EN_PRODUCCION" class="form-control">
                                            <option value="1">EN PRODUCCION</option>
                                            <option value="0">EN PRUEBA</option>
                                        </select>
                                    </div>
                                </div>
                                
                                <div style="display: none;" id="datos_produccion">
                                    <div class="row mt-3">
                                        <div class="col-md-6">
                                            <label for="CLIENT_ID">CLIENT_ID <span class="text-danger">*</span></label>
                                            <input type="text" id="CLIENT_ID" name="CLIENT_ID" class="form-control" value="{{$row_obj->CLIENT_ID}}">
                                        </div>
                                        <div class="col-md-6">
                                            <label for="CLIENT_SECRET">CLIENT_SECRET <span class="text-danger">*</span></label>
                                            <input type="text" id="CLIENT_SECRET" name="CLIENT_SECRET" class="form-control" value="{{$row_obj->CLIENT_SECRET}}">
                                        </div>
                                    </div>
                                </div>

                                <div style="display: none;" id="datos_desarrollo">
                                
                                    <div class="row mt-3">
                                        <div class="col-md-6">
                                            <label for="CLIENT_ID_SANDBOX">CLIENT_ID_SANDBOX <span class="text-danger">*</span></label>
                                            <input type="text" id="CLIENT_ID_SANDBOX" name="CLIENT_ID_SANDBOX" class="form-control" value="{{$row_obj->CLIENT_ID_SANDBOX}}">
                                        </div>
                                        <div class="col-md-6">
                                            <label for="CLIENT_SECRET_SANDBOX">CLIENT_SECRET_SANDBOX <span class="text-danger">*</span></label>
                                            <input type="text" id="CLIENT_SECRET_SANDBOX" name="CLIENT_SECRET_SANDBOX" class="form-control" value="{{$row_obj->CLIENT_SECRET_SANDBOX}}">
                                        </div>
                                    </div>

                                    <div class="row mt-4">
                                        <div class="col-md-3">
                                            <label for="VENDEDOR_ID">VENDEDOR_ID</label>
                                            <input type="text" id="VENDEDOR_ID" name="VENDEDOR_ID" class="form-control" value="{{$row_obj->VENDEDOR_ID}}">
                                        </div>
                                        <div class="col-md-3">
                                            <label for="VENDEDOR_NICKNAME">VENDEDOR_NICKNAME</label>
                                            <input type="text" id="VENDEDOR_NICKNAME" name="VENDEDOR_NICKNAME" class="form-control" value="{{$row_obj->VENDEDOR_NICKNAME}}">
                                        </div>
                                        <div class="col-md-3">
                                            <label for="VENDEDOR_PASSWORD">VENDEDOR_PASSWORD</label>
                                            <input type="text" id="VENDEDOR_PASSWORD" name="VENDEDOR_PASSWORD" class="form-control" value="{{$row_obj->VENDEDOR_PASSWORD}}">
                                        </div>
                                        <div class="col-md-3">
                                            <label for="VENDEDOR_EMAIL">VENDEDOR_EMAIL</label>
                                            <input type="text" id="VENDEDOR_EMAIL" name="VENDEDOR_EMAIL" class="form-control" value="{{$row_obj->VENDEDOR_EMAIL}}">
                                        </div>
                                    </div>

                                    <div class="row mt-4">
                                        <div class="col-md-3">
                                            <label for="COMPRADOR_ID">COMPRADOR_ID</label>
                                            <input type="text" id="COMPRADOR_ID" name="COMPRADOR_ID" class="form-control" value="{{$row_obj->COMPRADOR_ID}}">
                                        </div>
                                        <div class="col-md-3">
                                            <label for="COMPRADOR_NICKNAME">COMPRADOR_NICKNAME</label>
                                            <input type="text" id="COMPRADOR_NICKNAME" name="COMPRADOR_NICKNAME" class="form-control" value="{{$row_obj->COMPRADOR_NICKNAME}}">
                                        </div>
                                        <div class="col-md-3">
                                            <label for="COMPRADOR_PASSWORD">COMPRADOR_PASSWORD</label>
                                            <input type="text" id="COMPRADOR_PASSWORD" name="COMPRADOR_PASSWORD" class="form-control" value="{{$row_obj->COMPRADOR_PASSWORD}}">
                                        </div>
                                        <div class="col-md-3">
                                            <label for="COMPRADOR_EMAIL">COMPRADOR_EMAIL</label>
                                            <input type="text" id="COMPRADOR_EMAIL" name="COMPRADOR_EMAIL" class="form-control" value="{{$row_obj->COMPRADOR_EMAIL}}">
                                        </div>
                                    </div>
                                </div>


                                <div class="row mt-4">
                                    <div class="col-md-12" style="text-align:center;">
                                        <button class="{{$config_buttons['save']['class']}}">
                                            <i class="{{$config_buttons['save']['icon']}}"></i> {{$config_buttons['save']['title']}}
                                        </button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
			</div>
		</div>
    </div>
</div>
@endsection


@section("modals")
@endsection

@section("js_code")
<script type="text/javascript">

$(document).ready(function(){
    $("#EN_PRODUCCION").val({{$row_obj->EN_PRODUCCION}}).trigger("change");
});

$("#EN_PRODUCCION").change(function(){

    if($("#EN_PRODUCCION").val() == 1)
    {
        $("#datos_produccion").css("display","block");
        $("#datos_desarrollo").css("display","none");
    }   
    else
    {
        $("#datos_produccion").css("display","none");
        $("#datos_desarrollo").css("display","block");
    }
});

$("#formulario_editar").submit(function(){

    var formdata = new FormData();

    formdata.append("EN_PRODUCCION",$("#EN_PRODUCCION").val());
    formdata.append("CLIENT_ID",$("#CLIENT_ID").val());
    formdata.append("CLIENT_SECRET",$("#CLIENT_SECRET").val());
    formdata.append("CLIENT_ID_SANDBOX",$("#CLIENT_ID_SANDBOX").val());
    formdata.append("CLIENT_SECRET_SANDBOX",$("#CLIENT_SECRET_SANDBOX").val());
    formdata.append("VENDEDOR_ID",$("#VENDEDOR_ID").val());
    formdata.append("VENDEDOR_NICKNAME",$("#VENDEDOR_NICKNAME").val());
    formdata.append("VENDEDOR_PASSWORD",$("#VENDEDOR_PASSWORD").val());
    formdata.append("VENDEDOR_EMAIL",$("#VENDEDOR_EMAIL").val());
    formdata.append("COMPRADOR_ID",$("#COMPRADOR_ID").val());
    formdata.append("COMPRADOR_NICKNAME",$("#COMPRADOR_NICKNAME").val());
    formdata.append("COMPRADOR_PASSWORD",$("#COMPRADOR_PASSWORD").val());
    formdata.append("COMPRADOR_EMAIL",$("#COMPRADOR_EMAIL").val());
    
    $.ajax({
        url: "{{ url('/backend/configuraciones/mercado_pago/update') }}",
        type: "POST",
        contentType: false,
        cache: false,
        processData:false,
        data: formdata,
        headers:
        {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function(event){
          abrir_loading();
        },
        success: function(data)
        {
            cerrar_loading();

            try
            {
              if(data["response"] == true)
              {
                mostrar_mensajes_success("Editado!","Se han editado los datos correctamente","{{ url('/backend/configuraciones/mercado_pago') }}");
              }
              else
              {
                mostrar_mensajes_errores(data["messages_errors"]);
              }
            }
            catch(e)
            {
              mostrar_mensajes_errores();
            }

        },
        error: function(error){
          cerrar_loading();
          mostrar_mensajes_errores();
        },
    });

    return false;
});

</script>

@endsection
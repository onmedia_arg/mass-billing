@extends('backend.template.master')

@section('title', $title_page)

@section('contenido')
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">{{$title_page}}</h4>
            </div>
            
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">

                            <div class="card-header">
                                <div class="d-flex align-items-center">
                                    <a class='{{$config_buttons["go_back"]["class"]}}' href="{{ $link_controlador }}">
                                        <i class='{{$config_buttons["go_back"]["icon"]}}'></i>
                                        {{$config_buttons["go_back"]["title"]}}
                                    </a>
                                </div>
                            </div>
                            <form action="#" id="formulario_editar">
                                <div class="card-body">

                                    
                                    <div class="row">

                                        <div class="col-md-12">
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="card-head-row">
                                                        <div class="card-title">Configuración General:</div>
                                                    </div>
                                                </div>
                                                
                                                <div class="card-body">
                                                    
                                                    <div class="row">

                                                        <div class="col-md-12">
                                                            <label for="ambiente">Ambiente: <span class="text-danger">*</span></label>
                                                            <select name="ambiente" id="ambiente" class="form-control">
                                                                <option value="produccion">Produccion</option>
                                                                <option value="testing">Testing</option>
                                                            </select>
                                                        </div>

                                                        <div class="col-md-12 mt-2">
                                                            <label for="razon_social">Razon Social: <span class="text-danger">*</span></label>
                                                            <input type="text" name="razon_social" id="razon_social" class="form-control" value="{{$row_obj->razon_social}}">
                                                        </div>

                                                        <div class="col-md-12 mt-2">
                                                            <label for="cuit">CUIT: <span class="text-danger">*</span></label>
                                                            <input type="text" name="cuit" id="cuit" class="form-control" value="{{$row_obj->cuit}}">
                                                        </div>

                                                        <div class="col-md-12 mt-2">
                                                            <label for="domicilio_comercial">Domicilio Comercial: <span class="text-danger">*</span></label>
                                                            <input type="text" name="domicilio_comercial" id="domicilio_comercial" class="form-control" value="{{$row_obj->domicilio_comercial}}">
                                                        </div>

                                                        <div class="col-md-12 mt-2">
                                                            <label for="ingresos_brutos">Ingresos Brutos: <span class="text-danger">*</span></label>
                                                            <input type="text" name="ingresos_brutos" id="ingresos_brutos" class="form-control" value="{{$row_obj->ingresos_brutos}}">
                                                        </div>

                                                        <div class="col-md-12 mt-2">
                                                            <label for="inicio_actividades">Inicio Actividades: <span class="text-danger">*</span></label>
                                                            <input type="text" name="inicio_actividades" id="inicio_actividades" class="form-control" value="{{$row_obj->inicio_actividades}}">
                                                        </div>

                                                        <div class="col-md-12 mt-2">
                                                            <label for="id_condicion_iva">Condición Iva: <span class="text-danger">*</span></label>
                                                            <select name="id_condicion_iva" id="id_condicion_iva" class="form-control">
                                                                <option value="{{App\CondicionIva::IVA_RESPONSABLE_INSCRIPTO}}">IVA RESPONSABLE INSCRIPTO</option>
                                                                <option value="{{App\CondicionIva::RESPONSABLE_MONOTRIBUTO}}">RESPONSABLE MONOTRIBUTO</option>
                                                            </select>
                                                        </div>

                                                        <div class="col-md-12 mt-2">
                                                            <label for="">Logo comprobantes: <span class="text-danger">*</span></label>
                                                            <br>
                                                            <img id="preview_logo_comprobante" src="{{asset('/storage/imagenes/logo_comprobante/'.$row_obj->logo_comprobante)}}" class="img-fluid">
                                                            <div id="logo_comprobante_dropzone" style="display:none;"></div>
                                                            <input type="text" hidden="true" id="logo_comprobante" name="logo_comprobante" value="{{$row_obj->logo_comprobante}}">
                                                            <p class="text-info">Tamaño recomendado: 280 x 90px</p>
                                                            <button type="button" class="btn btn-sm btn-primary" style="margin-top: 2px;" onClick="cargarImagenLogoComprobante()">
                                                                Cambiar
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                        

                                    

                                    <div class="row">
                                        
                                        <div class="col-md-6">
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="card-head-row">
                                                        <div class="card-title">Configuración de Producción</div>
                                                    </div>
                                                </div>
                                                
                                                <div class="card-body">
                                                    
                                                    <div class="row">
                                                        <div class="col-md-12 mt-3">
                                                            <label for="csr_afip_produccion">CSR PARA AFIP: <span class="text-danger">*</span></label>
                                                            <textarea name="csr_afip_produccion" id="csr_afip_produccion" class="form-control" readonly>{{$row_obj->csr_afip_produccion}}</textarea>
                                                            <a href="{{$link_controlador.'descargarCsrParaAfip?ambiente=produccion&id='.$row_obj->id}}" class="btn btn-info btn-block"><i class="fa fa-download"></i> DESCARGAR</a>
                                                        </div>

                                                        <div class="col-md-12 mt-3">
                                                            <label for="key_afip_produccion">KEY DE AFIP: <span class="text-danger">*</span></label>
                                                            <textarea name="key_afip_produccion" id="key_afip_produccion" class="form-control" rows="4">{{$row_obj->key_afip_produccion}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-6">
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="card-head-row">
                                                        <div class="card-title">Configuración de Testing</div>
                                                    </div>
                                                </div>
                                                
                                                <div class="card-body">
                                                    
                                                    <div class="row">
                                                        <div class="col-md-12 mt-3">
                                                            <label for="csr_afip_testing">CSR PARA AFIP: <span class="text-danger">*</span></label>
                                                            <textarea name="csr_afip_testing" id="csr_afip_testing" class="form-control" readonly>{{$row_obj->csr_afip_testing}}</textarea>
                                                            <a href="{{$link_controlador.'descargarCsrParaAfip?ambiente=testing&id='.$row_obj->id}}" class="btn btn-info btn-block"><i class="fa fa-download"></i> DESCARGAR</a>
                                                        </div>

                                                        <div class="col-md-12 mt-3">
                                                            <label for="key_afip_testing">KEY DE AFIP: <span class="text-danger">*</span></label>
                                                            <textarea name="key_afip_testing" id="key_afip_testing" class="form-control" rows="4">{{$row_obj->key_afip_testing}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row justify-content-center">
                                        <div class="col-md-4" style="text-align: center;">
                                            <button class='{{$config_buttons["edit"]["class"]}}'>
                                                <i class='fa fa-save'></i>
                                                Guardar Cambios
                                            </button>
                                        </div>
                                    </div>


                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </form>
		</div>
    </div>
</div>
@endsection



@section("js_code")

<script type="text/javascript">

$(document).ready(function(){

    $("#id_condicion_iva").val("{{$row_obj->id_condicion_iva}}");
    $("#ambiente").val("{{$row_obj->ambiente}}");

    Dropzone.autoDiscover = false;

    $("#logo_comprobante_dropzone").dropzone({
        autoProcessQueue: true,
        url: "{{url('/backend/configuraciones/afip/upload_imagen_logo_comprobante')}}",
        acceptedFiles: 'image/*',
        paramName: "file",
        uploadMultiple: false,
        chunking: true,
        chunkSize: 1000000,
        addRemoveLinks: true,
        dictRemoveFile : "Eliminar",

        init: function() {

            this.on("sending", function(file, xhr, formData) {
            abrir_loading();

            try{
                xhr.onreadystatechange = function() {

                    if (xhr.readyState == XMLHttpRequest.DONE) {

                        var respuesta = JSON.parse(xhr.responseText);

                        if(respuesta["done"] == undefined)
                        {
                            var file_name =  respuesta["name"];

                            $("#preview_logo_comprobante").attr("src","{{asset('storage/imagenes/logo_comprobante')}}/"+file_name);
                            $("#formulario_editar [name=logo_comprobante]").val(file_name);
                            cerrar_loading();
                        }
                    }
                }
            }
            catch(e)
            {
            }
            
        });

        this.on("dictResponseError",function(error){
            alert("ERROR!");
        });

        this.on("removedfile", function (file) {
        });

        },
        success: function(file, response){
            cerrar_loading();
        },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
    });
});

function cargarImagenLogoComprobante()
{
    $("#logo_comprobante_dropzone").click();
}

$("#formulario_editar").submit(function(){
    editar();
    return false;
});

function editar()
{
    var formdata = new FormData();
    
    formdata.append("id",{{$row_obj->id}});
    formdata.append("ambiente",$("#ambiente").val());

    formdata.append("razon_social",$("#razon_social").val());
    formdata.append("cuit",$("#cuit").val());
    formdata.append("domicilio_comercial",$("#domicilio_comercial").val());
    formdata.append("ingresos_brutos",$("#ingresos_brutos").val());
    formdata.append("inicio_actividades",$("#inicio_actividades").val());
    formdata.append("id_condicion_iva",$("#id_condicion_iva").val());

    formdata.append("key_afip_produccion",$("#key_afip_produccion").val());
    formdata.append("key_afip_testing",$("#key_afip_testing").val());
    formdata.append("logo_comprobante",$("#logo_comprobante").val());

    $.ajax({
        url: "{{$link_controlador}}update",
        type: "POST",
        contentType: false,
        cache: false,
        processData:false,
        data: formdata,
        headers:
        {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function(event){
          abrir_loading();
        },
        success: function(data)
        {
            cerrar_loading();

            try
            {
              if(data["response"] == true)
              {
                $("#modal_editar").modal("hide");

                mostrar_mensajes_success(
                    "{{$abm_messages['success_edit']['title']}}",
                    "{{$abm_messages['success_edit']['description']}}",
                    "{{$link_controlador}}"
                );

              }
              else
              {
                mostrar_mensajes_errores(data["messages_errors"]);
              }
            }
            catch(e)
            {
                mostrar_mensajes_errores();
            }

        },
        error: function(error){
          cerrar_loading();
          mostrar_mensajes_errores();
        },
    });
}
</script>

@endsection
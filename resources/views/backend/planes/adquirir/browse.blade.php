@extends('backend.template.master')

@section('title', $title_page)

@section('contenido')
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">{{$title_page}}</h4>
            </div>

            @if($cobro_pago_servicio)
                @if($cobro_pago_servicio->id_estado == \App\EstadoCobroServicio::PAGO_PENDIENTE)
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-warning" role="alert">
                            <h3 class="text-center">Usted tiene un pago en <strong style="color: #ffad46;">PENDIENTE</strong>, estamos esperando la respuesta de MercadoPago</h3>
                        </div>
                    </div>
                </div>
                @endif
            @else

            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-info" role="alert">
                        <h3 class="text-center">DEBE SELECCIONAR UN PLAN PARA PODER USAR EL SISTEMA</h3>
                    </div>
                </div>
            </div>

            <div class="row">
                
                @foreach($planes as $plan_row)
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">{{$plan_row->nombre}}</div>
                            <h4 class="text-center">$ {{number_format($plan_row->precio,2,",",".")}}</h4>
                        </div>
                        <div class="card-body">
                            
                            <div>
                            <?php
                            $items_plan = explode("\n",$plan_row->descripcion)
                            ?>

                            @if(is_array($items_plan) && count($items_plan))
                                @foreach($items_plan as $item_plan_row)
                                @if(trim($item_plan_row) != "")
                                    <div style="margin-top: 10px">
                                        <span class="badge badge-success">
                                            <i class="fa fa-check"></i>
                                        </span> 
                                        {{$item_plan_row}}
                                    </div>
                                @endif
                                @endforeach
                            @endif
                            </div>

                            <div style="text-align: center;" class="mt-5">
                                <a href="{{url('/backend/planes/adquirir/adquirir_plan/'.$plan_row->id)}}" class="btn btn-primary">
                                    Adquirir
                                </a>  
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
			</div>

            @endif
		</div>
    </div>
</div>
@endsection


@section("modals")
@endsection

@section("js_code")

<script type="text/javascript">
</script>

@endsection
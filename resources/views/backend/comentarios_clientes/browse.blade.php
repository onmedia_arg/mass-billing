@extends('backend.template.master')

@section('title', $title_page)

@section('contenido')
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">{{$title_page}}</h4>
            </div>

            <div class="row">
                
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            @if($add_active === true)
                            <div class="d-flex align-items-center">
                                @if($is_ajax == true)
                                <button class='{{$config_buttons["add"]["class"]}}' onClick="abrir_modal_agregar()">
                                    <i class='{{$config_buttons["add"]["icon"]}}'></i>
                                    {{$config_buttons["add"]["title"]}} {{$entity}}
                                </button>
                                @else
                                <a href="{{$link_controlador.'nuevo'}}" class='{{$config_buttons["add"]["class"]}}'>
                                    <i class='{{$config_buttons["add"]["icon"]}}'></i>
                                    {{$config_buttons["add"]["title"]}} {{$entity}}
                                </a>
                                @endif
                            </div>
                            @endif
                        </div>
                        <div class="card-body">
                            
                            <div class="table-responsive">
                                <table id="tabla_listado" class="display table table-striped table-hover" >
                                    <thead>
                                        <tr>
                                            @foreach($columns as $column)
                                            <th>{{$column["name"]}}</th>
                                            @endforeach
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>
    </div>
</div>
@endsection


@section("modals")

    @component('backend.template.modal_agregar')
        @slot('entidad')
            {{$entity}}
        @endslot

        @slot('inputs')
            <div class="row">
                <div class="col-md-12">
                    <label for="texto_agregar">Texto: <span class="text-danger">*</span></label>
                    <textarea class="form-control" id="texto_agregar" rows="5"></textarea>
                </div>
            </div>

            <div class="row mt-2">
                <div class="col-md-6">
                    <label for="calificacion_agregar">Calificación: <span class="text-danger">*</span></label>
                    <select class="form-control" id="calificacion_agregar">
                        <option value="5">5</option>
                        <option value="4">4</option>
                        <option value="3">3</option>
                        <option value="2">2</option>
                        <option value="1">1</option>
                    </select>
                </div>

                <div class="col-md-6">
                    <label for="activo_agregar">Activo: <span class="text-danger">*</span></label>
                    <select class="form-control" id="activo_agregar">
                        <option value="1">SI</option>
                        <option value="0">NO</option>
                    </select>
                </div>
            </div>

            
            <div class="row mt-2">
                <div class="col-md-12">
                    <div style="text-align: center">
                        <img id="preview_imagen_agregar" src="{{asset('/storage/imagenes/comentarios_clientes/default.jpg')}}" class="img-fluid">
                        <div id="imagen_agregar_dropzone" style="display:none;"></div>
                        <input type="text" hidden="true" id="imagen_agregar" name="imagen" value="default.jpg">
                        <br>
                        <button type="button" class="btn btn-sm btn-primary" style="margin-top: 2px;" onClick="$('#imagen_agregar_dropzone').click()">
                            Cambiar
                        </button>
                        <br>
                        <p class="text-info">Se recomienda una imagen de 150 x 150px</p>
                    </div>
                </div>
            </div>
        @endslot
    @endcomponent

    @component('backend.template.modal_editar')
        @slot('entidad')
            {{$entity}}
        @endslot

        @slot('inputs')
            <div class="row">
                <div class="col-md-12">
                    <label for="texto_editar">Texto: <span class="text-danger">*</span></label>
                    <textarea class="form-control" id="texto_editar" rows="5"></textarea>
                </div>
            </div>

            <div class="row mt-2">
                <div class="col-md-6">
                    <label for="calificacion_editar">Calificación: <span class="text-danger">*</span></label>
                    <select class="form-control" id="calificacion_editar">
                        <option value="5">5</option>
                        <option value="4">4</option>
                        <option value="3">3</option>
                        <option value="2">2</option>
                        <option value="1">1</option>
                    </select>
                </div>

                <div class="col-md-6">
                    <label for="activo_editar">Activo: <span class="text-danger">*</span></label>
                    <select class="form-control" id="activo_editar">
                        <option value="1">SI</option>
                        <option value="0">NO</option>
                    </select>
                </div>
            </div>

            
            <div class="row mt-2">
                <div class="col-md-12">
                    <div style="text-align: center">
                        <img id="preview_imagen_editar" src="{{asset('/storage/imagenes/comentarios_clientes/default.jpg')}}" class="img-fluid">
                        <div id="imagen_editar_dropzone" style="display:none;"></div>
                        <input type="text" hidden="true" id="imagen_editar" name="imagen" value="default.jpg">
                        <br>
                        <button type="button" class="btn btn-sm btn-primary" style="margin-top: 2px;" onClick="$('#imagen_editar_dropzone').click()">
                            Cambiar
                        </button>
                        <br>
                        <p class="text-info">Se recomienda una imagen de 150 x 150px</p>
                    </div>
                </div>
            </div>
        @endslot
    @endcomponent

@endsection

@section("js_code")
<script src="{{asset('/assets/plugins/dropzone/dropzone.min.js')}}"></script>

<script type="text/javascript">
var listado = null;
var id_trabajando = 0;

$(document).ready( function () {

    listado= $('#tabla_listado').DataTable( {
        "processing": true,
        "serverSide": true,
        "responsive":false,
        "ajax":{
        url : "{{$link_controlador}}get_listado_dt", // json datasource
        type: "post",
        headers:
        {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        error: function(error){
            $(".employee-grid-error").html("");
            $("#employee-grid").append('<tbody class="employee-grid-error"><tr><th colspan="3">No hay datos</th></tr></tbody>');
            $("#employee-grid_processing").css("display","none");
        }
        }
    });

    Dropzone.autoDiscover = false;

    $("#imagen_agregar_dropzone").dropzone({
        autoProcessQueue: true,
        url: "{{url('/backend/comentarios_clientes/upload_imagen')}}",
        acceptedFiles: 'image/*',
        paramName: "file",
        uploadMultiple: false,
        chunking: true,
        chunkSize: 1000000,
        addRemoveLinks: true,
        dictRemoveFile : "Eliminar",

        init: function() {

            this.on("sending", function(file, xhr, formData) {
            abrir_loading();

            try{
                xhr.onreadystatechange = function() {

                    if (xhr.readyState == XMLHttpRequest.DONE) {

                        var respuesta = JSON.parse(xhr.responseText);

                        if(respuesta["done"] == undefined)
                        {
                            var file_name =  respuesta["name"];

                            $("#preview_imagen_agregar").attr("src","{{asset('storage/imagenes/comentarios_clientes')}}/"+file_name);
                            $("#imagen_agregar").val(file_name);
                            cerrar_loading();
                        }
                    }
                }
            }
            catch(e)
            {
            }
            
        });

        this.on("dictResponseError",function(error){
            alert("ERROR!");
        });

        this.on("removedfile", function (file) {
        });

        },
        success: function(file, response){
            cerrar_loading();
        },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
    });

    $("#imagen_editar_dropzone").dropzone({
        autoProcessQueue: true,
        url: "{{url('/backend/comentarios_clientes/upload_imagen')}}",
        acceptedFiles: 'image/*',
        paramName: "file",
        uploadMultiple: false,
        chunking: true,
        chunkSize: 1000000,
        addRemoveLinks: true,
        dictRemoveFile : "Eliminar",

        init: function() {

            this.on("sending", function(file, xhr, formData) {
            abrir_loading();

            try{
                xhr.onreadystatechange = function() {

                    if (xhr.readyState == XMLHttpRequest.DONE) {

                        var respuesta = JSON.parse(xhr.responseText);

                        if(respuesta["done"] == undefined)
                        {
                            var file_name =  respuesta["name"];

                            $("#preview_imagen_editar").attr("src","{{asset('storage/imagenes/comentarios_clientes')}}/"+file_name);
                            $("#imagen_editar").val(file_name);
                            cerrar_loading();
                        }
                    }
                }
            }
            catch(e)
            {
            }
            
        });

        this.on("dictResponseError",function(error){
            alert("ERROR!");
        });

        this.on("removedfile", function (file) {
        });

        },
        success: function(file, response){
            cerrar_loading();
        },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
    });

});

function abrir_modal_agregar()
{
    $("#texto_agregar").val("");
    $("#calificacion_agregar").val(5);
    $("#activo_agregar").val(1);
    $("#imagen_agregar").val("default.jpg");
    $("#preview_imagen_agregar").attr("src","{{asset('/storage/imagenes/comentarios_clientes/default.jpg')}}");

    $("#modal_agregar").modal("show");
}

function abrir_modal_editar(id)
{
    $.ajax({
        url: "{{$link_controlador}}get",
        type: "POST",
        data: {id:id},
        headers:
        {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function(event){
          abrir_loading();
        },
        success: function(data)
        {
            cerrar_loading();

            try
            {
              if(data["response"] == true)
              {
                id_trabajando = id;

                $("#texto_editar").val(data["data"]["texto"]);
                $("#calificacion_editar").val(data["data"]["calificacion"]);
                $("#activo_editar").val(data["data"]["activo"]);
                $("#imagen_editar").val(data["data"]["imagen"]);
                $("#preview_imagen_editar").attr("src","{{asset('/storage/imagenes/comentarios_clientes')}}/"+data["data"]["imagen"]);

                $("#modal_editar").modal("show");
              }
              else
              {
                mostrar_mensajes_errores(data["messages_errors"]);
              }
            }
            catch(e)
            {
              mostrar_mensajes_errores();
            }
        },
        error: function(error){
          cerrar_loading();
          mostrar_mensajes_errores();
        },
    });
}

function agregar()
{
    var formdata = new FormData();

    formdata.append("texto",$("#texto_agregar").val());
    formdata.append("calificacion",$("#calificacion_agregar").val());
    formdata.append("activo",$("#activo_agregar").val());
    formdata.append("imagen",$("#imagen_agregar").val());

    $.ajax({
        url: "{{$link_controlador}}store",
        type: "POST",
        contentType: false,
        cache: false,
        processData:false,
        data: formdata,
        headers:
        {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function(event){
          abrir_loading();
        },
        success: function(data)
        {
            cerrar_loading();

            try
            {
              if(data["response"] == true)
              {
                $("#modal_agregar").modal("hide");

                mostrar_mensajes_success(
                    "{{$abm_messages['success_add']['title']}}",
                    "{{$abm_messages['success_add']['description']}}"
                );

                listado.draw();
              }
              else
              {
                mostrar_mensajes_errores(data["messages_errors"]);
              }
            }
            catch(e)
            {
              mostrar_mensajes_errores();
            }

        },
        error: function(error){
          cerrar_loading();
          mostrar_mensajes_errores();
        },
    });
}

function editar()
{
    var formdata = new FormData();

    formdata.append("id",id_trabajando);
    formdata.append("texto",$("#texto_editar").val());
    formdata.append("calificacion",$("#calificacion_editar").val());
    formdata.append("activo",$("#activo_editar").val());
    formdata.append("imagen",$("#imagen_editar").val());

    $.ajax({
        url: "{{$link_controlador}}update",
        type: "POST",
        contentType: false,
        cache: false,
        processData:false,
        data: formdata,
        headers:
        {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function(event){
          abrir_loading();
        },
        success: function(data)
        {
            cerrar_loading();

            try
            {
              if(data["response"] == true)
              {
                $("#modal_editar").modal("hide");

                mostrar_mensajes_success(
                    "{{$abm_messages['success_edit']['title']}}",
                    "{{$abm_messages['success_edit']['description']}}"
                );

                listado.draw();
              }
              else
              {
                mostrar_mensajes_errores(data["messages_errors"]);
              }
            }
            catch(e)
            {
              mostrar_mensajes_errores();
            }

        },
        error: function(error){
          cerrar_loading();
          mostrar_mensajes_errores();
        },
    });
}

function eliminar(id)
{
    swal({
        title: "{{$abm_messages['delete']['title']}}",
        text: "{{$abm_messages['delete']['text']}}",
        icon: "warning",
        buttons: ["Cancelar", "Aceptar"],
        dangerMode: true,
    })
    .then((willDelete) => {

        if (willDelete) 
        {
            $.ajax({
                url: "{{$link_controlador}}delete",
                type: "POST",
                data: {id:id},
                headers:
                {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                beforeSend: function(event){
                    abrir_loading();
                },
                success: function(data)
                {
                    cerrar_loading();

                    try{

                        if(data["response"])
                        {
                            mostrar_mensajes_success(
                                "{{$abm_messages['delete']['success_text']}}",
                                "{{$abm_messages['delete']['success_description']}}"
                            );

                            listado.draw();
                        }
                        else
                        {
                            mostrar_mensajes_errores(data["messages_errors"]);
                        }
                        }
                        catch(e)
                        {
                        mostrar_mensajes_errores();
                        }
                },
                error: function(error){
                    cerrar_loading();
                    mostrar_mensajes_errores();
                },
            });
        }
    });
}
</script>

@endsection
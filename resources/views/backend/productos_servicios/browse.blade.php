@extends('backend.template.master')

@section('title', $title_page)

@section('contenido')
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">{{$title_page}}</h4>
            </div>

            <div class="row">
                
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            @if($add_active === true)
                            <div class="d-flex align-items-center">
                                @if($is_ajax == true)
                                <button class='{{$config_buttons["add"]["class"]}}' onClick="abrir_modal_agregar()">
                                    <i class='{{$config_buttons["add"]["icon"]}}'></i>
                                    {{$config_buttons["add"]["title"]}} {{$entity}}
                                </button>
                                @else
                                <a href="{{$link_controlador.'nuevo'}}" class='{{$config_buttons["add"]["class"]}}'>
                                    <i class='{{$config_buttons["add"]["icon"]}}'></i>
                                    {{$config_buttons["add"]["title"]}} {{$entity}}
                                </a>
                                @endif
                            </div>
                            @endif
                        </div>
                        <div class="card-body">
                            
                            <div class="table-responsive">
                                <table id="tabla_listado" class="display table table-striped table-hover" >
                                    <thead>
                                        <tr>
                                            @foreach($columns as $column)
                                            <th>{{$column["name"]}}</th>
                                            @endforeach
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>
    </div>
</div>
@endsection


@section("modals")

    @component('backend.template.modal_agregar')
        @slot('entidad')
            {{$entity}}
        @endslot

        @slot('inputs')
            <div class="row">
                <div class="col-md-12">
                    <label for="codigo_agregar">Código: <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" id="codigo_agregar">
                </div>
            </div>
            
            <div class="row mt-2">
                <div class="col-md-12">
                    <label for="descripcion_agregar">Descripción: <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" id="descripcion_agregar">
                </div>
            </div>
            
            <div class="row mt-2">
                <div class="col-md-6">
                    <label for="precio_agregar">Precio:</label>
                    <input type="text" class="form-control" id="precio_agregar" onkeypress="return escribiendoPrecioProducto(event);">
                </div>
                <div class="col-md-6">
                    <label for="precio_dolar_agregar">Precio (USD):</label>
                    <input type="text" class="form-control" id="precio_dolar_agregar" onkeypress="return escribiendoPrecioProducto(event);">
                </div>
            </div>

            <div class="row mt-2">
                <div class="col-md-6">
                    <label for="id_unidad_medida_agregar">Unidad de medida: <span class="text-danger">*</span></label>
                    <select class="form-control" id="id_unidad_medida_agregar">
                        <option value="">Seleccionar</option>
                        @foreach($unidades_de_medidas as $unidad_de_medida_row)
                        <option value="{{$unidad_de_medida_row->id}}">{{$unidad_de_medida_row->descripcion}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-md-6">
                    <label for="tipo_agregar">Tipo: <span class="text-danger">*</span></label>
                    <select class="form-control" id="tipo_agregar">
                        <option value="">Seleccionar</option>
                        <option value="producto">Producto</option>
                        <option value="servicio">Servicio</option>
                    </select>
                </div>
            </div>
        @endslot
    @endcomponent

    @component('backend.template.modal_editar')
        @slot('entidad')
            {{$entity}}
        @endslot

        @slot('inputs')
            <div class="row">
                <div class="col-md-12">
                    <label for="codigo_editar">Código: <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" id="codigo_editar">
                </div>
            </div>
            
            <div class="row mt-2">
                <div class="col-md-12">
                    <label for="descripcion_editar">Descripción: <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" id="descripcion_editar">
                </div>
            </div>
            
            <div class="row mt-2">
                <div class="col-md-6">
                    <label for="precio_editar">Precio:</label>
                    <input type="text" class="form-control" id="precio_editar"  onkeypress="return escribiendoPrecioProducto(event);">
                </div>
                <div class="col-md-6">
                    <label for="precio_dolar_editar">Precio (USD):</label>
                    <input type="text" class="form-control" id="precio_dolar_editar"  onkeypress="return escribiendoPrecioProducto(event);">
                </div>
            </div>

            <div class="row mt-2">
                <div class="col-md-6">
                    <label for="id_unidad_medida_editar">Unidad de medida: <span class="text-danger">*</span></label>
                    <select class="form-control" id="id_unidad_medida_editar">
                        <option value="">Seleccionar</option>
                        @foreach($unidades_de_medidas as $unidad_de_medida_row)
                        <option value="{{$unidad_de_medida_row->id}}">{{$unidad_de_medida_row->descripcion}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-md-6">
                    <label for="tipo_editar">Tipo: <span class="text-danger">*</span></label>
                    <select class="form-control" id="tipo_editar">
                        <option value="">Seleccionar</option>
                        <option value="producto">Producto</option>
                        <option value="servicio">Servicio</option>
                    </select>
                </div>
            </div>
        @endslot
    @endcomponent

@endsection

@section("js_code")

<script type="text/javascript">
var listado = null;
var id_trabajando = 0;

$(document).ready( function () {
    listado= $('#tabla_listado').DataTable( {
        "processing": true,
        "serverSide": true,
        "responsive":false,
        "ajax":{
        url : "{{$link_controlador}}get_listado_dt", // json datasource
        type: "post",
        headers:
        {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        error: function(error){
            $(".employee-grid-error").html("");
            $("#employee-grid").append('<tbody class="employee-grid-error"><tr><th colspan="3">No hay datos</th></tr></tbody>');
            $("#employee-grid_processing").css("display","none");
        }
        }
    });
});

function abrir_modal_agregar()
{
    $("#codigo_agregar").val("");
    $("#descripcion_agregar").val("");
    $("#precio_agregar").val("");
    $("#precio_dolar_agregar").val("");
    $("#id_unidad_medida_agregar").val(7);
    $("#tipo_agregar").val("");

    $("#modal_agregar").modal("show");
}

function abrir_modal_editar(id)
{
    $.ajax({
        url: "{{$link_controlador}}get",
        type: "POST",
        data: {id:id},
        headers:
        {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function(event){
          abrir_loading();
        },
        success: function(data)
        {
            cerrar_loading();

            try
            {
              if(data["response"] == true)
              {
                id_trabajando = id;

                $("#codigo_editar").val(data["data"]["codigo"]);
                $("#descripcion_editar").val(data["data"]["descripcion"]);
                $("#precio_editar").val(data["data"]["precio"]);
                $("#precio_dolar_editar").val(data["data"]["precio_dolar"]);
                $("#id_unidad_medida_editar").val(data["data"]["id_unidad_medida"]);
                $("#tipo_editar").val(data["data"]["tipo"]);

                $("#modal_editar").modal("show");
              }
              else
              {
                mostrar_mensajes_errores(data["messages_errors"]);
              }
            }
            catch(e)
            {
              mostrar_mensajes_errores();
            }
        },
        error: function(error){
          cerrar_loading();
          mostrar_mensajes_errores();
        },
    });
}

function agregar()
{
    var formdata = new FormData();

    formdata.append("codigo",$("#codigo_agregar").val());
    formdata.append("descripcion",$("#descripcion_agregar").val());
    formdata.append("precio",$("#precio_agregar").val());
    formdata.append("precio_dolar",$("#precio_dolar_agregar").val());
    formdata.append("id_unidad_medida",$("#id_unidad_medida_agregar").val());
    formdata.append("tipo",$("#tipo_agregar").val());

    $.ajax({
        url: "{{$link_controlador}}store",
        type: "POST",
        contentType: false,
        cache: false,
        processData:false,
        data: formdata,
        headers:
        {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function(event){
          abrir_loading();
        },
        success: function(data)
        {
            cerrar_loading();

            try
            {
              if(data["response"] == true)
              {
                $("#modal_agregar").modal("hide");

                mostrar_mensajes_success(
                    "{{$abm_messages['success_add']['title']}}",
                    "{{$abm_messages['success_add']['description']}}"
                );

                listado.draw();
              }
              else
              {
                mostrar_mensajes_errores(data["messages_errors"]);
              }
            }
            catch(e)
            {
              mostrar_mensajes_errores();
            }

        },
        error: function(error){
          cerrar_loading();
          mostrar_mensajes_errores();
        },
    });
}

function editar()
{
    var formdata = new FormData();

    formdata.append("id",id_trabajando);
    formdata.append("codigo",$("#codigo_editar").val());
    formdata.append("descripcion",$("#descripcion_editar").val());
    formdata.append("precio",$("#precio_editar").val());
    formdata.append("precio_dolar",$("#precio_dolar_editar").val());
    formdata.append("id_unidad_medida",$("#id_unidad_medida_editar").val());
    formdata.append("tipo",$("#tipo_editar").val());

    $.ajax({
        url: "{{$link_controlador}}update",
        type: "POST",
        contentType: false,
        cache: false,
        processData:false,
        data: formdata,
        headers:
        {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function(event){
          abrir_loading();
        },
        success: function(data)
        {
            cerrar_loading();

            try
            {
              if(data["response"] == true)
              {
                $("#modal_editar").modal("hide");

                mostrar_mensajes_success(
                    "{{$abm_messages['success_edit']['title']}}",
                    "{{$abm_messages['success_edit']['description']}}"
                );

                listado.draw();
              }
              else
              {
                mostrar_mensajes_errores(data["messages_errors"]);
              }
            }
            catch(e)
            {
              mostrar_mensajes_errores();
            }

        },
        error: function(error){
          cerrar_loading();
          mostrar_mensajes_errores();
        },
    });
}

function eliminar(id)
{
    swal({
        title: "{{$abm_messages['delete']['title']}}",
        text: "{{$abm_messages['delete']['text']}}",
        icon: "warning",
        buttons: ["Cancelar", "Aceptar"],
        dangerMode: true,
    })
    .then((willDelete) => {

        if (willDelete) 
        {
            $.ajax({
                url: "{{$link_controlador}}delete",
                type: "POST",
                data: {id:id},
                headers:
                {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                beforeSend: function(event){
                    abrir_loading();
                },
                success: function(data)
                {
                    cerrar_loading();

                    try{

                        if(data["response"])
                        {
                            mostrar_mensajes_success(
                                "{{$abm_messages['delete']['success_text']}}",
                                "{{$abm_messages['delete']['success_description']}}"
                            );

                            listado.draw();
                        }
                        else
                        {
                            mostrar_mensajes_errores(data["messages_errors"]);
                        }
                        }
                        catch(e)
                        {
                        mostrar_mensajes_errores();
                        }
                },
                error: function(error){
                    cerrar_loading();
                    mostrar_mensajes_errores();
                },
            });
        }
    });
}

function escribiendoPrecioProducto(e)
{
    tecla = (document.all) ? e.keyCode : e.which;
    tecla = String.fromCharCode(tecla);

    if($(e.target).attr("id") == "precio_dolar_agregar")
    {
        $("#precio_agregar").val("");
    }
    else if($(e.target).attr("id") == "precio_agregar")
    {
        $("#precio_dolar_agregar").val("");
    }
    else if($(e.target).attr("id") == "precio_dolar_editar")
    {
        $("#precio_editar").val("");
    }
    else if($(e.target).attr("id") == "precio_editar")
    {
        $("#precio_dolar_editar").val("");
    }
    
    if(!isNaN(tecla) || e.keyCode == 8 || e.keyCode == 9 || tecla == ",")
    {
        if(e.keyCode == 9 || e.keyCode == 8)
        {
            return true;
        }

        if(tecla == ",")
        {
            var valor_actual = $(e.target).val();

            if(valor_actual.search(",") != -1)
            {
                return false;
            }
            else{
                return true;
            }
        }
        else
        {
            return true;     
        }   
    }

    return false;
}

function escribe_dni(e)
{
    tecla = (document.all) ? e.keyCode : e.which;
    tecla = String.fromCharCode(tecla);
    
    if(!isNaN(tecla) || e.keyCode == 8 || e.keyCode == 9)
    {

        if(e.keyCode == 9 || e.keyCode == 8)
        {
            return true;
        }

        var valor_actual = $(e.target).val();

        if(valor_actual.length < 8)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    return false;
}
</script>

@endsection
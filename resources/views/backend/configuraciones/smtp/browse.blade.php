@extends('backend.template.master')

@section('title', $title_page)

@section('style')
@endsection

@section('contenido')
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">{{$title_page}}</h4>
            </div>

            <form action="#" id="formulario_editar">


                <div class="row">

                    <div class="col-md-12">
                        <div class="card">
                            
                            <div class="card-body">
                                
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="MAIL_FROM_ADDRESS">MAIL_FROM_ADDRESS: <span class="text-danger">*</span></label>
                                        <input type="text" name="MAIL_FROM_ADDRESS" id="MAIL_FROM_ADDRESS" class="form-control" value="{{$row_obj->MAIL_FROM_ADDRESS}}">
                                    </div>
                                </div>
                                
                                <div class="row mt-2">
                                    <div class="col-md-12">
                                        <label for="MAIL_DRIVER">MAIL_DRIVER: <span class="text-danger">*</span></label>
                                        <input type="text" name="MAIL_DRIVER" id="MAIL_DRIVER" class="form-control" value="{{$row_obj->MAIL_DRIVER}}">
                                    </div>
                                </div>
                                
                                <div class="row mt-2">

                                    <div class="col-md-12">
                                        <label for="MAIL_HOST">MAIL_HOST: <span class="text-danger">*</span></label>
                                        <input type="text" name="MAIL_HOST" id="MAIL_HOST" class="form-control" value="{{$row_obj->MAIL_HOST}}">
                                    </div>
                                </div>
                                
                                <div class="row mt-2">

                                    <div class="col-md-12">
                                        <label for="MAIL_PORT">MAIL_PORT: <span class="text-danger">*</span></label>
                                        <input type="text" name="MAIL_PORT" id="MAIL_PORT" class="form-control" value="{{$row_obj->MAIL_PORT}}">
                                    </div>
                                </div>
                                
                                <div class="row mt-2">

                                    <div class="col-md-12">
                                        <label for="MAIL_USERNAME">MAIL_USERNAME: <span class="text-danger">*</span></label>
                                        <input type="text" name="MAIL_USERNAME" id="MAIL_USERNAME" class="form-control" value="{{$row_obj->MAIL_USERNAME}}">
                                    </div>
                                </div>
                                
                                <div class="row mt-2">

                                    <div class="col-md-12">
                                        <label for="MAIL_PASSWORD">MAIL_PASSWORD: <span class="text-danger">*</span></label>
                                        <input type="text" name="MAIL_PASSWORD" id="MAIL_PASSWORD" class="form-control" value="{{$row_obj->MAIL_PASSWORD}}">
                                    </div>
                                </div>
                                
                                <div class="row mt-2">

                                    <div class="col-md-12">
                                        <label for="MAIL_ENCRYPTION">MAIL_ENCRYPTION: <span class="text-danger">*</span></label>
                                        <input type="text" name="MAIL_ENCRYPTION" id="MAIL_ENCRYPTION" class="form-control" value="{{$row_obj->MAIL_ENCRYPTION}}">
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row justify-content-center">
                    <div class="col-md-4" style="text-align: center;">
                        <button class='{{$config_buttons["edit"]["class"]}}'>
                            <i class='fa fa-save'></i>
                            Guardar Cambios
                        </button>
                    </div>
                </div>
            </form>
		</div>
    </div>
</div>
@endsection


@section("js_code")

<script type="text/javascript">

$(document).ready(function(){  
});

$("#formulario_editar").submit(function(){
    editar();
    return false;
});

function editar()
{
    var formdata = new FormData();
    
    formdata.append("MAIL_FROM_ADDRESS",$("#MAIL_FROM_ADDRESS").val());
    formdata.append("MAIL_DRIVER",$("#MAIL_DRIVER").val());
    formdata.append("MAIL_HOST",$("#MAIL_HOST").val());
    formdata.append("MAIL_PORT",$("#MAIL_PORT").val());
    formdata.append("MAIL_USERNAME",$("#MAIL_USERNAME").val());
    formdata.append("MAIL_PASSWORD",$("#MAIL_PASSWORD").val());
    formdata.append("MAIL_ENCRYPTION",$("#MAIL_ENCRYPTION").val());

    $.ajax({
        url: "{{$link_controlador}}update",
        type: "POST",
        contentType: false,
        cache: false,
        processData:false,
        data: formdata,
        headers:
        {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function(event){
          abrir_loading();
        },
        success: function(data)
        {
            cerrar_loading();

            try
            {
              if(data["response"] == true)
              {
                $("#modal_editar").modal("hide");

                mostrar_mensajes_success(
                    "{{$abm_messages['success_edit']['title']}}",
                    "{{$abm_messages['success_edit']['description']}}",
                    "{{$link_controlador}}"
                );
              }
              else
              {
                mostrar_mensajes_errores(data["messages_errors"]);
              }
            }
            catch(e)
            {
                mostrar_mensajes_errores();
            }
        },
        error: function(error){
          cerrar_loading();
          mostrar_mensajes_errores();
        },
    });
}
</script>

@endsection
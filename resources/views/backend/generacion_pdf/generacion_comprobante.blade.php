<!DOCTYPE html>
<html>
<head>
<title>Comprobante</title>

<!--<link href="{{asset('/assets/css/reset.css')}}" rel="stylesheet"> -->
   
<style type="text/css">

@page {
     margin-top: 10px;
     margin-left: 10px;
     margin-right: 10px;
    }

body{
    padding: 0px 0px 0px 0px;
    margin: 0px 0px 0px 0px;
    font-size: 11px;
} 

.tabla_bordeada{
    border-width: 1px;
    border-color: #000;
    border-style: solid;
}

.texto_dato_header{
    font-size: 13px;
    margin-top: 10px;
    padding-left: 50px;
}

.texto_dato_izquierdo_header{
    font-size: 12px;
    padding-top: 25px;
    padding-left: 20px;
}


.tx_resaltado_header{
    font-weight: bold;
}

.texto_original{
    font-size: 22px;
    font-weight: bold;
    text-align: center;
    padding-top: 10px;
    padding-bottom: 10px;
}

.tabla_sin_bordeado_superior
{
    border-top: 0px;
}

.titulo_nombre_header{
    font-size: 20px;
    font-weight: bold;
    padding-left: 20px;
    padding-top: 20px;
    padding-bottom: 20px;
}

.titulo_factura_header
{
    font-size: 20px;
    font-weight: bold;
    padding-left: 50px;
    padding-top: 20px;
    padding-bottom: 20px;
}

.tabla_detalles{
    font-size: 12px;
}

.th_gris{
    background-color: #cccccc;
    font-weight: bold;
}

</style>
</head>
<body>

    <table width="100%" class="tabla_header tabla_bordeada">
        <tr width="100%" style="text-align:center;">
            <td width="100%" style="text-align: center;">
                <span class="texto_original">ORIGINAL<span>
            </td>
        </tr>
    </table>
    <table width="100%" class="tabla_bordeada tabla_sin_bordeado_superior">
        <tr width="100%">
            <td>
                <h1 class='titulo_nombre_header'>Mario Ernesto Olivera Iribarren</h1>
            </td>
            <td>
                <h1 class='titulo_factura_header'>FACTURA C</h1>
            </td>
        </tr>
        <tr width="100%">
            <td width="50%">
                <table width="100%">
                    <tr width="100%">
                        <td  style="padding-top: 32px;">
                            <p class="texto_dato_izquierdo_header">
                                <span class="tx_resaltado_header">Razón Social:</span> Mario Ernesto Olivera Iribarren
                            </p>
                        </td>
                    </tr>
                    <tr width="100%">
                        <td  style="padding-top: 32px;">
                            <p class="texto_dato_izquierdo_header">
                                <span class="tx_resaltado_header">Domicilio Comercial:</span> Luis N Palma 3448
                            </p>
                        </td>
                    </tr>
                    <tr width="100%">
                        <td  style="padding-top: 32px;">
                            <p class="texto_dato_izquierdo_header">
                                <span class="tx_resaltado_header">Condición frente al IVA:  Responsable Monotributo</span>
                            </p>
                        </td>
                    </tr>
                </table>
            </td>
            <td width="50%">

                <table width="100%">
                    <tr width="100%">
                        <td style="padding-top: 10px;">
                            <p class="texto_dato_header"><span class="tx_resaltado_header">Punto de Venta:</span> 00001</p>
                        </td>
                        <td style="padding-top: 10px;">
                            <p class="texto_dato_header"><span class="tx_resaltado_header">Núm. de Comprobante:</span> 00000073</p>
                        </td>
                    </tr>
                    <tr width="100%">
                        <td colspan=2>
                            <p class="texto_dato_header"><span class="tx_resaltado_header">Fecha de Emisión:</span> 21/05/2018</p>   
                        </td>
                    </tr>
                    <tr width="100%">
                        <td colspan="2" style="padding-top: 20px;">
                            <p class="texto_dato_header"><span class="tx_resaltado_header">CUIT:</span> 20397182089</p>   
                        </td>
                    </tr>
                    <tr width="100%">
                        <td colspan="2">
                            <p class="texto_dato_header"><span class="tx_resaltado_header">Ingresos Brutos:</span> 0</p>   
                        </td>
                    </tr>
                    <tr width="100%">
                        <td colspan="2">
                            <p class="texto_dato_header"><span class="tx_resaltado_header">Fecha de inicio de actividades:</span> 16/07/2016</p>   
                        </td>
                    </tr>
                </table>

            </td>
        </tr>
    </table>

    <table width="100%" class="tabla_bordeada tabla_sin_bordeado_superior">
        <tr width="100%" >
            <td style="padding-left: 10px;padding-top: 5px;padding-bottom: 5px;">
                <p class="texto_dato_header"><span class="tx_resaltado_header">Período Facturado desde:</span> 20/05/2019</p>
            </td>
            <td style="padding-left: 10px;padding-top: 5px;padding-bottom: 5px;">
                <p class="texto_dato_header"><span class="tx_resaltado_header">Hasta:</span> 21/05/2019</p>
            </td>
            <td style="padding-left: 10px;padding-top: 5px;padding-bottom: 5px;">
                <p class="texto_dato_header"><span class="tx_resaltado_header">Fecha de Vto. para el pago:</span> 24/05/2019</p>
            </td>
        </tr>
    </table>

    <table width="100%" class="tabla_bordeada" style="margin-top: 3px;">
        <tr width="100%">
            <td style="padding-left: 10px;padding-top: 5px;padding-bottom: 5px;">
                <p class="texto_dato_header" ><span class="tx_resaltado_header">CUIT:</span> 298766785</p>
            </td>
            <td>
                <p class="texto_dato_header" style="padding-left: 10px;"><span class="tx_resaltado_header">Apellido y Nombre / Razón Social:</span> Marito Systems</p>
            </td>
        </tr>
        <tr width="100%">
            <td style="padding-left: 10px;padding-top: 5px;padding-bottom: 5px;">
                <p class="texto_dato_header"><span class="tx_resaltado_header">Condición frente al IVA:</span> IVA Responsable Inscripto</p>
            </td>
            <td>
                <p class="texto_dato_header"><span class="tx_resaltado_header">Domicilio:</span> Rocamora 123</p>
            </td>
        </tr>
        <tr width="100%">
            <td colspan="2" style="padding-left: 10px;padding-top: 5px;padding-bottom: 5px;">
                <p class="texto_dato_header"><span class="tx_resaltado_header">Condición de venta:</span> Contado</p>
            </td>
        </tr>
    </table>

    <table width="100%" class="tabla_bordeada tabla_detalles" style="margin-top: 3px;padding-top: 5px;padding-left: 5px;">
        <thead>
            <tr>
                <th class="th_gris">Código</th>
                <th class="th_gris">Producto/Servicio</th>
                <th class="th_gris">Cantidad</th>
                <th class="th_gris">U. Medida</th>
                <th class="th_gris">Precio. Unit</th>
                <th class="th_gris">% Bonif</th>
                <th class="th_gris">Imp. Bonif</th>
                <th class="th_gris">Subtotal</th>
            </tr>
        </thead>
        <tbody>
            <tbody>
                <tr>
                    <td >01</td>
                    <td >Programación web, ajustes back</td>
                    <td >1,0</td>
                    <td >Unidades</td>
                    <td >1000,00</td>
                    <td >0,00</td>
                    <td >0,0</td>
                    <td >1000,00</td>
                </tr>
            </tbody>
        </tbody>
    </table>


    


</body>
</html>
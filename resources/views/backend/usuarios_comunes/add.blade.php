@extends('backend.template.master')

@section('title', $title_page)

@section('style')
<link rel="stylesheet" href="<?php echo url("/assets/plugins/dropzone/basic.css")?>">
<link rel="stylesheet" href="<?php echo url("/assets/plugins/dropzone/dropzone.css")?>">

<link href="<?php echo url("/assets/plugins/lightGallery/dist/css/lightgallery.css")?>" rel="stylesheet">
@endsection

@section('contenido')
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">{{$title_page}}</h4>
            </div>
            
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">

                            <div class="card-header">
                                <div class="d-flex align-items-center">
                                    <a class='{{$config_buttons["go_back"]["class"]}}' href="{{ $link_controlador }}">
                                        <i class='{{$config_buttons["go_back"]["icon"]}}'></i>
                                        {{$config_buttons["go_back"]["title"]}}
                                    </a>
                                </div>
                            </div>
                            <form action="#" id="formulario_agregar">
                                <div class="card-body">
                                    
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h2>Datos personales</h2>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-2" style="text-align:center;">
                                            <img id="preview_foto_perfil" src="{{asset('/storage/imagenes/usuarios/default.jpg')}}" class="img-fluid">
                                            <div id="foto_perfil_dropzone" style="display:none;"></div>
                                            <input type="text" hidden="true" id="foto_perfil" name="foto_perfil" value="default.jpg">
                                            <button type="button" class="btn btn-sm btn-primary" style="margin-top: 2px;" onClick="cargarImagenDePerfil()">
                                                Cambiar
                                            </button>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row">

                                                <div class="col-md-3 mt-2">
                                                    <label for="correo">Correo: <span class="text-danger">*</span></label>
                                                    <input type="text" name="correo" id="correo" class="form-control">
                                                </div>

                                                <div class="col-md-3 mt-2">
                                                    <label for="usuario">Usuario: <span class="text-danger">*</span></label>
                                                    <input type="text" name="usuario" id="usuario" class="form-control">
                                                </div>
                                                
                                                <div class="col-md-3 mt-2">
                                                    <label for="password">Contraseña: <span class="text-danger">*</span></label>
                                                    <input type="password" name="password" id="password" class="form-control">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3 mt-2">
                                                    <label for="nombre">Nombre: <span class="text-danger">*</span></label>
                                                    <input type="text" name="nombre" id="nombre" class="form-control">
                                                </div>
                                                <div class="col-md-3 mt-2">
                                                    <label for="apellido">Apellido: <span class="text-danger">*</span></label>
                                                    <input type="text" name="apellido" id="apellido" class="form-control">
                                                </div>

                                                <div class="col-md-3 mt-2">
                                                    <label for="dni">DNI: <span class="text-danger">*</span></label>
                                                    <input type="text" name="dni" id="dni" class="form-control">
                                                </div>
                                            </div>

                                            <div class="row">

                                                <div class="col-md-3 mt-2">
                                                    <label for="pais">País: <span class="text-danger">*</span></label>
                                                    <input type="text" name="pais" id="pais" class="form-control">
                                                </div>

                                                <div class="col-md-3 mt-2">
                                                    <label for="provincia">Provincia: <span class="text-danger">*</span></label>
                                                    <input type="text" name="provincia" id="provincia" class="form-control">
                                                </div>

                                                <div class="col-md-3 mt-2">
                                                    <label for="ciudad">Ciudad: <span class="text-danger">*</span></label>
                                                    <input type="text" name="ciudad" id="ciudad" class="form-control">
                                                </div>

                                                <div class="col-md-3 mt-2">
                                                    <label for="domicilio">Domicilio: <span class="text-danger">*</span></label>
                                                    <input type="text" name="domicilio" id="domicilio" class="form-control">
                                                </div>
                                            </div>

                                            <div class="row">    
                                                <div class="col-md-3 mt-2">
                                                    <label for="caracteristica_telefono">Caracteristica Teléfono: <span class="text-danger">*</span></label>
                                                    <input type="text" name="caracteristica_telefono" id="caracteristica_telefono" class="form-control">
                                                </div>

                                                <div class="col-md-3 mt-2">
                                                    <label for="telefono">Teléfono: <span class="text-danger">*</span></label>
                                                    <input type="text" name="telefono" id="telefono" class="form-control">
                                                </div>

                                                <div class="col-md-3 mt-2">
                                                    <label for="margen_facturacion_categoria">Margen Fact. Categoria:</label>
                                                    <input type="text" name="margen_facturacion_categoria" id="margen_facturacion_categoria" class="form-control">
                                                </div>

                                                <div class="col-md-3 mt-2">
                                                    <label for="id_estado_usuario" >Estado: <span class="text-danger">*</span></label>
                                                    <select name="id_estado_usuario" id="id_estado_usuario"  class="form-control">
                                                        <option value="">Seleccionar</option>

                                                        @foreach($estados_usuarios as $estado_usuario)
                                                            <option value="{{$estado_usuario->id}}">{{$estado_usuario->estado}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            
                                        </div>

                                        <div class="col-md-12">
                                            <div class="row" style="margin-top: 20px;" id="galeria_dni">
                                                <div class="col-md-12">
                                                    <h2 >Fotos de DNI</h2>
                                                </div>

                                                <div class="col-md-6 mt-2">
                                                    <p class="text-center">DNI PARTE DELANTERA</p>
                                                    <div style="text-align: center;">
                                                        <a id="link_preview_dni_parte_delantera" href="{{asset('storage/imagenes/dni/no_image.jpg')}}">
                                                            <img  id="preview_dni_parte_delantera" class="img-fluid" style="margin:0 auto !important;" src="{{asset('storage/imagenes/dni/no_image.jpg')}}">
                                                        </a>
                                                    </div>
                                                    <div style="text-align:center;margin-top: 2px;">
                                                        <div id="file_dni_parte_delantera" style="display:none;"></div>
                                                        <input type="text" hidden="true" id="dni_parte_delantera" name="dni_parte_delantera" value="no_image.jpg">

                                                        <button type="button" class="btn btn-sm btn-primary" onclick='$("#file_dni_parte_delantera").click()'>
                                                            Cambiar
                                                        </button>
                                                    </div>
                                                </div>  
                                                <div class="col-md-6 mt-2">
                                                    <p class="text-center">DNI PARTE TRASERA</p>
                                                    <div style="text-align: center;">
                                                        <a id="link_preview_dni_parte_trasera" href="{{asset('storage/imagenes/dni/no_image.jpg')}}">
                                                            <img  id="preview_dni_parte_trasera" class="img-fluid" style="margin:0 auto !important;" src="{{asset('storage/imagenes/dni/no_image.jpg')}}">
                                                        </a>
                                                    </div>
                                                    <div style="text-align:center;margin-top: 2px;">
                                                        <div id="file_dni_parte_trasera" style="display:none;"></div>
                                                        <input type="text" hidden="true" id="dni_parte_trasera" name="dni_parte_trasera" value="no_image.jpg">

                                                        <button type="button" class="btn btn-sm btn-primary" onclick='$("#file_dni_parte_trasera").click()'>
                                                            Cambiar
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row mt-3">
                                        <div class="col-md-12">
                                            <h2>Datos datos fiscales</h2>
                                        </div>
                                    </div>

                                    <div class="row">

                                        <div class="col-md-3 mt-2">
                                            <label for="cuit">CUIT/CUIL: <span class="text-danger">*</span></label>
                                            <input type="text" name="cuit" id="cuit" class="form-control">
                                        </div> 

                                        <div class="col-md-3 mt-2">
                                            <label for="razon_social">Razon Social: <span class="text-danger">*</span></label>
                                            <input type="text" name="razon_social" id="razon_social" class="form-control">
                                        </div>

                                        <div class="col-md-3 mt-2">
                                            <label for="ingresos_brutos">Ingresos Brutos: <span class="text-danger">*</span></label>
                                            <input type="text" name="ingresos_brutos" id="ingresos_brutos" class="form-control">
                                        </div>
                                        
                                        <div class="col-md-3 mt-2">
                                            <label for="nro_inscripcion_municipal">Num. inscripcion municipal:</label>
                                            <input type="text" name="nro_inscripcion_municipal" id="nro_inscripcion_municipal" class="form-control">
                                        </div>

                                        <div class="col-md-3 mt-2">
                                            <label for="domicilio_comercial">Domicilio Comercial:</label>
                                            <input type="text" name="domicilio_comercial" id="domicilio_comercial" class="form-control">
                                        </div>

                                        <div class="col-md-3 mt-2">
                                            <label for="inicio_actividades">Inicio de Actividades: <span class="text-danger">*</span></label>
                                            <input type="text" name="inicio_actividades" id="inicio_actividades" class="form-control datepicker" readonly="true" style="background-color: #fff !important;">
                                        </div>

                                        <div class="col-md-3 mt-2">
                                            <label for="actividad">Actividad:</label>
                                            <input type="text" name="actividad" id="actividad" class="form-control">
                                        </div>

                                        <div class="col-md-3 mt-2">
                                            <label for="id_condicion_iva">Condición frente al iva <span class="text-danger">*</span></label>
                                            <select name="id_condicion_iva" id="id_condicion_iva" class="form-control">
                                                <option value="">Seleccionar</option>

                                                @foreach($condiciones_frente_al_iva as $condicion_frente_al_iva)
                                                <option value="{{$condicion_frente_al_iva->id}}">{{$condicion_frente_al_iva->descripcion}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="col-md-3 mt-2">
                                            <label for="trabaja_en_relacion_dependencia">¿Trabaja en relación de depencencia? <span class="text-danger">*</span></label>
                                            <select name="trabaja_en_relacion_dependencia" id="trabaja_en_relacion_dependencia" class="form-control">
                                                <option value="">Seleccionar</option>
                                                <option value="1">Si</option>
                                                <option value="0">No</option>
                                            </select>
                                        </div>

                                        <div class="col-md-3 mt-2">
                                            <label for="aporta_caja_previsional">¿Aporta caja previsional? <span class="text-danger">*</span></label>
                                            <select name="aporta_caja_previsional" id="aporta_caja_previsional" class="form-control">
                                                <option value="">Seleccionar</option>
                                                <option value="1">Si</option>
                                                <option value="0">No</option>
                                            </select>
                                        </div>

                                        <div class="col-md-3 mt-2">
                                            <label for="jubilado">¿Jubilado? <span class="text-danger">*</span></label>
                                            <select name="jubilado" id="jubilado" class="form-control">
                                                <option value="">Seleccionar</option>
                                                <option value="1">Si</option>
                                                <option value="0">No</option>
                                            </select>
                                        </div>

                                        <div class="col-md-3 mt-2">
                                            <label for="facturacion_anual_estimada">Fac. anual estimada:</label>
                                            <input type="text" name="facturacion_anual_estimada" id="facturacion_anual_estimada" class="form-control">
                                        </div>

                                        
                                        <div class="col-md-3 mt-2">
                                            <label for="local_comercial">¿Local comercial? <span class="text-danger">*</span></label>
                                            <select name="local_comercial" id="local_comercial" class="form-control">
                                                <option value="">Seleccionar</option>
                                                <option value="1">Si</option>
                                                <option value="0">No</option>
                                            </select>
                                        </div>

                                        <div class="col-md-3 mt-2">
                                            <label for="metros_cuadrados_local">Metros cuadrados</label>
                                            <input type="text" name="metros_cuadrados_local" id="metros_cuadrados_local" class="form-control">
                                        </div>

                                        <div class="col-md-3 mt-2">
                                            <label for="monto_alquileres">Si alquila, monto de alquileres</label>
                                            <input type="text" name="monto_alquileres" id="monto_alquileres" class="form-control">
                                        </div>

                                        <div class="col-md-3 mt-2">
                                            <label for="kw_consumidos">KW consumidos</label>
                                            <input type="text" name="kw_consumidos" id="kw_consumidos" class="form-control">
                                        </div>

                                        <div class="col-md-3 mt-2">
                                            <label for="obra_social">Obra social</label>
                                            <input type="text" name="obra_social" id="obra_social" class="form-control">
                                        </div>


                                    </div>

                                    <div class="row mt-3">
                                        <div class="col-md-12">
                                            <h2>Tus impuestos</h2>
                                        </div>
                                    </div>

                                    <div class="row">

                                        <div class="col-md-12 mt-2">
                                            <label for="tus_impuestos">Texto a mostrar<span class="text-danger">*</span></label>
                                            <textarea name="tus_impuestos" id="tus_impuestos" class="form-control" rows="7"></textarea>
                                        </div> 

                                    </div>

                                    <div class="row mt-5">
                                        <div class="col-md-12 mt-4" style="text-align: center;">
                                            <button class='{{$config_buttons["save"]["class"]}}'>
                                                <i class='{{$config_buttons["save"]["icon"]}}'></i>
                                                {{$config_buttons["save"]["title"]}} {{$entity}}
                                            </button>
                                        </div>
                                    </div>



                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </form>
		</div>
    </div>
</div>
@endsection



@section("js_code")

<script src="{{asset('/assets/plugins/dropzone/dropzone.min.js')}}"></script>

<script src="<?php echo url("/assets/plugins/lightGallery/dist/js/lightgallery-all.min.js")?>"></script>

<script type="text/javascript">

$(document).ready(function(){

    $('#galeria_dni').lightGallery({
        selector: 'a',
        thumbnail:true,
        animateThumb: false,
        showThumbByDefault: false,

        autoplay: false,
        autoplayControls: false,
        fullScreen: false,
        share: false,
    });

    Dropzone.autoDiscover = false;

    $("#foto_perfil_dropzone").dropzone({
        autoProcessQueue: true,
        url: "{{url('/backend/usuarios_comunes/upload_foto_perfil')}}",
        acceptedFiles: 'image/*',
        paramName: "file",
        uploadMultiple: false,
        chunking: true,
        chunkSize: 1000000,
        addRemoveLinks: true,
        dictRemoveFile : "Eliminar",

        init: function() {

            this.on("sending", function(file, xhr, formData) {
            abrir_loading();

            try{
                xhr.onreadystatechange = function() {

                    if (xhr.readyState == XMLHttpRequest.DONE) {

                        var respuesta = JSON.parse(xhr.responseText);

                        if(respuesta["done"] == undefined)
                        {
                            var file_name =  respuesta["name"];

                            $("#preview_foto_perfil").attr("src","{{asset('storage/imagenes/usuarios')}}/"+file_name);
                            $("#formulario_agregar [name=foto_perfil]").val(file_name);
                            cerrar_loading();
                        }
                    }
                }
            }
            catch(e)
            {
                
            }
            
        });

        this.on("dictResponseError",function(error){
            cerrar_loading();
        });

        this.on("removedfile", function (file) {
        });

        },
        success: function(file, response){
            cerrar_loading();
        },
        error: function(error)
        {
            cerrar_loading();
        },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
    });

    $("#file_dni_parte_delantera").dropzone({
        autoProcessQueue: true,
        url: "{{url('/backend/usuarios_comunes/upload_imagen_dni')}}",
        acceptedFiles: 'image/*',
        paramName: "file",
        uploadMultiple: false,
        chunking: true,
        chunkSize: 1000000,
        addRemoveLinks: true,
        dictRemoveFile : "Eliminar",

        init: function() {

            this.on("sending", function(file, xhr, formData) {
            abrir_loading();

            xhr.onreadystatechange = function() {

                if (xhr.readyState == XMLHttpRequest.DONE) {

                    var respuesta = JSON.parse(xhr.responseText);

                    if(respuesta["done"] == undefined)
                    {
                        var file_name =  respuesta["name"];

                        $("#preview_dni_parte_delantera").attr("src","{{asset('storage/imagenes/dni')}}/"+file_name);
                        $("#link_preview_dni_parte_delantera").attr("href","{{asset('storage/imagenes/dni')}}/"+file_name);
                        $("#formulario_agregar [name=dni_parte_delantera]").val(file_name);
                        cerrar_loading();
                    }
                }
            }
        });

        this.on("removedfile", function (file) {
        });

        },
        success: function(file, response){
            cerrar_loading();
        },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
    });

    $("#file_dni_parte_trasera").dropzone({
        autoProcessQueue: true,
        url: "{{url('/backend/usuarios_comunes/upload_imagen_dni')}}",
        acceptedFiles: 'image/*',
        paramName: "file",
        uploadMultiple: false,
        chunking: true,
        chunkSize: 1000000,
        addRemoveLinks: true,
        dictRemoveFile : "Eliminar",

        init: function() {

            this.on("sending", function(file, xhr, formData) {
            abrir_loading();

            xhr.onreadystatechange = function() {

                if (xhr.readyState == XMLHttpRequest.DONE) {

                    var respuesta = JSON.parse(xhr.responseText);

                    if(respuesta["done"] == undefined)
                    {
                        var file_name =  respuesta["name"];

                        $("#preview_dni_parte_trasera").attr("src","{{asset('storage/imagenes/dni')}}/"+file_name);
                        $("#link_preview_dni_parte_trasera").attr("href","{{asset('storage/imagenes/dni')}}/"+file_name);
                        $("#formulario_agregar [name=dni_parte_trasera]").val(file_name);
                        cerrar_loading();
                    }
                }
            }
        });

        this.on("removedfile", function (file) {
        });

        },
        success: function(file, response){
            cerrar_loading();
        },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
    });
    
});

function cargarImagenDePerfil()
{
    $("#foto_perfil_dropzone").click();
}

$("#formulario_agregar").submit(function(){
    agregar();
    return false;
});

function agregar()
{
    var formdata = new FormData();

    formdata.append("dni",$("#dni").val());
    formdata.append("cuit",$("#cuit").val());
    formdata.append("dni_parte_delantera",$("#dni_parte_delantera").val());
    formdata.append("dni_parte_trasera",$("#dni_parte_trasera").val());
    formdata.append("correo",$("#correo").val());
    formdata.append("usuario",$("#usuario").val());
    formdata.append("nombre",$("#nombre").val());
    formdata.append("apellido",$("#apellido").val());
    formdata.append("password",$("#password").val());
    formdata.append("id_estado_usuario",$("#id_estado_usuario").val());
    formdata.append("id_condicion_iva",$("#id_condicion_iva").val());
    formdata.append("razon_social",$("#razon_social").val());
    formdata.append("ingresos_brutos",$("#ingresos_brutos").val());
    formdata.append("inicio_actividades",$("#inicio_actividades").val());
    formdata.append("pais",$("#pais").val());
    formdata.append("provincia",$("#provincia").val());
    formdata.append("ciudad",$("#ciudad").val());
    formdata.append("domicilio",$("#domicilio").val());
    formdata.append("telefono",$("#telefono").val());
    formdata.append("caracteristica_telefono",$("#caracteristica_telefono").val());
    formdata.append("margen_facturacion_categoria",$("#margen_facturacion_categoria").val());
    formdata.append("foto_perfil",$("#foto_perfil").val());
    
    /*
    formdata.append("key_ws_afip",$("#key_ws_afip").val());
    formdata.append("csr_ws_afip",$("#csr_ws_afip").val());
    formdata.append("csr_ws_de_afip",$("#csr_ws_de_afip").val());
    */

    formdata.append("domicilio_comercial",$("#domicilio_comercial").val());
    formdata.append("nro_inscripcion_municipal",$("#nro_inscripcion_municipal").val());
    formdata.append("actividad",$("#actividad").val());
    formdata.append("trabaja_en_relacion_dependencia",$("#trabaja_en_relacion_dependencia").val());
    formdata.append("aporta_caja_previsional",$("#aporta_caja_previsional").val());
    formdata.append("jubilado",$("#jubilado").val());
    formdata.append("facturaccion_anual_estimada",$("#facturaccion_anual_estimada").val());
    formdata.append("local_comercial",$("#local_comercial").val());
    formdata.append("metros_cuadrados_local",$("#metros_cuadrados_local").val());
    formdata.append("monto_alquileres",$("#monto_alquileres").val());
    formdata.append("kw_consumidos",$("#kw_consumidos").val());
    formdata.append("obra_social",$("#obra_social").val());
    formdata.append("facturacion_anual_estimada",$("#facturacion_anual_estimada").val());

    formdata.append("tus_impuestos",$("#tus_impuestos").val());

    $.ajax({
        url: "{{$link_controlador}}store",
        type: "POST",
        contentType: false,
        cache: false,
        processData:false,
        data: formdata,
        headers:
        {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function(event){
          abrir_loading();
        },
        success: function(data)
        {
            cerrar_loading();

            try
            {
              if(data["response"] == true)
              {
                $("#modal_agregar").modal("hide");

                mostrar_mensajes_success(
                    "{{$abm_messages['success_add']['title']}}",
                    "{{$abm_messages['success_add']['description']}}",
                    "{{$link_controlador}}"
                );
              }
              else
              {
                mostrar_mensajes_errores(data["messages_errors"]);
              }
            }
            catch(e)
            {
              mostrar_mensajes_errores();
            }

        },
        error: function(error){
          cerrar_loading();
          mostrar_mensajes_errores();
        },
    });
}
</script>

@endsection
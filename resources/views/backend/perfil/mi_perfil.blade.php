@extends('backend.template.master')

@section('title', $title_page)

@section('style')
<link rel="stylesheet" href="<?php echo url("/assets/plugins/dropzone/basic.css")?>">
<link rel="stylesheet" href="<?php echo url("/assets/plugins/dropzone/dropzone.css")?>">

<link href="<?php echo url("/assets/plugins/lightGallery/dist/css/lightgallery.css")?>" rel="stylesheet">
@endsection

@section('contenido')
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">{{$title_page}}</h4>
            </div>

            <div class="row">
                
                <div class="col-md-12">
                    <div class="card">

                        <div class="card-header">
                            <div class="d-flex align-items-center">
                                <a class='{{$config_buttons["go_back"]["class"]}}' href="{{ $link_controlador }}">
                                    <i class='{{$config_buttons["go_back"]["icon"]}}'></i>
                                    {{$config_buttons["go_back"]["title"]}}
                                </a>
                            </div>
                        </div>

                        <form action="#" id="formulario_editar">
                            <div class="card-body">
                                
                                <div class="row">
                                    <div class="col-md-2" style="text-align:center;">
                                        <img id="preview_foto_perfil" src="{{asset('/storage/imagenes/usuarios/'.$row_obj->foto_perfil)}}" class="img-fluid">
                                        <div id="foto_perfil_dropzone" style="display:none;"></div>
                                        <input type="text" hidden="true" id="foto_perfil" name="foto_perfil" value="{{$row_obj->foto_perfil}}">
                                        <button type="button" class="btn btn-sm btn-primary" style="margin-top: 2px;" onClick="cargarImagenDePerfil()">
                                            Cambiar
                                        </button>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-md-3 mt-2">
                                                <label for="correo">Correo: <span class="text-danger">*</span></label>
                                                <input type="text" name="correo" id="correo" class="form-control" value="{{$row_obj->correo}}">
                                            </div>
                                            
                                            <div class="col-md-3 mt-2">
                                                <label for="password_new">Cambiar Contraseña: <span class="text-danger">*</span></label>
                                                <input type="password_new" name="password_new" id="password_new" class="form-control" value="">
                                            </div>

                                            <div class="col-md-3 mt-2">
                                                <label for="password_new_2">Repetir Contraseña: <span class="text-danger">*</span></label>
                                                <input type="password_new_2" name="password_new_2" id="password_new_2" class="form-control" value="">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 mt-2">
                                                <label for="nombre">Nombre: <span class="text-danger">*</span></label>
                                                <input type="text" name="nombre" id="nombre" class="form-control" value="{{$row_obj->nombre}}">
                                            </div>
                                            <div class="col-md-3 mt-2">
                                                <label for="apellido">Apellido: <span class="text-danger">*</span></label>
                                                <input type="text" name="apellido" id="apellido" class="form-control" value="{{$row_obj->apellido}}">
                                            </div>

                                            <div class="col-md-3 mt-2">
                                                <label for="dni">DNI: <span class="text-danger">*</span></label>
                                                <input type="text" name="dni" id="dni" class="form-control" value="{{$row_obj->dni}}">
                                            </div>

                                            <div class="col-md-3 mt-2">
                                                <label for="cuit">CUIT/CUIL: <span class="text-danger">*</span></label>
                                                <input type="text" name="cuit" id="cuit" class="form-control"  value="{{$row_obj->cuit}}">
                                            </div>

                                            <div class="col-md-4 mt-2">
                                                <label for="pais">País: <span class="text-danger">*</span></label>
                                                <input type="text" name="pais" id="pais" class="form-control" value="{{$row_obj->pais}}">
                                            </div>

                                            <div class="col-md-4 mt-2">
                                                <label for="provincia">Provincia: <span class="text-danger">*</span></label>
                                                <input type="text" name="provincia" id="provincia" class="form-control" value="{{$row_obj->provincia}}">
                                            </div>

                                            <div class="col-md-4 mt-2">
                                                <label for="ciudad">Ciudad: <span class="text-danger">*</span></label>
                                                <input type="text" name="ciudad" id="ciudad" class="form-control"  value="{{$row_obj->ciudad}}">
                                            </div>

                                            <div></div>
                                            
                                            <div class="col-md-4 mt-2">
                                                <label for="caracteristica_telefono">Caracteristica Teléfono: <span class="text-danger">*</span></label>
                                                <input type="text" name="caracteristica_telefono" id="caracteristica_telefono" class="form-control" value="{{$row_obj->caracteristica_telefono}}">
                                            </div>

                                            <div class="col-md-4 mt-2">
                                                <label for="telefono">Teléfono: <span class="text-danger">*</span></label>
                                                <input type="text" name="telefono" id="telefono" class="form-control"  value="{{$row_obj->telefono}}">
                                            </div>

                                            <div class="col-md-4 mt-2">
                                                <label for="id_estado_usuario" >Estado: <span class="text-danger">*</span></label>
                                                <select name="id_estado_usuario" id="id_estado_usuario"  class="form-control"  value="{{$row_obj->id_estado_usuario}}">
                                                    <option value="">Seleccionar</option>

                                                </select>
                                            </div>
                                        </div>

                                        
                                    </div>

                                    <div class="col-md-12">
                                        <div class="row" style="margin-top: 20px;" id="galeria_dni">
                                            <div class="col-md-12">
                                                <h2 >Fotos de DNI</h2>
                                            </div>
                                            <div class="col-md-4 mt-2">
                                                <p class="text-center">DNI PARTE DELANTERA</p>
                                                <div style="text-align: center;">
                                                    <a id="link_preview_dni_parte_delantera" href="{{asset('storage/imagenes/dni/'.$row_obj->dni_parte_delantera)}}">
                                                        <img  id="preview_dni_parte_delantera" class="img-fluid" style="margin:0 auto !important;" src="{{asset('storage/imagenes/dni/'.$row_obj->dni_parte_delantera)}}">
                                                    </a>
                                                </div>
                                                <div style="text-align:center;margin-top: 2px;">
                                                    <div id="file_dni_parte_delantera" style="display:none;"></div>
                                                    <input type="text" hidden="true" id="dni_parte_delantera" name="dni_parte_delantera" value="{{$row_obj->dni_parte_delantera}}">

                                                    <button type="button" class="btn btn-sm btn-primary" onclick='$("#file_dni_parte_delantera").click()'>
                                                        Cambiar
                                                    </button>
                                                </div>
                                            </div>  
                                            <div class="col-md-4 mt-2">
                                                <p class="text-center">DNI PARTE TRASERA</p>
                                                <div style="text-align: center;">
                                                    <a id="link_preview_dni_parte_trasera" href="{{asset('storage/imagenes/dni/'.$row_obj->dni_parte_trasera)}}">
                                                        <img  id="preview_dni_parte_trasera" class="img-fluid" style="margin:0 auto !important;"  src="{{asset('storage/imagenes/dni/'.$row_obj->dni_parte_trasera)}}"">
                                                    </a>
                                                </div>
                                                <div style="text-align:center;margin-top: 2px;">
                                                    <div id="file_dni_parte_trasera" style="display:none;"></div>
                                                    <input type="text" hidden="true" id="dni_parte_trasera" name="dni_parte_trasera" value="{{$row_obj->dni_parte_trasera}}">

                                                    <button type="button" class="btn btn-sm btn-primary" onclick='$("#file_dni_parte_trasera").click()'>
                                                        Cambiar
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 mt-4" style="text-align: center;">
                                        <button class='{{$config_buttons["edit"]["class"]}}'>
                                            <i class='{{$config_buttons["edit"]["icon"]}}'></i>
                                            {{$config_buttons["edit"]["title"]}} {{$entity}}
                                        </button>
                                    </div>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
			</div>
		</div>
    </div>
</div>
@endsection


@section("js_code")
<script src="{{asset('/assets/plugins/dropzone/dropzone.min.js')}}"></script>

<script src="<?php echo url("/assets/plugins/lightGallery/dist/js/lightgallery-all.min.js")?>"></script>

<script type="text/javascript">

$(document).ready(function(){

    $("#id_estado_usuario").val(<?php echo $row_obj->id_estado_usuario?>);

    $('#galeria_dni').lightGallery({
        selector: 'a',
        thumbnail:true,
        animateThumb: false,
        showThumbByDefault: false,

        autoplay: false,
        autoplayControls: false,
        fullScreen: false,
        share: false,
    });

    Dropzone.autoDiscover = false;

    $("#foto_perfil_dropzone").dropzone({
        autoProcessQueue: true,
        url: "{{url('/backend/usuarios/upload_foto_perfil')}}",
        acceptedFiles: 'image/*',
        paramName: "file",
        uploadMultiple: false,
        chunking: true,
        chunkSize: 1000000,
        addRemoveLinks: true,
        dictRemoveFile : "Eliminar",

        init: function() {

            this.on("sending", function(file, xhr, formData) {
            abrir_loading();

            xhr.onreadystatechange = function() {

                if (xhr.readyState == XMLHttpRequest.DONE) {

                    var respuesta = JSON.parse(xhr.responseText);

                    if(respuesta["done"] == undefined)
                    {
                        var file_name =  respuesta["name"];

                        $("#preview_foto_perfil").attr("src","{{asset('storage/imagenes/usuarios')}}/"+file_name);
                        $("#formulario_editar [name=foto_perfil]").val(file_name);
                        cerrar_loading();
                    }
                }
            }
        });

        this.on("removedfile", function (file) {
        });

        },
        success: function(file, response){
            cerrar_loading();
        },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
    });

    $("#file_dni_parte_delantera").dropzone({
        autoProcessQueue: true,
        url: "{{url('/backend/usuarios/upload_imagen_dni')}}",
        acceptedFiles: 'image/*',
        paramName: "file",
        uploadMultiple: false,
        chunking: true,
        chunkSize: 1000000,
        addRemoveLinks: true,
        dictRemoveFile : "Eliminar",

        init: function() {

            this.on("sending", function(file, xhr, formData) {
            abrir_loading();

            xhr.onreadystatechange = function() {

                if (xhr.readyState == XMLHttpRequest.DONE) {

                    var respuesta = JSON.parse(xhr.responseText);

                    if(respuesta["done"] == undefined)
                    {
                        var file_name =  respuesta["name"];

                        $("#preview_dni_parte_delantera").attr("src","{{asset('storage/imagenes/dni')}}/"+file_name);
                        $("#link_preview_dni_parte_delantera").attr("href","{{asset('storage/imagenes/dni')}}/"+file_name);
                        $("#formulario_editar [name=dni_parte_delantera]").val(file_name);
                        cerrar_loading();
                    }
                }
            }
        });

        this.on("removedfile", function (file) {
        });

        },
        success: function(file, response){
            cerrar_loading();
        },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
    });

    $("#file_dni_parte_trasera").dropzone({
        autoProcessQueue: true,
        url: "{{url('/backend/usuarios/upload_imagen_dni')}}",
        acceptedFiles: 'image/*',
        paramName: "file",
        uploadMultiple: false,
        chunking: true,
        chunkSize: 1000000,
        addRemoveLinks: true,
        dictRemoveFile : "Eliminar",

        init: function() {

            this.on("sending", function(file, xhr, formData) {
            abrir_loading();

            xhr.onreadystatechange = function() {

                if (xhr.readyState == XMLHttpRequest.DONE) {

                    var respuesta = JSON.parse(xhr.responseText);

                    if(respuesta["done"] == undefined)
                    {
                        var file_name =  respuesta["name"];

                        $("#preview_dni_parte_trasera").attr("src","{{asset('storage/imagenes/dni')}}/"+file_name);
                        $("#link_preview_dni_parte_trasera").attr("href","{{asset('storage/imagenes/dni')}}/"+file_name);
                        $("#formulario_editar [name=dni_parte_trasera]").val(file_name);
                        cerrar_loading();
                    }
                }
            }
        });

        this.on("removedfile", function (file) {
        });

        },
        success: function(file, response){
            cerrar_loading();
        },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
    });
    
});

function cargarImagenDePerfil()
{
    $("#foto_perfil_dropzone").click();
}

$("#formulario_editar").submit(function(){
    editar();
    return false;
});

function editar()
{
    var formdata = new FormData();

    formdata.append("dni",$("#dni").val());
    formdata.append("cuit",$("#cuit").val());
    formdata.append("dni_parte_delantera",$("#dni_parte_delantera").val());
    formdata.append("dni_parte_trasera",$("#dni_parte_trasera").val());
    formdata.append("correo",$("#correo").val());
    formdata.append("nombre",$("#nombre").val());
    formdata.append("apellido",$("#apellido").val());
    formdata.append("password",$("#password").val());
    formdata.append("id_estado_usuario",$("#id_estado_usuario").val());
    formdata.append("pais",$("#pais").val());
    formdata.append("provincia",$("#provincia").val());
    formdata.append("ciudad",$("#ciudad").val());
    formdata.append("telefono",$("#telefono").val());
    formdata.append("caracteristica_telefono",$("#caracteristica_telefono").val());
    formdata.append("foto_perfil",$("#foto_perfil").val());
    
    formdata.append("key_ws_afip",$("#key_ws_afip").val());
    formdata.append("csr_ws_afip",$("#csr_ws_afip").val());
    formdata.append("csr_ws_de_afip",$("#csr_ws_de_afip").val());

    $.ajax({
        url: "{{$link_controlador}}update",
        type: "POST",
        contentType: false,
        cache: false,
        processData:false,
        data: formdata,
        headers:
        {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function(event){
          abrir_loading();
        },
        success: function(data)
        {
            cerrar_loading();

            try
            {
              if(data["response"] == true)
              {
                $("#modal_editar").modal("hide");

                mostrar_mensajes_success(
                    "{{$abm_messages['success_edit']['title']}}",
                    "{{$abm_messages['success_edit']['description']}}",
                    "{{$link_controlador}}"
                );
              }
              else
              {
                mostrar_mensajes_errores(data["messages_errors"]);
              }
            }
            catch(e)
            {
                mostrar_mensajes_errores();
            }

        },
        error: function(error){
          cerrar_loading();
          mostrar_mensajes_errores();
        },
    });
}
</script>

@endsection
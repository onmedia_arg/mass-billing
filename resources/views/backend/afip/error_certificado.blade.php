@extends('backend.template.master')

@section('title', 'Escritorio')

@section('contenido')
<div class="main-panel">
    <div class="content">
        <div class="panel-header bg-primary-gradient">
            <div class="page-inner py-5">
                <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                    <div>
                        <h2 class="text-white pb-2 fw-bold">Error certificado</h2>
                        <h5 class="text-white op-7 mb-2">{{Config("app.name")}}</h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="page-inner mt--5">

            <div class="row mt-2 justify-content-center">
                <div class="col-md-4">
                    <div class="card full-height">
                        <div class="card-body">
                            <div class="card-title">ERROR CERTIFICADO AFIP</div>

                            <div class="row" style="margin-top: 20px;">
                                <div class="col-md-12 text-center">
                                    <img src="{{asset('/admin/assets/img/error.png')}}" alt="" width="200px">
                                </div>
                                <div class="col-md-12 mt-3">
                                    <p class="text-center text-danger" style="font-size: 16px;font-weight: bold;">
                                        Lo sentimos, ha ocurrido un error con su certificado de AFIP, si sigue sucediendo contactese con nosotros
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection

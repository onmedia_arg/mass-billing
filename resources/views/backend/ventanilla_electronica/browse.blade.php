@extends('backend.template.master')

@section('title', $title_page)

@section('contenido')
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">{{$title_page}}</h4>
            </div>


            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-info" role="alert">
                        El Domicilio Fiscal Electrónico es un domicilio virtual, gratuito, seguro, personalizado y valido para la recepción de comunicaciones de AFIP y otros organismos, con la finalidad de facilitar el cumplimiento de las obligaciones.

                        Este domicilio es obligatorio y produce, en el ámbito administrativo, los efectos del domicilio fiscal constituido, por lo que, a través de las comunicaciones que se reciban por este medio, se considerará al contribuyente como efectivamente notificado.
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-md-12 mt-3" >
                    
                    <div class="card mt-5">
                        <div class="card-body">

                            <div class="table-responsive">
                                <table id="tabla_listado" class="display table table-striped table-hover" >
                                    <thead>
                                        <tr>
                                            @foreach($columns as $column)
                                            <th>{{$column["name"]}}</th>
                                            @endforeach
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>
    </div>
</div>
@endsection


@section("modals")

@endsection

@section("js_code")

<script type="text/javascript">
var listado = null;
var id_trabajando = 0;

$(document).ready( function () {
    
    listado= $('#tabla_listado').DataTable( {
        "processing": true,
        "serverSide": true,
        "responsive":false,
        "ordering": false,
        "ajax":{
        url : "{{$link_controlador}}get_listado_dt", // json datasource
        type: "post",
        data: {
            
        },
        headers:
        {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        error: function(error){
            $(".employee-grid-error").html("");
            $("#employee-grid").append('<tbody class="employee-grid-error"><tr><th colspan="3">No hay datos</th></tr></tbody>');
            $("#employee-grid_processing").css("display","none");
        }
        }
    });
});


function consumirComunicacion(idComunicacion)
{
    $.ajax({
        url: "{{$link_controlador}}consumirComunicacion",
        type: "POST",
        data: {
            idComunicacion:idComunicacion
        },
        headers:
        {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function(event){
            abrir_loading();
        },
        success: function(data)
        {
            cerrar_loading();

            try
            {

                if(data["response"])
                {
                    $("#asunto_ver_comunicacion").val(data["data"]["asunto"]);
                    $("#mensaje_ver_comunicacion").val(data["data"]["mensaje"]);

                    $("#modal_ver_comunicacion").modal("show");
                }
                else
                {
                    mostrar_mensajes_errores(data["messages_errors"]);
                }
            }
            catch(e)
            {
                mostrar_mensajes_errores();
            }
        },
        error: function(error){
            cerrar_loading();
            mostrar_mensajes_errores();
        },
    });
}
</script>

@endsection

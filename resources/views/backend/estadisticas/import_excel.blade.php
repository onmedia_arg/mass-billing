@extends('backend.template.master')

@section('title', $title_page)

@section('style')

<style>

#contenedor_mensajes
{
    height: 300px; overflow-y: scroll;padding: 5px 5px 5px 5px; background-color: #DDD;
}

.mensaje_consola
{
    font-weight: bold;
}

</style>
@endsection

@section('contenido')
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <h4 class="page-title">{{$title_page}}</h4>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">Importación de archivo</div>
                        </div>
                        <div class="card-body">
                            <div class="row">

                                <div class="col-md-12">
                                    <p style="text-align: right;">Tipos de archivos permitidos:&nbsp;&nbsp;&nbsp;<span class="badge badge-success">.xlsx</span></p>
                                </div>

                                <div class="col-md-12">

                                    <h5>Salida de mensajes importación</h5>

                                    <div style="" id="contenedor_mensajes">
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div style="text-align: center;">
                                        <button style="margin-top: 50px;" class="btn btn-lg btn-primary" onclick="$('#upload_excel_import_stats_dropzone').click()">
                                            <i class="fa fa-file"></i> Subir archivo
                                        </button>
                                    </div>
                                </div>

                                <div id="upload_excel_import_stats_dropzone" style="display: none;">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section("modals")

<div class="modal" tabindex="-1" role="dialog" id="modal_importacion" data-backdrop="false" data-keyboard="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Realizando importación</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div clasS="col-md-12">
            <div class="progress">
                <div id="barra_progreso" class="progress-bar" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">0%</div>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection


@section("js_code")

<script src="{{asset('/assets/plugins/dropzone/dropzone.min.js')}}"></script>

<script>

var porcentaje_test = 0;
var importacion_pendiente = {{$id_importacion_pendiente}};

var id_importacion_trabajando = null;

$(document).ready(function(){

    if(importacion_pendiente > 0)
    {
        $("#contenedor_mensajes").html("");
        realizarImportacionPendiente();
    }
});


$("#upload_excel_import_stats_dropzone").dropzone({
    autoProcessQueue: true,
    url: "{{url('/backend/estadisticas/upload_excel_import_stats')}}",
    acceptedFiles: '.xlsx',
    paramName: "file",
    uploadMultiple: false,
    chunking: true,
    chunkSize: 1000000,
    addRemoveLinks: true,
    dictRemoveFile : "Eliminar",

    init: function() {

        this.on("sending", function(file, xhr, formData) {
        abrir_loading();

        try{
            xhr.onreadystatechange = function() {

                if (xhr.readyState == XMLHttpRequest.DONE) {

                    var respuesta = JSON.parse(xhr.responseText);

                    if(respuesta["done"] == undefined)
                    {
                        cerrar_loading();
                        $("#contenedor_mensajes").html("");
                        realizarImportacionPendiente();
                    }
                }
            }
        }
        catch(e)
        {

        }
        
    });

    this.on("dictResponseError",function(error){
        alert("ERROR!");
    });

    this.on("removedfile", function (file) {
    });

    },
    success: function(file, response){
        cerrar_loading();
    },
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
});

function realizarImportacionPendiente()
{
    $("#modal_importacion").modal("show");

    $.ajax({
        url: "{{url('/backend/estadisticas/realizar_importacion_stats')}}",
        type: 'POST',
        data: 
        {},
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (data) 
        {
            
            try
            {
                for(var i=0; i < data["data"]["mensajes"].length;i++)
                {
                    $("#contenedor_mensajes").append("<p class='mensaje_consola'>"+data["data"]["mensajes"][i]+"</p>");
                }

                $("#contenedor_mensajes").animate({ scrollTop: $('#contenedor_mensajes')[0].scrollHeight}, 1000);

                if(data["data"]["id_importacion"] != null)
                {
                    id_importacion_trabajando = data["data"]["id_importacion"];

                    cambiarPorcentajeBarraProgreso(data["data"]["porcentaje"]);

                    if(data["data"]["porcentaje"] < 100)
                    {
                        realizarImportacionPendiente();
                    }
                    else
                    {
                        if(data["data"]["cant_importaciones_pendientes"] == 0)
                        {
                            cambiarPorcentajeBarraProgreso(100);
                            $("#modal_importacion").modal("hide");
                        }
                        else
                        {
                            cambiarPorcentajeBarraProgreso(100);
                            realizarImportacionPendiente();    
                        }
                    }
                }
                else
                {
                    cambiarPorcentajeBarraProgreso(0);
                    $("#modal_importacion").modal("hide");
                }
            }
            catch(e)
            {
                //alert("ERROR");
            }
            
        },
        error: function(error)
        {
            //alert("ERROR");
            cambiarPorcentajeBarraProgreso(0);
            $("#modal_importacion").modal("hide");
        }
    });
}

function cambiarPorcentajeBarraProgreso(porcentaje)
{
    if(!isNaN(porcentaje))
    {
        porcentaje = porcentaje.toFixed(2);
    }

    if(porcentaje <= 100)
    {
        $("#barra_progreso").attr("aria-valuenow",porcentaje);
        $("#barra_progreso").css("width",porcentaje+"%");
        $("#barra_progreso").text(porcentaje+"%");
    }
}

</script>
@endsection

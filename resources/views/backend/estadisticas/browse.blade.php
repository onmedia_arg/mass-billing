@extends('backend.template.master')

@section('title', $title_page)

@section('contenido')
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <h4 class="page-title">Estadisticas</h4>
			<!--<div class="page-category">Ejemplo de estadisticas</div>-->
			
			<div class="row">
				<div class="col-md-12">

					<div class="card">
                        <div class="card-header">
                            <div class="card-title">Selector estadísticas a ver</div>
                        </div>
                        <div class="card-body">
							<form action="" method="post" id="formulario_filtro">
								{{ csrf_field() }}

								@if($id_rol == 1)
								<div class="row mt-3">
									<div class="col-md-12">
										<h4>VER DATOS DEL CUIT: <span class="text-danger">*</span></h4>
										
										<select class="form-control" name="cliente_cuit_importacion" id="cliente_cuit_importacion"> 
											<option value="{{$cliente_cuit_importacion}}">{{$cliente_cuit_importacion}}</option>
										</select>

									</div>
								</div>
								@endif
								
								<div class="row mt-3">
									<div class="col-md-12">
										<h4>Seleccionar la información a ver <span class="text-danger">*</span></h4>
										
										<select class="form-control" name="grafico_a_mostrar" id="grafico_a_mostrar"> 
											<option value="1">Comprobantes emitidos vs recibidos (Importe en $)</option>
											<option value="2">Comprobantes emitidos vs consumo (“Total de gastos en $”) </option>
											<option value="3">Comprobantes emitidos por Denominación del receptor (Importe en $)</option>
											<option value="4">Comprobantes recibidos por Denominación del emisor (Importe en $)</option>
											<option value="5">Facturación mensual vs Facturación promedio</option>
											<option value="6">Consumo total vs consumo discriminado (Tarjeta, débito, compra u$s)</option>
											<option value="7">Comprobantes emitidos vs Denominación del receptor</option>
											<option value="8">Comprobantes recibidos vs Denominación del emisor</option>
										</select>

									</div>
								</div>

								<div class="row mt-3" id="contenedor_filtro_fecha" style="display: none;">
									<div class="col-md-6">
										<div class="row">
											<div class="col-md-12">
												<h4>Fecha desde:</h4>
											</div>
										</div>
										<div class="row">
											
											<div class="col-md-6">
												<label for="mes_desde">Mes desde: <span class="text-danger">*</span></label>
												<select name="mes_desde" id="mes_desde" class="form-control">
													<option value="1">Enero</option>
													<option value="2">Febrero</option>
													<option value="3">Marzo</option>
													<option value="4">Abril</option>
													<option value="5">Mayo</option>
													<option value="6">Junio</option>
													<option value="7">Julio</option>
													<option value="8">Agosto</option>
													<option value="9">Septiembre</option>
													<option value="10">Octubre</option>
													<option value="11">Noviembre</option>
													<option value="12">Diciembre</option>
												</select>
											</div>
											
											<div class="col-md-6">
												<label for="anio_desde">Año desde: <span class="text-danger">*</span></label>
												<select name="anio_desde" id="anio_desde" class="form-control">
													@for($i=1999; $i <= Date("Y");$i++ )
													<option value="{{$i}}">{{$i}}</option>
													@endfor
												</select>
											</div>

										</div>

										
									</div>
									<div class="col-md-6">

										<div class="row">
											<div class="col-md-12">
												<h4>Fecha hasta:</h4>
											</div>
										</div>

										<div class="row">
											
											<div class="col-md-6">
												<label for="mes_hasta">Mes hasta: <span class="text-danger">*</span></label>
												<select name="mes_hasta" id="mes_hasta" class="form-control">
													<option value="1">Enero</option>
													<option value="2">Febrero</option>
													<option value="3">Marzo</option>
													<option value="4">Abril</option>
													<option value="5">Mayo</option>
													<option value="6">Junio</option>
													<option value="7">Julio</option>
													<option value="8">Agosto</option>
													<option value="9">Septiembre</option>
													<option value="10">Octubre</option>
													<option value="11">Noviembre</option>
													<option value="12">Diciembre</option>
												</select>
											</div>
											
											<div class="col-md-6">
												<label for="anio_hasta">Año hasta: <span class="text-danger">*</span></label>
												<select name="anio_hasta" id="anio_hasta" class="form-control">
													@for($i=1999; $i <= Date("Y");$i++ )
													<option value="{{$i}}">{{$i}}</option>
													@endfor
												</select>
											</div>

										</div>

									</div>
								</div>

								<div class="row mt-3" id="contenedor_filtro_cuit" style="display: none;">
									<div class="col-md-12">
										<h4>Seleccionar cuit <span class="text-danger">*</span></h4>
										
										<select class="form-control" name="cuit_seleccionado" id="cuit_seleccionado" style="width: 100% !important;"> 
										
										</select>

									</div>
								</div>

								<div class="row mt-3">
									<div class="col-md-12">
										<h4>Tipo de gráfico a ver <span class="text-danger">*</span></h4>
										
										<select class="form-control" name="tipo_grafico_a_mostrar" id="tipo_grafico_a_mostrar"> 
											<option value="1">Lineal</option>
											<option value="2">Torta</option>
										</select>

									</div>
								</div>

								<div class="row mt-4">
									<div class="col-md-12" style="text-align:center;">
										<button class="btn btn-lg btn-primary">Ver Estadística</button>
									</div>
								</div>
							</form>

                        </div>
					</div>
					
				</div>
			</div>

			@if($tipo_grafico_a_mostrar == 1)
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">
								{{$titulo_grafico_a_mostrar}}
							</div>
                        </div>
                        <div class="card-body">
                            <div class="chart-container">
                                <canvas id="GraficoLineal"></canvas>
                            </div>
                        </div>
                    </div>
				</div>
			</div>
			@elseif($tipo_grafico_a_mostrar == 2)

				@if($grafico_a_mostrar != 5)
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<div class="card-title">{{$titulo_grafico_a_mostrar}}</div>
							</div>
							<div class="card-body">
								<div class="chart-container">
									<canvas id="pieChart" style="width: 50%; height: 50%"></canvas>
								</div>
							</div>
						</div>
					</div>
				</div>
				@else
				<div class="row">
					<div class="col-md-6">
						<div class="card">
							<div class="card-header">
								<div class="card-title">Año actual</div>
							</div>
							<div class="card-body">
								<div class="chart-container">
									<canvas id="pieChart2" style="width: 50%; height: 50%"></canvas>
								</div>
							</div>
						</div>
					</div>

					
					<div class="col-md-6">
						<div class="card">
							<div class="card-header">
								<div class="card-title">Promedio</div>
							</div>
							<div class="card-body">
								<div class="chart-container">
									<canvas id="pieChart" style="width: 50%; height: 50%"></canvas>
								</div>
							</div>
						</div>
					</div>
				</div>
				@endif

			@endif

        </div>
    </div>
</div>
@endsection


@section("js_code")

<script src="{{ asset('/admin/assets/js/plugin/chart.js/chart.min.js')}}"></script>

    <script>

		var grafico_a_mostrar = {{$grafico_a_mostrar}};
		var tipo_grafico_a_mostrar = {{$tipo_grafico_a_mostrar}};

		$(document).ready(function(){

			$("#mes_desde").val({{$mes_desde}});
			$("#anio_desde").val({{$anio_desde}});
			$("#mes_hasta").val({{$mes_hasta}});
			$("#anio_hasta").val({{$anio_hasta}});

			$("#grafico_a_mostrar").val(grafico_a_mostrar).trigger("change");

			$("#tipo_grafico_a_mostrar").val(tipo_grafico_a_mostrar);

			@if($id_rol == 1)
			$("#cliente_cuit_importacion").select2({
				//dropdownParent: $('#modal_editar_turno'),
				ajax: {
					url: "{{url('/backend/usuarios/search_for_cuit')}}",
					dataType: 'json',
					type: 'post',
					delay: 250,
					headers:
					{
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					data: function (params) {
						return {
						q: params.term
						};
					},
					processResults: function (data) {
						return {
						results: data
						};
					},
					cache: true
				},
				minimumInputLength: 1
			});
			@endif


			$("#cuit_seleccionado").select2({
				//dropdownParent: $('#modal_editar_turno'),
				ajax: {
					url: "{{url('/backend/estadisticas/search_for_cuit')}}",
					dataType: 'json',
					type: 'post',
					delay: 250,
					headers:
					{
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					data: function (params) {
						return {
						q: params.term
						};
					},
					processResults: function (data) {
						return {
						results: data
						};
					},
					cache: true
				},
				minimumInputLength: 1
			});
		});

		$("#formulario_filtro").submit(function(){

			var grafico_a_mostrar = $.trim($("#grafico_a_mostrar").val());
			var cuit_seleccionado = $.trim($("#cuit_seleccionado").val());
			var tipo_grafico_a_mostrar = $.trim($("#tipo_grafico_a_mostrar").val());

			var mensajes_errores = new Array();

			if(grafico_a_mostrar == "")
			{
				mensajes_errores.push("Seleccione el grafico a mostrar");
			}
			else
			{
				if(grafico_a_mostrar == 3 || grafico_a_mostrar == 4 || grafico_a_mostrar == 7  || grafico_a_mostrar == 8)
				{
					if(cuit_seleccionado == "")
					{
						mensajes_errores.push("Seleccione un cuit");
					}
				}
			}

			if(tipo_grafico_a_mostrar == "")
			{
				mensajes_errores.push("Seleccione el tipo de grafico a mostrar");
			}

			if(mensajes_errores.length > 0)
			{
				mostrar_mensajes_errores(mensajes_errores);

				return false;
			}
			else
			{
				return true;
			}
		});

		$("#grafico_a_mostrar").change(function(){

			var valor_select = parseInt($(this).val());

			$("#contenedor_filtro_cuit").css("display","none");
			$("#contenedor_filtro_fecha").css("display","flex");

			switch(valor_select)
			{
				case 1:
					
				break;

				case 2:
					
				break;

				case 3:
					$("#contenedor_filtro_cuit").css("display","block");
				break;

				case 4:
					$("#contenedor_filtro_cuit").css("display","block");
				break;

				case 5:
					$("#contenedor_filtro_fecha").css("display","none");
					
				break;

				case 6:
					
				break;

				case 7:
					$("#contenedor_filtro_cuit").css("display","block");
				break;

				case 8:
					$("#contenedor_filtro_cuit").css("display","block");
				break;
			}

		});

		var labels_estadisticas = new Array();

		<?php
		if(isset($data_estadistica["labels"]))
		{
			foreach($data_estadistica["labels"] as $comprobante_emitido_row)
			{
				echo 'labels_estadisticas.push("'.$comprobante_emitido_row.'");';
			}
		}
		?>

		@if($tipo_grafico_a_mostrar == 1)

			function get_data_set_lineal(simple_data_set)
			{
				var data_set_response = new Array();

				for(var i=0; i < simple_data_set.length;i++)
				{
					data_set_response.push({
						label: simple_data_set[i]["label"],
						borderColor: simple_data_set[i]["color"],
						pointBorderColor: "#FFF",
						pointBackgroundColor: simple_data_set[i]["color"],
						pointBorderWidth: 2,
						pointHoverRadius: 4,
						pointHoverBorderWidth: 1,
						pointRadius: 4,
						backgroundColor: 'transparent',
						fill: true,
						borderWidth: 2,
						data: simple_data_set[i]["data"]
					});
				}

				return data_set_response;
			}

			

			var data_comprobantes_emitidos = new Array();
			var data_comprobantes_recibidos = new Array();
			var data_consumos = new Array();
			var total_consumo_tarjeta_credito = new Array();
			var total_consumo_tarjeta_debito = new Array();
			var total_compra_moneda_extranjera = new Array();

			<?php

			

			if(isset($data_estadistica["comprobantes_emitidos"]))
			{
				foreach($data_estadistica["comprobantes_emitidos"] as $comprobante_emitido_row)
				{
					echo 'data_comprobantes_emitidos.push('.$comprobante_emitido_row.');';
				}
			}

			if(isset($data_estadistica["comprobantes_recibidos"]))
			{
				foreach($data_estadistica["comprobantes_recibidos"] as $comprobante_emitido_row)
				{
					echo 'data_comprobantes_recibidos.push('.$comprobante_emitido_row.');';
				}
			}

			if(isset($data_estadistica["total_consumos"]))
			{
				foreach($data_estadistica["total_consumos"] as $total_consumo_row)
				{
					echo 'data_consumos.push('.$total_consumo_row.');';
				}
			}

			if(isset($data_estadistica["total_consumo_tarjeta_credito"]))
			{
				foreach($data_estadistica["total_consumo_tarjeta_credito"] as $total_consumo_tarjeta_credito_row)
				{
					echo 'total_consumo_tarjeta_credito.push('.$total_consumo_tarjeta_credito_row.');';
				}
			}

			if(isset($data_estadistica["total_consumo_tarjeta_debito"]))
			{
				foreach($data_estadistica["total_consumo_tarjeta_debito"] as $total_consumo_tarjeta_debito_row)
				{
					echo 'total_consumo_tarjeta_debito.push('.$total_consumo_tarjeta_debito_row.');';
				}
			}

			if(isset($data_estadistica["total_compra_moneda_extranjera"]))
			{
				foreach($data_estadistica["total_compra_moneda_extranjera"] as $total_compra_moneda_extranjera_row)
				{
					echo 'total_compra_moneda_extranjera.push('.$total_compra_moneda_extranjera_row.');';
				}
			}

			?>

			var titulo_1 = "";
			var data_1 = new Array();

			var titulo_2 = "";
			var data_2 = new Array();

			var data_set_lineal = new Array();

			switch(grafico_a_mostrar)
			{
				case 1:
					var simple_data_set = new Array();

					simple_data_set.push({
						label: "Comprobantes Emitidos",
						color: "#218838",
						data: data_comprobantes_emitidos
					});

					simple_data_set.push({
						label: "Comprobantes Recibidos",
						color: "#dc3545",
						data: data_comprobantes_recibidos
					});

					data_set_lineal = get_data_set_lineal(simple_data_set);
				break;

				case 2:
					var simple_data_set = new Array();

					simple_data_set.push({
						label: "Comprobantes Emitidos",
						color: "#218838",
						data: data_comprobantes_emitidos
					});

					simple_data_set.push({
						label: "Total Consumo",
						color: "#dc3545",
						data: data_consumos
					});

					data_set_lineal = get_data_set_lineal(simple_data_set);
				break;

				case 3:
				
					var simple_data_set = new Array();

					<?php


					if(isset($data_estadistica["comprobantes_emitidos_por_denominacion"]))
					{
						echo 'var data_of_row = new Array();';

						foreach($data_estadistica["comprobantes_emitidos_por_denominacion"] as $comprobantes_emitidos_por_denominacion_row)
						{
							echo 'data_of_row.push('.$comprobantes_emitidos_por_denominacion_row.');';
						}

						echo '
						simple_data_set.push({
							label: "'.$cuit_seleccionado.' - '.$descripcion_cuit.'",
							color: "#218838",
							data: data_of_row
						});
						';
						
					}

					?>

					data_set_lineal = get_data_set_lineal(simple_data_set);
				break;

				case 4:
				
				
					var simple_data_set = new Array();

					<?php


					if(isset($data_estadistica["comprobantes_recibidos_por_denominacion"]))
					{
						echo 'var data_of_row = new Array();';

						foreach($data_estadistica["comprobantes_recibidos_por_denominacion"] as $comprobantes_recibidos_por_denominacion_row)
						{
							echo 'data_of_row.push('.$comprobantes_recibidos_por_denominacion_row.');';
						}

						echo '
						simple_data_set.push({
							label: "'.$cuit_seleccionado.' - '.$descripcion_cuit.'",
							color: "#218838",
							data: data_of_row
						});
						';
						
					}

					?>

					data_set_lineal = get_data_set_lineal(simple_data_set);
				break;

				case 5:
					var simple_data_set = new Array();

					var meses_promedio = new Array();
					var meses_anio = new Array();

					<?php

					if(isset($data_estadistica["meses_promedio"]))
					{
						foreach($data_estadistica["meses_promedio"] as $meses_promedio_row)
						{
							echo 'meses_promedio.push('.$meses_promedio_row.');';
						}
					}

					if(isset($data_estadistica["meses_anio"]))
					{
						foreach($data_estadistica["meses_anio"] as $meses_anio_row)
						{
							echo 'meses_anio.push('.$meses_anio_row.');';
						}
					}

					?>

					simple_data_set.push({
						label: "Facturación mensual",
						color: "#218838",
						data: meses_anio
					});

					simple_data_set.push({
						label: "Facturación promedio",
						color: "#dc3545",
						data: meses_promedio
					});

					data_set_lineal = get_data_set_lineal(simple_data_set);
				break;

				case 6:
					var simple_data_set = new Array();

					simple_data_set.push({
						label: "Tarjeta de Credito",
						color: "#218838",
						data: total_consumo_tarjeta_credito
					});

					simple_data_set.push({
						label: "Tarjeta de Debito",
						color: "#dc3545",
						data: total_consumo_tarjeta_debito
					});

					simple_data_set.push({
						label: "Moneda extranjera",
						color: "#138496",
						data: total_compra_moneda_extranjera
					});

					simple_data_set.push({
						label: "Total Consumo",
						color: "#23272b",
						data: data_consumos
					});

					data_set_lineal = get_data_set_lineal(simple_data_set);
				break;

				case 7:
					var simple_data_set = new Array();

					simple_data_set.push({
						label: "Comprobantes emitidos",
						color: "#218838",
						data: data_comprobantes_emitidos
					});

					var data_comprobantes_por_denominacion = new Array();

					<?php


					if(isset($data_estadistica["comprobantes_emitidos_por_denominacion"]))
					{
						foreach($data_estadistica["comprobantes_emitidos_por_denominacion"] as $comprobantes_emitidos_por_denominacion_row)
						{
							echo 'data_comprobantes_por_denominacion.push('.$comprobantes_emitidos_por_denominacion_row.');';
						}
					}

					?>


					simple_data_set.push({
						label: "Comprobantes emitidos por <?php echo $cuit_seleccionado.' - '.$descripcion_cuit?>",
						color: "#dc3545",
						data: data_comprobantes_por_denominacion
					});

					data_set_lineal = get_data_set_lineal(simple_data_set);
				break;

				case 8:
					var simple_data_set = new Array();

					simple_data_set.push({
						label: "Comprobantes recibidos",
						color: "#218838",
						data: data_comprobantes_recibidos
					});

					var data_comprobantes_por_denominacion = new Array();

					<?php


					if(isset($data_estadistica["comprobantes_recibidos_por_denominacion"]))
					{
						foreach($data_estadistica["comprobantes_recibidos_por_denominacion"] as $comprobantes_recibidos_por_denominacion_row)
						{
							echo 'data_comprobantes_por_denominacion.push('.$comprobantes_recibidos_por_denominacion_row.');';
						}
					}

					?>


					simple_data_set.push({
						label: "Comprobantes recibidos por <?php echo $cuit_seleccionado.' - '.$descripcion_cuit?>",
						color: "#dc3545",
						data: data_comprobantes_por_denominacion
					});

					data_set_lineal = get_data_set_lineal(simple_data_set);
				break;
			}

			var myGraficoLineal = new Chart(GraficoLineal, {
				type: 'line',
				data: {
					labels: labels_estadisticas,
					datasets: data_set_lineal
				},
				options : {
					responsive: true, 
					maintainAspectRatio: false,
					legend: {
						position: 'top',
					},
					tooltips: {
						bodySpacing: 4,
						mode:"nearest",
						intersect: 0,
						position:"nearest",
						xPadding:10,
						yPadding:10,
						caretPadding:10
					},
					layout:{
						padding:{left:15,right:15,top:15,bottom:15}
					}
				}
			});
		@else

		var pieChart = document.getElementById('pieChart').getContext('2d');

		var labels = new Array();
		var data = new Array();

		switch(grafico_a_mostrar)
		{
			case 1:
				@if(isset($data_estadistica['porcentaje_comprobantes_emitidos']) && isset($data_estadistica['porcentaje_comprobantes_recibidos']))
					labels = ["Comprobantes Emitidos <?php echo $data_estadistica['porcentaje_comprobantes_emitidos']?> %", "Comprobantes Recibidos <?php echo $data_estadistica['porcentaje_comprobantes_recibidos']?> %"];
					data = [
						<?php echo $data_estadistica['porcentaje_comprobantes_emitidos'] ?>,
						<?php echo $data_estadistica['porcentaje_comprobantes_recibidos'] ?>
					];
				@endif
			break;

			case 2:
			
				@if(isset($data_estadistica['porcentaje_comprobantes_emitidos']) && isset($data_estadistica['porcentaje_consumo_total']))
					labels = ["Comprobantes Emitidos <?php echo $data_estadistica['porcentaje_comprobantes_emitidos']?> %", "Consumo Total <?php echo $data_estadistica['porcentaje_consumo_total']?> %"];

					data = [
						<?php echo $data_estadistica['porcentaje_comprobantes_emitidos'] ?>,
						<?php echo $data_estadistica['porcentaje_consumo_total'] ?>
					];
				@endif
			break;

			case 3:

				labels = ["Comprobantes Emitidos por <?php echo $cuit_seleccionado.' '.$descripcion_cuit?> ", "Resto Comprobantes emitidos"];

				data = [
					@if(isset($data_estadistica['porcentaje_emitido_por_cuit']))
						<?php echo $data_estadistica['porcentaje_emitido_por_cuit'] ?>,
					@else
						0,
					@endif

					@if(isset($data_estadistica['porcentaje_emitido_restante']))
						<?php echo $data_estadistica['porcentaje_emitido_restante'] ?>
					@else
						0
					@endif
				];
			break;

			case 4:

				labels = ["Comprobantes Recibidos por <?php echo $cuit_seleccionado.' '.$descripcion_cuit?> ", "Resto Comprobantes recibidos"];

				data = [
					@if(isset($data_estadistica['porcentaje_recibido_por_cuit']))
						<?php echo $data_estadistica['porcentaje_recibido_por_cuit'] ?>,
					@else
						0,
					@endif

					@if(isset($data_estadistica['porcentaje_recibido_restante']))
						<?php echo $data_estadistica['porcentaje_recibido_restante'] ?>
					@else
						0
					@endif
				];
			break;

			case 5:

				labels = labels_estadisticas;

				data = [
					<?php
					if(isset($data_estadistica['meses_anio']))
					{
						foreach($data_estadistica['meses_anio'] as $mes_promedio_row)
						{
							echo $mes_promedio_row.',';
						}
					}
					?>
				];

			break;

			case 6:

				labels = [
					"Consumo tarjeta de credito",
					"Consumo tarjeta de debito",
					"Consumo compra moneda extranjera",
				];

				data = [
					@if(isset($data_estadistica['porcentaje_consumo_tarjeta_credito']))
						<?php echo $data_estadistica['porcentaje_consumo_tarjeta_credito'] ?>,
					@else
						0,
					@endif

					@if(isset($data_estadistica['porcentaje_consumo_tarjeta_debito']))
						<?php echo $data_estadistica['porcentaje_consumo_tarjeta_debito'] ?>,
					@else
						0,
					@endif

					@if(isset($data_estadistica['porcentaje_compra_moneda_extranjera']))
						<?php echo $data_estadistica['porcentaje_compra_moneda_extranjera'] ?>
					@else
						0
					@endif
				];
			break;
		}

		var myPieChart = new Chart(pieChart, {
			type: 'pie',
			data: {
				datasets: [{
					data: data,
					backgroundColor :["#1d7af3","#f3545d","#fdaf4b","#28a745","#117a8b","#1d2124","#ff4f64","#8352ff","#52ff66","#09edfb","#ff592c","#ebe428"],
					borderWidth: 0
				}],
				labels: labels 
			},
			options : {
				responsive: true, 
				maintainAspectRatio: false,
				legend: {
					position : 'bottom',
					labels : {
						fontColor: 'rgb(154, 154, 154)',
						fontSize: 11,
						usePointStyle : true,
						padding: 20
					}
				},
				pieceLabel: {
					render: 'percentage',
					fontColor: 'white',
					fontSize: 14,
				},
				tooltips: false,
				layout: {
					padding: {
						left: 20,
						right: 20,
						top: 20,
						bottom: 20
					}
				}
			}
		});

			@if($grafico_a_mostrar == 5)

			var data_pie_chart_2 = [
				<?php
				if(isset($data_estadistica['meses_promedio']))
				{
					foreach($data_estadistica['meses_promedio'] as $mes_promedio_row)
					{
						echo $mes_promedio_row.',';
					}
				}
				?>
			];

			var myPieChart2 = new Chart(pieChart2, {
				type: 'pie',
				data: {
					datasets: [{
						data: data_pie_chart_2,
						backgroundColor :["#1d7af3","#f3545d","#fdaf4b","#28a745","#117a8b","#1d2124","#ff4f64","#8352ff","#52ff66","#09edfb","#ff592c","#ebe428"],
						borderWidth: 0
					}],
					labels: labels 
				},
				options : {
					responsive: true, 
					maintainAspectRatio: false,
					legend: {
						position : 'bottom',
						labels : {
							fontColor: 'rgb(154, 154, 154)',
							fontSize: 11,
							usePointStyle : true,
							padding: 20
						}
					},
					pieceLabel: {
						render: 'percentage',
						fontColor: 'white',
						fontSize: 14,
					},
					tooltips: false,
					layout: {
						padding: {
							left: 20,
							right: 20,
							top: 20,
							bottom: 20
						}
					}
				}
			});
			@endif

		@endif

		
	</script>

@endsection

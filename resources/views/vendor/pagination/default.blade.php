@if ($paginator->hasPages())
    <ul class="pagination" style="justify-content: center;">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="disabled btn btn-paginacion"><span>&laquo;</span></li>
        @else
            <li><a href="{{ $paginator->previousPageUrl() }}" rel="prev" class="btn btn-paginacion">&laquo;</a></li>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="disabled btn btn-paginacion"><span>{{ $element }}</span></li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="active btn btn-paginacion" ><span>{{ $page }}</span></li>
                    @else
                        <li><a href="{{ $url }}" class="btn btn-paginacion">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li><a href="{{ $paginator->nextPageUrl() }}" rel="next" class="btn btn-paginacion">&raquo;</a></li>
        @else
            <li class="disabled btn btn-paginacion"><span>&raquo;</span></li>
        @endif
    </ul>
@endif

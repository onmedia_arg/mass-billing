$(document).ready(function () {
    $('.main-slider__carousel').owlCarousel({
        items: 1,
        loop: true,
        autoplay: true,

        lazyLoad: true
    })

    $('.main-destinos__carousel').owlCarousel({
        items: 3,
        nav: true,
        navText: ["<img src='assets/img/icon-arrow.svg'>", "<img src='assets/img/icon-arrow.svg'>"],
        loop: true,
        responsive : {
            0 : {
                items: 1
            },
            576 : {
                items: 2
            },
            992 : {
                items: 3
            }
        }
    })
})